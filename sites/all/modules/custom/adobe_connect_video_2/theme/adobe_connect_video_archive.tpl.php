<?php
// $Id: block.tpl.php,v 1.3.2.1 2011/01/12 23:05:22 mauritsl Exp $

/**
 * @file
 * Advanced
 */
?>
<div class="btn-group adobe-connect-recordings-holder">
  <?php
  $class = '';
  $button_label = t('Recordings') .'<span class="badge">' . count($recordings) . '</span>';
  if (empty($recordings)) {
    $button_label = t('No recordings');
    $class = ' disabled';
  }
  ?>
  <a class="btn dropdown-toggle<?php print $class;?>" data-toggle="dropdown" href="#"><?php print $button_label; ?></a>
  <ul class="adobe-connect-recordings dropdown-menu">
  <?php
    $link_options = array(
      'attributes' => array(
        'target' => '_blank',
      ),
    );
    foreach ($recordings as $recording ) {
      $link_text = $recording['name'] . ' (' . $recording['duration']. ')';
      print '<li>' . l($link_text, 'adobe_connect/recording' . $recording['url'], $link_options) . '</li>';
    }
  ?>
  </ul>
</div>
