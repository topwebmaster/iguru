/**
 * Define actions for the frontpage elements.
 */
(function ($) {
  Drupal.behaviors.SocialLinksToggle = {
    attach: function() {
      //React on AJAX responses - useful for pagers, togglers and add more AJAX links.
      $(document).ajaxComplete(function(event, xhr, settings) {

        //React to views ajax requests.
        if(strpos(settings.url,'views/ajax')) {

          //Initiate AddThis social links.
          var script = 'http://s7.addthis.com/js/250/addthis_widget.js#domready=1';
          if (window.addthis) {
              window.addthis = null;
              window._adr = null;
              window._atc = null;
              window._atd = null;
              window._ate = null;
              window._atr = null;
              window._atw = null;
          }
          $.getScript(script);

          //Initiate VKontakte Links for AJAX responses.
          $('a.vk-like-button').each(function() {
            if($(this).html() == '') {
              var id = $(this).attr('id');
              var elementID = $(this).attr('element-id');
              pageURL = $(this).attr('page-url');
              VK.Widgets.Like(id, {'type':'vertical', 'pageUrl': pageURL}, elementID);
            }
          });
        }
      });
    }
  }
})(jQuery);

/**
 * Strpos-like function for JS.
 * @param haystack
 * @param needle
 * @param offset
 */
function strpos (haystack, needle, offset) {
  var i = (haystack+'').indexOf(needle, (offset || 0));
  return i === -1 ? false : i;
}