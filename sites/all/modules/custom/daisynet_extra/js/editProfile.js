(function ($) {
  Drupal.behaviors.daisyEditProfile = {
    attach: function(context) {
      $('#edit-field-user-agreement .description', context).dialog({
        show:'drop',
        hide:'drop',
        autoOpen:false,
        resizable:false,
        draggable:true,
        height:450,
        width:700,
        title:Drupal.t('Term and conditions of use'),
        modal:true,
        buttons:{
          "OK":function(){
            $(this).dialog("close");
          },
          "print":function(){

            var dl = $(this);
            var style = $('.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-draggable').attr('style')
            $('.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-draggable').css({'top':'0','left':'0','width':'100%'}).children('.description').css({'height':'auto'})
            var h = $('.ui-widget-overlay').height();
            $('.ui-widget-overlay').css({'height':'auto'})
            $('#device_type').hide()
            $('.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix').hide()
            $('.frby').hide()
            window.print()
            window.setTimeout(function(){
              $('#device_type').show()
              $('.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix').show()
              $('.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-draggable').attr("style", style)
              $('.frby').show();
              dl.dialog("close");
            },100)
          }
        }
      });
      $('.agreement', context).click(function(event) {
        $('.description').dialog('open');
        event.preventDefault();
      });

      $('textarea#edit-field-user-about-und-0-value', context).after('<span class="notification"></span>');

      $('textarea#edit-field-user-about-und-0-value', context).keyup(function() {
        var charLength = $(this).val().length;
        $('span.notification').html('1000 / ' + charLength);
        if($(this).val().length > 1000) {
          $(this).val($(this).val().substr(0, 999));
        }
        $.ajax({
          url: Drupal.settings.basePath + "daisynet_extra/input",
          data: 'name=' + $(this).attr('name') + '&value=' + $(this).val(),
          success: function(data){
            $('#pcp-percent-bar').css('width', data + '%');
            $('#pcp-wrapper .complete').css('left', (data - 3) + '%').html(data + '%');
            if(data == 100) {
              $('#edit-send-mail').show();
            }
            else {
              $('#edit-send-mail').hide();
            }
          }
        });
      });

      $('.lang-sel select', context).once('lang-once').change(function(){
        Drupal.behaviors.daisy.updateLang();
      });

      $('.lang-sel a.lang-rem', context).once('rem-lang-once').click(function(event){
        $(this).parent().remove();
        Drupal.behaviors.daisyEditProfile.updateLang();
        event.preventDefault();
      });

      $('.lang-sel-add a', context).once('lang-sel-add').click(function(event){
        var options = $('#edit-field-user-languages-und').html();
        $('.lang-sel:last').after('<div class="lang-sel"><select>' + options + '</select><a href="#" class="lang-rem delete-field">rm</a></div>');
        Drupal.attachBehaviors('.lang-sel');
        $('.lang-sel:last select option').removeAttr('selected');
        event.preventDefault();
      });

      //default values for inputs
      var default_values = new Array();
      default_values['edit-field-user-grade-und-#-value'] = Drupal.t('Indicate a degree');
      default_values['edit-field-user-recommended-und-#-value'] = Drupal.t('Name  Degree  Field  Email Country');
      default_values['edit-field-user-name-und-#-value'] = Drupal.t('First name');
      default_values['edit-field-user-last-name-und-#-value'] = Drupal.t('Last name');
      default_values['edit-field-user-father-name-und-#-value'] = Drupal.t('Father name');

      $("#user-profile-form input[type=text]")
      .once('daisy-input-defval')
      .each(function(){
        var el_id = $(this).attr('id').replace(/[0-9]/,'#'); //replace number with # for multiple fields
        if($(this).val().length == 0 && default_values[el_id] != undefined) {
          $(this).val(default_values[el_id]);
        }
      })
      .focus(function() {
        var el_id = $(this).attr('id').replace(/[0-9]/,'#'); //replace number with # for multiple fields
        if($(this).val() == default_values[el_id]) {
          $(this).val('');
        }
      })
      .blur(function() {
        var el_id = $(this).attr('id').replace(/[0-9]/,'#'); //replace number with # for multiple fields
        if (this.value == '') {
          //this.style.color = inactive_color;
          $(this).val(default_values[el_id]);
        }
      });

      $('#user-profile-form #edit-submit', context).click(function(){
        $("#user-profile-form input[type=text]")
        .each(function(){
          var el_id = $(this).attr('id').replace(/[0-9]/,'#'); //replace number with # for multiple fields
          if($(this).val().length != 0 && default_values[el_id] != undefined && $(this).val() == default_values[el_id]) {
            $(this).val('');
          }
        });
      });

      //заполнение рекомендуемых полей в select из бд
  $.ajax({
    url: Drupal.settings.basePath+'prefered-categories',
    type: 'post',
    data: 'check=true',
    success: function(data){

    }
  })

      $('#edit-field-prefered-categories option').each(function() {
        if ($(this).is(':selected')) {
          $('#edit-term-prefered-categories a[rel="'+$(this).val()+'"]').addClass('selected')
        }
      })

        $('#edit-term-prefered-categories a').click(function(event) {
        event.preventDefault();
        var rel = $(this).attr('rel');
      if ( $('#edit-field-prefered-categories option[value="'+rel+'"]').is(':selected') ) {
        $('#edit-field-prefered-categories option[value="'+rel+'"]').removeAttr('selected')
        $(this).removeClass('selected')
      } else {
        $('#edit-field-prefered-categories option[value="'+rel+'"]').attr('selected','selected')
        $(this).addClass('selected')
      }
     });

    },
    updateLang: function () {
      $('#edit-field-user-languages-und option:selected', context).removeAttr('selected');
      $.each($('.lang-sel select option:selected', context), function(index, val) {
        $('#edit-field-user-languages-und option[value=' + $(val).val() + ']').attr('selected', 'selected');
      });
    }
  };
})(jQuery);
