

(function ($) {
  Drupal.behaviors.daisynet_extra = {
    attach: function (context, settings) {
      // Bind an AJAX callback to our link
      var basket = $('.archive-video .basket');
      
      basket.live('click',function(event) {
        event.preventDefault();
        var id = $(this).attr('id')
			
		$.post(Drupal.settings.basePath + 'ajax-add-to-cart', {nid:id}, function(response) {
		  response = Drupal.parseJson(response);
		 
		  // Показываем сообщение если что-то пошло не так
		  if (!response.status) {
		    return alert(response.data);
		  } else {
		  	alert(response)
		  }
		 
		  // Обновляем блок с корзиной
		  $('#block-uc_cart-0 .content').html(response.data);
		});
			
      });
    }
  };
})(jQuery);