(function ($) {
  Drupal.behaviors.cancelhide = {
    attach: function (context, settings) {

      var cancelhide = $('.webinar-link .meeting_connect');
      cancelhide.bind('click',function(event) {
        var nid = $(this).parent().attr('id');
        nid = nid.substr(2, nid.length);


        var ajaxUrl = Drupal.settings.basePath+'cancelhideajax';
        $.ajax({
          type: "POST",
          url: ajaxUrl,
          data: {
            'nid': nid,
          },
          dataType: "json",
          success: function (data) {
            // Display the time from successful response
            $('#cancel_order').remove();
          },
          error: function (xmlhttp) {
            // Error alert for failure
            alert('An error occured: ' + xmlhttp.status);
          }
        });
      });
    }
  };
})(jQuery);
