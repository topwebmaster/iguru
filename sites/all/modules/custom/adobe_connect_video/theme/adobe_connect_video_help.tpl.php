<?php
// $Id: top_5_block.tpl.php,v 1.3.2.1 2011/01/12 23:05:22 mauritsl Exp $

/**
 * @file
 * Advanced
 */
?>
<h3>To add content to Adobe Connect Video module:</h3>
<ol>
    <li>Login to Adobe Media Server as admin user. Ex: http://meet52575686.adobeconnect.com</li>
    <li>Add video to content/user content. Ex: User Content >  user@example.com</li>
    <li>Enter on video details and Set Permissions for Learners user. Ex: User Content >  user@example.com >  My_lorem_content </li>
    <li>Enter in content/user content/download content and copy link from dwonload output files without "?download=video_file". Ex: http://meet52575686.adobeconnect.com/my_lorem_content/output/Website_small.mp4</li>
    <li>Open webinar to edit or create new one</li>
    <li>Enter link in Abobe Connect Video custom field</li>
</ol>
