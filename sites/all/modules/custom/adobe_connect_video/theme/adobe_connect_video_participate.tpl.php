<?php
$message = t('Join webinar');
$ready_to_start = 30 * 60;
if ($is_author) {
  $message = t('Start webinar');
}
// Webinar is not accessible after it ends
if (($remaining_time + $duration) < 0) {
  print t('Webinar is over');
}
elseif ($user_access || $is_author) {
  // Show subscribe/unsubscribe for users with access to this webinar
  if ($remaining_time > 0 && !$is_author && $is_free) {
    print '<div class="subscribe-action">' . PHP_EOL;
    if (!$is_participant) {
      print l(t('Subscribe'), "daisy/event/$nid/register", array('query' => drupal_get_destination()));
    }
    else {
      print l(t('Unsubscribe'), "daisy/event/$nid/unregister", array('query' => drupal_get_destination()));
    }
    print '</div>' . PHP_EOL;
  }
  // Show webinar access link
  if ((($remaining_time < $ready_to_start) && $is_author) || ($remaining_time < 0 && $user_access)) {
    print l($message, "adobe_connect/meeting$meeting_url$nid", array('attributes' => array('target' => '_blank', 'class' => array('participate-link'))));
  }
  elseif($is_participant || $is_author) {
    if ($is_author) {
      $remaining_time = $remaining_time - $ready_to_start;
    }
    print '<div id="adobe-timer-' . $nid . '" class="adobe-countdown-timer">' . $remaining_time . t('seconds') . '</div>';
    drupal_add_js('function adobe_webinar_ready_to_start() {location.reload(true);}', 'inline');
    $countdown_options = array(
      'until' => $remaining_time,
      'onExpiry' => 'adobe_webinar_ready_to_start',
      'compact' => TRUE,
      'layout' => '{d<}{dn} {dl} {d>}{h<}{hn} {hl} {h>}{m<}{mn} {ml} {m>}{s<}{sn} {sl}{s>}',
    );
    global $language;
    if ($language->language == 'ru') {
      $countdown_options['langcode'] = $language->language;
    }
    jquery_countdown_add("#adobe-timer-" . $nid, $countdown_options);
  }
}
else {
  print t('You can\'t participate in this webinar');
}

?>
