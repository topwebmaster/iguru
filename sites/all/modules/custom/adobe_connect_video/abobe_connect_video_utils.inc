<?php
define("ADOBE_DEBUG", TRUE);
/**
 * Helper function to debug via drupal_set_message
 */
function _adobe_debug($short_name = '', $variable = NULL) {
  if (ADOBE_DEBUG) {
    $message = 'Adobe Debug :: ' . $short_name;
    if (!is_null($variable)) {
      $message .= "<br />\n" . var_export($variable, TRUE);
    }
    drupal_set_message($message);
  }
}

/**
 * Helper function to generate a random password
 * that complies with most Adobe Connect policies
 */
function _adobe_generate_password() {
  return '0xrc'. substr(md5(microtime()), rand(0, 26), 8) . substr('DAISY', rand(0,4), 5);
}

/**
 * Helper function to init an empty cookie from Adobe Connect
 */
function _adobe_init_cookie() {
  $session = adobe_make_request('common-info', '');
  return $session['common']['cookie'];
}

/**
 * Helper function to refresh a user's cookie
 */
function adobe_refresh_user_cookie($cookie = FALSE, $email = FALSE) {
  if ($cookie === FALSE || $email === FALSE) {
    return FALSE;
  }

  $response = adobe_make_request('common-info', 'session=' . $cookie);
  if (!isset($response['common']['user'])) {
    $cookie = adobe_get_user_cookie($email);
  }

  return $cookie;
}
/**
 * Gets an admin cookie to operate
 */
function adobe_get_admin_cookie() {
  $cookie = _adobe_init_cookie();
  $params = 'login=' . variable_get('adobe_connect_video_abobe_site_user_name') . '&password=' . variable_get('adobe_connect_video_abobe_site_user_password') . '&session=' . $cookie;
  $data = adobe_make_request('login', $params);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return $cookie;
}

/**
 * Login a simple user
 * If no password is given - it will override it via admin access and login the user
 */
function adobe_get_user_cookie($email = FALSE, $password = FALSE) {
  if ($email === FALSE) {
    return FALSE;
  }
  // Force login with no password by changing it as admin
  if ($password === FALSE) {
    $admin_cookie = adobe_get_admin_cookie();
    $password = _adobe_generate_password();
    adobe_update_user_password($email, $password, $admin_cookie);
    adobe_logout($admin_cookie);
  }
  $cookie = _adobe_init_cookie();
  $params = 'login=' . $email . '&password=' . $password . '&session=' . $cookie;
  $data = adobe_make_request('login', $params);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return $cookie;
}

/**
 * Get data from Adobe Central
 */


function adobe_make_request($action, $params) {
  $url = variable_get('adobe_connect_video_abobe_site_url') . '/api/';
  $url .= 'xml?action=' . $action;
  $url .= '&' . $params;



  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "");
  $output = curl_exec($ch);

  curl_close($ch);
  $xml = simplexml_load_string($output);
  $json = json_encode($xml);
  $data = json_decode($json, TRUE);

  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }

  return ($data);
}





/**
 * Register a new user
 */
function adobe_register_user($user_name, $user_email, $user_password, $cookie) {
  $params = 'first-name=' . $user_name .
      '&last-name=' . $user_name .
      '&login=' . $user_email .
      '&email=' . $user_email .
      '&type=user' .
      '&password=' . $user_password .
      '&has-children=0' .
      '&send-email=true' .
      '&session=' . $cookie;

  $data = adobe_make_request('principal-update', $params);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return $data;
}

/**
 * Set a new password for a given user
 */
function adobe_update_user_password($email, $password, $cookie) {
  $principal_id = adobe_get_principal_id($cookie, $email);

  $data = adobe_make_request('user-update-pwd', 'user-id=' . $principal_id . '&password=' . $password . '&password-verify=' . $password . '&session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return TRUE;
}

/**
 * Get the group id
 */
function adobe_get_group_id($group_name, $cookie) {
  $data = adobe_make_request('principal-list', 'filter-type=' . $group_name .
      '&session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return $data['principal-list']['principal']['@attributes']['principal-id'];
}

function adobe_get_group_principal_id($cookie) {
  $data = adobe_make_request('principal-list', 'filter-type=user' . '&session=' . $cookie);

  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return $data['principal-list']['principal']['@attributes']['principal-id'];
}

/**
 * Helper function to add an user to group
 */
function adobe_add_to_group($group_id, $principal_id, $cookie) {
  $data = adobe_make_request('group-membership-update', 'group-id=' . $group_id . '&principal-id=' . $principal_id . '&is-member=true' . '&session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return TRUE;
}

/**
 * Helper function to add an user to group
 */
function adobe_remove_from_group($group_id, $principal_id, $cookie) {
  $data = adobe_make_request('group-membership-update', 'group-id=' . $group_id . '&principal-id=' . $principal_id . '&is-member=false' . '&session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return TRUE;
}

/**
 * Helper function to empty 'rooms' from 'inactive' hosts
 */
function adobe_empty_rooms($cookie) {
  $data = adobe_make_request('report-active-meetings', 'session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  // Get list of all hosts
   $dont_remove = array(
    'admin@goweby.com',
  );

  $hosts_id = adobe_get_group_id('live-admins', $cookie);

  // Get list of participants that have to retain their host status (meetings in progress)
  if (!empty($data['report-active-meetings'])) {
    $meetings = $data['report-active-meetings']['sco'];
    $test = array_keys($meetings);
    if (!ctype_digit($test[0])) {
      $meetings = array($meetings);
    }
    foreach($meetings as $meeting) {
      $permissions = adobe_make_request('permissions-info', 'acl-id=' . $meeting['@attributes']['sco-id'] . '&filter-permission-id=host&session=' .$cookie);
      if (!isset($permissions['status']['@attributes']['code']) || $permissions['status']['@attributes']['code'] !== 'ok') {
        return FALSE;
      }
      $current_hosts = $permissions['permissions']['principal'];
      $test = array_keys($current_hosts);
      if (!ctype_digit($test[0])) {
        $current_hosts = array($current_hosts);
      }
      foreach ($current_hosts as $current_host) {
        //TODO: If email != login this fails
        $dont_remove[] = $current_host['login'];
      }
    }
  }

  // Get list of current hosts and remove all that are not active at the moment
  $hosts = adobe_make_request(
      'principal-list',
      'group-id='
      . $hosts_id
      . '&filter-is-member=true&session='
      .$cookie);
  if (!isset($hosts['status']['@attributes']['code']) || $hosts['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  if (isset($hosts['principal-list']['principal']) && !empty($hosts['principal-list']['principal'])) {
    foreach($hosts['principal-list']['principal'] as $host) {
      if (!in_array($host['login'], $dont_remove)) {
        $principal_id = adobe_get_principal_id($cookie, $host['email']);
        $removed = adobe_remove_from_group($hosts_id, $principal_id, $cookie);
        //adobe_remove_from_group($hosts_id, $principal_id, $cookie);
      }
    }
  }

  return TRUE;
}

/**
 * Create a meeting
 *
 * $meeting_name = Plain text(must be unique)
 * $meeting_description = Plain text string
 * $language = 2 symbol language code
 * $start_date = UNIX timestamp
 * $end_date = UNIX timestamp
 * $url = desired url_path(must be unique)
 */
function adobe_create_meeting($cookie, $email = FALSE, $meeting_title = '', $meeting_description = '', $language = 'en', $start_date = FALSE, $end_date = FALSE, $url = FALSE) {
  if (empty($email) || empty($meeting_title)) {
    return FALSE;
  }
  if (empty($meeting_description)) {
    $meeting_description = 'nan';
  }

  $folder_id = get_user_folder_id($email, $cookie);
  $params = 'folder-id=' . $folder_id .
      '&description=' . urlencode($meeting_description) .
      '&name=' . urlencode($meeting_title) .
      '&lang=' . $language .
      '&type=meeting' .
      '&session=' . $cookie;


  // Add the date if specified
  if ($start_date !== FALSE) {
    $params .= '&date-begin=' . format_date($start_date, 'custom', "Y-m-d\TH:i");
  }
  if ($end_date !== FALSE) {
    $params .= '&date-end=' . format_date($end_date, 'custom', "Y-m-d\TH:i");
  }
  if ($url !== FALSE) {
    $params .= '&url-path=' . $url;
  }

  $data = adobe_make_request('sco-update', $params);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return $data;
}

/**
 * Update a meeting
 *
 * $meeting_name = Plain text(must be unique)
 * $meeting_description = Plain text string
 * $language = 2 symbol language code
 * $start_date = UNIX timestamp
 * $end_date = UNIX timestamp
 * $url = desired url_path(must be unique)
 */
function adobe_update_meeting($cookie, $meeting_sco_id = FALSE, $email = FALSE, $meeting_title = '', $meeting_description = '', $language = 'en', $start_date = FALSE, $end_date = FALSE, $url = FALSE) {
  if (empty($email) || empty($meeting_title) || empty($meeting_sco_id)) {
    return FALSE;
  }
  if (empty($meeting_description)) {
    $meeting_description = 'nan';
  }

  $folder_id = get_user_folder_id($email, $cookie);
  $params = 'folder-id=' . $folder_id .
    '&sco-id=' . $meeting_sco_id .
    '&description=' . urlencode($meeting_description) .
    '&name=' . urlencode($meeting_title) .
    '&lang=' . $language .
    '&type=meeting' .
    '&session=' . $cookie;

  // Add the date if specified
  if ($start_date !== FALSE) {
    $params .= '&date-begin=' . format_date($start_date, 'custom', "Y-m-d\TH:i");
  }
  if ($end_date !== FALSE) {
    $params .= '&date-end=' . format_date($end_date, 'custom', "Y-m-d\TH:i");
  }
  if ($url !== FALSE) {
    $params .= '&url-path=' . $url;
  }

  $data = adobe_make_request('sco-update', $params);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return $data;
}

/**
 * Clear the permissions of a meeting
 */
function adobe_empty_participants($cookie, $sco_id = FALSE) {
  if ($sco_id === FALSE) {
    return FALSE;
  }
  $params = 'acl-id=' . $sco_id . '&session=' . $cookie;
  $data = adobe_make_request('permissions-reset', $params);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return TRUE;
}

/**
 * Set the permissions on a meeting
 */
function adobe_set_meeting_access($cookie, $sco_id = FALSE, $permission = 'public') {
  if ($sco_id === FALSE) {
    return FALSE;
  }
  if (!in_array($permission, array('public', 'protected', 'private'))) {
    return FALSE;
  }

  $params = 'acl-id=' . $sco_id .
    '&session=' . $cookie;
  switch ($permission) {
    case 'public':
      // Public (Anyone who has the URL for the meeting can enter the room)
      $params .= '&principal-id=public-access&permission-id=view-hidden';
      break;
    case 'protected':
      // Protected (Only registered users and accepted guests can enter the room)
      $params .= '&principal-id=public-access&permission-id=remove';
      break;
    case 'private':
      // Private (Only registered users and participants can enter)
      $params .= '&principal-id=public-access&permission-id=denied';
      break;
  }

  $data = adobe_make_request('permissions-update', $params);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return $data;
}

/**
 * Rename a webinar identified by it's sco-id
 */
function adobe_rename_meeting($cookie, $meeting_number, $sco_id) {
  $data = adobe_make_request('sco-update', 'sco-id=' . $sco_id .
      '&name=webinar-' . $meeting_number .
      '&session=' . $cookie);

  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return $data;
}

/**
 * Get meetings folder for a given user
 */
function adobe_get_meeting_folder_id($cookie) {
  $folder = adobe_make_request('sco-shortcuts', 'session=' . $cookie);
  if ($folder === FALSE) {
    return $folder;
  }

  foreach ($folder['shortcuts']['sco'] as $key => $val) {
    if ($val['@attributes']['type'] == 'user-meetings') {
      $folder_id = $val['@attributes']['sco-id'];
      break;
    }
  }

  return $folder_id;
}

/**
 * Add an user as host for a meeting
 */
function adobe_add_host($cookie, $principal_id, $sco_id) {
  $dat = adobe_make_request('permissions-update', 'acl-id=' . $sco_id . '&principal-id=public-access&permission-id=denied' . '&session=' . $cookie);
  $data = adobe_make_request('permissions-update', 'principal-id=' . $principal_id . '&acl-id=' . $sco_id . '&permission-id=host' .
      '&session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }

  return $data;
}

/**
 * Get the principal-id value for an adobe connect user identified by email
 */
function adobe_get_account_id($cookie, $email = FALSE) {
  if ($email === FALSE) {
    global $user;
    $email = $user->mail;
  }
  $data = adobe_make_request('principal-list', 'filter-login=' . $email . '&session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok' || !isset($data['principal-list']['principal']['@attributes'])) {
    return FALSE;
  }
  return $data['principal-list']['principal']['@attributes']['account-id'];
}

/**
 * Get the principal-id value for an adobe connect user identified by email
 */
function adobe_get_principal_id($cookie, $email = FALSE) {
  if ($email === FALSE) {
    global $user;
    $email = $user->mail;
  }
  $data = adobe_make_request('principal-list', 'filter-login=' . $email . '&session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok' || !isset($data['principal-list']['principal']['@attributes'])) {
    return FALSE;
  }
  $result = $data['principal-list']['principal']['@attributes']['principal-id'];
  return $result;
}

/**
 * Get meeting data
 */
function adobe_get_meeting($meeting_sco_id, $cookie) {
  $data = adobe_make_request('sco-expanded-contents', 'sco-id=' . $meeting_sco_id . '&session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return $data;
}

/**
 * Get a meeting's forced records stored as links in the meeting's folder
 */
function adobe_get_forced_recordings($meeting_sco_id, $cookie) {
  $data = adobe_make_request('sco-expanded-contents', 'sco-id=' . $meeting_sco_id . '&session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }

  $recordings = array();
  foreach ($data['expanded-scos']['sco'] as $meeting_object) {
    // TODO: $meeting_object['@attributes']['icon'] is also 'archive' ?
    // Check if we have any recordings and not only this meeting
    if (isset($meeting_object['@attributes'])) {
      $attributes = $meeting_object['@attributes'];
      if ($attributes['type'] == 'link' && isset($attributes['source-sco-type']) && $attributes['source-sco-type'] == 0) {
        $recordings[] = $meeting_object;
      }
    }
  }

  return $recordings;
}

/**
 * Get a user's meeting by its URL and folder
 */
function adobe_get_expanded_meeting($user_folder_id, $field_value, $cookie) {
  $data = adobe_make_request('sco-expanded-contents', 'sco-id=' . $user_folder_id . '&filter-url-path=' . $field_value . '&session=' . $cookie);
  if ($data['status']['@attributes']['code'] !== 'ok' || empty($data['expanded-scos'])) {
    return FALSE;
  }
  return $data['expanded-scos'];
}

/**
 * Finds meeting on the server by its URL
 * (Search results depend on the cookie's user access)
 * TODO: Check if works for all meetings for all users
 */
function adobe_find_meeting_by_url($meeting_url, $cookie) {
  $data = adobe_make_request('sco-by-url', 'url-path=' . $meeting_url . '&session=' . $cookie);

  if ($data['status']['@attributes']['code'] !== 'ok' || empty($data['sco'])) {
    return FALSE;
  }
  return $data;
}

function adobe_get_expanded_meeting_name($user_folder_id, $field_value, $cookie) {
  $data = adobe_make_request('sco-expanded-contents', 'sco-id=' . $user_folder_id . '&filter-name=' . $field_value . '&session=' . $cookie);

  if ($data['status']['@attributes']['code'] !== 'ok' || empty($data['expanded-scos'])) {
    return FALSE;
  }
  return $data;
}

/**
 * Return the ID of user's folder
 */
function get_user_folder_id($user_email, $cookie) {
  $folder_id = adobe_get_meeting_folder_id($cookie);
  $data = adobe_make_request('sco-contents', 'sco-id=' . $folder_id . '&session=' . $cookie);

  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  $data_array = '';
  foreach ($data['scos']['sco'] as $key => $value) {
    if ($value['name'] == $user_email) {
      $data_array = $value;
      break;
    }
  }
  return $data_array['@attributes']['sco-id'];
}

/**
 * Gets the current logged in user
 */
function adobe_get_current_user($cookie) {
  $data = adobe_make_request('common-info', 'session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok' || !isset($data['common']['user'])) {
    return FALSE;
  }

  return $data['common']['user'];
}
/**
 * Check if a user has a given role on a given object
 */
function adobe_user_has_role($sco_id, $role, $cookie) {
  $data = adobe_make_request('permissions-info', 'acl-id=' . $sco_id . '&filter-permission-id=' . $role . '&session=' . $cookie);
  $current_user = adobe_get_current_user($cookie);

  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok' || empty($data['permissions'])) {
    return FALSE;
  }

  foreach ($data['permissions'] as $user){
    if ($user['@attributes']['principal-id'] == $current_user['@attributes']['user-id']) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Check if an user exits and load its info
 */
function adobe_check_user($user = '', $cookie) {
  $data = adobe_make_request('principal-list', 'filter-login=' . $user . '&session=' . $cookie);

  if ($data['status']['@attributes']['code'] !== 'ok' || empty($data['principal-list']) || empty($user)) {
    return FALSE;
  }
  return $data;
}

function adobe_check_group_permission($user, $group_id, $cookie) {
  $data = adobe_make_request('principal-list', 'group-id=' . $group_id . '&filter-is-member=true&filter-login=' . $user . '&session=' . $cookie);
  if ((!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') || empty($data['principal-list'])) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Logs out an user by cookie
 */
function adobe_logout($cookie) {
  $data = adobe_make_request('logout', 'session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }

  return TRUE;
}

/**
 * Add one or more participants to a meeting
 */
function adobe_add_participants($sco_id, $principal_ids, $cookie) {
  if (!is_array($principal_ids) && is_string($principal_ids)) {
    $principal_ids = array($principal_ids);
  }
  $principal_data = '';
  foreach ($principal_ids as $principal_id) {
    $principal_data .= 'acl-id=' . $sco_id . '&principal-id=' . $principal_id . '&permission-id=view&';
  }
  $data = adobe_make_request('permissions-update', $principal_data . 'session=' . $cookie);

  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return TRUE;
}

/**
 * Load info about an already finished meeting
 */
function adobe_get_meeting_archive($sco_id, $cookie) {
  $data = adobe_make_request('sco-contents', 'sco-id=' . $sco_id .
      '&session=' . $cookie);

  if ($data['status']['@attributes']['code'] !== 'ok' || empty($data['scos'])) {
    return FALSE;
  }
  return $data;
}

/**
 * Force recording of all webinars
 */
function adobe_force_recording($account_id, $cookie) {
  $data = adobe_make_request('meeting-feature-update', 'account-id=' . $account_id .
      '&feature-id=fid-archive&enable=false&feature-id=fid-archive-force&enable=true&feature-id=fid-archive-publish-link&enable=true' .
      '&session=' . $cookie);
  if (!isset($data['status']['@attributes']['code']) || $data['status']['@attributes']['code'] !== 'ok') {
    return FALSE;
  }
  return TRUE;
}
