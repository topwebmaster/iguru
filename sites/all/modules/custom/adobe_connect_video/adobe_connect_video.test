<?php

/**
 * @file
 * Test case for Testing the page example module.
 *
 * This file contains the test cases to check if module is performing as
 * expected.
 *
 */
class AdobeConnectVideoTestCase extends DrupalWebTestCase {

  public static function getInfo() {
    return array(
      'name' => 'Adobe Connect Video functionality',
      'description' => 'Create a content type with Adobe Connect Video fields, create a node, check for correct values.',
      'group' => 'Daisynet',
    );
  }

  /**
   * Enable modules and create user with specific permissions.
   */
  function setUp() {
    parent::setUp('adobe_connect_video');
  }

 /**
   * Test basic functionality of the example field.
   *
   * - Creates a content type.
   * - Adds a single-valued adobe_connect_video_url to it.
   * - Creates a node of the new type.
   * - Populates the single-valued field.
   * - Tests the result.
   */
  function testAdobeConnectVideoBasic() {
    $content_type_machine = strtolower($this->randomName(10));
    $title = $this->randomName(20);

    // Create and login user.
    $account = $this->drupalCreateUser(array('administer content types'));
    $this->drupalLogin($account);

    $this->drupalGet('admin/structure/types');

    // Create the content type.
    $this->clickLink(t('Add content type'));

    $edit = array(
      'name' => $content_type_machine,
      'type' => $content_type_machine,
    );
    $this->drupalPost(NULL, $edit, t('Save and add fields'));
    $this->assertText(t('The content type @name has been added.', array('@name' => $content_type_machine)));

    $single_text_field = strtolower($this->randomName(10));

    // Description of fields to be created;
    $fields[$single_text_field] = array(
      'widget' => 'adobe_connect_video_url',
      'cardinality' => '1',
    );

    foreach ($fields as $fieldname => $details) {
      $this->create_field($fieldname, $details['widget'], $details['cardinality']);
    }

    // Somehow clicking "save" isn't enough, and we have to do a
    // node_types_rebuild().
    node_types_rebuild();
    menu_rebuild();
    $type_exists = db_query('SELECT 1 FROM {node_type} WHERE type = :type', array(':type' => $content_type_machine))->fetchField();
    $this->assertTrue($type_exists, 'The new content type has been created in the database.');

    $permission = array('create ' . $content_type_machine . ' content','access adobe_connect_video','administer adobe_connect_video');
    // Reset the permissions cache.
    $this->checkPermissions($permission, TRUE);

    // Now that we have a new content type, create a user that has privileges
    // on the content type.
    $account = $this->drupalCreateUser($permission);
    $this->drupalLogin($account);

    $this->drupalGet('node/add/' . $content_type_machine);

    // Add a node.
    $edit = array(
      'title' => $title,
      'field_' . $single_text_field . '[und][0][url]' => 'Proba'
    );
     //$this->assertText(t(implode(';',$edit)));
    $this->drupalPost(NULL, $edit, t('Save'));
    $this->assertText(t('@content_type_machine @title has been created', array('@content_type_machine' => $content_type_machine, '@title' => $title)));


    $output_strings = $this->xpath("//div[contains(@class,'field-type-adobe-connect-video-url')]/div/div/div/text()");
   // $this->assertText(t(implode(';',$output_strings)));
    $this->assertEqual((string)$output_strings[0], t("Sorry, no access!"));
  }

  /**
   * Utility function to create fields on a content type.
   * @param $field_name
   *   Name of the field, like field_something
   * @param $widget_type
   *   Widget type, like field_example_3text
   * @param $cardinality
   *   Cardinality
   */
  protected function create_field($field_name, $widget_type, $cardinality) {
        // Add a singleton field_example_text field.
    $edit = array(
      'fields[_add_new_field][label]' => $field_name,
      'fields[_add_new_field][field_name]' => $field_name,
      'fields[_add_new_field][type]' => 'adobe_connect_video_url',
      'fields[_add_new_field][widget_type]' => $widget_type,

    );
    $this->drupalPost(NULL, $edit, t('Save'));

    // There are no settings for this, so just press the button.
    $this->drupalPost(NULL, array(), t('Save field settings'));

    $edit = array('field[cardinality]' => (string)$cardinality);

    // Using all the default settings, so press the button.
    $this->drupalPost(NULL, $edit, t('Save settings'));
    debug(t('Saved settings for field %field_name with widget %widget_type and cardinality %cardinality', array('%field_name' => $field_name, '%widget_type' => $widget_type, '%cardinality' => $cardinality)));
    $this->assertText(t('Saved @name configuration.', array('@name' => $field_name)));
  }
}
