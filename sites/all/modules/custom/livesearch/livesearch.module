<?php
/**
 * @file
 * Contains the Live Search feature, using Finder + Views module enhancements.
 */

/**
 * Implements hook_views_api().
 */
function livesearch_views_api() {
  return array(
    'api' => 3.0,
  );
}

/**
 * Implements hook_views_query_alter().
 */
function livesearch_views_query_alter(&$view, &$query) {
  //Attempt to retrieve the keywords from 2 basic sources.
  $keywords = isset($_GET['keywords']) ? $_GET['keywords'] : (arg(1) ? arg(1) : '');

  //Perform alterations only on module views.
  if ($view->name == 'search_results') {
    // Traverse through the 'where' part of the query.
    foreach ($query->where as &$condition_group) {
      foreach ($condition_group['conditions'] as $key => &$condition) {
        //Unset the original conditions for title and body in the view - we are adding them later via an 'OR' statement.
        if ($condition['field'] == 'node.title' || $condition['field'] == 'field_data_body.body_value') {
          $condition['value'] = '%' . db_like($keywords) . '%';
          unset($condition_group['conditions'][$key]);
        }
      }
      //Traverse the keywords and build the 'OR' conditions group using 'LIKE' statements.
      if($keywords !== '') {
        $words = explode(',', $keywords);
        foreach($words as $word) {
          if($word != '') {
            $search_conditions_group = array(
              'conditions' => array(
                array(
                  'field' => 'node.title',
                  'value' => '%' . db_like($word) . '%',
                  'operator' => 'LIKE',
                ),
                array(
                  'field' => 'field_data_body.body_value',
                  'value' => '%' . db_like($word) . '%',
                  'operator' => 'LIKE',
                ),
              ),
              'type' => 'OR',
            );

            $query->where[] = &$search_conditions_group;
          }
        }
      }
    }
  }

  //Perform alterations only on module views.
  if($view->name == 'search_results_users') {
    // Traverse through the 'where' part of the query.
    foreach ($query->where as &$condition_group) {
      foreach ($condition_group['conditions'] as $key => &$condition) {
        //Unset the original conditions for searched user fields in the view - we are adding them later via an 'OR' statement.
        if (in_array($condition['field'],
          array(
            //'field_data_field_user_about.field_user_about_value',
            'field_data_field_user_last_name.field_user_last_name_value',
            'field_data_field_user_name.field_user_name_value',
          )
        )) {
          unset($condition_group['conditions'][$key]);
        }
      }
      //Traverse the keywords and build the 'OR' conditions group using 'LIKE' statements.
      if($keywords !== '') {
        $words = explode(',', $keywords);
        foreach($words as $word) {
          if($word != '') {
            $search_conditions_group = array(
              'conditions' => array(
                /*
                array(
                  'field' => 'field_data_field_user_about.field_user_about_value',
                  'value' => '%' . db_like($word) . '%',
                  'operator' => 'LIKE',
                ),
                */
                array(
                  'field' => 'users.name',
                  'value' => '%' . db_like($word) . '%',
                  'operator' => 'LIKE',
                ),
                array(
                  'field' => 'field_data_field_user_last_name.field_user_last_name_value',
                  'value' => '%' . db_like($word) . '%',
                  'operator' => 'LIKE',
                ),
                array(
                  'field' => 'field_data_field_user_name.field_user_name_value',
                  'value' => '%' . db_like($word) . '%',
                  'operator' => 'LIKE',
                ),
              ),
              'type' => 'OR',
            );

            $query->where[] = &$search_conditions_group;
          }
        }
      }
    }
  }
}