<?php
  /**
   * @file
   * Includes the default views generated for the LiveSearch.
   */

  /**
   * Implements hook_views_default_views().
   */
  function livesearch_views_default_views() {
    $views = array();

    /* Builds the user search view. */
    $view = new view();
    $view->name = 'search_results_users';
    $view->description = 'Defines search results constructed using \'query_alter\' statements. Built inside the \'LiveSearch\' module.';
    $view->tag = 'default';
    $view->base_table = 'users';
    $view->human_name = 'Search Results (Users)';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = t('Search Results');
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['access']['perm'] = 'access user profiles';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['query']['options']['distinct'] = TRUE;
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '10';
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['row_plugin'] = 'fields';
    $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
    /* Field: User: Name */
    $handler->display->display_options['fields']['name']['id'] = 'name';
    $handler->display->display_options['fields']['name']['table'] = 'users';
    $handler->display->display_options['fields']['name']['field'] = 'name';
    $handler->display->display_options['fields']['name']['label'] = '';
    $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
    $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
    /* Field: User: Last Name */
    $handler->display->display_options['fields']['field_user_last_name']['id'] = 'field_user_last_name';
    $handler->display->display_options['fields']['field_user_last_name']['table'] = 'field_data_field_user_last_name';
    $handler->display->display_options['fields']['field_user_last_name']['field'] = 'field_user_last_name';
    $handler->display->display_options['fields']['field_user_last_name']['label'] = '';
    $handler->display->display_options['fields']['field_user_last_name']['element_label_colon'] = FALSE;
    /* Field: User: Name */
    $handler->display->display_options['fields']['field_user_recommended_name']['id'] = 'field_user_recommended_name';
    $handler->display->display_options['fields']['field_user_recommended_name']['table'] = 'field_data_field_user_recommended_name';
    $handler->display->display_options['fields']['field_user_recommended_name']['field'] = 'field_user_recommended_name';
    $handler->display->display_options['fields']['field_user_recommended_name']['label'] = '';
    $handler->display->display_options['fields']['field_user_recommended_name']['element_label_colon'] = FALSE;
    /* Sort criterion: User: Created date */
    $handler->display->display_options['sorts']['created']['id'] = 'created';
    $handler->display->display_options['sorts']['created']['table'] = 'users';
    $handler->display->display_options['sorts']['created']['field'] = 'created';
    $handler->display->display_options['sorts']['created']['order'] = 'DESC';
    /* Filter criterion: User: Active */
    $handler->display->display_options['filters']['status']['id'] = 'status';
    $handler->display->display_options['filters']['status']['table'] = 'users';
    $handler->display->display_options['filters']['status']['field'] = 'status';
    $handler->display->display_options['filters']['status']['value'] = '1';
    $handler->display->display_options['filters']['status']['group'] = 1;
    $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
    /* Filter criterion: User: Educational / Professional Bio (field_user_about) */
    $handler->display->display_options['filters']['field_user_about_value']['id'] = 'field_user_about_value';
    $handler->display->display_options['filters']['field_user_about_value']['table'] = 'field_data_field_user_about';
    $handler->display->display_options['filters']['field_user_about_value']['field'] = 'field_user_about_value';
    $handler->display->display_options['filters']['field_user_about_value']['operator'] = 'contains';
    /* Filter criterion: User: Last Name (field_user_last_name) */
    $handler->display->display_options['filters']['field_user_last_name_value']['id'] = 'field_user_last_name_value';
    $handler->display->display_options['filters']['field_user_last_name_value']['table'] = 'field_data_field_user_last_name';
    $handler->display->display_options['filters']['field_user_last_name_value']['field'] = 'field_user_last_name_value';
    $handler->display->display_options['filters']['field_user_last_name_value']['operator'] = 'contains';
    /* Filter criterion: User: Name (field_user_name) */
    $handler->display->display_options['filters']['field_user_name_value']['id'] = 'field_user_name_value';
    $handler->display->display_options['filters']['field_user_name_value']['table'] = 'field_data_field_user_name';
    $handler->display->display_options['filters']['field_user_name_value']['field'] = 'field_user_name_value';
    $handler->display->display_options['filters']['field_user_name_value']['operator'] = 'contains';

    /* Display: Search Results (User) */
    $handler = $view->new_display('page', t('Search Results (User)'), 'user_search_results');
    $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
    $handler->display->display_options['path'] = 'people-finder/%';
    $translatables['search_results_users'] = array(
      t('Master'),
      t('Search Results'),
      t('more'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Items per page'),
      t('- All -'),
      t('Offset'),
      t('« first'),
      t('‹ previous'),
      t('next ›'),
      t('last »'),
      t('Search Results (User)'),
    );

    $views['search_results_users'] = $view;

    /* Builds the content search view */
    $view = new view();
    $view->name = 'search_results';
    $view->description = 'Defines search results constructed using \'query_alter\' statements. Built inside the \'LiveSearch\' module.';
    $view->tag = 'default';
    $view->base_table = 'node';
    $view->human_name = t('Search Results (Content)');
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = t('Search Results');
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '20';
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['row_plugin'] = 'fields';
    /* Field: Content: Title */
    $handler->display->display_options['fields']['title']['id'] = 'title';
    $handler->display->display_options['fields']['title']['table'] = 'node';
    $handler->display->display_options['fields']['title']['field'] = 'title';
    $handler->display->display_options['fields']['title']['label'] = '';
    $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
    $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
    /* Sort criterion: Content: Post date */
    $handler->display->display_options['sorts']['created']['id'] = 'created';
    $handler->display->display_options['sorts']['created']['table'] = 'node';
    $handler->display->display_options['sorts']['created']['field'] = 'created';
    $handler->display->display_options['sorts']['created']['order'] = 'DESC';
    /* Filter criterion: Content: Published */
    $handler->display->display_options['filters']['status']['id'] = 'status';
    $handler->display->display_options['filters']['status']['table'] = 'node';
    $handler->display->display_options['filters']['status']['field'] = 'status';
    $handler->display->display_options['filters']['status']['value'] = 1;
    $handler->display->display_options['filters']['status']['group'] = 1;
    $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

    /* Display: Search Results (Content) */
    $handler = $view->new_display('page', 'Search Results (Content)', 'content_search_results');
    $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
    $handler->display->display_options['defaults']['filter_groups'] = FALSE;
    $handler->display->display_options['defaults']['filters'] = FALSE;
    /* Filter criterion: Content: Published */
    $handler->display->display_options['filters']['status']['id'] = 'status';
    $handler->display->display_options['filters']['status']['table'] = 'node';
    $handler->display->display_options['filters']['status']['field'] = 'status';
    $handler->display->display_options['filters']['status']['value'] = 1;
    $handler->display->display_options['filters']['status']['group'] = 1;
    $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
    /* Filter criterion: Content: Title */
    $handler->display->display_options['filters']['title']['id'] = 'title';
    $handler->display->display_options['filters']['title']['table'] = 'node';
    $handler->display->display_options['filters']['title']['field'] = 'title';
    $handler->display->display_options['filters']['title']['operator'] = 'contains';
    /* Filter criterion: Content: Body (body:format) */
    $handler->display->display_options['filters']['body_format']['id'] = 'body_format';
    $handler->display->display_options['filters']['body_format']['table'] = 'field_data_body';
    $handler->display->display_options['filters']['body_format']['field'] = 'body_format';
    $handler->display->display_options['filters']['body_format']['operator'] = 'contains';
    $handler->display->display_options['path'] = 'content-finder/%';

    /* Display: Search Results (Groups) */
    $handler = $view->new_display('page', 'Search Results (Groups)', 'groups_search_results');
    $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
    $handler->display->display_options['defaults']['filter_groups'] = FALSE;
    $handler->display->display_options['defaults']['filters'] = FALSE;
    /* Filter criterion: Content: Published */
    $handler->display->display_options['filters']['status']['id'] = 'status';
    $handler->display->display_options['filters']['status']['table'] = 'node';
    $handler->display->display_options['filters']['status']['field'] = 'status';
    $handler->display->display_options['filters']['status']['value'] = 1;
    $handler->display->display_options['filters']['status']['group'] = 1;
    $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
    /* Filter criterion: Content: Title */
    $handler->display->display_options['filters']['title']['id'] = 'title';
    $handler->display->display_options['filters']['title']['table'] = 'node';
    $handler->display->display_options['filters']['title']['field'] = 'title';
    $handler->display->display_options['filters']['title']['operator'] = 'contains';
    /* Filter criterion: Content: Body (body:format) */
    $handler->display->display_options['filters']['body_format']['id'] = 'body_format';
    $handler->display->display_options['filters']['body_format']['table'] = 'field_data_body';
    $handler->display->display_options['filters']['body_format']['field'] = 'body_format';
    $handler->display->display_options['filters']['body_format']['operator'] = 'contains';
    /* Filter criterion: Content: Type */
    $handler->display->display_options['filters']['type']['id'] = 'type';
    $handler->display->display_options['filters']['type']['table'] = 'node';
    $handler->display->display_options['filters']['type']['field'] = 'type';
    $handler->display->display_options['filters']['type']['value'] = array(
      'group' => 'group',
    );
    $handler->display->display_options['path'] = 'groups-finder/%';
    $translatables['search_results'] = array(
      t('Master'),
      t('Search Results'),
      t('more'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Items per page'),
      t('- All -'),
      t('Offset'),
      t('« first'),
      t('‹ previous'),
      t('next ›'),
      t('last »'),
      t('Search Results (Content)'),
      t('Search Results (Groups)'),
    );

    $views['search_results'] = $view;

    return $views;
  }
