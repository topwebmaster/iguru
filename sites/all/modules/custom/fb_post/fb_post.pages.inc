<?php

/**
 * Settings form callback
 */
function fb_post_admin_settings($form, &$form_state) {

  fb_post_config();

  $form['fb_post_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable posting to wall.'),
    '#default_value' => variable_get('fb_post_enable', 1),
  );

  $form['fb_post_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('fb_post_types', array('story' => 'story', 'blog' => 'blog')),
  );

  $form['fb_pageid'] = array(
    '#type' => 'textfield',
    '#title' => t('Page ID'),
    '#default_value' => variable_get('fb_pageid', NULL),
  );

  $form['fb_appid'] = array(
    '#type' => 'textfield',
    '#title' => t('Application ID'),
    '#default_value' => variable_get('fb_appid', NULL),
  );

  $form['fb_app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Application secret'),
    '#default_value' => variable_get('fb_app_secret', NULL),
  );

  $form['fb_new_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access Token New'),
    '#default_value' => variable_get('fb_new_access_token', NULL),
    '#maxlength' => 255,
    '#description' => 'Login with your account with admin access to page, go to: <br />'
    . 'http://developers.facebook.com/tools/explorer/' . variable_get('fb_appid', 'APP_ID') . '/?method=GET&path=' . variable_get('fb_pageid', 'PAGE_ID')
    . '<br /> click: "Get Access Token"'
  );
  $form['fb_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access Token'),
    '#default_value' => variable_get('fb_access_token', NULL),
    '#maxlength' => 255,
    '#disabled' => TRUE
  );

  return system_settings_form($form);
}

