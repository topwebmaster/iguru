<?php
/**
 * @file
 * Implements Ubercart hooks, theming overrides, payment gateways and other functions.
 * Custom Ubercart processing for the DaisyNet platform.
 *
 * TODO: Extract all page callback to daisy_ubercart.pages.inc when .module file reaches 1000 lines.
 */


/**
 * Implements hook_menu_alter().
 */
function daisy_ubercart_menu_alter(&$items){
  //Override the original checkout complete callback, by adding extra processing for post-payment redirect.
  $items['cart/checkout/complete'] = array(
    'title' => 'Order complete',
    'description' => 'Display information upon completion of an order.',
    'page callback' => 'daisy_ubercart_checkout_complete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
}


/**
 * Implements a custom checkout complete processing, with advanced redirect conditions.
 */
function daisy_ubercart_checkout_complete() {
  if (empty($_SESSION['cart_order']) || empty($_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_complete'])) {
    drupal_goto('cart');
  }

  $checkout_complete_redirect_path = '';
  $order = uc_order_load(intval($_SESSION['cart_order']));

  if (empty($order)) {
    // Display messages to customers and the administrator if the order was lost.
    drupal_set_message(t("We're sorry.  An error occurred while processing your order that prevents us from completing it at this time. Please contact us and we will resolve the issue as soon as possible."), 'error');
    watchdog('uc_cart', 'An empty order made it to checkout! Cart order ID: @cart_order', array('@cart_order' => $_SESSION['cart_order']), WATCHDOG_ERROR);
    drupal_goto('cart');
  }
  else {
    // Add to participants list
    $webinars = $order->products;
    foreach ($webinars as $webinar) {
      // Check for participations, key = 3
      $purchase_type = $webinar->data['Type'];
      if (array_key_exists(3, $purchase_type)) {
        $node = node_load($webinar->nid);
        $node_wrapper = entity_metadata_wrapper('node', $node);
        $participants = $node_wrapper->field_participants->value();
        $new_participant = user_load($webinar->order_uid);
        $participants[] = $new_participant;
        $node_wrapper->field_participants->set($participants);
        entity_save('node', $node_wrapper);
      }
    }
    //Retrieves the latest item in the order and build the url to it.
    $latest_product = end($order->products);
    $checkout_complete_redirect_path = url('node/' . $latest_product->nid, array('absolute' => TRUE));
  }

  $build = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
  unset($_SESSION['uc_checkout'][$order->order_id], $_SESSION['cart_order']);

  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');

  //Use the latest product in order redirect, otherwise use the default redirect.
  if($checkout_complete_redirect_path != '') {
    $page = $checkout_complete_redirect_path;
  }
  else {
    $page = variable_get('uc_cart_checkout_complete_page', '');
  }

  if (!empty($page)) {
    drupal_goto($page);
  }

  return $build;
}

/**
 * Implements hook_form_alter().
 * Alters the checkout forms for webinars.
 */
function daisy_ubercart_form_alter(&$form, &$form_state, $form_id) {
  if(strpos($form_id, 'uc_product_add_to_cart_form') !== FALSE) {
    $nid = $form['nid']['#value'];

    $attributes = uc_product_get_attributes($nid);

    //TODO: Extend this to flexible solution for all possible attributes.
    foreach($attributes as $attribute) {
      if($attribute->aid == 2) {
        foreach($attribute->options as $option) {
          $price = ((float) $option->price *10)/10;
          if($option->oid == 3 || $option->oid == 4) {
            $form['actions'][$option->oid . '-option'] = array(
              '#type' => 'submit',
              '#value' => t($option->name),
              '#attributes' => array(
                'class' => array(
                  'type-' . preg_replace('@[^a-z0-9_]+@','_',strtolower($option->name))
                )
              ),
              '#prefix' => '<div class="btn-wrapper">',
              '#suffix' => '<span class="btn-over price">$' . $price . '</span></div>',
            );
          }
        }

        if(count(element_children($form['actions'])) > 1) {
          //Remove the original 'Add to cart' button.
          $form['actions']['submit']['#access'] = FALSE;
        }
      }
    }

    $form['attributes'][2]['#type'] = 'hidden';

    //Adds additional validation to the cart form.
    $form['#validate'] = array_unshift($form['#validate'], 'daisy_ubercart_uc_add_to_cart_options_validate');
  }
}

/**
 * Additional validation of ubercart product forms with various options.
 */
function daisy_ubercart_uc_add_to_cart_options_validate($form, &$form_state){
  $values = $form_state['values'];
  $node = $values['node'];

  //Retrieve the triggering element.
  $triggering_element = $values['op'];
}

/**
 * Implements hook_node_insert().
 * @param $node
 * @return void
 */
function daisy_ubercart_node_insert($node) {
  //Adds the attribute options for the product content type.
  if ($node->type == 'product' && $node->field_webinar_price[LANGUAGE_NONE][0]['value'] == 'paid') {
    $wb_view = $node->field_webinar_pr_view[LANGUAGE_NONE][0]['value'];
    $wb_participate = $node->field_webinar_pr_participate[LANGUAGE_NONE][0]['value'];

    //TODO: replace hard coded attribute with flexible solution for all possible attributes.
    $attribute = uc_attribute_load(2);
    $attribute->label = 'Type';
    $attribute->description = '';
    $attribute->default_option = 3;
    $attribute->nid = $node->nid;

    $attribute->options = array();
    $attribute->options[4] = new stdClass();
    $attribute->options[4]->oid = 4;
    $attribute->options[4]->nid = $node->nid;
    $attribute->options[4]->price = $wb_view;

    $attribute->options[3] = new stdClass();
    $attribute->options[3]->oid = 3;
    $attribute->options[3]->nid = $node->nid;
    $attribute->options[3]->price = $wb_participate;

    //Write the attribute + options to the database - assign to newly created node.
    drupal_write_record('uc_product_attributes', $attribute);
    foreach ($attribute->options as $option) {
      drupal_write_record('uc_product_options', $option);
    }
  }
}


/**
 * Implements hook_node_update().
 * @param $node
 * @return void
 */
function daisy_ubercart_node_update($node) {
  //Adds the attribute options for the product content type.
  if ($node->type == 'product' && $node->field_webinar_price[LANGUAGE_NONE][0]['value'] == 'paid') {
    $wb_view = $node->field_webinar_pr_view[LANGUAGE_NONE][0]['value'];
    $wb_participate = $node->field_webinar_pr_participate[LANGUAGE_NONE][0]['value'];

    //TODO: replace hard coded attribute with flexible solution for all possible attributes.
    $attribute = uc_attribute_load(2);
    $attribute->label = 'Type';
    $attribute->description = '';
    $attribute->default_option = 3;
    $attribute->nid = $node->nid;

    $attribute->options = array();
    $attribute->options[4] = new stdClass();
    $attribute->options[4]->oid = 4;
    $attribute->options[4]->nid = $node->nid;
    $attribute->options[4]->price = $wb_view;

    $attribute->options[3] = new stdClass();
    $attribute->options[3]->oid = 3;
    $attribute->options[3]->nid = $node->nid;
    $attribute->options[3]->price = $wb_participate;

    //Delete the previous values for type attribute and options for current node.
    db_delete('uc_product_attributes')->condition('nid', $node->nid)->condition('aid', 2)->execute();
    db_delete('uc_product_options')->condition('nid', $node->nid)->condition('oid', array(3,4), 'IN')->execute();
    $attributes = uc_product_get_attributes($node->nid);

    //Write the new attributes + options for current node.
    drupal_write_record('uc_product_attributes', $attribute);
    foreach ($attribute->options as $key => $option) {
      drupal_write_record('uc_product_options', $option);
    }
  }
}


/**
 * Define default product classes.
 *
 * The results of this hook are eventually passed through hook_node_info(),
 * so you may include any keys that hook_node_info() uses. Defaults will
 * be provided where keys are not set. This hook can also be used to
 * override the default "product" product class name and description.
 */
function daisy_ubercart_uc_product_default_classes() {
  return array(
    'product' => array(
      'name' => t('DaisyNet Webinar (basic)'),
      'description' => t('The basic product sold by the DaisyNet platform - an online webinar.'),
    ),
  );
}

/**
 * Implements hook_uc_product_types().
 * TODO: Use this to define future default product classes.
 * @return array
 */
function daisy_ubercart_uc_product_types() {
  return array(
    'product', //Default product class, declared by the Ubercart core.
  );
}

/**
 * Performs extra processing when an item is added to the shopping cart.
 *
 * Some modules need to be able to hook into the process of adding items to a
 * cart. For example, an inventory system may need to check stock levels and
 * prevent an out of stock item from being added to a customer's cart. This hook
 * lets developers squeeze right in at the end of the process after the product
 * information is all loaded and the product is about to be added to the cart.
 * In the event that a product should not be added to the cart, you simply have
 * to return a failure message described below. This hook may also be used
 * simply to perform some routine action when products are added to the cart.
 *
 * @param $nid
 *   The node ID of the product.
 * @param $qty
 *   The quantity being added.
 * @param $data
 *   The data array, including attributes and model number adjustments.
 *
 * @return
 *   The function can use this data to whatever purpose to see if the item
 *   can be added to the cart or not. The function should return an array
 *   containing the result array. (This is due to the nature of Drupal's
 *   module_invoke_all() function. You must return an array within an array
 *   or other module data will end up getting ignored.) At this moment,
 *   there are only three keys:
 *   - success: TRUE or FALSE for whether the specified quantity of the item
 *     may be added to the cart or not; defaults to TRUE.
 *   - message: The fail message to display in the event of a failure; if
 *     omitted, Ubercart will display a default fail message.
 *   - silent: Return TRUE to suppress the display of any messages; useful
 *     when a module simply needs to do some other processing during an add
 *     to cart or fail silently.
 */
function daisy_ubercart_uc_add_to_cart($nid, $qty, $data) {
  $result = array();

  //Limit the purchase time if a given start date is indicated for the product (Webinar).
  //TODO: Correct time check
  if(isset($data['date']) && $data['date'] < time() && $data['Type'] == 3) {
    $result[] = array(
      'success' => FALSE,
      'message' => t('You can\t purchase participation to a past webinar.'),
    );
  }

  //Allow only a single item of a kind to be bought at once.
  if ($qty > 1) {
    $result[] = array(
      'success' => FALSE,
      'message' => t('Sorry, you can purchase only 1 webinar at a time.'),
    );
  }

  return $result;
}

/**
 * Adds extra information to a cart item's "data" array.
 *
 * This is effectively the submit handler of any alterations to the Add to Cart
 * form. It provides a standard way to store the extra information so that it
 * can be used by daisy_ubercart_uc_add_to_cart().
 *
 * @param $form_values
 *   The values submitted to the Add to Cart form.
 *
 * @return
 *   An array of data to be merged into the item added to the cart.
 */
function daisy_ubercart_uc_add_to_cart_data($form_values) {
  $node = node_load($form_values['nid']);

  //Assign default values for item price and description (may alter based on submitted option button).
  $price = 0;
  $description = '';

  //Retrieve the triggering element.
  $triggering_element = $form_values['op'];

  /**
   * Compare the triggering element with any of the attribute options.
   * If they match, perform an alteration in the attributes data for form item.
   */
  $attributes = $form_values['attributes'];
  $type_attribute = uc_attribute_load(2, $node->nid);
  if(isset($attributes[2])) {
    foreach($type_attribute->options as $option) {
      if(t($option->name) == t($triggering_element)) {
        $attributes[2] = $option->oid;
        switch($option->oid) {
          case '3':
            $price = $node->field_webinar_pr_participate[LANGUAGE_NONE][0]['value'];
            $description = '<p>' . t($type_attribute->name . ': ' . $option->name) . '</p>';
            break;
          case '4':
            $price = $node->field_webinar_pr_view[LANGUAGE_NONE][0]['value'];
            $description = '<p>' . t($type_attribute->name . ': ' . $option->name) . '</p>';
            break;
        }
      }
    }
  }

  if($node->type == 'product') {
    return array(
      //Switch module to daisy_ubercart to enable Daisy specific product in cart processing.
      'module' => 'daisy_ubercart',

      //Add webinar specific and crucial values: participation prices and start date.
      'price' => $price,
      'price_view' => $node->field_webinar_pr_view[LANGUAGE_NONE][0]['value'],
      'price_participate' => $node->field_webinar_pr_participate[LANGUAGE_NONE][0]['value'],
      'attributes' => $attributes, //Re-assignes the attributes for the element, with the possible changes.
      'date' => strtotime($node->field_webinar_date[LANGUAGE_NONE][0]['value']),
      'description' => $description,
      'selected_attributes' => $attributes,
      //Adds the webinar type attribute as individual value.
      'Type' => array($attributes[2] => $triggering_element),
    );
  }
}

/**
 * Controls the display of an item in the cart.
 *
 * Product type modules allow the creation of nodes that can be added to the
 * cart. The cart determines how they are displayed through this hook. This is
 * especially important for product kits, because it may be displayed as a
 * single unit in the cart even though it is represented as several items.
 *
 * This hook is only called for the module that owns the cart item in
 * question, as set in $item->module.
 *
 * @param $item
 *   The item in the cart to display.
 *
 * @return
 *   A form array containing the following elements:
 *   - "nid"
 *     - #type: value
 *     - #value: The node id of the $item.
 *   - "module"
 *     - #type: value
 *     - #value: The module implementing this hook and the node represented by
 *       $item.
 *   - "remove"
 *     - #type: submit
 *     - #value: t('Remove'); when clicked, will remove $item from the cart.
 *   - "description"
 *     - #type: markup
 *     - #value: Themed markup (usually an unordered list) displaying extra
 *       information.
 *   - "title"
 *     - #type: markup
 *     - #value: The displayed title of the $item.
 *   - "#total"
 *     - type: float
 *     - value: Numeric price of $item. Notice the '#' signifying that this is
 *       not a form element but just a value stored in the form array.
 *   - "data"
 *     - #type: hidden
 *     - #value: The serialized $item->data.
 *   - "qty"
 *     - #type: textfield
 *     - #value: The quantity of $item in the cart. When "Update cart" is
 *       clicked, the customer's input is saved to the cart.
 */
function daisy_ubercart_uc_cart_display($item) {
  $element = array();
  $element['nid'] = array('#type' => 'value', '#value' => $item->nid);
  $element['module'] = array('#type' => 'value', '#value' => 'uc_product');
  $element['remove'] = array('#type' => 'checkbox');

  $element['title'] = array(
    '#markup' => l($item->title, 'node/' . $item->nid),
  );

  if($item->data['price'] != 0) {
    $item->price = $item->data['price'];
  }

  $element['#total'] = $item->price * $item->qty;
  $element['data'] = array('#type' => 'hidden', '#value' => serialize($item->data));
  $element['qty'] = array(
    '#type' => 'hidden',
    '#default_value' => $item->qty,
    '#size' => 5,
    '#maxlength' => 6,
    '#readonly' => TRUE,
    '#prefix' => '<span class="qty">' . $item->qty . '</span>',
  );

  //Use option based description, otherwise use default.
  if($item->data['description'] != '') {
    $element['description'] = array('#markup' => $item->data['description']);
  }
  elseif($description = uc_product_get_description($item)) {
    $element['description'] = array('#markup' => $description);
  }

  return $element;
}

/**
 * Implements hook_uc_cart_pane().
 */
function daisy_ubercart_uc_cart_pane($items) {
  $body = array();

  if (!is_null($items)) {
    $body = drupal_get_form('daisy_ubercart_cart_view_form', $items) + array(
      '#prefix' => '<div id="cart-form-pane">',
      '#suffix' => '</div>',
    );
  }

  $panes['daisy_cart_form'] = array(
    'title' => t('Daisy Ubercart cart form'),
    'enabled' => TRUE,
    'weight' => 0,
    'body' => $body,
  );

  return $panes;
}

/**
 * Displays the contents of the customer's cart.
 *
 * Handles simple or complex objects. Some cart items may have a list of
 * products that they represent. These are displayed but are not able to
 * be changed by the customer.
 *
 * @see uc_cart_view_form_submit()
 * @see uc_cart_view_form_continue_shopping()
 * @see uc_cart_view_form_checkout()
 * @see theme_uc_cart_view_form()
 * @see uc_cart_view_table()
 * @ingroup forms
 */
function daisy_ubercart_cart_view_form($form, &$form_state, $items = NULL) {
  $form['#attributes']['class'][] = 'daisy-ubercart-pane';
  $form['#attached']['css'][] = drupal_get_path('module', 'uc_cart') . '/uc_cart.css';
  $form['#attached']['css'][] = drupal_get_path('module', 'daisy_ubercart') . '/css/cart.css';
  $form['#submit'] = array('uc_cart_view_form_submit');

  $form['items'] = array(
    '#type' => 'tapir_table',
    '#tree' => TRUE,
  );

  $i = 0;
  $display_items = entity_view('uc_cart_item', $items, 'cart');
  foreach (element_children($display_items['uc_cart_item']) as $key) {
    $display_item = $display_items['uc_cart_item'][$key];
    if (count(element_children($display_item))) {
      // Setting up some additional columns
      $form['items'][$i] = $display_item;
      $form['items'][$i]['auto_number']['#markup'] = $i + 1;
      $form['items'][$i]['description']['#type'] = 'value';
      $type = $display_item['#entity']->data['Type'];
      $type_label = t('Undefined');
      if (isset($type[3])) {
        $type_label = t('Participant');
      }
      else if (isset($type[4])) {
        $type_label = t('Viewer');
      }
      $form['items'][$i]['type']['#markup'] = $type_label;
      $item_wrapper = entity_metadata_wrapper('node', $display_item['#entity']);
      $image = $item_wrapper->field_webinar_image->value();
      if (!empty($image)) {
        $image_options = array(
          'style_name' => 'user_image_small',
          'path' => $image['uri'],
          'width' => $image['width'],
          'height' => $image['height'],
          'alt' => $item_wrapper->title->value(),
          'title' => $item_wrapper->title->value(),
        );
        $image = theme_image_style($image_options);
      }
      else {
        $image = '';
      }
      $form['items'][$i]['image']['#markup'] = $image;

      if (isset($form['items'][$i]['remove'])) {
        // Backward compatibility with old checkbox method.
        if ($form['items'][$i]['remove']['#type'] == 'checkbox') {
          $form['items'][$i]['remove'] = array('#type' => 'submit', '#value' => t('Remove'));
        }

        $form['items'][$i]['remove']['#name'] = 'remove-' . $i;
      }

      if (empty($display_item['qty'])) {
        $form['items'][$i]['qty'] = array(
          '#type' => 'hidden',
          '#value' => 1,
        );
      }

      $form['items'][$i]['total'] = array(
        '#theme' => 'uc_price',
        '#price' => $display_item['#total'],
      );
      if (!empty($display_item['#suffixes'])) {
        $form['items'][$i]['total']['#suffixes'] = $display_item['#suffixes'];
      }
    }
    $i++;
  }

  $form['items'] = tapir_get_table('daisynet_ubercart_cart_view_table', $form['items']);

  $form['actions'] = array('#type' => 'actions');

  // If the continue shopping element is enabled...
  if (($cs_type = variable_get('uc_continue_shopping_type', 'link')) !== 'none') {
    // Setup the text used for the element.
    $cs_text = variable_get('uc_continue_shopping_text', '') ? variable_get('uc_continue_shopping_text', '') : t('Continue shopping');

    // Add the element to the form based on the element type.
    if (variable_get('uc_continue_shopping_type', 'link') == 'link') {
      $form['actions']['continue_shopping'] = array(
        '#markup' => l($cs_text, uc_cart_continue_shopping_url()),
      );
    }
    elseif (variable_get('uc_continue_shopping_type', 'link') == 'button') {
      $form['actions']['continue_shopping'] = array(
        '#type' => 'submit',
        '#value' => $cs_text,
        '#submit' => array('uc_cart_view_form_submit', 'uc_cart_view_form_continue_shopping'),
      );
    }
  }

  // Add the empty cart button if enabled.
  if (variable_get('uc_cart_empty_button', FALSE)) {
    $form['actions']['empty'] = array(
      '#type' => 'submit',
      '#value' => t('Empty cart'),
      '#submit' => array('uc_cart_view_form_empty'),
    );
  }

  // Add the control buttons for updating and proceeding to checkout.
  $form['actions']['update'] = array(
    '#type' => 'submit',
    '#name' => 'update-cart',
    '#value' => t('Update cart'),
    '#submit' => array('uc_cart_view_form_submit', 'uc_cart_view_form_update_message'),
  );
  $form['actions']['checkout'] = array(
    '#theme' => 'uc_cart_checkout_buttons',
  );
  if (variable_get('uc_checkout_enabled', TRUE)) {
    $form['actions']['checkout']['checkout'] = array(
      '#type' => 'submit',
      '#value' => t('Checkout'),
      '#submit' => array('uc_cart_view_form_submit', 'uc_cart_view_form_checkout'),
    );
  }

  return $form;
}

/**
 * Lists the products in the cart in a TAPIr table - DaisyNet style.
 */
function daisynet_ubercart_cart_view_table($table) {
  $table['#columns'] = array(

    'auto_number' => array(
      'cell' => array(
        'data' => '#',
        'class' => 'auto-number',
      ),
      'weight' => 1,
      ),
    'image' => array(
      'cell' => '',
      'weight' => 2,
      ),
    'title' => array(
      'cell' => array(
        'data' => t('Product name'),
        'class' => 'product-name',
      ),
      'weight' => 3,
    ),
    'remove' => array(
      'cell' => array(
        'data' => t('Operations'),
        'class' => 'remove',
      ),
      'weight' => 4,
    ),
    'type' => array(
      'cell' => array(
        'data' => t('Type'),
        'class' => 'total',
      ),
      'weight' => 5,
    ),
    'qty' => array(
      'cell' => array(
        'data' => t('Quantity'),
        'class' => 'qty'
      ),
      'weight' => 6,
    ),
    'total' => array(
      'cell' => array(
        'data' => t('Total'),
        'class' => 'total'
      ),
      'weight' => 7,
    ),
  );

  $subtotal = 0;
  foreach (element_children($table) as $i) {
    $subtotal += $table[$i]['#total'];
    $table[$i]['auto_number']['#cell_attributes'] = array('class' => array('auto-number'));
    $table[$i]['image']['#cell_attributes'] = array('class' => array('image'));
    $table[$i]['title']['#cell_attributes'] = array('class' => array('product-name'));
    $table[$i]['type']['#cell_attributes'] = array('class' => array('type'));
    $table[$i]['qty']['#cell_attributes'] = array('class' => array('qty'));
    $table[$i]['total']['#cell_attributes'] = array('class' => array('price'));
    $table[$i]['remove']['#cell_attributes'] = array('class' => array('remove'));
  }

  $table[] = array(
    'auto_number' => array(
      'cell' => '',
      '#cell_attributes' => array('class' => 'auto-number'),
      ),
    'title' => array(
      '#prefix' => '<span id="subtotal-title">' . t('Subtotal:') . '</span> ',
      '#cell_attributes' => array(
        'colspan' => '5',
        'class' => 'product-name',
      ),
    ),
    'total' => array(
      '#theme' => 'uc_price',
      '#price' => $subtotal,
    ),
  );

  return $table;
}

/**
 * Implements hook_checkout_pane().
 */
function daisy_ubercart_uc_checkout_pane() {

  $panes[] = array(
    'id' => 'daisy_ubercart_checkout_pane',
    'callback' => '_daisy_ubercart_checkout_pane',
    'title' => '',
    'desc' => t('Daisynet represenation of cart contents'),
    'weight' => 1,
    'process' => FALSE,
    'enabled' => FALSE,
    'collapsible' => FALSE,
  );

  return $panes;
}

/**
 * Daisynet checkout pane
 */
function _daisy_ubercart_checkout_pane($op, $order, $form = NULL, &$form_state = NULL) {
  drupal_set_title(t('My shopping cart'));
  switch ($op) {
    case 'view' :
      $i = 0;
      $rows = array();
      $items = $order->products;
      $display_items = entity_view('uc_order_product', $items, 'cart');
      foreach (element_children($display_items['uc_order_product']) as $key) {
        $display_item = $display_items['uc_order_product'][$key];
        if (count(element_children($display_item))) {
          // Setting up some additional columns
          $rows[$i] = $display_item;
          $rows[$i]['auto_number']['#markup'] = $i + 1;
          $rows[$i]['description']['#type'] = 'value';
          $type = $display_item['#entity']->data['Type'];
          $type_label = t('Undefined');
          $item_wrapper = entity_metadata_wrapper('node', $display_item['#entity']);
          $image = $item_wrapper->field_webinar_image->value();
          if (!empty($image)) {
            $image_options = array(
              'style_name' => 'user_image_small',
              'path' => $image['uri'],
              'width' => $image['width'],
              'height' => $image['height'],
              'alt' => $item_wrapper->title->value(),
              'title' => $item_wrapper->title->value(),
            );
            $image = theme_image_style($image_options);
          }
          else {
            $image = '';
          }
          $rows[$i]['image']['#markup'] = $image;

          if (isset($rows[$i]['remove'])) {
            unset($rows[$i]['remove']);
          }

          if (empty($display_item['qty'])) {
            $rows[$i]['qty'] = array(
              '#type' => 'hidden',
              '#value' => 1,
            );
          }

          $rows[$i]['total'] = array(
            '#theme' => 'uc_price',
            '#price' => $display_item['#total'],
          );
          if (!empty($display_item['#suffixes'])) {
            $rows[$i]['total']['#suffixes'] = $display_item['#suffixes'];
          }
        }
        $i++;
      }
      $rows['#type'] = 'tapir_table';
      $rows['#tree'] = TRUE;

      $checkout_table = tapir_get_table('_daisynet_ubercart_checkout_view_table', $rows);
      $contents['checkout_table'] = $checkout_table;
      return array('contents' => $contents, 'description' => '');
      break;
  }

}
/**
 * Lists the products in the cart in a TAPIr table - DaisyNet style.
 */
function _daisynet_ubercart_checkout_view_table($table) {
  $table['#attached']['css'][] = drupal_get_path('module', 'uc_cart') . '/uc_cart.css';
  $table['#attached']['css'][] = drupal_get_path('module', 'daisy_ubercart') . '/css/cart.css';
  $table['#prefix'] = '<div class="daisy-ubercart-pane">';
  $table['#suffix'] = '</div>';

  $table['#columns'] = array(
    'auto_number' => array(
      'cell' => array(
        'data' => '#',
        'class' => 'auto-number',
      ),
      'weight' => 1,
      ),
    'image' => array(
      'cell' => '',
      'weight' => 2,
      ),
    'title' => array(
      'cell' => array(
        'data' => t('Product name'),
        'class' => 'product-name',
      ),
      'weight' => 3,
    ),
    'qty' => array(
      'cell' => array(
        'data' => t('Quantity'),
        'class' => 'qty'
      ),
      'weight' => 6,
    ),
    'total' => array(
      'cell' => array(
        'data' => t('Total'),
        'class' => 'total'
      ),
      'weight' => 7,
    ),
  );

  $subtotal = 0;
  foreach (element_children($table) as $i) {
    $subtotal += $table[$i]['#total'];
    $table[$i]['auto_number']['#cell_attributes'] = array('class' => array('auto-number'));
    $table[$i]['image']['#cell_attributes'] = array('class' => array('image'));
    $table[$i]['title']['#cell_attributes'] = array('class' => array('product-name'));
    $table[$i]['qty']['#cell_attributes'] = array('class' => array('qty'));
    $table[$i]['total']['#cell_attributes'] = array('class' => array('price'));
  }

  $table[] = array(
    'auto_number' => array(
      'cell' => '',
      '#cell_attributes' => array('class' => 'auto-number'),
      ),
    'title' => array(
      '#prefix' => '<span id="subtotal-title">' . t('Subtotal:') . '</span> ',
      '#cell_attributes' => array(
        'colspan' => '3',
        'class' => 'product-name',
      ),
    ),
    'total' => array(
      '#theme' => 'uc_price',
      '#price' => $subtotal,
    ),
  );

  return $table;
}


/**
 * Implementats hook_form_FORM_ID_alter().
 * Alters the Store Checkout Settings form, adding the 'Skip checkout review' option.
 */
function daisy_ubercart_form_uc_cart_checkout_settings_form_alter(&$form, &$form_state, $form_id) {
  $form['checkout']['uc_optional_checkout_review'] = array(
    '#type' => 'checkbox',
    '#title' => t('Skip checkout review'),
    '#default_value' => variable_get('uc_optional_checkout_review', FALSE),
  );
}

/**
 * Implementats hook_form_FORM_ID_alter().
 * Add additional processing for the checkout form, by removing the review step.
 */
function daisy_ubercart_form_uc_cart_checkout_form_alter(&$form, &$form_state, $form_id) {
  if (variable_get('uc_optional_checkout_review', FALSE)) {
    $form['actions']['continue']['#submit'][] = 'daisy_ubercart_uc_cart_checkout_form_submit';
    $form['actions']['continue']['#value'] = t('Submit order');
  }
}

/**
 * Submit handler for the checkout form.
 * Performs redirect to payment gateway or submits the order directly.
 */
function daisy_ubercart_uc_cart_checkout_form_submit($form, &$form_state) {
  $order = uc_order_load($_SESSION['cart_order']);
  //TODO: Check if this fix for default payment-gateway value doesn't break anything else (?)
  $payment_method = $form_state['values']['panes']['payment']['payment_method'];
  $order->payment_method = $payment_method;
  if (variable_get('uc_optional_checkout_review', FALSE)) {
    if ($order->payment_method == 'dgonline') {
      $dgo_form = drupal_build_form('uc_dgonline_submit_form', $form_state, $order);
      $dgo_url = $dgo_form['#action'] . '?';
      foreach (element_children($dgo_form) as $key) {
        $dgo_url .= urlencode($key) . '=' . urlencode($dgo_form[$key]['#value']) . '&';
      }
      $dgo_url = trim($dgo_url, '&');
      drupal_goto($dgo_url);
    }

    elseif ($order->payment_method == 'paypal_wps') {
      //Builds the data form for the Paypal request.
      $wps_form = drupal_build_form('uc_paypal_wps_form', $form_state, $order);
      $wps_url = $wps_form['#action'] . '?';
      foreach (element_children($wps_form) as $key) {
        $wps_url .= urlencode($key) . '=' . urlencode($wps_form[$key]['#value']) . '&';
      }
      $wps_url = trim($wps_url, '&');
      drupal_goto($wps_url);
    }
    else {
      $form_state['values']['op'] = t('Submit order');
      $form_state['uc_order'] = $order;
      //drupal_form_submit('uc_cart_checkout_review_form', $form_state);
      $form_state['programmed'] = FALSE;
    }
  }
}

/**
 * Helper function to prepare the ubercart orders.
 * @param $order
 * @return void
 */
function daisy_ubercart_prepare_order(&$order) {
  //Reset the order total for recalculation.
  $order->order_total = 0;

  foreach($order->products as $index => $product) {
    //If price was altered inside the product data info, retrieve the altered price and replace the original price.
    if(isset($product->data['price'])) {
      $order->products[$index]->price = $product->data['price'];
    }

    //Merge any selected attributes into the product object.
    if(isset($product->data['selected_type'])) {
      $order->products[$index]->attributes = array_merge($order->products[$index]->attributes, $product->data['selected_attributes']);
    }

    //Update the Type attribute for the product;
    $order->products[$index]->attributes['Type'] = $product->data['Type'];

    //Re-add the product value to the order total.
    $order->order_total += $product->qty * $order->products[$index]->price;
  }

  $order->line_items = uc_order_load_line_items($order);
}

/**
 * Helper function to check if webinar was already bought.
 */
function daisy_ubercart_node_order_status($nid, $uid = NULL) {
  //Attempt to retrieve uid from global user object if missing argument.
  if (is_null($uid)) {
    global $user;
    $uid = $user->uid;
  }
  else {
    $uid = (int)$uid;
  }

  //If nid or uid don't validate, return false.
  if ((boolean)$uid == FALSE || (boolean)$nid == FALSE) {
    return FALSE;
  }

  //Query the database for the forst
  $user_product_order_query = db_select('uc_orders', 'uo');
  $user_product_order_query->join('uc_order_products', 'uop', 'uo.order_id = uop.order_id');
  $user_product_order_query->fields('uo', array('order_id'))
    ->fields('uop', array('data'))
    ->condition('uop.nid', $nid)
    ->condition('uo.uid', $uid)
    ->range(0,1);
  $user_product_order = $user_product_order_query->execute()->fetchAssoc();
  // Return the type of the purchase
  if ($user_product_order) {
    $product_type = unserialize($user_product_order['data']);
    if (isset($product_type['Type']) && is_array($product_type['Type'])) {
      if (array_key_exists(3, $product_type['Type'])) {
        $product_type = 'View';
      }
      elseif (array_key_exists(4, $product_type['Type'])) {
        $product_type = 'Participate';
      }
    }
  }
  return ($user_product_order) ? $product_type : FALSE;
}


/**
 * Helper function to retrieve total count of items + total sum.
 */
function daisy_ubercart_cart_summary_assoc() {
  $subtotal = 0;
  $items = 0;
  foreach (uc_cart_get_contents() as $item) {
    $items += $item->qty;
    $subtotal += $item->price * $item->qty;
  }

  return array(
    'items' => $items,
    'subtotal' => $subtotal
  );
}

/**
 * Act on a cart item before it is about to be created or updated.
 *
 * @param $entity
 *   The cart item entity object.
 */
function daisy_ubercart_uc_cart_item_presave($entity) {
  $entity->changed = REQUEST_TIME;
}

/**
 * Implements hook_uc_order().
 * @param $op
 * @param $order
 * @param $arg2
 * @return void
 */
function daisy_ubercart_uc_order($op, $order, $arg2) {
  switch ($op) {
    case 'new':
    case 'presave':
      daisy_ubercart_prepare_order($order);
      break;
  }
}
