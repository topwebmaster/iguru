<?php

/**
 * @file media_youtube/includes/themes/media-youtube-video.tpl.php
 *
 * Template file for theme('media_youtube_video').
 *
 * Variables available:
 *  $uri - The uri to the YouTube video, such as youtube://v/xsy7x8c9.
 *  $video_id - The unique identifier of the YouTube video.
 *  $width - The width to render.
 *  $height - The height to render.
 *  $autoplay - If TRUE, then start the player automatically when displaying.
 *  $fullscreen - Whether to allow fullscreen playback.
 *
 * Note that we set the width & height of the outer wrapper manually so that
 * the JS will respect that when resizing later.
 */
?>
<object id="<?php print $unique_id; ?>" type="application/x-shockwave-flash" data="/<?php print $uppod_player; ?>" width="<?php print $player_width; ?>" height="<?php print $player_height; ?>">
  <param name="uid" value="<?php print $unique_id; ?>">
  <param name="bgcolor" value="#ffffff" />
  <param name="allowFullScreen" value="true" />
  <param name="allowScriptAccess" value="always" />
  <param name="wmode" value="opaque" />
  <param name="movie" value="/<?php print $uppod_player; ?>" />
  <param name="flashvars" value="<?php print $uppod_options; ?>" />
  <embed src="/<?php print $uppod_player; ?>" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="window" flashvars="<?php print $uppod_options; ?>" width="<?php print $player_width; ?>" height="<?php print $player_height; ?>"></embed>
</object>
