<?php

/**
 * Implements module_HOOK_action_info().
 * Provides an action for cloning caprica products.
 */
function daisy_groups_invite_action_info() {
  $actions = array();

  $actions['daisy_groups_vbo_invite_user'] = array(
    'type' => 'entity',
    'label' => t('Send invite'),
    'configurable' => FALSE,
    'parameters' => array(
      'invited_user_uid' => arg(2)
    ),
  );

  return $actions;
}

/**
 * Implements the clone action for entity item(s).
 */
function daisy_groups_vbo_invite_user($entity, $context) {
  global $user;
  $invited_user = user_load($context['invited_user_uid']);
  daisy_send_notification($invited_user->uid, FALSE, 'invite_to_group', array('uid' => $user->uid,'nid' => $entity->nid));
}
