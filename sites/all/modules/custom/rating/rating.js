(function ($) {
  Drupal.behaviors.rating = {
    attach: function (context, settings) {

      var rating = $('.logged-in .rating li');
      // Bind an AJAX callback to our link
      rating.live('click',function(event) {
        event.preventDefault();
        var prevMark = $(this).parent().children('li.active').size()
        var index = $(this).index()
        $(this).parent().children('li').each(function() {
          if ( $(this).index() <= index ) {
            $(this).addClass('active');
          } else {
            $(this).removeClass('active');
          }
        })

        var id = $(this).parents('.catalog-front').attr('id')
        id = id.substr(2, id.length);
        var mark = $(this).children().val();
        var ajaxUrl = './ratingajax';
        $.ajax({
          type: "POST",
          url: ajaxUrl,
          data: {
            // For server checking
            'nid': id,
            'mark' : mark
          },
          dataType: "json",
          success: function (data) {
            // Display the time from successful response
            if (data.message) {
              var newRate = ( (data.total - data.userRating)*1 +  mark*1) / data.totalCount;
              if (newRate != 'Infinity') {
                $('.catalog-front#wn'+id+' .average-rating').each(function(){
                  $(this).html(newRate);
                  $(this).siblings('ul').children('li').each(function() {
                    if ( $(this).index() < mark ) {
                    $(this).addClass('active');
                  } else {
                    $(this).removeClass('active');
                  }
                  })
                })
              }
            }
          },
          error: function (xmlhttp) {
            // Error alert for failure
            alert('An error occured: ' + xmlhttp.status);
          }
        });
      });
    }
  };

  Drupal.behaviors.colorRating = {
    attach: function(context, settings) {
      $('ul.current-rating li').hover(function(){
        $(this).prevAll().addClass('current-vote');
      },
      function(){
        $(this).prevAll().removeClass('current-vote');
      });
    }
  }
})(jQuery);
