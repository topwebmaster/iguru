<?php

/**
 * Implements hook_views_pre_render().
 */
function daisy_product_views_pre_render(&$view) {
  // Remove Webinar Filters where not needed
  if (isset($view->exposed_widgets) && $view->name == 'cabinet'
    && ($view->current_display == 'block_1' ^ $view->current_display == 'content_block')) {
    unset($view->exposed_widgets);
  }
}

/**
 * Implements hook_views_query_alter().
 */
function daisy_product_views_query_alter(&$view, &$query) {
  // Alter the Query of the View Groups Page
  if ($view->current_display == 'groups_page' && $view->name == 'og_list') {
    // Show User's Groups
    global $user;
    $group_type = isset($_GET['group_type']) ? true : false;
    if ($group_type) {
      $query->add_where('og_membership_og', 'og_membership_og.etid', $user->uid, '=');
    }
  }
}

