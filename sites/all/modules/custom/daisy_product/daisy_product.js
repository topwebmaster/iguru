(function ($) {
  $(document).ready(function () {
    function changeWebinarSubCats() {
      mainSelect = $('#edit-main-category').val();
      $('div[id^=main-filters-secondary-category-]').each(function(i) {
        if ($(this).attr('id') !== 'main-filters-secondary-category-'+mainSelect) {
          $(this).hide();
        }
        else {
          $(this).show();
        }
      });
    }
    // Add Events to Main-Category-Select Input
    // Add the onChange Event
    $("#edit-main-category").change(function(e) {
      return changeWebinarSubCats();
    });
    // Add the onLoad Event
    changeWebinarSubCats();
  });
})(jQuery);
