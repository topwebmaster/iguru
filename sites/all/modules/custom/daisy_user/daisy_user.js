(function ($) {
  /**
   * Toggle Category selection on container click.
   */
  Drupal.behaviors.PopupSelectCategory = {
    attach: function(context) {
      $(document).ready(function() {
        //Hide checkboxes and disable the submit by default.
        $('#popup-message-window #edit-categories .form-type-checkbox input[type=checkbox]')
          .css({'visibility': 'hidden'});

        $('#popup-message-window #edit-categories .form-type-checkbox input[type=checkbox]').each(function(){
          if($(this).attr('checked')){
            $(this).parents('.form-type-checkbox').addClass('selected');
          }
        });

        //Disabled the submit by default if no category is active.
        if($('#edit-categories .selected').size() == 0)
          $('#popup-message-window #edit-continue-button').attr('disabled', 'disabled');

        //Checkbox triggerer
        $('#popup-message-window #edit-categories div.form-type-checkbox').click(function(){
          $(this).toggleClass('selected');
          var categoryCheckbox = $(this).find("input[type=checkbox]");
          if($(this).hasClass('selected')) {
            categoryCheckbox.attr('checked', 'checked');
          }
          else {
            categoryCheckbox.attr('checked', false);
          }

          //Enable the submit if at least 1 category is selected.
          if($('#edit-categories .selected').size() > 0) {
            $('#popup-message-window #edit-continue-button').attr('disabled', false);
          }
          else {
            $('#popup-message-window #edit-continue-button').attr('disabled', 'disabled');
          }
        });

        // Disable the closing and keypress events, making the popup permanent.
        jQuery("#popup-message-close, #popup-message-background").unbind('click');
        jQuery(document).unbind('keypress');
      });
    }
  }
})(jQuery);
