<?php
include_once('daisy_user_field_relationships_status_link.inc');

/**
 * Implements hook_views_data().
 */
function daisy_user_views_data() {
  $data = array();

  $data['daisy_expert_status']['table']['group'] = t('Daisy expert application');

  // Define our relationship with users table.
  $data['daisy_expert_status']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field'      => 'uid',
    ),
  );

  // Fields definition
  $data['daisy_expert_status']['uid'] = array(
    'title' => t('Daisy expert application'),
    'help'  => t('The Expert application status for an user.'),
    'relationship' => array(
      'base'    => 'users',
      'field'   => 'uid',
      'handler' => 'views_handler_relationship',
      'label'   => t('Daisy expert application'),
    ),
  );

  $data['daisy_expert_status']['status'] = array(
    'title' => t('Expert application status'),
    'help'  => t('Status of an application: 0 if not applied, 1 if in process'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label'   => t('Applied'),
      'type'    => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function daisy_user_views_data_alter(&$data) {
  // Custom output for a user's name
  $data['users']['daisy_full_name'] = array(
    'title' => t('Daisy Full Name'),
    'help' => t('Daisynet formatted full user name'),
    'field' => array(
      'field'   => 'uid',
      'handler' => 'views_handler_field_daisy_full_name',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'label' => t('Sort'),
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['user_relationships']['status_link']['field']['handler'] = 'daisy_user_field_relationships_status_link';
}

/**
 * Handler for daisynet full name of the user
 */
class views_handler_field_daisy_full_name extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);
    $account = user_load($value);
    return daisy_user_get_username($account);
  }
}

