/**
 * Create a degradeable star rating interface out of a simple form structure.
 */
(function($){ // Create local scope.

Drupal.ajax.prototype.commands.fivestarUpdate = function (ajax, response, status) {
  response.selector = $('.fivestar-form-item', ajax.element.form);
  //alert(response.selector.html)
  //alert(ajax.element.form.id)
  ajax.commands.insert(ajax, response, status);
};

})(jQuery);
