<?php

/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @category  PayIntelligent
 * @package   PayIntelligent_ClickandBuy
 * @copyright (C) 2010 PayIntelligent GmbH  <http://www.payintelligent.de/>
 * @license   http://www.gnu.org/licenses/  GNU General Public License 3
 */
/**
 * Set here static constants for ClickandBuy Library
 */

class pi_clickandbuy_constants
{

    public static $BUSINESS_ORIGIN_ID_SANDBOX = 'ubercart_v2.4';
    public static $BUSINESS_ORIGIN_ID = 'ubercart_v2.4';
    public static $SOAP_ENDPOINT_SANDBOX = 'https://api.clickandbuy-s1.com/webservices/soap/pay_1_1_0';//-d2 für merchant registration test
    public static $SOAP_ENDPOINT = 'https://api.clickandbuy.com/webservices/soap/pay_1_1_0';
    public static $SOAP_NAMESPACE = "http://api.clickandbuy.com/webservices/pay_1_1_0/\" xmlns=\"http://api.clickandbuy.com/webservices/pay_1_1_0/";
    public static $SOAP_ACTION = 'http://api.clickandbuy.com/webservices/pay_1_1_0/';
    public static $MMS_LOG = true;
    public static $MMS_EMAIL_TO = 'tommidog@web.de';
    public static $MMS_EMAIL_FROM = 'thomas.gobelet@payintelligent.de';
    public static $NUSOAP_FOLDER = 'lib/';
    public static $CANCEL_MODE_TX = 'TX';
    public static $CANCEL_MODE_RPA = 'RPA';
    public static $CANCEL_MODE_BOTH = 'BOTH';
    public static $SHOP_TYPE = 'Ubercart';
}
?>