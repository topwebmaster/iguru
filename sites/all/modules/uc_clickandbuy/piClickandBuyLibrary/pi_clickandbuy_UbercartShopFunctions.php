<?php

/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @category  PayIntelligent
 * @package   PayIntelligent_ClickandBuy
 * @copyright (C) 2010 PayIntelligent GmbH  <http://www.payintelligent.de/>
 * @license   http://www.gnu.org/licenses/  GNU General Public License 3
 */

require_once('pi_clickandbuy_ShopFunctionsInterface.php');
require_once('pi_clickandbuy_constants.php');

class pi_clickandbuy_UbercartShopFunctions implements pi_clickandbuy_ShopFunctionsInterface
{

    public function getSoapEndpoint()
    {
       include_once '../../../../../../sites/default/settings.php';
       define('DRUPAL_ROOT', '../../../../../../');
       $variables = array_map('unserialize', db_query('SELECT name, value FROM {variable}')->fetchAllKeyed());
       if($variables['uc_clickandbuy_sandbox']){
         return pi_clickandbuy_constants::$SOAP_ENDPOINT_SANDBOX; 
       }
       else{
         return pi_clickandbuy_constants::$SOAP_ENDPOINT;
       }
    }

    /**
     * Returns 'de' if prestashop is set to german, otherwise returns 'en'
     *
     * @todo get's active shoplanguage, not the language set by admin for the admin view
     * @return string language code 'de' or 'en'
     */
    public function getLanguage()
    {
        include_once '../../../../../../includes/database/database.inc';
        include_once '../../../../../../sites/default/settings.php';
        define('DRUPAL_ROOT', '../../../../../../');
        $variables = array_map('unserialize', db_query('SELECT name, value FROM {variable}')->fetchAllKeyed());
        return $variables['language_default']->language;
    }

    /**
     * Update Shop Settings with new merchant id and shared secret.
     *
     * @param string $merchantId The Merchant ID
     * @param string $sharedSecret The Shared Secret
     */
    public function updateMerchantData($merchantId, $sharedSecretProject, $sharedSecretMms, $projectId, $registrationSharedSecret)
    {
        
        $merchantIdLength=strlen($merchantId);
        $registrationSharedSecretLength = strlen($registrationSharedSecret);
        $sharedSecretProjectLength=strlen($sharedSecretProject);
        $sharedSecretMmsLength=strlen($sharedSecretMms);
        $projectIdLength=strlen($projectId);
        db_delete('variable')
          ->condition('name', 'uc_clickandbuy_registration_shared_secret')
          ->execute();
        db_insert('variable')
            ->fields(array(
                'name' => 'uc_clickandbuy_registration_shared_secret',
                'value' => 's:'.$registrationSharedSecretLength.':"'.$registrationSharedSecret.'";',
            ))->execute();
        $variables = array_map('unserialize', db_query('SELECT name, value FROM {variable}')->fetchAllKeyed());
        if(!$variables['uc_clickandbuy_merchant_id']){
            db_query("UPDATE {variable} SET value = :value WHERE name =:name", array(':value' => 's:'.$merchantIdLength.':"'.$merchantId.'";', ':name' => 'uc_clickandbuy_merchant_id'));
            db_query("UPDATE {variable} SET value = :value WHERE name =:name", array(':value' => 's:'.$sharedSecretProjectLength.':"'.$sharedSecretProject.'";', ':name' => 'uc_clickandbuy_secret_key'));
            db_query("UPDATE {variable} SET value = :value WHERE name =:name", array(':value' => 's:'.$sharedSecretMmsLength.':"'.$sharedSecretMms.'";', ':name' => 'uc_clickandbuy_secret_key_mms'));
            db_query("UPDATE {variable} SET value = :value WHERE name =:name", array(':value' => 's:'.$projectIdLength.':"'.$projectId.'";', ':name' => 'uc_clickandbuy_project_id'));
        }
        if(!$variables['uc_clickandbuy_recurring_merchant_id']){
            db_query("UPDATE {variable} SET value = :value WHERE name =:name", array(':value' => 's:'.$merchantIdLength.':"'.$merchantId.'";', ':name' => 'uc_clickandbuy_recurring_merchant_id'));
            db_query("UPDATE {variable} SET value = :value WHERE name =:name", array(':value' => 's:'.$sharedSecretProjectLength.':"'.$sharedSecretProject.'";', ':name' => 'uc_clickandbuy_recurring_secret_key'));
            db_query("UPDATE {variable} SET value = :value WHERE name =:name", array(':value' => 's:'.$sharedSecretMmsLength.':"'.$sharedSecretMms.'";', ':name' => 'uc_clickandbuy_recurring_secret_key_mms'));
            db_query("UPDATE {variable} SET value = :value WHERE name =:name", array(':value' => 's:'.$projectIdLength.':"'.$projectId.'";', ':name' => 'uc_clickandbuy_recurring_project_id'));    
        }
    }

    /**
     * Get merchant data from db.
     *
     * @return array merchantID and secretKey from db (picab_settings)
     */
    public function getMerchantData()
    {
        include_once '../../../../../../includes/database/database.inc';
        include_once '../../../../../../sites/default/settings.php';
        define('DRUPAL_ROOT', '../../../../../../');
        $variables = array_map('unserialize', db_query('SELECT name, value FROM {variable}')->fetchAllKeyed());
        $result['merchantID'] = $variables['uc_clickandbuy_merchant_id'];
        $result['secretKey'] = $variables['uc_clickandbuy_secret_key'];
        return $result;
    }

    public function creditEventList($creditEventList)
    {

    }

    public function doEventAction()
    {

    }

    public function payEventList($payEventList)
    {

    }

    public function recurringPaymentAuthorizationEventList($recurringEventList)
    {

    }

    public function refundEventList($refundEventList)
    {

    }

    public function setXml($xmlString)
    {

    }

}

?>