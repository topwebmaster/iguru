<?php

/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @category  PayIntelligent
 * @package   PayIntelligent_ClickandBuy
 * @copyright (C) 2010 PayIntelligent GmbH  <http://www.payintelligent.de/>
 * @license   http://www.gnu.org/licenses/  GNU General Public License 3
 */
/*
 * @todo boilerplate for shop specific functions
 */

interface pi_clickandbuy_ShopFunctionsInterface
{

    /**
     * @todo implement here shop specific code
     * @return string returns soap endpoint defaults to sandbox
     */
    public function getSoapEndpoint();

    public function getLanguage();

    public function updateMerchantData($merchantId, $sharedSecretProject, $sharedSecretMms, $projectId, $registrationSharedSecret);

    public function getMerchantData();
    
    public function setXml($xmlString);
    
    /**
     * Execute the xml events
     */
    public function doEventAction();
    
    /**
     * Insert pay event list into mms
     * 
     * @param array $payEventList
     */
    public function payEventList($payEventList);

    /**
     * Insert refund event list into mms
     * 
     * @param array $refundEventList
     */
    public function refundEventList($refundEventList);

    /**
     * Insert credit event list into mms
     * 
     * @param array $creditEventList
     */
    public function creditEventList($creditEventList);

    /**
     * Insert recurring payment into mms
     * 
     * @param array $recurringEventList
     */
    public function recurringPaymentAuthorizationEventList($recurringEventList);
}

?>