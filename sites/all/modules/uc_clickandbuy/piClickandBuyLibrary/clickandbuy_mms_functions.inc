<?php 
/**
 * ClickandBuy Sample PHP Script 
 * Code by PayIntelligent GmbH  <http://www.payintelligent.de/>
 * Sponsored by ClickandBuy <http://www.clickandbuy.com/>
 */

function payEventList($payEventList,$signature) {				
	global $xml;

	if (!is_array($payEventList[0])) {
		$payEventList = array($payEventList);
	}
					
	foreach($payEventList as $key => $value) {

		$merchantID = $value['merchantID'];
		$projectID = $value['projectID'];
		$eventID = $value['eventID'];
		$creationDateTime = $value['creationDateTime'];
		$transactionID = $value['transactionID'];
		$externalID = $value['externalID'];
		$crn = $value['crn'];
		$transactionAmount = $value['transactionAmount']['amount'];
		$transactionCurrency = $value['transactionAmount']['currency'];
		$merchantAmount = $value['merchantAmount']['amount'];
		$merchantCurrency = $value['merchantAmount']['currency'];
		$oldState = $value['oldState'];
		$newState = $value['newState'];

		$count = getCountEventID($eventID);			
		if(empty($count) || ($count < 1)) {					
                    $shopOrderID = 0;			

                    $shopOrderIDOrder = isOrder($externalID);
                    $shopOrderIDTransaction = isTransaction($externalID);

                    if(!empty($shopOrderIDTransaction)) {
                            $shopOrderID = $shopOrderIDTransaction;					
                            updateTransactionStatus($externalID,$newState);								
                    } else {
                            $shopOrderID = $shopOrderIDOrder;									
                    }					

                    $serializeArray = array();
                    $serializeArray['eventlist']['signature'] = $signature;
                    $serializeArray['eventlist']['payEvent'] = $value;
                    $eventXML = XML_serialize($serializeArray);		

                    insert_mms($eventID,$shopOrderID,$externalID,$transactionID,$oldState,$newState,$eventXML);											
		}		
	}
}
	
function refundEventList($refundEventList,$signature) {
	global $xml;
	
	if (!is_array($refundEventList[0])) {
		$refundEventList = array($refundEventList);
	}
	
	foreach($refundEventList as $key => $value) {
		$merchantID = $value['merchantID'];
		$projectID = $value['projectID'];
		$eventID = $value['eventID'];
		$creationDateTime = $value['creationDateTime'];
		$refundTransactionID = $value['refundTransactionID'];
		$externalID = $value['externalID'];
		$associatedTransactionID = $value['associatedTransactionID'];
		$crn = $value['crn'];
		$transactionAmount = $value['transactionAmount']['amount'];
		$transactionCurrency = $value['transactionAmount']['currency'];
		$merchantAmount = $value['merchantAmount']['amount'];
		$merchantCurrency = $value['merchantAmount']['currency'];
		$oldState = $value['oldState'];
		$newState = $value['newState'];
	
		$count = getCountEventID($eventID);			
		if(empty($count) || ($count < 1)) {					
			$shopOrderID = 0;			
                        $shopOrderID = db_query("SELECT uc_order_id FROM {uc_payment_clickandbuy} WHERE transaction_id = :transactionID",array(':transactionID' =>$refundTransactionID))->fetchField();
												
			if(!empty($shopOrderID)) {
                                db_query("UPDATE {uc_payment_clickandbuy_mms} SET old_state = :old_state, new_state = :new_state WHERE transaction_id =:transactionID", array(':old_state' => $oldState, ':new_state' => $newState, 'transactionID' => $refundTransactionID));
			} 								

			$serializeArray = array();
			$serializeArray['eventlist']['signature'] = $signature;
			$serializeArray['eventlist']['refundEvent'] = $value;
			$eventXML = XML_serialize($serializeArray);		
			
			insert_mms($eventID,$shopOrderID,$externalID,$refundTransactionID,$oldState,$newState,$eventXML);											
		}		
	}						
}
	
function creditEventList($creditEventList,$signature) {
	global $xml;
	
	if (!is_array($creditEventList[0])) {
		$creditEventList = array($creditEventList);
	}
	
	foreach($creditEventList as $key => $value) {
		$merchantID = $value['merchantID'];
		$projectID = $value['projectID'];
		$eventID = $value['eventID'];
		$creationDateTime = $value['creationDateTime'];
		$transactionID = $value['transactionID'];
		$externalID = $value['externalID'];
		$email = $value['email'];
		$crn = $value['crn'];
		$transactionAmount = $value['transactionAmount']['amount'];
		$transactionCurrency = $value['transactionAmount']['currency'];
		$merchantAmount = $value['merchantAmount']['amount'];
		$merchantCurrency = $value['merchantAmount']['currency'];
		$oldState = $value['oldState'];
		$newState = $value['newState'];
	
		$count = getCountEventID($eventID);			
		if(empty($count) || ($count < 1)) {					
			$shopOrderID = 0;			
		        $shopOrderID = db_query("SELECT uc_order_id FROM {uc_payment_clickandbuy} WHERE transaction_id = :transactionID",array(':transactionID' =>$transactionID))->fetchField();

												
			if(!empty($shopOrderID)) {
                                db_query("UPDATE {uc_payment_clickandbuy_mms} SET old_state = :old_state, new_state = :new_state WHERE transaction_id =:transactionID", array(':old_state' => $oldState, ':new_state' => $newState, 'transactionID' => $transactionID));
			} 							

			$serializeArray = array();
			$serializeArray['eventlist']['signature'] = $signature;
			$serializeArray['eventlist']['creditEvent'] = $value;
			$eventXML = XML_serialize($serializeArray);	
											
			insert_mms($eventID,$shopOrderID,$externalID,$transactionID,$oldState,$newState,$eventXML);											
		}		
	}			
}
	
function recurringPaymentAuthorizationEventList($recurringEventList,$signature) {
	if (!is_array($recurringEventList[0])) {
		$recurringEventList = array($recurringEventList);
	}
	
	foreach($recurringEventList as $key => $value) {
		$merchantID = $value['merchantID'];
		$projectID = $value['projectID'];
		$eventID = $value['eventID'];
		$creationDateTime = $value['creationDateTime'];
		$transactionID = $value['transactionID'];
		$externalID = $value['externalID'];
		$crn = $value['crn'];
		$amount = $value['amount']['amount'];
		$currency = $value['amount']['currency'];
		$oldState = $value['oldState'];
		$newState = $value['newState'];
		$remainingAmount = $value['remainingAmount']['amount'];
		$remainingCurrency = $value['remainingAmount']['currency'];
		$remainingRetries = $value['remainingRetries'];
		$validUntil = $value['validUntil'];

		$shopOrderIDOrder = isOrder($externalID);
		$shopOrderIDTransaction = isTransaction($externalID);
	
		if(!empty($shopOrderIDTransaction)) {
			$shopOrderID = $shopOrderIDTransaction;					
		}		
	
		$serializeArray = array();
		$serializeArray['eventlist']['signature'] = $signature;
		$serializeArray['eventlist']['recurringPaymentAuthorizationEvent'] = $value;
		$eventXML = XML_serialize($serializeArray);	
				
		insert_mms($eventID,$shopOrderID,$externalID,$transactionID,$oldState,$newState,$eventXML);											
	}
}


function insert_mms($eventID,$shopOrderID,$externalID,$transactionID,$oldState,$newState,$xml) {
        if($shopOrderID=="")$shopOrderID=0;
        db_insert('uc_payment_clickandbuy_mms')
            ->fields(array(
                'event_id' => $eventID,
                'uc_order_id' => $shopOrderID,
                'external_id' => $externalID,
                'transaction_id' => $transactionID,
                'old_state' => $oldState,
                'new_state' => $newState,
                'xml' => $xml,
                'created' => date('Y-m-d H:i:s'),
            ))->execute();			
}	
		
function isOrder($externalID){
        $shopOrderID = db_query("SELECT uc_order_id FROM {uc_payment_clickandbuy} WHERE external_id = :externalID",array(':externalID' =>$externalID))->fetchField();	
	return $shopOrderID;
} 	

function isTransaction($externalID){ 
        $shopOrderID = db_query("SELECT uc_order_id FROM {uc_payment_clickandbuy} WHERE external_id = :externalID",array(':externalID' =>$externalID))->fetchField();	
	return $shopOrderID;
} 	
	
function getCountEventID($eventID) {	
	$Events = db_query("SELECT * FROM {uc_payment_clickandbuy} WHERE external_id = :externalID",array(':externalID' =>$externalID))->fetchField();		
	$i = 0;
	
	foreach ($Events as $event) {
		$i++;
	}
	
	return $i;
		
}	
	
function updateTransactionStatus($externalID,$status){
	db_query("UPDATE {uc_payment_clickandbuy} SET transaction_status = :transaction_status WHERE external_id =:externalID", array(':transaction_status' => $status, ':externalID' => $externalID)); 
}


function getMmsSecretKey($recurring = false) {	
	if($recurring) {		
		$value_query = @Configuration::get('CLICKANDBUY_SECRET_KEY_MMS');  
	} else {		
		$value_query = @Configuration::get('CLICKANDBUY_SECRET_KEY_MMS'); 
	}	
	
	return $value_query; 	
}	
	
function checkSignature($xml,$recurring = false) {
        return true;
	$sharedKey = ($recurring);

	// find signature
	$signatureTag = "<signature>";
	$signatureTagStart = strpos($xml, $signatureTag);
	$signatureTagLength = strlen($signatureTag);

	$signatureStart = $signatureTagStart + $signatureTagLength;
	$signatureLength = 40;
	$signature = substr($xml,$signatureStart,$signatureLength);

	//singature from xml
	$xmlSignature = "<signature>".$signature."</signature>";	
	$emptySignature = "<signature />";
	$xmlWithoutSignature = str_replace($xmlSignature, $emptySignature, $xml);

	//Hash 
	$textToHash = $sharedKey.$xmlWithoutSignature;
	$hash = sha1 ($textToHash);
				
	($signature == $hash) ?  $result = true : $result = false;
	
		
	return $result;
}


?>