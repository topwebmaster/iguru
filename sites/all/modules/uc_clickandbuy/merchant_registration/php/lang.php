<?php
require_once '../../../../../../../includes/database/database.inc';
require_once '../../../../../../../sites/default/settings.php';
define('DRUPAL_ROOT', '../../../../../../../');

$variables = array_map('unserialize', db_query('SELECT name, value FROM {variable}')->fetchAllKeyed());
if (isset($variables['language_default']->language) && $variables['language_default']->language == 'de') {
    echo 'de';
} else {
    echo 'en';
}
?>