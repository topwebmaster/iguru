<?php
/*
 * Success site of the embedded merchant registration
 */
include_once '../../../../../../../includes/database/database.inc';
include_once '../../../../../../../sites/default/settings.php';
define('DRUPAL_ROOT', '../../../../../../..');
$variables = array_map('unserialize', db_query('SELECT name, value FROM {variable}')->fetchAllKeyed());
require_once '../../piClickandBuyLibrary/pi_clickandbuy_functions.php';
$clickandbuyFunctions = new pi_clickandbuy_functions();

if (isset($variables['language_default']->language) && $variables['language_default']->language == 'de') {
    $successSiteHeadline = 'ClickandBuy Anbieterregistrierung erfolgreich abgeschlossen!';
    $noResponse           = 'Keine Antwort vom Server!';
    $error                 = 'Es ist ein Fehler aufgetreten!';
    $success               = 'Ihre ClickandBuy Konfigurationseinstellungen wurden automatisch in Ihren Shop &uuml;bertragen.';
    $close                 = 'Schlie&szlig;en';
} else {
    $successSiteHeadline = 'ClickandBuy merchant registration successful completed!';
    $noResponse           = 'No response from the server!';
    $error                 = 'There is an error!';
    $success               = 'Your ClickandBuy configuration settings are automatically stored into your shop.';
    $close                 = 'Close';
}

$token = $clickandbuyFunctions->generateMerchantRegistrationToken(pi_clickandbuy_constants::$BUSINESS_ORIGIN_ID,
        $variables['uc_clickandbuy_merchant_id'],
        $variables['uc_clickandbuy_registration_shared_secret']
);

$details = array(
    'businessOriginID' => pi_clickandbuy_constants::$BUSINESS_ORIGIN_ID,
    'merchantID' => $variables['uc_clickandbuy_merchant_id'],
    'token' => $token
);

$requestArray = array(
    'details' => $details
);
$soapObject = $clickandbuyFunctions->getMerchantResponse($requestArray, 'getMerchantRegistrationStatus_Request');
$responseArray = $clickandbuyFunctions->getMerchantRegistrationResponseData($soapObject);

?>
<div id="piCabRegistrationFirstStep"  class="piCabLeft">
<?php require_once('image.inc.php'); ?>
    <div style="text-align:center;">
    <?php if (!empty($responseArray['success']) && $responseArray['success'] == true) : ?>
        <h2><?php echo $successSiteHeadline; ?></h2>
        <p><?php echo $success; ?></p>
    <?php endif; ?>
        
    <?php if (empty($responseArray)) : ?>
        <h2 class="piCabCenter"><?php echo $noResponse; ?></h2>
    <?php endif; ?>
        
    <?php if (empty($responseArray['success'])) : ?>
        <h2 class="piCabCenter"><?php echo $error; ?></h2>
        <div class="piCabErrorBox"><?php echo $responseArray['description']; ?></div>
    <?php endif; ?>
        <div class="piCabCenter"><a href="#" onclick="toggleWrapper('piCabEmbeddedRegistration')"><?php echo $close; ?></a></div>
    </div>
</div>