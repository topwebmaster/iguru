<?php

/*
 * Call script for the embedded merchant registration
 */

require_once '../../../../../../../includes/database/database.inc';
require_once '../../../../../../../sites/default/settings.php';
define('DRUPAL_ROOT', '../../../../../../../');
require_once '../../piClickandBuyLibrary/pi_clickandbuy_functions.php';
$clickandbuyFunctions = new pi_clickandbuy_functions();
$variables = array_map('unserialize', db_query('SELECT name, value FROM {variable}')->fetchAllKeyed());

$url = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://'
        . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF'])
        . '/../../../../../../../admin/store/settings/payment/method/clickandbuy?success=true';
$registrationData = array(
    'businessOriginID' => pi_clickandbuy_constants::$BUSINESS_ORIGIN_ID,
    'returnURL' => $url
);
$registrationData = $clickandbuyFunctions->removeEmptyTag($registrationData);
$StreetParts = preg_split('~([^\d]*) (.*)~' ,$variables['address']['uc_store_street1'], -1 ,PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

$companyAddressData = array(
    'street'            => $StreetParts[0],
    'houseNumber'       => $StreetParts[1],
    'houseNumberSuffix' => '',
    'zip'               => $variables['address']['uc_store_postal_code'],
    'city'              => $variables['address']['uc_store_city'],
    'country'           => $variables['site_default_country'],
    'state'             => '',
    'addressSuffix'     => ''
);
$companyAddressData = $clickandbuyFunctions->removeEmptyTag($companyAddressData);

$telephoneNumber = array(
    'countryCode' => '',
    'phoneNumber' => ''
);
$telephoneNumber = $clickandbuyFunctions->removeEmptyTag($telephoneNumber);

$merchantData = array(
    'companyName' => $variables['uc_store_name'],
    'vatID' => '',
    'countryOfIncorporation' => '',
    'dateOfIncorporation' => '',
    'companyAddress' => $companyAddressData,
    'companyType' => '',
    'emailAddress' =>  $variables['uc_store_email'],
    'website' => str_replace("/clickandbuy/upa/mms", "", $variables['uc_clickandbuy_mms_push_url']),
    'timeZone' => '',
    'adminFirstName' => '',
    'adminMiddleName' => '',
    'adminLastName' => '',
    'adminGender' => '',
    'telephoneNumber' => $telephoneNumber,
    'language' => ''
);
$merchantData = $clickandbuyFunctions->removeEmptyTag($merchantData);

$settlementData = array(
    'currency'      => 'EUR',
    'name'          => '',
    'categoryID'    => '3052'
);
$settlementData = $clickandbuyFunctions->removeEmptyTag($settlementData);

$averageTicketSize = array(
    'amount'    => 50,
    'currency'  => 'EUR'
);

$feeCardData = array(
    'invoicingCycle' => 14,
    'settlementDelay' => 5,
    'averageTicketSize' => $averageTicketSize
);
$feeCardData = $clickandbuyFunctions->removeEmptyTag($feeCardData);

$projectData = array(
    'name' => '',
    'mmsURL' => $variables['uc_clickandbuy_mms_push_url']
);
$projectData = $clickandbuyFunctions->removeEmptyTag($projectData);

$integrationData = array(
    'settlementData' => $settlementData,
    'feeCardData' => $feeCardData,
    'projectData' => $projectData
);
$integrationData = $clickandbuyFunctions->removeEmptyTag($integrationData);

$details = array(
    'registrationData' => $registrationData,
    'merchantData' => $merchantData,
    'integrationData' => $integrationData
);
$details = $clickandbuyFunctions->removeEmptyTag($details);

$requestArray = array(
    'details' => $details
);
$soapObject = $clickandbuyFunctions->getMerchantResponse($requestArray, 'createMerchantRegistration_Request');
$responseData = $clickandbuyFunctions->getMerchantRegistrationResponseData($soapObject);
echo json_encode($responseData);
?>