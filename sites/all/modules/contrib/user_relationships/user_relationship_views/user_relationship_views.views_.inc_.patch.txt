--- user_relationships/user_relationship_views/user_relationship_views.views.inc	2011-09-25 14:46:26.000000000 +0300
+++ user_relationships/user_relationship_views/user_relationship_views.views.inc	2011-12-20 15:59:26.819686400 +0200
@@ -86,7 +86,8 @@
       ),
       'relationship' => array(// this relationship brings in all fields the users table offers
         'base' => 'users',
-        'field' => 'uid',
+        'left_field' => 'uid',
+        'field' => 'requester_id',
         'label' => 'requester',
         'help' => t('Bring in data about the user who started the relationship, or link to relationships that they have established'),
       ),
@@ -98,6 +99,13 @@
       'filter' => array(
         'handler' => 'views_handler_filter_user_current'
       ),
+      'relationship' => array(// this relationship brings in all fields the users table offers
+        'base' => 'users',
+        'left_field' => 'uid',
+        'field' => 'requester_id',
+        'label' => 'requester',
+        'help' => t('Bring in data about the user who started the relationship, or link to relationships that they have established'),
+      ),
     ),
     'requestee_id' => array(
       'title' => t('Requestee user'),
@@ -117,7 +125,8 @@
       ),
       'relationship' => array(// this relationship brings in all fields the users table offers
         'base' => 'users',
-        'field' => 'uid',
+        'left_field' => 'uid',
+        'field' => 'requestee_id',
         'label' => 'requestee',
         'help' => t('Bring in data about the user who accepted the relationship, or link to relationships that other users have established to this one'),
       ),
@@ -129,6 +138,13 @@
       'filter' => array(
         'handler' => 'views_handler_filter_user_current'
       ),
+      'relationship' => array(// this relationship brings in all fields the users table offers
+        'base' => 'users',
+        'left_field' => 'uid',
+        'field' => 'requestee_id',
+        'label' => 'requestee',
+        'help' => t('Bring in data about the user who accepted the relationship, or link to relationships that other users have established to this one'),
+      ),
     ),
     'approved' => array(
       'title' => t('Relationship status'),
