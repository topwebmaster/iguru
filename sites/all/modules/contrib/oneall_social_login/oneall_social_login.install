<?php
/**
 * @file
 * On Install: creates the tables required by the OneAll Social Login module
 * On UnInstall: removes the previously created tables
 */

/**
 * @package   	OneAll Social Login
 * @copyright 	Copyright 2012 http://www.oneall.com - All rights reserved.
 * @license   	GNU/GPL 2 or later
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,USA.
 *
 * The "GNU General Public License" (GPL) is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 */

/**
 * Database schema
 */
function oneall_social_login_schema() {

  // Used to link social network profiles to authmap entries.
  $schema ['oneall_social_login_identities'] = array(
    'description' => 'Used to link social network profiles to authmap entries.',
    'fields' => array(
      'oasliid' => array(
        'description' => 'The primary key of this table.',
        'type' => 'serial',
        'length' => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'aid' => array(
        'description' => 'Foreign key to link entries of this table to entries in the authmap table.',
        'type' => 'int',
        'length' => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'identity_token' => array(
        'description' => 'Unique identity_token for a social network profile, returned by the OneAll API.',
        'type' => 'varchar',
        'length' => 48,
        'not null' => TRUE,
      ),
      'provider_name' => array(
        'description' => 'Social network provider (Facebook, Twitter ...) of this identity.',
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
      ),
    ),
    'indexes' => array(
      'identity_token' => array(
        'identity_token',
      ),
      'aid' => array(
        'aid',
      ),
    ),
    'foreign keys' => array(
      'aid' => array(
        'table' => 'authmap',
        'columns' => array(
          'aid' => 'aid',
        ),
      ),
    ),
    'primary key' => array(
      'oasliid',
    ),
  );

  // Used to store module settings.
  $schema ['oneall_social_login_settings'] = array(
    'description' => 'Used to store module settings.',
    'fields' => array(
      'oaslsid' => array(
        'description' => 'The primary key of this table.',
        'type' => 'serial',
        'length' => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'setting' => array(
        'description' => 'The name of the setting.',
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
      ),
      'value' => array(
        'description' => 'The value of this setting.',
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
      ),
    ),
    'indexes' => array(
      'setting' => array(
        'setting',
      ),
    ),
    'primary key' => array(
      'oaslsid',
    ),
  );

  return $schema;
}

/**
 * Install: create the required tables
 */
function oneall_social_login_install() {

  // Start transaction.
  $db_transaction = db_transaction();

  // Install.
  try {
    drupal_install_schema('oneall_social_login_identities');
    drupal_install_schema('oneall_social_login_settings');
  }
  catch (Exception $e) {
    $db_transaction->rollback();
  }
}

/**
 * UnInstall: remove the module tables and cleanup module entries
 */
function oneall_social_login_uninstall() {

  // Drop tables.
  drupal_uninstall_schema('oneall_social_login_identities');
  drupal_uninstall_schema('oneall_social_login_settings');

  // Remove entries that module might have created.
  db_delete('authmap')->condition('module', 'oneall_social_login')->execute();
}
