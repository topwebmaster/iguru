<?php


define('_MEMBER_ACCESS_ALL','All members in system');
define('_MEMBER_ACCESS_CONTACTS','Only member I am assigned to as a contact');
define('_MEMBER_ACCESS_TASKS','Only members I am assigned to in a job');

class module_member extends module_base{

	public $links;
	public $member_types;
    public $member_id;

    public static function can_i($actions,$name=false,$category=false,$module=false){
        if(!$module)$module=__CLASS__;
        return parent::can_i($actions,$name,$category,$module);
    }
	public static function get_class() {
        return __CLASS__;
    }
    public function init(){
		$this->links = array();
		$this->member_types = array();
		$this->module_name = "member";
		$this->module_position = 20.1;
        $this->version = 2.1;

	}

    public function pre_menu(){

        if($this->can_i('view','Members')){

            // how many members are there?
            $link_name = _l('Members');
            if(module_config::c('member_show_summary',1)){
                $member_count = module_cache::time_get('member_menu_count');
                if($member_count===false){
                    $sql = "SELECT COUNT(member_id) AS c FROM `"._DB_PREFIX."member` m";
                    $res = query($sql);
                    $member_count = mysql_num_rows($res);
                    module_cache::time_save('member_menu_count',$member_count);
                }
                if($member_count>0){
                    $link_name .= " <span class='menu_label'>".$member_count."</span> ";
                }
            }

            $this->links['members'] = array(
                "name"=>$link_name,
                "p"=>"member_admin_list",
                "args"=>array('member_id'=>false),
            );
        }
    }

    /** static stuff */

    
     public static function link_generate($member_id=false,$options=array(),$link_options=array()){
        // we accept link options from a bubbled link call.
        // so we have to prepent our options to the start of the link_options array incase
        // anything bubbled up to this method.
        // build our options into the $options variable and array_unshift this onto the link_options at the end.
        $key = 'member_id'; // the key we look for in data arrays, on in _REQUEST variables. for sub link building.

        // we check if we're bubbling from a sub link, and find the item id from a sub link
        if(${$key} === false && $link_options){
            foreach($link_options as $link_option){
                if(isset($link_option['data']) && isset($link_option['data'][$key])){
                    ${$key} = $link_option['data'][$key];
                    break;
                }
            }
            if(!${$key} && isset($_REQUEST[$key])){
                ${$key} = $_REQUEST[$key];
            }
        }
        // grab the data for this particular link, so that any parent bubbled link_generate() methods
        // can access data from a sub item (eg: an id)

        if(isset($options['full']) && $options['full']){
            // only hit database if we need to print a full link with the name in it.
            if(!isset($options['data']) || !$options['data']){
                $data = self::get_member($member_id);
                $options['data'] = $data;
            }else{
                $data = $options['data'];
            }
            // what text should we display in this link?
            $options['text'] = $data['first_name'] . ' '.$data['last_name'];
        }
        $options['text'] = isset($options['text']) ? htmlspecialchars($options['text']) : '';
        // generate the arguments for this link
        $options['arguments'] = array(
            'member_id' => $member_id,
        );
        // generate the path (module & page) for this link
        $options['page'] = 'member_admin_' . (($member_id||$member_id=='new') ? 'open' : 'list');
        $options['module'] = 'member';

        // append this to our link options array, which is eventually passed to the
        // global link generate function which takes all these arguments and builds a link out of them.

         if(!self::can_i('view','Members')){
            if(!isset($options['full']) || !$options['full']){
                return '#';
            }else{
                return isset($options['text']) ? $options['text'] : 'N/A';
            }
        }

        // optionally bubble this link up to a parent link_generate() method, so we can nest modules easily
        // change this variable to the one we are going to bubble up to:
     $bubble_to_module = false;
        /*$bubble_to_module = array(
            'module' => 'people',
            'argument' => 'people_id',
        );*/
        array_unshift($link_options,$options);
        if($bubble_to_module){
            global $plugins;
            return $plugins[$bubble_to_module['module']]->link_generate(false,array(),$link_options);
        }else{
            // return the link as-is, no more bubbling or anything.
            // pass this off to the global link_generate() function
            return link_generate($link_options);
        }
    }


	public static function link_open($member_id,$full=false,$data=array()){
		return self::link_generate($member_id,array('full'=>$full,'data'=>$data));
	}



	public static function get_members($search=array()){
		return get_multiple("member",$search,"member_id","fuzzy","first_name");
	}

	public static function get_member($member_id){
        $member_id = (int)$member_id;
        $member = false;
        if($member_id>0){
            $member = get_single("member","member_id",$member_id);

        }
        if(!$member){
            $member = array(
                'member_id' => 'new',
                'first_name' => '',
                'last_name' => '',
                'business' => '',
                'email' => '',
                'phone' => '',
                'mobile' => '',
            );
        }
		return $member;
	}


    /**
     * @static
     * @param $member_id
     * @return array
     *
     * return a member recipient ready for sending a newsletter based on the member id.
     *
     */
    public static function get_newsletter_recipient($member_id) {
        $member = self::get_member($member_id);
        $member['company_name'] = $member['business'];
        // some other details the newsletter system might need.
        $member['_edit_link'] = self::link_open($member_id,false,$member);
        return $member;
    }

    /** methods  */

    
	public function process(){
		if(isset($_REQUEST['butt_del']) && $_REQUEST['butt_del'] && $_REQUEST['member_id']){
			$data = self::get_member($_REQUEST['member_id']);
            if(module_form::confirm_delete('member_id',"Really delete member: ".$data['member_name'],self::link_open($_REQUEST['member_id']))){
                $this->delete_member($_REQUEST['member_id']);
                set_message("Member deleted successfully");
                redirect_browser(self::link_open(false));
            }
		}else if("save_member" == $_REQUEST['_process']){
			$member_id = $this->save_member($_REQUEST['member_id'],$_POST);
			set_message("Member saved successfully");
			redirect_browser(self::link_open($member_id));
		}
	}

    public function load($member_id){
        $data = self::get_member($member_id);
        foreach($data as $key=>$val){
            $this->$key = $val;
        }
        return $data;
    }
	public function save_member($member_id,$data){
		$member_id = update_insert("member_id",$member_id,"member",$data);

        module_extra::save_extras('member','member_id',$member_id);

		return $member_id;
	}


	public function delete_member($member_id){
		$member_id=(int)$member_id;
        $member = self::get_member($member_id);
        if($member && $member['member_id'] == $member_id){
            $sql = "DELETE FROM "._DB_PREFIX."member WHERE member_id = '".$member_id."' LIMIT 1";
            query($sql);
            module_extra::delete_extras('member','member_id',$member_id);
        }
	}

    public static function handle_import($data,$add_to_group){

        // woo! we're doing an import.

        // our first loop we go through and find matching members by their "member_name" (required field)
        // and then we assign that member_id to the import data.
        // our second loop through if there is a member_id we overwrite that existing member with the import data (ignoring blanks).
        // if there is no member id we create a new member record :) awesome.

        foreach($data as $rowid => $row){
            if(!isset($row['member_name']) || !trim($row['member_name'])){
                unset($data[$rowid]);
                continue;
            }
            if(!isset($row['member_id']) || !$row['member_id']){
                $data[$rowid]['member_id'] = 0;
            }
            // search for a custoemr based on name.
            $member = get_single('member','member_name',$row['member_name']);
            if($member && $member['member_id'] > 0){
                $data[$rowid]['member_id'] = $member['member_id'];
            }
        }

        // now save the data.
        foreach($data as $rowid => $row){
            $member_id = (int)$row['member_id'];
            // check if this ID exists.
            $member = self::get_member($member_id);
            if(!$member || $member['member_id'] != $member_id){
                $member_id = 0;
            }
            $member_id = update_insert("member_id",$member_id,"member",$row);


            foreach($add_to_group as $group_id => $tf){
                module_group::add_to_group($group_id,$member_id,'member');
            }

        }


    }


    public function get_install_sql(){
        ob_start();
        ?>

CREATE TABLE `<?php echo _DB_PREFIX; ?>member` (
  `member_id` int(11) NOT NULL auto_increment,
  `first_name` varchar(255) NOT NULL DEFAULT '',
  `last_name` varchar(255) NOT NULL DEFAULT '',
  `business` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `date_created` date NOT NULL,
  `date_updated` date NULL,
  PRIMARY KEY  (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;


<?php
        return ob_get_clean();
    }


}
