<?php 


$search = isset($_REQUEST['search']) ? $_REQUEST['search'] : array();
$members = module_member::get_members($search);

// hack to add a "group" option to the pagination results.
if(class_exists('module_group',false)){
    module_group::enable_pagination_hook(
        // what fields do we pass to the group module from this members?
        array(
            'fields'=>array(
                'owner_id' => 'member_id',
                'owner_table' => 'member',
            ),
        )
    );
}
// hack to add a "export" option to the pagination results.
if(class_exists('module_import_export',false) && module_member::can_i('view','Export Members')){
    module_import_export::enable_pagination_hook(
        // what fields do we pass to the import_export module from this members?
        array(
            'name' => 'Member Export',
            'fields'=>array(
                'Member ID' => 'member_id',
                'First Name' => 'first_name',
                'Last Name' => 'last_name',
                'Business Name' => 'business',
                'Email' => 'email',
                'Phone' => 'phone',
                'Mobile' => 'mobile',
            ),
            // do we look for extra fields?
            'extra' => array(
                'owner_table' => 'member',
                'owner_id' => 'member_id',
            ),
        )
    );
}
$pagination = process_pagination($members);

?>

<h2>
    <?php if(module_member::can_i('create','Members')){ ?>
	<span class="button">
		<?php echo create_link("Create New Member","add",module_member::link_open('new')); ?>
	</span>
    <?php
    }
    if(class_exists('module_import_export',false) && module_member::can_i('view','Import Members')){
        $link = module_import_export::import_link(
            array(
                'callback'=>'module_member::handle_import',
                'name'=>'Members',
                'return_url'=>$_SERVER['REQUEST_URI'],
                'group'=>array('member','newsletter_subscription'),
                'fields'=>array(
                    'Member ID' => 'member_id',
                    'First Name' => 'first_name',
                    'Last Name' => 'last_name',
                    'Business Name' => 'business',
                    'Email' => 'email',
                    'Phone' => 'phone',
                    'Mobile' => 'mobile',
                ),
                // do we try to import extra fields?
                'extra' => array(
                    'owner_table' => 'member',
                    'owner_id' => 'member_id',
                ),
            )
        );
        ?>
        <span class="button">
            <?php echo create_link("Import Members","add",$link); ?>
        </span>
        <?php
    }
    ?>
	<span class="title">
		<?php echo _l('Members'); ?>
	</span>
</h2>


<form action="" method="post">

<table class="search_bar" width="100%">
	<tr>
		<th><?php _e('Filter By:'); ?></th>
		<td width="140px">
			<?php _e('Names, Phone or Email:');?>
		</td>
		<td>
			<input type="text" style="width: 240px;" name="search[generic]" class="" value="<?php echo isset($search['generic'])?htmlspecialchars($search['generic']):''; ?>">
		</td>
		<td align="right" rowspan="2">
			<?php echo create_link("Reset","reset",module_member::link_open(false)); ?>
			<?php echo create_link("Search","submit"); ?>
		</td>
	</tr>
</table>

<?php echo $pagination['summary'];?>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tableclass tableclass_rows">
	<thead>
	<tr class="title">
		<th><?php echo _l('Member Name'); ?></th>
		<th><?php echo _l('Business'); ?></th>
		<th><?php echo _l('Phone'); ?></th>
		<th><?php echo _l('Mobile'); ?></th>
		<th><?php echo _l('Email Address'); ?></th>
        <?php if(class_exists('module_group',false)){ ?>
        <th><?php echo _l('Group'); ?></th>
            <?php if(class_exists('module_newsletter',false)){ ?>
            <th><?php echo _l('Newsletter Subscription'); ?></th>
            <?php } ?>
        <?php } ?>
    </tr>
    </thead>
    <tbody>
    <?php
	$c=0;
	foreach($pagination['rows'] as $member){ ?>
        <tr class="<?php echo ($c++%2)?"odd":"even"; ?>">
            <td class="row_action">
	            <?php echo module_member::link_open($member['member_id'],true); ?>
            </td>
            <td>
				<?php
				echo htmlspecialchars($member['business']);
				?>
            </td>
            <td>
				<?php
                echo htmlspecialchars($member['phone']);
				?>
            </td>
            <td>
				<?php
                echo htmlspecialchars($member['mobile']);
				?>
            </td>
            <td>
                <?php echo htmlspecialchars($member['email']); ?>
            </td>
            <?php if(class_exists('module_group',false)){ ?>
                <td><?php
                // find the groups for this member.
                    $g=array();
                    $groups = module_group::get_groups_search(array(
                        'owner_table' => 'member',
                        'owner_id' => $member['member_id'],
                    ));
                    foreach($groups as $group){
                        $g[] = $group['name'];
                    }
                    echo implode(', ',$g);
                ?></td>
                <?php if(class_exists('module_newsletter',false)){ ?>
                <td><?php
                    // find the groups for this member.
                    $g=array();
                    $groups = module_group::get_groups_search(array(
                        'owner_table' => 'newsletter_subscription',
                        'owner_id' => $member['member_id'],
                    ));;
                    foreach($groups as $group){
                        $g[] = $group['name'];
                    }
                    echo implode(', ',$g);
                    ?></td>
                <?php } ?>
            <?php } ?>
        </tr>
	<?php } ?>
  </tbody>
</table>
<?php echo $pagination['links'];?>
</form>