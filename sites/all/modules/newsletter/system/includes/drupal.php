<?php


// load up the configuration file from drupal.
// hacky! pass this off to a drupal plugin maybe.
$file = '../../../../default/settings.php';
if(!is_file($file)){
    echo "Drupal configuration file: ".$file." not found. Please contact support with FTP details for this to be resolved. <a href='http://support.dtbaker.com.au/support-ticket.html' target='_blank'>http://support.dtbaker.com.au/support-ticket.html</a>";
    exit;
}
include($file);
$drupal_database = $databases['default']['default'];
if(!$drupal_database || !$drupal_database['username']){
    echo "Drupal database configuration not found. Please contact support with FTP details for this to be resolved. <a href='http://support.dtbaker.com.au/support-ticket.html' target='_blank'>http://support.dtbaker.com.au/support-ticket.html</a>";
    exit;
}
//echo '<pre>';print_r($drupal_database);exit;

define('_DEFAULT_SYSTEM_NAME','Drupal Newsletter System');

//todo: don't need these here: we can just do our own db_connect with the drupal details ourselves.
define('_DB_SERVER',$drupal_database['host']);
define('_DB_NAME',$drupal_database['database']);
define('_DB_USER',$drupal_database['username']);
define('_DB_PASS',$drupal_database['password']);
define('_DB_PREFIX',$drupal_database['prefix'].'ucm_');

// redirect here instead of showing dashbaord
define('_CUSTOM_UCM_HOMEPAGE','?m=newsletter&p=newsletter_admin');