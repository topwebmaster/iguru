<form action="" method="post">


    <?php
module_form::prevent_exit(array(
    'valid_exits' => array(
        // selectors for the valid ways to exit this form.
        '.submit_button',
    ))
);
?>

    <input type="hidden" name="_config_settings_hook" value="save_config">

    <table class="tableclass tableclass_rows">
        <thead>
        <tr>
            <!--<th>
                <?php /*_e('Key');*/?>
            </th>-->
            <th>
                <?php _e('Description');?>
            </th>
            <th>
                <?php _e('Value');?>
            </th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($settings as $setting){ ?>
            <tr>
                <!--<td>
                    <?php /*echo $setting['key'];*/?>
                </td>-->
                <td><?php echo $setting['description'];?></td>
                <td>

                    <?php switch($setting['type']){
                        case 'text':
                        ?>
                            <input type="text" name="config[<?php echo $setting['key'];?>]" value="<?php echo htmlspecialchars(module_config::c($setting['key'],$setting['default']));?>" size="60">
                            <?php
                        break;
                        case 'select':
                        ?>
                            <select name="config[<?php echo $setting['key'];?>]">
                                <option value=""><?php _e('N/A');?></option>
                                <?php foreach($setting['options'] as $key=>$val){ ?>
                                <option value="<?php echo $key;?>"<?php echo module_config::c($setting['key'],$setting['default']) == $key ? ' selected':'' ?>><?php echo htmlspecialchars($val);?></option>
                                <?php } ?>
                            </select>
                            <?php
                        break;
                        case 'checkbox':
                        ?>
                            <input type="hidden" name="config_default[<?php echo $setting['key'];?>]" value="1">
                            <input type="checkbox" name="config[<?php echo $setting['key'];?>]" value="1" <?php if(module_config::c($setting['key'])) echo ' checked'; ?>>
                            <?php
                        break;

                        }

                    if(isset($setting['help'])){
                        _h($setting['help']);
                    }
                        ?>

                </td>
            </tr>
            <?php } ?>

            <tr>
                <td colspan="3" align="center">
                    <input type="submit" name="save" value="<?php _e('Save settings');?>" class="submit_button save_button">
                </td>
            </tr>
        </tbody>
    </table>

</form>