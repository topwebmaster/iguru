<?php


if(class_exists('module_security',false)){
    // if they are not allowed to "edit" a page, but the "view" permission exists
    // then we automatically grab the page and regex all the crap out of it that they are not allowed to change
    // eg: form elements, submit buttons, etc..
    module_security::check_page(array(
        'category' => 'Config',
        'page_name' => 'Settings',
        'module' => 'config',
        'feature' => 'Edit',
    ));
}
$module->page_title = 'Settings';

print_heading('Basic System Settings');


$settings = array(
         /*array(
            'key'=>'_installation_code',
            'default'=>'',
             'type'=>'text',
             'description'=>'Your license code',
             'help' => 'You can find your unique license code in the "license" file from CodeCanyon.net after you purchase this item. It looks like this: 30d91230-a8df-4545-1237-467abcd5b920 ',
         ),*/
         array(
            'key'=>'system_base_dir',
            'default'=>'/',
             'type'=>'text',
             'description'=>'Base URL for your system (eg: / or /admin/)',
         ),
         array(
            'key'=>'system_base_href',
            'default'=>'',
             'type'=>'text',
             'description'=>'URL for your system (eg: http://foo.com)',
         ),
         array(
            'key'=>'admin_system_name',
            'default'=>_DEFAULT_SYSTEM_NAME,
             'type'=>'text',
             'description'=>'Name your system',
         ),
         array(
            'key'=>'header_title',
            'default'=>'UCM',
             'type'=>'text',
             'description'=>'Text to appear in header',
         ),
         array(
            'key'=>'date_format',
            'default'=>'d/m/Y',
             'type'=>'text',
             'description'=>'Date format for system',
         ),
         array(
            'key'=>'timezone',
            'default'=>'America/New_York',
             'type'=>'text',
             'description'=>'Your timezone (<a href="http://php.net/manual/en/timezones.php">see all</a>) ',
         ),

         array(
            'key'=>'admin_email_address',
            'default'=>'info@example.com',
             'type'=>'text',
             'description'=>'The admins email address',
         ),
         /*array(
            'key'=>'envato_show_summary_what',
            'default'=>1,
             'type'=>'select',
             'options'=>array(
                 1=>'Show todays sales',
                 2=>'Show total balance (can be slower)',
             ),
             'description'=>'What to display in menu.',
         ),*/
);

module_config::print_settings_form(
     $settings
);

