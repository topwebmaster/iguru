<?php

$labels = array();
global $labels;

class module_language extends module_base{
	
    public $version = 2.1;

    public static function can_i($actions,$name=false,$category=false,$module=false){
        if(!$module)$module=__CLASS__;
        return parent::can_i($actions,$name,$category,$module);
    }
	public static function get_class() {
        return __CLASS__;
    }
	function init(){

        $this->module_name = "language";
        global $labels;

        if(module_security::is_logged_in()){
            $user = module_user::get_user(module_security::get_loggedin_id());
            if($user && $user['user_id'] && $user['language']){
                $language = basename($user['language']);
                if(is_file(@include('labels/'.$language.'.php'))){
                    define('_UCM_LANG',$language);
                }

            }
        }

	}

}