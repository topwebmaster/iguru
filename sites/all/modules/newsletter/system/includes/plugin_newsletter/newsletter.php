<?php

define('_NEWSLETTER_STATUS_NEW',0);
define('_NEWSLETTER_STATUS_PENDING',1);
define('_NEWSLETTER_STATUS_PAUSED',2);
define('_NEWSLETTER_STATUS_SENT',3);
define('_NEWSLETTER_STATUS_DELETED',4);
define('_NEWSLETTER_STATUS_FAILED',6);


class module_newsletter extends module_base{
	
	public $links;
	public $newsletter_types;

    public $version = 2.11;

    public static $images_dir = 'includes/plugin_newsletter/images/';

    static $attachment_dir = 'includes/plugin_newsletter/attachments/';
    public static function can_i($actions,$name=false,$category=false,$module=false){
        if(!$module)$module=__CLASS__;
        return parent::can_i($actions,$name,$category,$module);
    }
	public static function get_class() {
        return __CLASS__;
    }
    public function init(){
		$this->links = array();
		$this->newsletter_types = array();
		$this->module_name = "newsletter";
		$this->module_position = 20;

        if($this->can_i('view','Newsletters')){
            $this->links[] = array(
                "name"=>"Newsletters",
                "p"=>"newsletter_admin",
                'args'=>array('newsletter_id'=>false),
            );
            if(isset($_REQUEST['member_id']) && (int)$_REQUEST['member_id']>0){
                $this->links[] = array(
                    "name"=>"Member Newsletters",
                    "p"=>"newsletter_member",
                    'args'=>array('newsletter_id'=>false,'member_id'=>(int)$_REQUEST['member_id']),
                    'holder_module' => 'member', // which parent module this link will sit under.
                    'holder_module_page' => 'member_admin_open',  // which page this link will be automatically added to.
                    'menu_include_parent' => 0,
                );
            }
        }

        if($this->can_i('view','Users','Config')){
            $this->links[] = array(
                "name"=>"Newsletter Settings",
                "p"=>"newsletter_settings",
                "args"=>array('user_id'=>false),
                'holder_module' => 'config', // which parent module this link will sit under.
                'holder_module_page' => 'config_admin',  // which page this link will be automatically added to.
                'menu_include_parent' => 0,
                'order'=>3,
            );
            $this->links[] = array(
                "name"=>"Newsletter Templates",
                "p"=>"newsletter_template",
                "args"=>array('newsletter_template_id'=>false),
                'holder_module' => 'config', // which parent module this link will sit under.
                'holder_module_page' => 'config_admin',  // which page this link will be automatically added to.
                'menu_include_parent' => 0,
                'order'=>3,
            );
        }

        module_config::register_css('newsletter','newsletter.css');

        // todo - search the newsletter_send list for subjects as well..
        $this->ajax_search_keys = array(
            _DB_PREFIX.'newsletter' => array(
                'plugin' => 'newsletter',
                'search_fields' => array(
                    'subject',
                ),
                'key' => 'newsletter_id',
                'title' => _l('Newsletter: '),
            ),
        );
		
	}

    public static function link_generate($newsletter_id=false,$options=array(),$link_options=array(),$data=false){

        $key = 'newsletter_id';
        if($newsletter_id === false && $link_options){
            foreach($link_options as $link_option){
                if(isset($link_option['data']) && isset($link_option['data'][$key])){
                    ${$key} = $link_option['data'][$key];
                    break;
                }
            }
            if(!${$key} && isset($_REQUEST[$key])){
                ${$key} = $_REQUEST[$key];
            }
        }
        $bubble_to_module = false;
        if(!isset($options['type']))$options['type']='newsletter';

        if(!isset($options['arguments'])){
            $options['arguments'] = array();
        }
        $options['arguments']['newsletter_id'] = $newsletter_id;
        $options['module'] = 'newsletter';
        $data = isset($options['data'])?$options['data']:array();
        if(isset($options['full']) && $options['full']){
            if(!$data)$data = self::get_newsletter($newsletter_id);
            $options['data'] = $data;
        }
        // what text should we display in this link?
        if(!isset($options['text'])||!$options['text'])$options['text'] = (!isset($data['subject'])||!trim($data['subject'])) ? 'N/A' : $data['subject'];
        if(!$link_options){
            // only bubble up once to this same module.
            //$options['page'] = 'newsletter_edit';
            if(!isset($options['page']))$options['page']='newsletter_edit';
            $bubble_to_module = array(
                'module' => 'newsletter',
            );
        }else{
            // for first loop
            $options['page'] = 'newsletter_admin';
        }

        array_unshift($link_options,$options);

        if(!module_security::has_feature_access(array(
            'name' => 'Newsletters',
            'module' => 'newsletter',
            'category' => 'Newsletter',
            'view' => 1,
            'description' => 'view',
        ))){
            if(!isset($options['full']) || !$options['full']){
                return '#';
            }else{
                return isset($options['text']) ? $options['text'] : 'N/A';
            }

        }
        if($bubble_to_module){
            global $plugins;
            return $plugins[$bubble_to_module['module']]->link_generate(false,array(),$link_options);
        }else{
            // return the link as-is, no more bubbling or anything.
            // pass this off to the global link_generate() function
            return link_generate($link_options);

        }
    }

	public static function link_open($newsletter_id,$full=false,$data=array()){
        return self::link_generate($newsletter_id,array('full'=>$full,'data'=>$data));
    }
	public static function link_list($newsletter_id,$full=false){
        return self::link_generate($newsletter_id,array('full'=>$full,'page'=>'newsletter_list'));
    }
	public static function link_preview($newsletter_id,$full=false){
        return self::link_generate($newsletter_id,array('full'=>$full,'page'=>'newsletter_preview'));
    }
	public static function link_statistics($newsletter_id,$send_id,$full=false){
        return self::link_generate($newsletter_id,array('full'=>$full,'page'=>'newsletter_statistics','arguments'=>array('send_id'=>$send_id)));
    }
	public static function link_send($newsletter_id,$full=false,$send_id=false){
        return self::link_generate($newsletter_id,array('full'=>$full,'page'=>'newsletter_send','arguments'=>array('send_id'=>$send_id)));
    }
	public static function link_queue($newsletter_id,$send_id,$full=false){
        return self::link_generate($newsletter_id,array('full'=>$full,'page'=>'newsletter_queue','arguments'=>array('send_id'=>$send_id)));
    }
	public static function link_queue_watch($newsletter_id,$send_id,$full=false){
        return self::link_generate($newsletter_id,array('full'=>$full,'page'=>'newsletter_queue_watch','arguments'=>array('send_id'=>$send_id)));
    }
	public static function link_queue_manual($newsletter_id,$send_id,$full=false){
        return self::link_generate($newsletter_id,array('full'=>$full,'page'=>'newsletter_queue_manual','arguments'=>array('send_id'=>$send_id)));
    }

	public static function link_open_template($newsletter_template_id,$full=false){
        $data = self::get_newsletter_template($newsletter_template_id);
        return self::link_generate(false,array(
                                              'full'=>$full,
                                              'page'=>'newsletter_template',
                                              'arguments'=>array(
                                                  'newsletter_template_id'=>$newsletter_template_id,
                                              ),
                                            'text'=>$data['newsletter_template_name'],
                                         ),array(),$data);
    }

	
	public function process(){
		$errors=array();
		if(isset($_REQUEST['butt_del']) && $_REQUEST['butt_del'] && $_REQUEST['newsletter_id']){
            $data = self::get_newsletter($_REQUEST['newsletter_id']);
            if(module_form::confirm_delete('newsletter_id',"Really delete newsletter: ".$data['subject'],self::link_open($_REQUEST['newsletter_id']))){
                $this->delete_newsletter($_REQUEST['newsletter_id']);
                set_message("Newsletter deleted successfully");
                redirect_browser(self::link_list(false));
            }
		}else if("save_newsletter" == $_REQUEST['_process']){
			$newsletter_id = $this->save_newsletter($_REQUEST['newsletter_id'],$_POST);
            if(isset($_REQUEST['butt_send'])){
                redirect_browser($this->link_send($newsletter_id));
            }
            if(isset($_REQUEST['butt_duplicate'])){
                $newsletter_id = $this->duplicate_newsetter($newsletter_id);
                set_message('Newsletter duplicated successfully');
                redirect_browser($this->link_open($newsletter_id));
            }
            if(isset($_REQUEST['butt_preview_email'])){
                if($this->send_preview($newsletter_id,$_REQUEST['quick_email'])){
                    //set_message("Newsletter preview sent successfully.");
                    redirect_browser($this->link_open($newsletter_id));
                }/*else{
                    echo "<br><br>Failed to send preview. <br><br>";
                    echo '<a href="'.$this->link_open($newsletter_id).'">try again</a> ';
                    exit;
                }*/
            }
            if(isset($_REQUEST['butt_preview'])){
                redirect_browser($this->link_preview($newsletter_id));
            }
            set_message("Newsletter saved successfully");
            redirect_browser($this->link_open($newsletter_id));
		}else if("send_send" == $_REQUEST['_process']){
            $newsletter_id = (int)$_REQUEST['newsletter_id'];
            $send_id = (int)$_REQUEST['send_id'];
            if($newsletter_id && $send_id){
                $sql = "UPDATE `"._DB_PREFIX."newsletter_send` SET `status` = "._NEWSLETTER_STATUS_PENDING." WHERE send_id = $send_id AND newsletter_id = $newsletter_id";
                query($sql);
                self::update_member_data_for_send($send_id);
                //ready to send
                redirect_browser($this->link_queue_watch($newsletter_id,$send_id));
            }

		}else if("modify_send" == $_REQUEST['_process']){
            $send_id = (int)$_REQUEST['send_id'];
            $newsletter_id = (int)$_REQUEST['newsletter_id'];
            $send = get_single('newsletter_send',array('send_id','newsletter_id'),array($send_id,$newsletter_id));
            if(isset($_POST['status']) && $_POST['status']=='delete'){
                if(module_form::confirm_delete('newsletter_id',"Really delete this send?",self::link_queue_watch($newsletter_id,$send_id))){
                    if($send && $send['send_id']==$send_id){
                        set_message("Newsletter send deleted successfully");
                        update_insert('send_id',$send_id,'newsletter_send',array(
                                                   'status' => _NEWSLETTER_STATUS_DELETED,
                                               ));
                    }
                    redirect_browser(self::link_list(false));
                }
                unset($_POST['status']);
            }
            if(!$send['start_time'])$_POST['start_time']=time(); // hack cos sometimes it doesn't save start time? i think i fixed this bug though.
            if($send && $send['send_id']==$send_id){
                update_insert('send_id',$send_id,'newsletter_send',$_POST);
                
                redirect_browser($this->link_queue_watch($newsletter_id,$send_id));
            }
            
		}else if("enque_send" == $_REQUEST['_process']){
            $newsletter_id = (int)$_REQUEST['newsletter_id'];
            $send_id = (int)$_REQUEST['send_id'];

            $newsletter_data = self::get_newsletter($newsletter_id);
            if($newsletter_data['newsletter_id'] != $newsletter_id){
                die('failed to enqueue send');
            }

            // are we adding members to an existing send? or overwriting them to an existing draft / or creating a new blank send.
            if($send_id > 0){
                $adding_members = true;
            }else{
                $adding_members = false;
            }

            $members = array();
            //todo: pass this off as a hook.
            // so we could have another module (eg: module_drupal or module_wordpress) that
            // checks which members were selected on the previous screen, and return a standard member array
            if(class_exists('module_group',false)){
                // find the groups we are sending to.
                $send_groups = array();
                $groups = module_group::get_groups();
                foreach($groups as $group){
                    if(isset($_REQUEST['group']) && isset($_REQUEST['group'][$group['group_id']]) && $_REQUEST['group'][$group['group_id']] == 'yes'){
                        // we are sending to this group
                        // get a list of members in this group and add them to a send table ready to go.
                        $send_groups[$group['group_id']] = true;
                    }
                }
                // find the members for these groups
                $callback = 'module_group::newsletter_callback';
                foreach($send_groups as $group_id => $tf){
                    $group_members = module_group::get_members($group_id);
                    //echo '<pre>';print_r($group_members);exit;
                    // give all these members a callback so the newsletter system can get more data from them.
                    foreach($group_members as &$group_member){
                        $args = array(
                             'group_id'=>$group_id,
                             'owner_id'=>$group_member['owner_id'],
                             'owner_table'=>$group_member['owner_table'],
                         );
                        // run this data callback to get the data from this group member.
                        $group_member = self::member_data_callback($callback,$args);
                        $group_member['data_callback'] = $callback;
                        $group_member['data_args'] = json_encode($args);
                        
                    }
                    $members = array_merge($members,$group_members);
                }
            }
            //echo '<pre>';print_r($members);exit;
            // todo - load CSV formats in too. IDEA! make a new CSV module, it will work in with GROUP hook above! YESS!
            if(!$adding_members && !count($members)){
                set_error('Please select at least 1 person to send this newsletter to');
                redirect_browser(self::link_send($newsletter_id));
            }
            if(!$adding_members && !$send_id){
                // see if we can re-use a previously unsent send (ie: draft send)
                $drafts = get_multiple('newsletter_send',array('newsletter_id'=>$newsletter_id,'status'=>_NEWSLETTER_STATUS_NEW));
                if(count($drafts)){
                    $draft = array_shift($drafts);
                    if($draft['send_id']){
                        $send_id = (int)$draft['send_id'];
                        $sql = "DELETE FROM `"._DB_PREFIX."newsletter_send_member` WHERE send_id = ".(int)$send_id;
                    }
                }
            }

            if(isset($_REQUEST['start_time'])){
                $start_time = strtotime(input_date($_REQUEST['start_time'],true));
                if(!$start_time)$start_time=time();
            }else{
                $start_time = time();
            }
            $allow_duplicates = isset($_REQUEST['allow_duplicates']) ? $_REQUEST['allow_duplicates'] : 0;
            $send_id = self::save_send($send_id,array(
                                                               'newsletter_id' => $newsletter_id,
                                                               'status' => _NEWSLETTER_STATUS_NEW, // don't send yet.
                                                                'start_time' => $start_time,
                                                                'allow_duplicates' => $allow_duplicates,
                                                    // cache a copy of the newsletter so we can pull back the old subject in histories
                                                   'cache' => serialize($newsletter_data),
                                                   'subject' => $newsletter_data['subject'],
                                                          ));
            $done_member = false;
            if($send_id){
                // add the members from this send into the listing.
                // this will be a snapshop of the members details at the time this send is created.
                // todo: figure out if this will come back and bite me in the bum :)
                foreach($members as $member){
                    // check uniquness of this member's email in the send listing.
                    // find this member by email.
                    $newsletter_member_id = self::member_from_email($member);
                    if($newsletter_member_id>0){
                        // found a member! add it to the send queue for this send.
                        if(!$allow_duplicates){
                            // check if this member has received this email before.
                            $sql = "SELECT * FROM `"._DB_PREFIX."newsletter_send_member` sm";
                            $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_send` s USING (send_id) ";
                            $sql .= " WHERE sm.newsletter_member_id = ".(int)$newsletter_member_id;
                            $sql .= " AND sm.send_id IN (SELECT send_id FROM `"._DB_PREFIX."newsletter_send` WHERE newsletter_id = $newsletter_id)";
                            $sql .= " AND sm.send_id != ".(int)$send_id;
                            $sql .= " AND s.status != 4 "; // so we ignore deleted sends.
                            $check = query($sql);
                            if(mysql_num_rows($check)){
                                // user has received this before.
                                mysql_free_result($check);
                                continue;
                            }
                            mysql_free_result($check);
                        }
                        $sql = "REPLACE INTO `"._DB_PREFIX."newsletter_send_member` SET ";
                        $sql .= " send_id = ".(int)$send_id." ";
                        $sql .= ", newsletter_member_id = ".(int)$newsletter_member_id." ";
                        $sql .= ", `sent_time` = 0";
                        $sql .= ", `status` = 0";
                        $sql .= ", `open_time` = 0";
                        $sql .= ", `bounce_time` = 0";
                        query($sql);
                        $done_member = true;
                    }
                }
                if(!$done_member && !$adding_members){
                    set_error('Please select at least 1 person to send this newsletter to.');
                    redirect_browser(self::link_send($newsletter_id));
                }
                redirect_browser($this->link_queue($newsletter_id,$send_id));
            }
		}else if("save_newsletter_template" == $_REQUEST['_process']){
			$newsletter_template_id = $this->save_newsletter_template($_REQUEST['newsletter_template_id'],$_POST);
            set_message("Newsletter template saved successfully");
            redirect_browser($this->link_open_template($newsletter_template_id));
		}
		if(!count($errors)){
			redirect_browser($_REQUEST['_redirect']);
			exit;
		}
		print_error($errors,true);
	}


    public static function member_from_email($data){
        $email = filter_var(strtolower(trim($data['email'])),FILTER_VALIDATE_EMAIL);
        if(strlen($email)>3){
            // search for this member by email
            $sql = "SELECT newsletter_member_id,`email`,`company_name`,`first_name`,`last_name` FROM `"._DB_PREFIX."newsletter_member` WHERE `email` LIKE '".mysql_real_escape_string($email)."'";
            $res = qa1($sql);
            $data_cache = $data;
            if(isset($data_cache['data_cache']))unset($data_cache['data_cache']);
            if(isset($data_cache['data_callback']))unset($data_cache['data_callback']);
            if(isset($data_cache['data_args']))unset($data_cache['data_args']);
            if($res && strtolower($res['email']) == strtolower($email)){
                // found existing member!

                $update_data = $data;
                // todo: update their name ? create new entry if names are different? meh..
                /*$update_data = array();
                if(!$res['company_name']&&$data['company_name']){
                    $update_data['company_name']=$data['company_name'];
                }
                if(!$res['first_name']&&$data['first_name']){
                    $update_data['first_name']=$data['first_name'];
                }
                if(!$res['last_name']&&$data['last_name']){
                    $update_data['last_name']=$data['last_name'];
                }*/
                if($update_data){
                    update_insert('newsletter_member_id',$res['newsletter_member_id'],'newsletter_member',$update_data);
                }
                // update this one with any new data callbacks.
                if(isset($data['data_callback']) && $data['data_callback']){
                    update_insert('newsletter_member_id',$res['newsletter_member_id'],'newsletter_member',array(
                                                            'data_callback' => $data['data_callback'],
                                                            'data_args' => isset($data['data_args']) ? $data['data_args'] : '',
                                                            'data_cache' => serialize($data_cache),
                                                                                                          ));
                }
                return $res['newsletter_member_id'];
            }else{
                // create member with this email / data
                if(isset($data['name']) && (!isset($data['first_name'])|| !isset($data['last_name']))){
                    $name_parts = explode(" ",preg_replace('/\s+/',' ',$data['name']));
                    $data['first_name'] = array_shift($name_parts);
                    $data['last_name'] = implode(' ',$name_parts);
                }
                $data['data_cache'] = serialize($data_cache);
                $data['join_date'] = date('Y-m-d');
                $newsletter_member_id = update_insert('newsletter_member_id','new','newsletter_member',$data);
                return $newsletter_member_id;
            }
        }
        return false; // no member found or able to be created.
    }
    public static function get_templates($search=array()){
        return get_multiple('newsletter_template',$search);
    }
    public static function get_newsletter_template($newsletter_template_id){
        $t = get_single('newsletter_template','newsletter_template_id',$newsletter_template_id);
        if(!$t){
            $t['newsletter_template_name'] = '';
            $t['body'] = '';
            $t['content_url'] = '';
        }
        return $t;
    }
	public static function get_newsletters($search=array()){
		// build up a custom search sql query based on the provided search fields
		$sql = "SELECT u.*,u.newsletter_id AS id ";
        $sql .= ' , nt.newsletter_template_name ';
        $sql .= ' , ns.start_time AS last_sent ';
        $sql .= ' , ns.send_id ';
        $sql .= ' , ns.status AS send_status ';
        $from = " FROM `"._DB_PREFIX."newsletter` u ";
        $from .= " LEFT JOIN `"._DB_PREFIX."newsletter_template` nt USING (newsletter_template_id) ";
        $from .= " LEFT JOIN `"._DB_PREFIX."newsletter_send` ns ON u.newsletter_id = ns.newsletter_id AND ns.`status` != "._NEWSLETTER_STATUS_DELETED;
		$where = " WHERE 1 ";
        //$where .= " AND ;
		if(isset($search['generic']) && $search['generic']){
			$str = mysql_real_escape_string($search['generic']);
			$where .= " AND ( ";
			$where .= " u.subject LIKE '%$str%' OR ";
			$where .= " ns.subject LIKE '%$str%' OR ";
			$where .= " u.from_name LIKE '%$str%' OR ";
			$where .= " u.from_email LIKE '%$str%' OR ";
			$where .= " u.bounce_email LIKE '%$str%' ";
			$where .= ' ) ';
		}
        if(isset($search['pending']) && $search['pending']){
            $where .= " AND ns.send_id IS NOT NULL AND ( ns.status = "._NEWSLETTER_STATUS_PAUSED;
            $where .= " OR ns.status = "._NEWSLETTER_STATUS_PENDING.") ";
        }else if(isset($search['draft']) && $search['draft']){
            $where .= ' AND (ns.newsletter_id IS NULL OR ns.`status` = '._NEWSLETTER_STATUS_NEW.")";
        }else{
            $where .= ' AND ns.newsletter_id IS NOT NULL ';
        }
        foreach(array('status') as $key){
            if(isset($search[$key]) && $search[$key] !== ''&& $search[$key] !== false){
                $str = mysql_real_escape_string($search[$key]);
                $where .= " AND u.`$key` = '$str'";
            }
        }
        $group_order = '';
        if(isset($search['draft']) && $search['draft']){
            // only show 1 newsletter in drafts
            $group_order .= 'GROUP BY u.newsletter_id ';
        }
		$group_order .= ' ORDER BY ns.start_time DESC ';
		$sql = $sql . $from . $where . $group_order;
        return query($sql);
		/*$result = qa($sql);
		module_security::filter_data_set("newsletter",$result);
		return $result;*/
//		return get_multiple("newsletter",$search,"newsletter_id","fuzzy","name");

	}
	public static function get_newsletter($newsletter_id){
        $newsletter_id = (int)$newsletter_id;
        if($newsletter_id){
		    $newsletter = get_single("newsletter","newsletter_id",$newsletter_id);
            $sql = "SELECT *, send_id AS id FROM `"._DB_PREFIX."newsletter_send` WHERE newsletter_id = $newsletter_id AND `status` != "._NEWSLETTER_STATUS_DELETED;
            $newsletter['sends'] = qa($sql); //get_multiple('newsletter_send',array('newsletter_id'=>$newsletter_id));
        }
        if(!$newsletter_id || !isset($newsletter) || !$newsletter){
            $template = array_shift(self::get_templates()); //todo - search for 'default' one.
            $default_template_id = $template['newsletter_template_id'];
            $newsletter = array(
                'newsletter_id' => 'new',
                'subject' => '',
                'newsletter_template_id' => $default_template_id,
                'from_name' => module_config::c('newsletter_default_from_name',module_config::c('admin_system_name')),
                'from_email' => module_config::c('newsletter_default_from_email',module_config::c('admin_email_address')),
                'to_name' => '{FIRST_NAME} {LAST_NAME}',
                'bounce_email' => module_config::c('newsletter_default_bounce',module_config::c('admin_email_address')),
                'content' => '',
                'sends'=>array(),
            );
        }
		return $newsletter;
	}
	public function save_newsletter($newsletter_id,$data){
		$newsletter_id = update_insert("newsletter_id",$newsletter_id,"newsletter",$data);
        module_extra::save_extras('newsletter','newsletter_id',$newsletter_id);
		return $newsletter_id;
	}
	public function save_newsletter_template($newsletter_template_id,$data){
		$newsletter_template_id = update_insert("newsletter_template_id",$newsletter_template_id,"newsletter_template",$data);
		return $newsletter_template_id;
	}

	public function delete_newsletter($newsletter_id){
		$newsletter_id=(int)$newsletter_id;
		if(_DEMO_MODE && $newsletter_id == 1){
			return;
		}
		$sql = "DELETE FROM "._DB_PREFIX."newsletter WHERE newsletter_id = '".$newsletter_id."' LIMIT 1";
		query($sql);
		$sql = "DELETE FROM `"._DB_PREFIX."newsletter_send` WHERE newsletter_id = '".$newsletter_id."' LIMIT 1";
		query($sql);
        // todo - empty the newsletter_send_member table based on the newsletter_send table.
		$sql = "DELETE FROM `"._DB_PREFIX."newsletter_campaign_newsletter` WHERE newsletter_id = '".$newsletter_id."' LIMIT 1";
		query($sql);
        module_file::delete_files('newsletter',$newsletter_id);
        module_file::delete_files('newsletter_files',$newsletter_id);
	}
    public function login_link($newsletter_id){
        return module_security::generate_auto_login_link($newsletter_id);
    }




    public static function render($newsletter_id,$send_id=false,$newsletter_member_id=false,$view='internal'){
        $newsletter = self::get_newsletter($newsletter_id);
        $newsletter_content = $newsletter['content'];
        $send_data = array();
        if($send_id){
            $send_data = self::get_send($send_id);
        }
        $replace = self::get_replace_fields($newsletter_id,$send_id,$newsletter_member_id);

        $template_html = '';
        if($newsletter['newsletter_template_id']){
            // load template in.
            $template = self::get_newsletter_template($newsletter['newsletter_template_id']);
            // check if there's a template file for this template
            // and pass the processing off to it's module.
            // this can replace the newsletter_content variable with something
            // more advanced like a listing from a WP database.
            if(!_DEMO_MODE){
                // todo - run the content url
                // execute any php code inside the template.
                $template_html = @eval($template['body']);
                if(!$template_html){
                    $template_html = $template['body'];
                }
            }else{
                $template_html = $template['body'];
            }
            if(is_dir('includes/plugin_newsletter/templates/'.(int)$newsletter['newsletter_template_id'].'/')){
                if(is_file('includes/plugin_newsletter/templates/'.(int)$newsletter['newsletter_template_id'].'/render.php')){
                    include('includes/plugin_newsletter/templates/'.(int)$newsletter['newsletter_template_id'].'/render.php');
                }
            }
        }
        if(!preg_match('#\{BODY\}#i',$template_html)){
            $template_html .= '{BODY}';
        }
        $template_html = str_replace('{BODY}',$newsletter_content,$template_html);
        // replace any content from our newsletter variables.
        $replace['BODY'] = $newsletter_content;


        // custom for send.

        // TODO: we build up a list of 'recipients' based on a search (or whatever) from any part of the application.
        // the list will be a CSV style file, with column headers. Each column can be linked into here as a replacement variable.
        // eg: the customer list will be "Customer Name" "Primary Contact" etc..
        // maybe tie this into the pagination function? so the table can be exported from anywhere?
        // store this listing of contacts as-is as new rows in the database. allow the user to edit this listing like a spreadsheet (ie: add new rows from another search, remove rows, edit details).
        // try to store a link back to the original record where this item came from (eg: a link to edit customer)
        // but always keep the original (eg:) email address of the customer on file, so if customer record changes the history will still show old address etc..

        // unsubscribes will add this users email to a separate black list, so that future sends will flag these members as unsubscribed.
        // admin has the option to ignore these unsubscribes if they want.

        // bounces will add this users email to a separate bounce list. after a certain number of bounces (from settings) we stop sending this user emails
        // and we notify future sends that this list of members wont be emailed due to bounces. (same warning/info listing that shows about unsubscribed users)

        // todo: end :)
        // nite dave!


        $final_newsletter_html = $template_html;
        foreach($replace as $key=>$val){
            //$val = str_replace('//','////',$val); // todo- check this is correct in older version
            //$val = str_replace('\\','\\\\',$val); // todo- check this is correct in older version
            $final_newsletter_html = str_replace('{'.strtoupper($key).'}',$val,$final_newsletter_html);
        }
        // todo - a text version of the html version.

        return $final_newsletter_html;
    }

    public static function save_send($send_id,$data)
    {
        $send_id = update_insert('send_id',$send_id,'newsletter_send',$data);
        return $send_id;
    }

    /*public static function get_pending_sends(){
        $sql = "SELECT * FROM `"._DB_PREFIX."newsletter_send` ns ";
        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter` n USING (newsletter_id)";
        $sql .= " WHERE ns.status = "._NEWSLETTER_STATUS_PAUSED;
        $sql .= " OR ns.status = "._NEWSLETTER_STATUS_PENDING;
        return qa($sql);
    }*/
    public static function get_send($send_id)
    {
        $send_id = (int)$send_id;
        if(!$send_id)return array();
        $sql = "SELECT ns.* ";
        $sql .= " , (SELECT COUNT(sm1.newsletter_member_id) FROM `"._DB_PREFIX."newsletter_send_member` sm1 WHERE ns.send_id = sm1.send_id) AS `total_member_count`";
        $sql .= " , (SELECT COUNT(sm2.newsletter_member_id) FROM `"._DB_PREFIX."newsletter_send_member` sm2 WHERE ns.send_id = sm2.send_id AND sm2.`status` != "._NEWSLETTER_STATUS_NEW." AND sm2.`status` != "._NEWSLETTER_STATUS_PAUSED.") AS `total_sent_count`";
        $sql .= " , (SELECT COUNT(sm3.newsletter_member_id) FROM `"._DB_PREFIX."newsletter_send_member` sm3 WHERE ns.send_id = sm3.send_id AND sm3.`open_time` > 0) AS `total_open_count`";
        $sql .= " , (SELECT COUNT(sm4.newsletter_member_id) FROM `"._DB_PREFIX."newsletter_send_member` sm4 WHERE ns.send_id = sm4.send_id AND sm4.`bounce_time` > 0) AS `total_bounce_count`";
        $sql .= " , (SELECT COUNT(sm5.newsletter_member_id) FROM `"._DB_PREFIX."newsletter_send_member` sm5 WHERE ns.send_id = sm5.send_id AND sm5.`unsubscribe_time` > 0) AS `total_unsubscribe_count`";
        $sql .= " FROM `"._DB_PREFIX."newsletter_send` ns ";
//        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_send_member` sm1 ON ns.send_id = sm1.send_id ";
//        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_send_member` sm2 ON ns.send_id = sm2.send_id AND sm2.`sent_time` > 0";
//        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_send_member` sm3 ON ns.send_id = sm3.send_id AND sm3.`open_time` > 0 ";
//        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_send_member` sm4 ON ns.send_id = sm4.send_id AND sm4.`bounce_time` > 0";
//        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_send_member` sm5 ON ns.send_id = sm5.send_id AND sm5.`unsubscribe_time` > 0";
        $sql .= " WHERE ns.send_id = ".(int)$send_id;
        //$sql .= " GROUP BY ns.send_id";
        //echo $sql;
        return qa1($sql,false);
        //return get_single('newsletter_send','send_id',$send_id);
    }
    public static function get_newsletter_member($newsletter_member_id){
        return get_single('newsletter_member','newsletter_member_id',$newsletter_member_id);
    }
    public static function get_member($member_id){
        return get_multiple('newsletter_member',array('member_id'=>$member_id),'newsletter_member_id');
    }
    public static function get_send_members($send_id,$all=false)
    {
        $sql = "SELECT * FROM `"._DB_PREFIX."newsletter_send_member` s ";
        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_member` m USING (newsletter_member_id) ";
        $sql .= " WHERE s.send_id = ".(int)$send_id;
        if($all){
            // return all, no matter what their settings./

            // used on the statistics page.
        }else{
            $sql .= " AND m.bounce_count < ".(int)module_config::c('newsletter_bounce_threshold',3);
            $sql .= " AND m.receive_email = 1";
            $sql .= " AND m.email != ''";
            $sql .= " AND (m.unsubscribe_date IS NULL OR m.unsubscribe_date = '0000-00-00')";
        }
        return query($sql);
    }
    public static function get_member_sends($member_id)
    {
        $sql = "SELECT * FROM `"._DB_PREFIX."newsletter_member` m ";
        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_send_member` sm USING (newsletter_member_id) ";
        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_send` s USING (send_id) ";
        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter` n USING (newsletter_id) ";
        $sql .= " WHERE m.member_id = ".(int)$member_id;
        $sql .= " AND s.status != 4 "; // deleted sends.
        return query($sql);
    }
    public static function get_problem_members($send_id)
    {
        $sql = "SELECT * FROM `"._DB_PREFIX."newsletter_send_member`s ";
        $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_member` m USING (newsletter_member_id) ";
        $sql .= " WHERE s.send_id = ".(int)$send_id;
        $sql .= " AND ( m.bounce_count >= ".(int)module_config::c('newsletter_bounce_threshold',3);
        $sql .= " OR m.receive_email = 0";
        $sql .= " OR m.email = ''";
        $sql .= " OR (m.unsubscribe_date IS NOT NULL OR m.unsubscribe_date != '0000-00-00') )";
        return query($sql);
    }

    public function duplicate_newsetter($newsletter_id){
        $newsletter_data = get_single('newsletter','newsletter_id',$newsletter_id);
        unset($newsletter_data['newsletter_id']);
        $new_newsletter_id = update_insert('newsletter_id','new','newsletter',$newsletter_data);
        // duplicate the images and attachments (these are in the file plugin)
        // images first
        $files = module_file::get_files(array('owner_table' => 'newsletter', 'owner_id' => $newsletter_id,));
        foreach ($files as $file_data){
            if(is_file($file_data['file_path'])){
                $new_file_path = 'includes/plugin_file/upload/'.md5(time().$file_data['file_name']);
                copy($file_data['file_path'],$new_file_path);
                $file_data['file_path'] = $new_file_path;
            }
            unset($file_data['file_id']);
            unset($file_data['date_updated']);
            $file_data['owner_id']=$new_newsletter_id;
            update_insert('file_id','new','file',$file_data);
        }
        // now attachemtns
        $files = module_file::get_files(array('owner_table' => 'newsletter_files', 'owner_id' => $newsletter_id,));
        foreach ($files as $file_data){
            if(is_file($file_data['file_path'])){
                $new_file_path = 'includes/plugin_file/upload/'.md5(time().$file_data['file_name']);
                copy($file_data['file_path'],$new_file_path);
                $file_data['file_path'] = $new_file_path;
            }
            unset($file_data['file_id']);
            unset($file_data['date_updated']);
            $file_data['owner_id']=$new_newsletter_id;
            update_insert('file_id','new','file',$file_data);
        }
        return $new_newsletter_id;
    }

    public static function send_preview($newsletter_id,$email_address){
        $newsletter = self::get_newsletter($newsletter_id);
        $email = module_email::new_email();
        $fields = self::get_replace_fields($newsletter_id);
        $email->replace_values = $fields;
        $email->set_bounce_address($newsletter['bounce_email']);
        $email->set_from_manual($newsletter['from_email'],$newsletter['from_name']);
        $email->set_to_manual($email_address,$newsletter['to_name']);
        $email->set_subject($newsletter['subject']);
        // do we send images inline?
        $html = self::render($newsletter_id);
        $email->set_html($html);

        // do we attach anything else?
        $files = module_file::get_files(array('owner_table' => 'newsletter_files', 'owner_id' => $newsletter_id,));
        foreach ($files as $file_data){
            if(is_file($file_data['file_path'])){
                $email->AddAttachment($file_data['file_path'],$file_data['file_name']);
            }
        }
        if($email->send()){
            set_message('Email preview sent successfully');
            return true;
        }else{
            set_error('Failed to send email preview: '.$email->error_text);
            return false;
        }
    }


    public static function process_send($newsletter_id,$send_id,$send_limit=1,$retry_bounces=false){
        $newsletter_id = (int)$newsletter_id;
        $send_id = (int)$send_id;
        // todo, make the result friendly for multiple members.
        $result = array(
            'send_count'=>0,
            'status'=>false,
            'email'=>'',
            'error'=>'',
            'newsletter_member_id'=>0,
        );
        $send_count=0;
        // we pick the next member off the list in this send and send it out.
        // check the status of this send is still ok to send.
        $sql = "SELECT `send_id`,`status` FROM `"._DB_PREFIX."newsletter_send` WHERE `newsletter_id` = $newsletter_id AND `send_id` = $send_id";
        // $sql .= " AND `status` = " . _NEWSLETTER_STATUS_PENDING ;
        $status_check = array_shift(qa($sql,false));
        if($status_check['send_id']==$send_id){ //$status_check['status']==_NEWSLETTER_STATUS_PENDING && 
            // we have a go for sending to the next member!
            $sql = "SELECT * FROM `"._DB_PREFIX."newsletter_send_member` sm ";
            $sql .= " LEFT JOIN `"._DB_PREFIX."newsletter_member` m ON sm.newsletter_member_id = m.newsletter_member_id ";
            $sql .= " WHERE sm.`send_id` = $send_id ";
            if($retry_bounces){
                $sql .= " AND sm.`status` = " . _NEWSLETTER_STATUS_FAILED ." AND sm.newsletter_member_id > 0";
            }else{
                $sql .= " AND (sm.`status` = " . _NEWSLETTER_STATUS_NEW ." OR sm.`status` = " . _NEWSLETTER_STATUS_PAUSED.") AND sm.newsletter_member_id > 0";
            }
            $send_member_list = query($sql);
            if(mysql_num_rows($send_member_list)>0){
                while($send_member = mysql_fetch_assoc($send_member_list)){
                    if($send_count >= $send_limit)break;
                    // a quick lock checking on the member to ensure we don't send it twice
                    $sql = "UPDATE `"._DB_PREFIX."newsletter_send_member` SET `status` = "._NEWSLETTER_STATUS_PENDING." WHERE `send_id` = $send_id AND newsletter_member_id = '".(int)$send_member['newsletter_member_id']."'";
                    query($sql);
                    if(mysql_affected_rows()){
                        // we haev a lock on this member! ok to proceed.


                        $result['newsletter_member_id'] = (int)$send_member['newsletter_member_id'];
                        // check this member hasn't unsubscribed or any other issues with it...

                        $newsletter = self::get_newsletter($newsletter_id);

                        // both these two do their own "replacey" things. same inside email.
                        $fields = self::get_replace_fields($newsletter_id,$send_id,$send_member['newsletter_member_id']);
                        $html = self::render($newsletter_id,$send_id,$send_member['newsletter_member_id']);


                        $email = module_email::new_email();
                        $email->replace_values = $fields;
                        $email->set_bounce_address($newsletter['bounce_email']);
                        $email->set_from_manual($newsletter['from_email'],$newsletter['from_name']);
                        $email->set_to_manual($send_member['email'],$newsletter['to_name']);
                        $email->set_subject($newsletter['subject']);
                        // do we send images inline?
                        $email->set_html($html);

                        // do we attach anything else?
                        $files = module_file::get_files(array('owner_table' => 'newsletter_files', 'owner_id' => $newsletter_id,));
                        foreach ($files as $file_data){
                            if(is_file($file_data['file_path'])){
                                $email->AddAttachment($file_data['file_path'],$file_data['file_name']);
                            }
                        }
                        if($email->send()){
                            // it worked successfully!!
                        }
                        $result['error']=$email->error_text;
                        $result['email']=$send_member['email'];

                        switch($email->status){
                            case _MAIL_STATUS_OVER_QUOTA:
                                // over quota! pause this send until another time.
                                $sql = "UPDATE `"._DB_PREFIX."newsletter_send_member` SET `status` = "._NEWSLETTER_STATUS_PAUSED.", `sent_time` = 0, `bounce_time` = 0 WHERE `send_id` = $send_id AND newsletter_member_id = '".(int)$send_member['newsletter_member_id']."'";
                                query($sql);
                                break;
                            case _MAIL_STATUS_FAILED:
                                $sql = "UPDATE `"._DB_PREFIX."newsletter_send_member` SET `status` = "._NEWSLETTER_STATUS_FAILED.", `bounce_time` = '".time()."' WHERE `send_id` = $send_id AND newsletter_member_id = '".(int)$send_member['newsletter_member_id']."'";
                                query($sql);
                                break;
                            case _MAIL_STATUS_SENT:
                            default:
                                // update its status to sent.
                                // do this as a default catch so we don't end up sending duplicates for some weird mail class error.
                                $sql = "UPDATE `"._DB_PREFIX."newsletter_send_member` SET `status` = "._NEWSLETTER_STATUS_SENT.", `sent_time` = '".time()."', `bounce_time` = 0 WHERE `send_id` = $send_id AND newsletter_member_id = '".(int)$send_member['newsletter_member_id']."'";
                                query($sql);
                                break;
                        }
                        $result['status'] = $email->status; // so the calling script can handle whatever as well.

                        $send_count++;

                    }else{
                        // didn't get a lock on that member for some reason.
                        // probably because the cron job is runing in the background on this same send.

                    }
                }
            }else{
                // no more members left?
                // update this send to completd.
                $sql = "UPDATE `"._DB_PREFIX."newsletter_send` SET `status` = "._NEWSLETTER_STATUS_SENT.", finish_time = '".time()."' WHERE `send_id` = $send_id";
                query($sql);
            }
        }
        $result['send_count']=$send_count;
        return $result;
    }

    private static function member_data_callback($callback, $args) {
        if(is_callable($callback)){
            return call_user_func($callback,$args);
        }
        return array();
    }

    public static function update_member_data_for_send($send_id) {
        if(!isset($_SESSION['_updated_member_data_for_send'])){
            $_SESSION['_updated_member_data_for_send']=array();
        }
        if(isset($_SESSION['_updated_member_data_for_send'][$send_id]) && $_SESSION['_updated_member_data_for_send'][$send_id]+30 > time()){
            // stops it running too quickly - every 30 seconds should be great.
            return;
        }
        $_SESSION['_updated_member_data_for_send'][$send_id]=time();
        $send_members = module_newsletter::get_send_members($send_id);
        while($send_member = mysql_fetch_assoc($send_members)){
            if(isset($send_member['data_callback']) && strlen($send_member['data_callback'])>1 && isset($send_member['data_cache']) && strlen($send_member['data_cache'])>1){
                // check if the cache matches our stored data
                $new_cache = self::member_data_callback($send_member['data_callback'],json_decode($send_member['data_args'],true));
                if($new_cache){
                    $new_cache = serialize($new_cache);

                    if($new_cache != $send_member['data_cache']){
                        update_insert('newsletter_member_id',$send_member['newsletter_member_id'],'newsletter_member',array(
                                                                'data_cache'=>$new_cache,
                                                                                                                      ));
                    }
                }
            }
        }
    }

    public static function get_replace_fields($newsletter_id=false, $send_id=false, $newsletter_member_id=false) {
        $newsletter = self::get_newsletter($newsletter_id);
        $send_data = self::get_send($send_id);
        $send_time = isset($send_data['start_time']) ? $send_data['start_time'] : time();
        $replace = array();
        
        $replace['COMPANY_NAME'] = '';
        $replace['FIRST_NAME'] = '';
        $replace['LAST_NAME'] = '';
        $replace['EMAIL'] = '';

        $replace['DAY'] = date('d',$send_time);
        $replace['MONTH'] = date('m',$send_time);
        $replace['YEAR'] = date('Y',$send_time);
        $replace['DATE'] = print_date($send_time);
        $replace['UNSUBSCRIBE'] = self::unsubscribe_url($newsletter_id,$newsletter_member_id,$send_id);
        $replace['SUBJECT'] = $newsletter['subject'];
        // these will be overridden when the send goes out
        $use_newsletter_member = array();
        
        if($send_id && $send_data){

            $send_members = module_newsletter::get_send_members($send_id);
            // what other fields are we pulling in here?
            // hunt through the recipient listing and find the extra fields.
            $extra_fields = array();
            while($send_member = mysql_fetch_assoc($send_members)){
                $cache = array();
                if(isset($send_member['data_cache']) && strlen($send_member['data_cache'])>1){
                    $cache = unserialize($send_member['data_cache']);
                    if($cache){
                        // we have extra fields! woo!
                        foreach($cache as $key=>$val){
                            if(strpos($key,'_id'))continue; // skip ids for now.
                            $extra_fields[$key]=true;
                        }
                    }
                }
                if($newsletter_member_id && $send_member['newsletter_member_id'] == $newsletter_member_id){
                    $use_newsletter_member = $send_member;
                }
            }
            //ksort($extra_fields);
            foreach($extra_fields as $extra_field=>$tf){
                if($extra_field[0]=='_')continue;
                $replace[strtoupper($extra_field)] = '';
            }
            mysql_free_result($send_members);
            if($use_newsletter_member){

                    // we are using this members data in this array.
                // insert its values into the replace values.
                foreach($use_newsletter_member as $key=>$val){
                    if(isset($replace[strtoupper($key)])){
                        $replace[strtoupper($key)] = $val;
                    }
                }
                if(isset($use_newsletter_member['data_cache']) && strlen($use_newsletter_member['data_cache'])>1){
                    $cache = unserialize($use_newsletter_member['data_cache']);
                    if(is_array($cache)){
                        foreach($cache as $key=>$val){
                            if(isset($replace[strtoupper($key)])){
                                $replace[strtoupper($key)] = $val;
                            }
                        }
                    }
                }
            }
        }

        return $replace;
    }

    public static function unsubscribe_member($newsletter_id,$newsletter_member_id=0,$send_id=0){
        $sql = "UPDATE `"._DB_PREFIX."newsletter_member` SET unsubscribe_date = NOW(), unsubscribe_send_id = ".(int)$send_id." WHERE newsletter_member_id = ".(int)$newsletter_member_id." LIMIT 1";
        query($sql);
        $sql = "UPDATE `"._DB_PREFIX."newsletter_send_member` SET unsubscribe_time = '".time()."' WHERE newsletter_member_id = ".(int)$newsletter_member_id." AND send_id = '".(int)$send_id."' LIMIT 1";
        query($sql);
        // also list this as opened.
        $sql = "UPDATE `"._DB_PREFIX."newsletter_send_member` SET open_time = '".time()."' WHERE newsletter_member_id = ".(int)$newsletter_member_id." AND send_id = '".(int)$send_id."' AND open_time = 0 LIMIT 1";
        query($sql);
    }


    /** external stuff */
    public static function unsubscribe_url($newsletter_id,$newsletter_member_id=0,$unsubscribe_send_id=0,$h=false){
        if($h){
            return md5('s3cret7hash for newsletter attacments '._UCM_FOLDER.' '.$newsletter_id.'-'.$newsletter_member_id.'='.$unsubscribe_send_id);
        }
        return full_link(_EXTERNAL_TUNNEL.'?m=newsletter&h=unsubscribe&n='.$newsletter_id.'&nm='.$newsletter_member_id.'&s='.$unsubscribe_send_id.'&hash='.self::unsubscribe_url($newsletter_id,$newsletter_member_id,$unsubscribe_send_id,true));
    }

    public function external_hook($hook){
        switch($hook){
            case 'unsubscribe':
                $newsletter_id = (isset($_REQUEST['n'])) ? (int)$_REQUEST['n'] : false;
                $newsletter_member_id = (isset($_REQUEST['nm'])) ? (int)$_REQUEST['nm'] : false;
                $send_id = (isset($_REQUEST['s'])) ? (int)$_REQUEST['s'] : false;
                $hash = (isset($_REQUEST['hash'])) ? trim($_REQUEST['hash']) : false;
                if($newsletter_id && $hash){
                    $correct_hash = $this->unsubscribe_url($newsletter_id,$newsletter_member_id,$send_id,true);
                    if($correct_hash == $hash){

                        $member = $this->get_newsletter_member($newsletter_member_id);

                        if(isset($_REQUEST['confirm'])){

                            $this->unsubscribe_member($newsletter_id,$newsletter_member_id,$send_id);

                            module_template::init_template('newsletter_unsubscribe_done','<h2>Unsubscribe Successful</h2>
    <p>Email Address: <strong>{EMAIL}</strong> </p>
    <p>You have been successfully unsubscribed from the newsletter system.</p>
    ','Used when displaying the external view of an invoice.','code',array(
                                'EMAIL'=>'The users email address',
                            ));
                            // correct!
                            // load up the receipt template.
                            $template = module_template::get_template_by_key('newsletter_unsubscribe_done');
                            $data = $member;
                            $template->page_title = htmlspecialchars(_l('Unsubscribe'));
                            $template->assign_values($data);
                            echo $template->render('pretty_html');
                            exit;
                        }else{

                            module_template::init_template('newsletter_unsubscribe','<h2>Unsubscribe</h2>
    <p>Email Address: <strong>{EMAIL}</strong> </p>
    <p>Please <a href="{UNSUB_CONFIRM}">Click Here</a> to Unsubscribe.</p>
    ','Used when displaying the external view of an invoice.','code',array(
                                'EMAIL'=>'The users email address',
                                'UNSUB_CONFIRM'=>'The URL to confirm unsubscription',
                            ));
                            // correct!
                            // load up the receipt template.
                            $template = module_template::get_template_by_key('newsletter_unsubscribe');
                            $data = $member;
                            $data['unsub_confirm'] = $this->unsubscribe_url($newsletter_id,$newsletter_member_id,$send_id).'&confirm';
                            $template->page_title = htmlspecialchars(_l('Unsubscribe'));

                            $template->assign_values($data);
                            echo $template->render('pretty_html');
                            exit;
                        }
                    }
                }
                // show different templates.

                break;
        }
    }


    public function get_upgrade_sql($installed_version,$new_version){
        $sql = '';
        $installed_version = (string)$installed_version;
        $new_version = (string)$new_version;
        // special hack to see if an upgrade is needed.
        // this is just to test a different way of doing an upgrade with a bit of php
        // rather than just handing back sql.
        $sql = '';
        switch($installed_version){
            case '2':
                switch($new_version){
                    case '2.1':
                        $fields = get_fields('newsletter_member');
                        if(isset($fields['name']) && !isset($fields['company_name'])){
                            $sql .= 'ALTER TABLE  `'._DB_PREFIX.'newsletter_member` CHANGE  `name`  `company_name` VARCHAR( 70 ) NOT NULL DEFAULT \'\';';
                        }
                        $fields = get_fields('newsletter_send_member');
                        if(!isset($fields['unsubscribe_time'])){
                            $sql .= 'ALTER TABLE  `'._DB_PREFIX.'newsletter_send_member` ADD  `unsubscribe_time` INT( 11 ) NOT NULL DEFAULT \'0\' AFTER `bounce_time`;';
                        }
                        break;
                }
                break;
        }
        /*$options = array(
            '2' => array(
                '2.1' =>   'ALTER TABLE  `'._DB_PREFIX.'newsletter_member` CHANGE  `name`  `company_name` VARCHAR( 70 ) NOT NULL DEFAULT \'\';' .
                         'ALTER TABLE  `'._DB_PREFIX.'newsletter_send_member` ADD  `unsubscribe_time` INT( 11 ) NOT NULL DEFAULT \'0\' AFTER `bounce_time`;',
            ),

        );
        if(isset($options[$installed_version]) && isset($options[$installed_version][$new_version])){
            $sql = $options[$installed_version][$new_version];
        }*/

        $fields = get_fields('newsletter_member');
        if(!isset($fields['member_id'])){
            $sql .= 'ALTER TABLE  `'._DB_PREFIX.'newsletter_member` ADD `member_id` INT(11) NOT NULL DEFAULT \'0\';';
        }

        $fields = get_fields('newsletter_template');
        if(!isset($fields['date_created'])){
            $sql .= 'ALTER TABLE  `'._DB_PREFIX.'newsletter_template` ADD `date_created` DATE NULL ;';
        }
        if(!isset($fields['date_updated'])){
            $sql .= 'ALTER TABLE  `'._DB_PREFIX.'newsletter_template` ADD `date_updated` DATE NULL ;';
        }
        if(!isset($fields['content_url'])){
            $sql .= 'ALTER TABLE  `'._DB_PREFIX.'newsletter_template` ADD `content_url` varchar(255) NOT NULL DEFAULT \'\';';
        }
        if(!isset($fields['body'])){
            $sql .= 'ALTER TABLE  `'._DB_PREFIX.'newsletter_template` ADD `body` LONGTEXT NOT NULL DEFAULT \'\';';
        }
        return $sql;
    }

    public function get_install_sql(){
        ob_start();
        ?>

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_campaign` (
      `campaign_id` int(11) NOT NULL auto_increment,
      `campaign_name` varchar(255) NOT NULL,
      `public` INT NOT NULL DEFAULT '1',
      `date_created` date NOT NULL,
      `date_updated` date NOT NULL,
      PRIMARY KEY  (`campaign_id`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_campaign_member` (
      `campaign_id` int(11) NOT NULL,
      `newsletter_member_id` int(11) NOT NULL,
      `current_newsletter_id` int(11) NOT NULL,
      `join_time` int(11) NOT NULL,
      PRIMARY KEY  (`campaign_id`,`newsletter_member_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_campaign_newsletter` (
      `campaign_id` int(11) NOT NULL,
      `newsletter_id` int(11) NOT NULL,
      `send_time` int(11) NOT NULL,
      PRIMARY KEY  (`campaign_id`,`newsletter_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_image` (
      `image_id` int(11) NOT NULL auto_increment,
      `image_url` text NOT NULL,
      `date_created` date NOT NULL,
      `date_updated` date NOT NULL,
      PRIMARY KEY  (`image_id`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_link` (
      `link_id` int(11) NOT NULL auto_increment,
      `link_url` text NOT NULL,
      `date_created` date NOT NULL,
      `date_updated` date NOT NULL,
      PRIMARY KEY  (`link_id`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_link_open` (
      `link_open_id` int(11) NOT NULL auto_increment,
      `link_id` int(11) NOT NULL,
      `newsletter_member_id` int(11) NOT NULL,
      `send_id` int(11) NOT NULL,
      `timestamp` int(11) NOT NULL,
      PRIMARY KEY  (`link_open_id`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_member` (
      `newsletter_member_id` int(11) NOT NULL auto_increment,
      `member_id` int(11) NOT NULL DEFAULT '0',
      `company_name` varchar(255) NOT NULL DEFAULT '',
      `first_name` varchar(255) NOT NULL DEFAULT '',
      `last_name` varchar(255) NOT NULL DEFAULT '',
      `email` varchar(255) NOT NULL DEFAULT '',
      `mobile` varchar(255) NOT NULL DEFAULT '',
      `join_date` date NULL,
      `ip_address` varchar(15) NOT NULL DEFAULT '',
      `receive_email` char(1) NOT NULL default '1',
      `receive_sms` char(1) NOT NULL default '1',
      `unsubscribe_date` date NULL,
      `unsubscribe_send_id` int(11) NOT NULL DEFAULT '0',
      `bounce_count` int(11) NOT NULL DEFAULT '0',
      `data_callback` VARCHAR( 255 ) NOT NULL DEFAULT  '' COMMENT  'will run this callback just before sending mail to get updated details and any more fields from it.',
      `data_args` VARCHAR( 255 ) NOT NULL DEFAULT  '',
      `data_cache` LONGTEXT NOT NULL DEFAULT  '',
      `date_created` date NULL,
      `date_updated` date NULL,
      PRIMARY KEY  (`newsletter_member_id`),
      KEY `join_date` (`join_date`),
      KEY `mobile` (`mobile`),
      KEY `email` (`email`),
      KEY `first_name` (`first_name`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_template` (
      `newsletter_template_id` int(11) NOT NULL auto_increment,
      `newsletter_template_name` varchar(255) character set utf8 NOT NULL,
      `content_url` varchar(255) NOT NULL,
      `body` longtext NOT NULL,
      `date_created` datetime NOT NULL,
      `date_updated` datetime NOT NULL,
      `create_user_id` int(11) NOT NULL,
      `update_user_id` int(11) NOT NULL,
      PRIMARY KEY  (`newsletter_template_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter` (
      `newsletter_id` int(11) NOT NULL auto_increment,
      `create_date` date NOT NULL,
      `last_sent` int(11) NOT NULL,
      `newsletter_template_id` int(11) NOT NULL,
      `subject` varchar(255) character set utf8 NOT NULL,
      `from_name` varchar(255) character set utf8 NOT NULL,
      `from_email` varchar(255) character set utf8 NOT NULL,
      `to_name` VARCHAR( 255 ) NOT NULL DEFAULT  '',
      `content` text character set utf8 NOT NULL,
      `bounce_email` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL,
      `extra` text collate utf8_bin NOT NULL,
      `date_created` date NOT NULL,
      `date_updated` date NOT NULL,
      PRIMARY KEY  (`newsletter_id`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_content` (
      `newsletter_content_id` int(11) NOT NULL auto_increment,
      `newsletter_id` int(11) NOT NULL,
      `group_title` varchar(255) NOT NULL,
      `position` float NOT NULL,
      `title` varchar(255) NOT NULL,
      `content_full` text NOT NULL,
      `content_summary` text NOT NULL,
      `image_thumb` varchar(255) NOT NULL,
      `image_main` varchar(255) NOT NULL,
      `create_date` datetime NOT NULL,
      `extra` text NOT NULL,
      `date_created` date NOT NULL,
      `date_updated` date NOT NULL,
      PRIMARY KEY  (`newsletter_content_id`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_send_member` (
      `send_id` int(11) NOT NULL,
      `newsletter_member_id` int(11) NOT NULL,
      `sent_time` int(11) NOT NULL DEFAULT '0',
      `status` int(11) NOT NULL DEFAULT '0',
      `open_time` int(11) NOT NULL DEFAULT '0',
      `bounce_time` int(11) NOT NULL DEFAULT '0',
      `unsubscribe_time` int(11) NOT NULL DEFAULT '0',
      PRIMARY KEY  (`send_id`,`newsletter_member_id`),
      KEY `open_time` (`open_time`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_send` (
      `send_id` int(11) NOT NULL auto_increment,
      `start_time` int(11) NOT NULL DEFAULT '0',
      `status` int(11) NOT NULL DEFAULT '0',
      `finish_time` int(11) NOT NULL DEFAULT '0',
      `newsletter_id` int(11) NOT NULL DEFAULT '0',
      `campaign_id` int(11) NOT NULL DEFAULT '0',
      `cache` longtext NOT NULL DEFAULT '',
      `subject` varchar(255) NOT NULL DEFAULT '',
      `allow_duplicates` tinyint(4) NOT NULL DEFAULT '0',
      `date_created` date NOT NULL,
      `date_updated` date NULL,
      PRIMARY KEY  (`send_id`),
      KEY `newsletter_id` (`newsletter_id`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_sync` (
      `sync_id` int(11) NOT NULL auto_increment,
      `sync_name` varchar(50) NOT NULL,
      `edit_url` varchar(255) NOT NULL,
      `db_username` varchar(40) NOT NULL,
      `db_password` varchar(40) NOT NULL,
      `db_host` varchar(40) NOT NULL,
      `db_name` varchar(40) NOT NULL,
      `db_table` varchar(40) NOT NULL,
      `db_table_key` varchar(40) NOT NULL,
      `db_table_email_key` varchar(40) NOT NULL,
      `db_table_fname_key` varchar(40) NOT NULL,
      `db_table_lname_key` varchar(40) NOT NULL,
      `callback_function` varchar(60) NOT NULL,
      `last_sync` int(11) NOT NULL,
      `date_created` date NOT NULL,
      `date_updated` date NOT NULL,
      PRIMARY KEY  (`sync_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_sync_group` (
      `sync_id` int(11) NOT NULL,
      `group_id` int(11) NOT NULL,
      PRIMARY KEY  (`sync_id`,`group_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

    CREATE TABLE IF NOT EXISTS `<?php echo _DB_PREFIX;?>newsletter_sync_member` (
      `sync_id` int(11) NOT NULL,
      `sync_unique_id` int(11) NOT NULL,
      `newsletter_member_id` int(11) NOT NULL,
      PRIMARY KEY  (`sync_id`,`sync_unique_id`,`newsletter_member_id`),
      KEY `sync_id` (`sync_id`),
      KEY `sync_unique_id` (`sync_unique_id`),
      KEY `newsletter_member_id` (`newsletter_member_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


        <?php
        return ob_get_clean();
    }

}