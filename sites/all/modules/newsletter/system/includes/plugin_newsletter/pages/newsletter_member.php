<?php

$module->page_title = _l('Member Statistics');

$member_id = isset($_REQUEST['member_id']) ? (int)$_REQUEST['member_id'] : false;
$newsletter_member_id = isset($_REQUEST['newsletter_member_id']) ? (int)$_REQUEST['newsletter_member_id'] : false;
if($member_id){
    $member_data = module_newsletter::get_member($member_id);
}else if($newsletter_member_id){
    $member_data = module_newsletter::get_newsletter_member($newsletter_member_id);
}else{
    set_error('Sorry no member id specified');
    redirect_browser(module_member::link_list(0));
}
print_heading('Member Newsletter History/Statistics');

?>

<table width="100%" class="tableclass tableclass_full">
        <tbody>
        <tr>
            <td width="70%" valign="top">
                <?php print_heading(array(
                            'type'=>'h3',
                            'title'=>'Newsletters Sent to this Member',
                )); ?>
                <table class="tableclass tableclass_rows tableclass_full">
                    <thead>
                    <tr>
                        <th><?php _e('Newsletter Subject');?></th>
                        <th><?php _e('Sent Date');?></th>
                        <th><?php _e('Opened');?></th>
                        <th><?php _e('Bounced');?></th>
                        <th><?php _e('Status');?></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        // list all members from thsi send.
                        $send_members = module_newsletter::get_member_sends($member_id);
                        $open_count=0;
                        $x=0;
                        $send_members_count = mysql_num_rows($send_members);
                        $sent_to_members = 0;
                        while($send_member = mysql_fetch_assoc($send_members)){
                            if($send_member['open_time']){
                                $open_count++;
                            }
                            ?>
                            <tr class="<?php echo $x++%2?'odd':'even';?>" id="newsletter_member_<?php echo $send_member['newsletter_member_id'];?>">
                                <td>
                                    <?php echo module_newsletter::link_open($send_member['newsletter_id'],true); ?>
                                </td>
                                <td class="sent_time"><?php echo $send_member['sent_time'] ? print_date($send_member['sent_time'],true) : _l('Not Yet');?></td>
                                <td><?php echo $send_member['open_time'] ? print_date($send_member['open_time'],true) : _l('Not Yet');?></td>
                                <td><?php echo $send_member['bounce_time'] ? print_date($send_member['bounce_time'],true) : _l('No');?></td>
                                <td class="status">
                                    <?php
                                    switch($send_member['status']){
                                        case _NEWSLETTER_STATUS_NEW:
                                            // hasnt been processed yet
                                            break;
                                        case _NEWSLETTER_STATUS_SENT;
                                            // sent!
                                            _e('sent');
                                            break;
                                        case _NEWSLETTER_STATUS_PENDING;
                                        case _NEWSLETTER_STATUS_PAUSED;
                                            // pending send..
                                            _e('pending');
                                            break;
                                        case _NEWSLETTER_STATUS_FAILED:
                                            _e('failed');
                                            break;
                                        default:
                                            echo '?';
                                    }?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <?php
                // show any probmeatic ones here (blocked due to bounce limit reached, or unsubscribed, or wont receive again due to previous receive.
                ?>
            </td>
            <td width="50%" valign="top">

                <?php
                foreach($member_data as $member){
                print_heading(array(
                            'type'=>'h3',
                            'title'=>'Member Statistics',
                )); ?>
                <table class="tableclass tableclass_form tableclass_full">
                    <tbody>
                    <tr>
                        <th class="width1"><?php _e('Email');?></th>
                        <td>
                            <?php echo htmlspecialchars($member['email']); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="width1"><?php _e('Member Subscribed');?></th>
                        <td>
                            <?php echo print_date($member['join_date']); ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php _e('Member Unsubscribed');?></th>
                        <td>
                            <?php echo print_date($member['unsubscribe_date']); ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php _e('Total opened');?></th>
                        <td>
                            <?php echo _l('%s of %s',$open_count,$send_members_count);?>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <?php } ?>
            </td>
        </tr>
        </tbody>
    </table>