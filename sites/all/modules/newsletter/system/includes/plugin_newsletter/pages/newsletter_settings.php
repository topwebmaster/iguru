<?php
$module->page_title = _l('Newsletter Settings');
print_heading('Newsletter Settings');




$settings = array(
    array(
        'key'=>'newsletter_default_from_name',
        'default'=>module_config::c('admin_system_name'),
        'type'=>'text',
        'description'=>'What sender name your newsletters will come from',
    ),
    array(
        'key'=>'newsletter_default_from_email',
        'default'=>module_config::c('admin_email_address'),
        'type'=>'text',
        'description'=>'What email address your newsletters will come from',
    ),
    array(
        'key'=>'newsletter_default_bounce',
        'default'=>module_config::c('admin_email_address'),
        'type'=>'text',
        'description'=>'What email address your bounced newsletters will go to (setup a new email address for this, eg: bounce@yourwebsite.com)',
    ),
);

module_config::print_settings_form(
    $settings
);

?>