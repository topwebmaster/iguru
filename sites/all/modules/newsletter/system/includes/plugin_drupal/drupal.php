<?php


class module_drupal extends module_base{

    public static function can_i($actions,$name=false,$category=false,$module=false){
        if(!$module)$module=__CLASS__;
        return parent::can_i($actions,$name,$category,$module);
    }
	public static function get_class() {
        return __CLASS__;
    }

    public function __construct(){
        // connect to the drupal db.
        // this happens after we do the normal UCM db_connect

    }

	public function init(){
		$this->module_name = "drupal";
		$this->module_position = -1; // before all others?


        // load in our drupal key file

        $drupal_key_file = 'temp/drupalkey.php';
        @include($drupal_key_file);
        if(isset($_REQUEST['drupal_key'])){
            $_SESSION['drupal_key'] = $_REQUEST['drupal_key'];
        }
        if(_UCM_INSTALLED && !isset($drupal_key ) || !$drupal_key || !$_SESSION['drupal_key'] || $_SESSION['drupal_key'] != $drupal_key){
            echo "Failed to find drupal key file. Send FTP details if you need assistance: http://support.dtbaker.com.au/support-ticket.html";
            exit;
        }

        if(!module_security::is_logged_in()){
            if($_SESSION['drupal_key'] == $drupal_key){
                // they are accessing this from drupal. wee!
                // hacky but it works.
                if(!isset($_REQUEST['auto_login'])){
                    redirect_browser("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].(strpos($_SERVER['REQUEST_URI'],'?')===false?'?':'&')."auto_login=".module_security::get_auto_login_string(1));
                }
            }
        }

	}



}

