<?php

$noredirect = true;
header( 'Content-Type: text/html; charset=UTF-8' );
require_once('init.php');

if(getcred()){
    $search_text = isset($_REQUEST['ajax_search_text']) ? trim(urldecode($_REQUEST['ajax_search_text'])) : false;
	if($search_text){
		$search_results = array();
		foreach($plugins as $plugin_name => &$plugin){
			$search_results = array_merge( $search_results , $plugin->ajax_search($search_text,$db) );
		}
        if(count($search_results)){
            echo '<ul>';
            foreach($search_results as $r){
                echo '<li>' . $r . '</li>';
            }
            echo '</ul>';
        }
	}else{
		echo '';
	}
	exit;
}

