<?php
$update_url = base_path().drupal_get_path('module', _NEWSLETTER_MODULE_NAME).'/system/ext.php?m=newsletter&h=update';

?>

    <?php /*if(variable_get('newsletter_sidebar_type',0)){ ?>
    <table width="100%" class="sidebar_newsletter">
        <tbody>
            <tr>
                <th>
                    <label for="newsletter_email"><?php echo t('Email:');?></label>
                </th>
                <td>
                    <input type="text" name="newsletter[email]" id="newsletter_email" value="">
                </td>
            </tr>
            <?php if(variable_get('newsletter_show_first_name', 1)){ ?>
            <tr>
                <th>
                    <label for="newsletter_first_name"><?php echo t('First Name:');?></label>
                </th>
                <td>
                   <input type="text" name="newsletter[first_name]" id="newsletter_first_name" value="">
                </td>
            </tr>
            <?php } ?>
            <?php if(variable_get('newsletter_show_last_name', 1)){ ?>
            <tr>
                <th>
                    <label for="newsletter_last_name"><?php echo t('Last Name:');?></label>
                </th>
                <td>
                   <input type="text" name="newsletter[last_name]" id="newsletter_last_name" value="">
                </td>
            </tr>
            <?php } ?>
            <?php if(variable_get('newsletter_show_business_name', 1)){ ?>
            <tr>
                <th>
                    <label for="newsletter_business_name"><?php echo t('Business Name:');?></label>
                </th>
                <td>
                   <input type="text" name="newsletter[business]" id="newsletter_business_name" value="">
                </td>
            </tr>
            <?php } ?>
            <?php if(variable_get('newsletter_show_groups', 1)){ ?>
                <tr>
                    <th colspan="2">
                        <?php echo t('Subscriptions:'); ?>
                    </th>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php
                        // do a sql query on our newsletter system.
                        $table_name = 'ucm_group';
                        $result = db_query_range('SELECT * FROM {'.$table_name.'} g WHERE g.owner_table = :table', 0, 500, array(':table' => 'newsletter_subscription'));
                        foreach ($result as $record) {
                            // Perform operations on $node->title, etc. here.
                            ?>
                            <input type="checkbox" name="newsletter[group][<?php echo $record->group_id;?>]">
                                <?php echo htmlspecialchars($record->name);?> <br/>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
            <?php } ?>
        <tr>
            <td colspan="2" align="center">
                <input type="submit" name="submit" value="<?php echo t('Subscribe');?>">
            </td>
        </tr>
    <?php if(variable_get('newsletter_show_update_link', 1)){ ?>
    <tr>
        <td colspan="2" align="center">
            <a href="<?php echo $update_url;?>"><?php echo t('Update Your Subscription');?></a>
        </td>
    </tr>
    <?php } ?>

        </tbody>
    </table>
    <?php }else{*/

        function newsletter_subscribe_form($form,&$form_state){
            /*$form['newsletter_subscribe'] = array(
                '#type' => 'fieldset',
                '#title' => t('Subscribe'),
            );*/
            $form['member[email]'] = array(
                '#type' => 'textfield',
                '#title' => t('Email'),
                //'#title_display' => 'invisible',
                '#size' => 27,
                '#default_value' => '',
                '#attributes' => array('title' => t('Enter your email address.')),
            );
            if(variable_get('newsletter_show_first_name', 1)){
                $form['member[first_name]'] = array(
                    '#type' => 'textfield',
                    '#title' => t('First Name'),
                    //'#title_display' => 'invisible',
                    '#size' => 27,
                    '#default_value' => '',
                    '#attributes' => array('title' => t('Enter your first name.')),
                );
            }
            if(variable_get('newsletter_show_last_name', 1)){
                $form['member[last_name]'] = array(
                    '#type' => 'textfield',
                    '#title' => t('Last Name'),
                    //'#title_display' => 'invisible',
                    '#size' => 27,
                    '#default_value' => '',
                    '#attributes' => array('title' => t('Enter your last name.')),
                );
            }
            if(variable_get('newsletter_show_business_name', 1)){
                $form['member[business]'] = array(
                    '#type' => 'textfield',
                    '#title' => t('Business Name'),
                    //'#title_display' => 'invisible',
                    '#size' => 27,
                    '#default_value' => '',
                    '#attributes' => array('title' => t('Enter your business name.')),
                );
            }
            if(variable_get('newsletter_show_groups', 1)){
                $form['newsletter_subscribe_groups'] = array(
                    '#type' => 'fieldset',
                    '#title' => t('Subscriptions'),
                );
                $table_name = 'ucm_group';
                try{
                $result = db_query_range('SELECT * FROM {'.$table_name.'} g WHERE g.owner_table = :table', 0, 500, array(':table' => 'newsletter_subscription'));
                foreach ($result as $record) {
                    $form['newsletter_subscribe_groups']['member[group]['.$record->group_id.']'] = array(
                        '#type' => 'checkbox',
                        '#title' => $record->name,
                        '#value' => '1',
                        '#default_value' => 'checked',
                    );
                }
                }catch(Exception $e){

                }
            }
            $subscribe_url = base_path().drupal_get_path('module', _NEWSLETTER_MODULE_NAME).'/system/ext.php?m=member&h=subscribe';
            $form['#action'] = $subscribe_url;
            $form['actions'] = array('#type' => 'actions');
            $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Subscribe'));
            $form['#submit'][] = 'newsletter_subscribe_form_submit';
            return $form;
        }

        function newsletter_subscribe_form_render() {
            $form = drupal_get_form('newsletter_subscribe_form');
            return $form;
        }
    $form = newsletter_subscribe_form_render();
    echo drupal_render($form);
        ?>

    <?php if(variable_get('newsletter_show_update_link', 0)){ ?>
        <div>
                <a href="<?php echo $update_url;?>"><?php echo t('Update Your Subscription');?></a>
        </div>
    <?php } ?>

    <?php /* } ?>
</form>
*/ ?>