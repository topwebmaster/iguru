<?php


/**
 * The administration settings form
 */

function newsletter_setting_form($form, &$form_state) {

    $drupal_key = newsletter_get_drupal_key();
    $url = base_path().drupal_get_path('module', _NEWSLETTER_MODULE_NAME).'/system/?m[0]=config&p[0]=config_admin&m[1]=newsletter&p[1]=newsletter_settings&drupal_key='.$drupal_key;


    $form['sidebar'] = array(
        '#type' => 'fieldset',
        '#title' => t('Newsletter Sidebar Settings'),
    );

   /* $form['sidebar']['newsletter_sidebar_type'] = array(
        '#type'          => 'select',
        '#title'         => t("Layout Type"),
        '#description'   => t("Choose one of the layout types, test both to see which you like."),
        '#options'       => array(
            0 => t('Div Layout'),
            1 => t('Table Layout'),
        ),
        '#default_value' => variable_get('newsletter_sidebar_type', 0),
        '#required'      => TRUE,
    );*/
    $form['sidebar']['newsletter_show_first_name'] = array(
        '#type'          => 'select',
        '#title'         => t("Ask for First Name"),
        '#description'   => t("Display the First Name box in the drupal sidebar"),
        '#options'       => array(
            0 => t('No'),
            1 => t('Yes'),
        ),
        '#default_value' => variable_get('newsletter_show_first_name', 1),
        '#required'      => TRUE,
    );
    $form['sidebar']['newsletter_show_last_name'] = array(
        '#type'          => 'select',
        '#title'         => t("Ask for Last Name"),
        '#description'   => t("Display the Last Name box in the drupal sidebar"),
        '#options'       => array(
            0 => t('No'),
            1 => t('Yes'),
        ),
        '#default_value' => variable_get('newsletter_show_last_name', 1),
        '#required'      => TRUE,
    );
    $form['sidebar']['newsletter_show_business_name'] = array(
        '#type'          => 'select',
        '#title'         => t("Ask for Business Name"),
        '#description'   => t("Display the Business Name box in the drupal sidebar"),
        '#options'       => array(
            0 => t('No'),
            1 => t('Yes'),
        ),
        '#default_value' => variable_get('newsletter_show_business_name', 1),
        '#required'      => TRUE,
    );
    $form['sidebar']['newsletter_show_groups'] = array(
        '#type'          => 'select',
        '#title'         => t("Ask for Subscription Groups"),
        '#description'   => t("Allows subscribers to choose what to be subscribed to"),
        '#options'       => array(
            0 => t('No'),
            1 => t('Yes'),
        ),
        '#default_value' => variable_get('newsletter_show_groups', 1),
        '#required'      => TRUE,
    );
    /*$form['sidebar']['newsletter_show_update_link'] = array(
        '#type'          => 'select',
        '#title'         => t("Show Update Details Link"),
        '#description'   => t("Allows customers to easily update their details."),
        '#options'       => array(
            0 => t('No'),
            1 => t('Yes'),
        ),
        '#default_value' => variable_get('newsletter_show_update_link', 1),
        '#required'      => TRUE,
    );*/
    $form['more_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('More Settings'),
        '#value' => sprintf(t('You can view more settings in the newsletter system. Please <a href="%s" target="_blank">click here</a> to view those settings'),$url),
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),
    );
    return $form;
}
function newsletter_setting_form_validate($form, &$form_state) {
    // Validation logic.
}
function newsletter_setting_form_submit($form, &$form_state) {
    // Submission logic.
  //  variable_set('newsletter_sidebar_type',$form['sidebar']['newsletter_sidebar_type']['#value']);
    variable_set('newsletter_show_first_name',$form['sidebar']['newsletter_show_first_name']['#value']);
    variable_set('newsletter_show_last_name',$form['sidebar']['newsletter_show_last_name']['#value']);
    variable_set('newsletter_show_business_name',$form['sidebar']['newsletter_show_business_name']['#value']);
    variable_set('newsletter_show_groups',$form['sidebar']['newsletter_show_groups']['#value']);
    variable_set('newsletter_show_update_link',0);
    //variable_set('newsletter_show_update_link',$form['sidebar']['newsletter_show_update_link']['#value']);
}
function newsletter_settings() {
    $form = drupal_get_form('newsletter_setting_form');
    //$form .= "test html (link to more settings)";
    return $form;
}