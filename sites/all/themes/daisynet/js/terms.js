(function ($) {
  Drupal.behaviors.daisyTerms = {
    attach: function(context) {
      $('div[id^=edit-term-user]:not(#edit-term-user-age-category) a', context).once('userCategory').click(function(event) {
      	//клик по главной категории
      	if ( $(this).parent().attr('id') == 'edit-term-user-categories-front' ) {
      		var selectedTerm = $(this).attr('rel').match(/[0-9]{1,4}$/);
      		//если выбрана
      		if($(this).hasClass('selected')) {
      			//удаляем класс выбора главной категории
      			$(this).removeClass('selected')
      			//удаляем класс выбора доп. категории, удаляем класс readonly
	            $('div[id^=edit-term-user] a[rel=' + selectedTerm + ']').removeClass('selected').removeClass('readonly')
	            //удаляем класс выбора и прячем субкатегорию, относящуюся к категории  
            	$('div[id^=edit-term-user] a[rel^=' + selectedTerm + '_]').addClass('hide-term').removeClass('selected').removeClass('show-term');
	            //снимаем отметку с radio 
	            $('#edit-field-webinar-category-front-und-'+ selectedTerm).attr('checked','')
			//если не выбрана
      		} else {
      			//гл.категории добавляется класс выбора, у остальных гл. удаляется
      			$(this).addClass('selected').siblings('a').removeClass('selected')
      			//прячем подкатегории выбраной категории
      			var readonly = $('div[id^=edit-term-user] a.readonly').attr('rel')
      			$('div[id^=edit-term-user] a[rel^=' + readonly + '_]').addClass('hide-term').removeClass('show-term').removeClass('selected');
      			//доп. категории добавляем класс выбора, класс readonly, у предидущей категории(равной гл.) удаляем класс выбора и readonly 
      			$('div[id^=edit-term-user] a[rel=' + selectedTerm + ']').addClass('selected').addClass('readonly').siblings('a.readonly').removeClass('readonly').removeClass('selected')
      			//добавляем отметку на radio
      			$('#edit-field-webinar-category-front-und-'+ selectedTerm).attr('checked','checked')
      			//перебор всех доп. категорий
      			$('div#edit-term-user-categories a').each(function(){
      				//если есть класс выбора
      				if ( $(this).hasClass('selected') ) {
      					//делаем субкатегорию видимой
      					$('div[id^=edit-term-user] a[rel^=' + $(this).attr('rel') + '_]').removeClass('hide-term').addClass('show-term');
      				//если нет
      				}else{
      					//делаем субкатегорию невидимой
      					$('div[id^=edit-term-user] a[rel^=' + $(this).attr('rel') + '_]').addClass('hide-term').removeClass('show-term');
      				}
      			})
      		}
      		
      	//клик по не главной категории
      	} else {
      		//если она не только для чтения( выбрана главной )
      		if (!$(this).hasClass('readonly')) {
      			//перекулючаем класс выбора на обратный
	      		$(this).toggleClass('selected');
	      		//запоминаем id
		        var selectedTerm = $(this).attr('rel').match(/[0-9]{1,4}$/);
		        //если уже выбрана
		        if($(this).hasClass('selected')) {
		        	//показываем подкатегорию 
		          $('div[id^=edit-term-user] a[rel^=' + selectedTerm + '_]').removeClass('hide-term').addClass('show-term');
		        }
		        //если не выбрана
		        else {
		          //проходим все подкатегории данной категории
		          $('div[id^=edit-term-user] a[rel^=' + selectedTerm + '_]').each(function(){
		          	//запоминаем id подкатегории
		            var selectedTerm2 = $(this).attr('rel').match(/[0-9]{1,4}$/);
		            //прячем кадую подкатегорию
		            $(this).addClass('hide-term').removeClass('selected').removeClass('show-term');
		            //прячем каждую под-под-категорию (??)
		            $('div[id^=edit-term-user] a[rel^=' + selectedTerm2 + '_]').addClass('hide-term').removeClass('selected').removeClass('show-term');
		          });
		        }
		        
		        
		        
		        
		        
		    }
      	}
      			
		        if($('#edit-field-webinar-category-und').length) {
		          var select_id = '#edit-field-webinar-category-und';
		        }
		        else {
		          var select_id = '#edit-field-user-category-und';
		        }
		        
		        $(select_id+' option:selected').removeAttr('selected');
		        $('div[id^=edit-term-user]:not(#edit-term-user-age-category) a.selected').each(function(){
		          $(select_id + ' option[value=' + $(this).attr('rel').match(/[0-9]{1,4}$/) + ']').attr('selected', 'selected');
		        });
		        $('div[id^=edit-term-user]:not(#edit-term-user-age-category)').each(function(){
		          if($(this).find('a.show-term').length) {
		            $(this).show();
		          }
		          else {
		            $(this).hide();
		          }
		        });
		        
      	event.preventDefault();
       
      });

      $('#edit-field-user-category-und option:selected').each(function(){
        $('div[id^=edit-term-user]:not(#edit-term-user-age-category) a[rel$=' + $(this).val() + ']').removeClass('hide-term').addClass('show-term').addClass('selected');
        $('div[id^=edit-term-user]:not(#edit-term-user-age-category) a[rel^=' + $(this).val() + '_]').removeClass('hide-term').addClass('show-term');
      });
      
      $('#edit-field-webinar-category-front-und input:checked').each(function() {
      	var selectedTerm = $(this).val();
      	$('#edit-term-user-categories-front a[rel=' + selectedTerm + ']').removeClass('hide-term').addClass('show-term').addClass('selected')
      	$('div[id^=edit-term-user] a[rel^=' + selectedTerm + '_]').removeClass('hide-term').addClass('show-term');
		$('div[id^=edit-term-user] a[rel=' + selectedTerm + ']').addClass('readonly').addClass('selected');
      	
      })

      $('div[id^=edit-term-user]:not(#edit-term-user-age-category)').each(function(){
        if($(this).find('a.show-term').length) {
          $(this).show();
        }
        else {
          $(this).hide();
        }
      });

      $('#edit-field-user-age-category-und option:selected').each(function(){
        $('#edit-term-user-age-category a[rel=' + $(this).val() + ']').removeClass('hide-term').addClass('show-term').addClass('selected');
      });

      $('#edit-term-user-age-category a', context).once('userCategory').click(function(event) {
        $(this).toggleClass('selected');
        $('#edit-field-user-age-category-und option:selected').removeAttr('selected');
        $('#edit-term-user-age-category a.selected').each(function(){
          $('#edit-field-user-age-category-und option[value=' + $(this).attr('rel') + ']').attr('selected', 'selected');
        });
        event.preventDefault();
      });
    }
  };
})(jQuery);