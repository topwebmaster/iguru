$ = jQuery;

jQuery(document).ready(function($) {
    if($.browser.msie && parseInt($.browser.version) <= 7) {
    var zIndexNumber = 1000;
    $('.friend-user-wrapper div').each(function() {
      $(this).css('zIndex', zIndexNumber);
      zIndexNumber -= 10;
    });

    $('.header-top li.expanded ul a').hover(function(){
      $(this).addClass('topMenuHover');
    }, function(){
      $(this).removeClass('topMenuHover');
    })
  }

  $('nav > ul > li[class^=tid]').hover(function() {
    $(this).find('.submenu').show();
  },
  function(){
    $(this).find('.submenu').hide();
  });

  $('nav > ul.menu > li > a, .categories-squares li a, #block-views-categories-block li a').click(function(event) {
    event.preventDefault();
    var cat = $(this).attr('href').split('/')
    //alert(link[link.length-1])
    if ( Drupal.settings.arg[0] == 'catalog' ) {
      category_selection(cat[cat.length-1])
    } else {
      window.location.href = Drupal.settings.basePath+'catalog?cat='+cat[cat.length-1];
    }
  })

  $('nav > ul.menu > li > ul > li a').click(function(event) {
    event.preventDefault();
    var cat = $(this).parent().parent().siblings('a').attr('href').split('/')
    var subcat = $(this).attr('href').split('/')
    //alert(link[link.length-1])
    if ( Drupal.settings.arg[0] == 'catalog' ) {
      category_selection(cat[cat.length-1], subcat[subcat.length-1])
    } else {
      window.location.href = Drupal.settings.basePath+'catalog?cat='+cat[cat.length-1]+'&subcat='+subcat[subcat.length-1];
    }
  })

  $('.lang-selected').click(function(event){
    $('.lang-switch').toggle();
    event.preventDefault();
  });

  $('.user-menu .user-name').click(function(event){
    $('.user-menu ul').toggle();
    event.preventDefault();
  });

  $(document).click(function(event) {
    if($(event.target).hasClass("get-started")){
      return;
    }
    if($(event.target).parents('.lang').length == 0 && $('.lang-switch:visible').length) {
      $('.lang-switch').hide();
    }
    if($(event.target).parents('.user-menu').length == 0 && $('.user-menu ul:visible').length) {
      $('.user-menu ul').hide();
    }
    if($(event.target).parents('.friend-user-wrapper').length == 0 && $('.friend-user-info:visible').length) {
      $('.friend-user-info').hide();
    }

    if($(event.target).parents('.user_info_block').length == 0 && $('.user_info_block ul.menu:visible').length) {
      $('.user_info_block ul.menu').hide();
    }

    if($(event.target).parents('#block-block-15').length == 0 && !$('#block-block-15').hasClass('hidden') && ! ($(event.target).hasClass('search_button') || $(event.target).is('#block-block-15') )) {
      $('#block-block-15').addClass('hidden');
    }
    if($(event.target).parents('.register_block').length == 0 && $('.register_block:visible').length && $(event.target).is('#top_logout .choose_daisy') && $(event.target).is('#top_logout .login') ) {
      $('.register_block').hide();
    }
  });

  $('.user_info_block .user-icon').click(function(){
    $('.user_info_block ul.menu').toggle()
  })

  $('.choose_daisy').click(function() {
    $('.register_block').toggleClass('hidden')
    $('#block-user-login').hide()
    $('#block-formblock-user-register').show()
  })

  $('#top_logout .login').click(function() {
    $('.register_block').toggleClass('hidden')
    $('#block-user-login').show()
    $('#block-formblock-user-register').hide()
  })

  //настройки datepicker для создания вебинара
  if (jQuery('#edit-field-webinar-date-und-0-value-datepicker-popup-0').length > 0) {
    jQuery('#edit-field-webinar-date-und-0-value-datepicker-popup-0').attr('readonly','readonly')


    //текущая дата
    var newDate = new Date();


  //дата, указанная в вебинаре
  var cd = jQuery('#edit-field-webinar-date-und-0-value-datepicker-popup-0').val().split('/')
  var webinarDate = new Date(cd[2]+'-'+cd[1]+'-'+cd[0]);
  var webinarEndDate = webinarDate;
  var day = webinarDate.getDate();
  webinarDate = webinarDate.setDate(day-1)
  webinarDate = new Date(webinarDate);

  var webinarEndDate = new Date(webinarEndDate.setDate(day))

  //если вебинар начнется менее чем через 3 дня

  //alert('today:'+newDate+'<br>  webinarDate:'+webinarDate+'<br>   webinarFutureDate:'+webinarEndDate)

  if ( (newDate.getTime() > webinarDate.getTime()) && (newDate.getTime() < webinarEndDate.getTime())) {

    $('#edit-title').attr('readonly','readonly');
    $('#edit-field-webinar-date-und-0-value-timeEntry-popup-1').hide().parent().append('<input type="text" readonly value="'+$('#edit-field-webinar-date-und-0-value-timeEntry-popup-1').val()+'" />');
    $('#edit-field-webinar-date-und-0-value-datepicker-popup-0').hide().parent().append('<input type="text" readonly value="'+$('#edit-field-webinar-date-und-0-value-datepicker-popup-0').val()+'" />');

  } else {

    jQuery('#edit-field-webinar-date-und-0-value-datepicker-popup-0').datepicker({ minDate: 1, dateFormat: "dd/mm/yy"})
    var time = '';
      for ( i = 0; i < 24; i++) {
        for ( j = 0; j < 4; j++ ) {
          var min = 15*j; //разделение по 15 минут
          if ( min == 0 ) {
            min = '00';
          }
          var selected = '';
          if ( i < 10 ) { //заполняем <option>
            var tt = '0'+i+':'+min;
            if ( $('#edit-field-webinar-date-und-0-value-timeEntry-popup-1').val() == tt ) {//--если поле уже было заполнено
              time += '<option selected val="'+tt+'">'+tt+'</option>';
            }else{
              time += '<option val="'+tt+'">'+tt+'</option>';
            }
          } else { //заполняем <option>

            var tt = i+':'+min;
            if ( $('#edit-field-webinar-date-und-0-value-timeEntry-popup-1').val() == tt ) {//--если поле уже было заполнено
              time += '<option selected val="'+tt+'">'+tt+'</option>';
            }else{
              time += '<option val="'+tt+'">'+tt+'</option>';
            }
          }
        }
      }
      /*----------------------- создание селекта для удобства выбора времени ------------------------------*/
      $('#edit-field-webinar-date-und-0-value-timeEntry-popup-1').css({'display':'none'}).after('<select id="timeEntrySelect">'+time+'</select>')
      if ($('#edit-field-webinar-date-und-0-value-timeEntry-popup-1').hasClass('error')) {
        $('#timeEntrySelect').addClass('error')
      }

      /*----------------------- передача значения из селекта в инпут ------------------------------*/
      $('#timeEntrySelect').change(function(e){
        $('#edit-field-webinar-date-und-0-value-timeEntry-popup-1').val($('#timeEntrySelect :selected').val())
        e.preventDefault();
      })
  }

  }


  /*    all languages    */

  $('.lang-switch .all').siblings('li').each(function() {
  if ( $(this).index() > 1 ) {
    $(this).hide()
  }
  })

 $('.lang-switch .all').click(function(e) {
  e.preventDefault()

  $(this).siblings('li').each(function() {
    if ( $(this).index() > 1 ) {
      $(this).toggle()
    }
  })
 })

 $('#edit-current-pass, #edit-pass-pass1, #edit-pass-pass2').keyup(function(){
  if ($(this).val().length < 8) {
    //$(this).addClass('error')
  }else{
    //$(this).removeClass('error')
  }
 })

  jQuery("#user-profile-form").submit(function(e) {
     var self = this;
     e.preventDefault();
     if ($('#edit-current-pass').is('.error') || $('#edit-pass-pass1').is('.error') || $('#edit-pass-pass2').is('.error') ) {
      alert(Drupal.t('Password must be at least 8 characters long!'))
     }else{
      self.submit();
     }

     return false; //is superfluous, but I put it here as a fallback
  });
});

var path = '';

(function ($) {


  Drupal.behaviors.daisy = {
    attach: function(context) {

      path = Drupal.settings.basePath;

      $('.pager-next a', context).
      not('.view-display-id-block_3 .pager-next a, .view-display-id-block_4 .pager-next a, .view-display-id-block_5 .pager-next a')
      .unbind('click').bind('click', function(event) {
        var href = $(this).attr('href');
        var element = $(this).parents('.view');
        $.each(Drupal.settings.views.ajaxViews, function(key, value) {

          if($(element).hasClass('view-dom-id-' + value.view_dom_id)) {
            var data = value;
            data.page = href.substr(href.length - 1);
            $('<div class="daisy-widget-overlay">&nbsp;</div>').appendTo(document.body);
            $.ajax({
              "type": "get",
              "url": Drupal.settings.views.ajax_path,
              "data": data,
              "dataType": "json",
              "success": function(response){
                $.each(response, function(key, val){
                  if(val.command == 'insert') {
                    $(val.selector + ' .item-list').remove();
                    $(val.selector).last().after(val.data);
                    Drupal.attachBehaviors(val.selector + ':last');
                  }
                });
                $('.daisy-widget-overlay').remove();


              }
            });
          }
        });
        event.preventDefault();
      });
    }
  };

  var hash = window.location.hash;
  hash = hash.substr(1, hash.length)

  $('#block-views-account-menu-block .'+hash).trigger('click');

  $('#block-views-account-menu-block .'+hash).parent().addClass('active')

})(jQuery);


  $('#block-views-account-menu-block li a').live('click', function() {
    cabinetChange('#'+$(this).attr('class'));
  })

var cabinetChange = function(hs) {
  $('#user-profile-form > div > *').each(function() {
    if ( $(this).attr('id') != 'edit-actions' ) {
      $(this).hide()
    }
  })


  var lang = $('html').attr('lang')
  hs = hs.substr(1, hs.length)
  $('#block-views-account-menu-block .'+hs).parent().addClass('active').siblings().removeClass('active')
    switch (hs) {
    case 'General' || '':
      $('#edit-field-user-name, #edit-field-user-last-name, #edit-field-user-father-name, #edit-field-user-image, #edit-field-user-title, #edit-timezone').css({'display':'block'})
      $('#edit-mail').parent().css({'display': 'block'});
      break
    case 'Profile':
      $('#edit-locale, #edit-field-prefered-categories').css({'display':'block'})
      break
    case 'Password':
      $('#edit-account > div').css({'display':'block'})
      $('#edit-mail').parent().css({'display':'none'})
      break
    case 'Notifications':
      //window.location.href = path+lang+'/notifications';
      break
  }
}

$('.popup.prefered li').live('click',function(){
  $(this).toggleClass('selected');
})

$('.popup.prefered button').live('click', function(){
  var catArr = '';
  var flag = true;
  $('.popup.prefered li.selected').each(function() {
    var cat = $(this).attr('id');
    cat = cat.substr(2,cat.length)

    if (flag) {
      catArr += cat;
    } else {
      catArr += ','+cat;
    }
    flag = false;
  })

  $('.popup.prefered').css({'z-index':'1001'})
  $.ajax({
    url: Drupal.settings.basePath+'prefered-categories',
    type: 'post',
    data: 'prefered_cat='+catArr,
    success: function(){
      $('.popup.prefered').hide()
      $('.popup-bg-wrapper').hide()
    }
  })
  category_selection(catArr)
})

