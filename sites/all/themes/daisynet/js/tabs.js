(function ($) {
  Drupal.behaviors.daisyTabs = {
    attach: function(context, settings) {

      //Default to the first tab as the active tab.
      $("ul.product-tabs li:first", context).addClass("active");

      var ds = Drupal.settings;
      var wh = window.location.hash;
      var current_holder = $('.tabset-holder .current-item');
      if (window.location.pathname.indexOf(ds.basePath + ds.pathPrefix + 'mywebinars') != -1 && wh.length > 1) {
        $(window.location.hash).show();
        if($('.post-list-holder:visible').height()) {
          if($.browser.msie && parseInt($.browser.version) <= 7) {
            $(this).parents('.block').css('padding-bottom', $('.post-list-holder:visible').height() + 30)
          }
          else {
            $(this).parents('.block').css('padding-bottom', $('.post-list-holder:visible').height())
          }
        }
        $('ul.cabinet-tabs a[href=' + window.location.hash + ']').parents('li').addClass('active');
        $('ul.tabset').hide();
        $('.tabset li.active').parents('ul').show();
        window.location.hash = '';
      }
      else {
        $('ul.cabinet-tabs li.today', context).addClass("active");
        if($("ul.cabinet-tabs li.today", context).length) {
          $('ul.cabinet-tabs', context).hide();
          $(this).parents('.block').css('padding-bottom', 0);
          $("ul.cabinet-tabs li.today", context).parents('ul').show();
        }
        current_holder.html($($("ul.cabinet-tabs li.today", context).find("a").attr("href")).html());
      }

      //Adds click event to tabs.
      $("ul.tabset li").once('daisy-tabs').click(function(event) {
        //Cancel any "active" tabs.
        $("ul.tabset li").not(this).removeClass("active");

        //Toggle the active class for the current tab.
        $(this).toggleClass("active");

        //Hide all tab contents.
        $(".s_tabs .post-list-holder").hide();

        //Toggle content depending on the current tab status.
        if($(this).hasClass('active')) {
          $(this).parents('.block').css('padding-bottom', 0);
          //Find the rel attribute value to identify the active tab + content.
          var activeTab = $(this).find("a").attr("href");
          if($.browser.msie && parseInt($.browser.version) <= 7 && typeof activeTab != 'undefined') {
            activeTab = activeTab.substring(activeTab.indexOf('#'));
          }

          // Fill in the bottom holder with the content of the active tab.
          if (typeof activeTab != 'undefined') {
            current_holder.html($(activeTab).html());
          } else {
            current_holder.html('<div class="no-webinars">' + Drupal.t('There are no webinars for this day.') + '</div>');
          }
        }
        else {
          //Remove any content from bottom holder if hiding the active tab.
          current_holder.html('');
        }

        event.preventDefault();
      });

    }
  };

  // Mini calendar follow up via link
  $(document).ready(function() {
    var query_date = window.location.hash;
    var selected_item = $('ul.tabset li a[href=' + query_date +']').parents('li');
    if (!selected_item.parents('ul.tabset').is(':visible')) {
      selected_item.parents('ul.tabset').siblings('ul').hide();
      selected_item.parents('ul.tabset').show();
    }
    if (!selected_item.hasClass('active')) {
      selected_item.trigger('click');
    }

    // Override the links from the small calendar on the left
    $('div.page-sidebar.sidebar-first ul.calendar-menu div.date-wrapper a').bind('click', function(e) {
      var query_date = $(this).attr('href');
      query_date = query_date.substring(query_date.indexOf('#'));
      var selected_item = $('ul.tabset li a[href=' + query_date +']').parents('li');
      if (!selected_item.parents('ul.tabset').is(':visible')) {
        selected_item.parents('ul.tabset').siblings('ul').hide();
        selected_item.parents('ul.tabset').show();
      }
      if (!selected_item.hasClass('active')) {
        selected_item.trigger('click');
      }
      $('body').animate({scrollTop: 0}, 200);
    });
  });

})(jQuery);
