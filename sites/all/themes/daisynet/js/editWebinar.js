(function ($) {
  Drupal.behaviors.daisyEditWebinar = {
    attach: function(context) {
      $(document).ready(function(){
        //Always show the submit buttons on webinar-edit form.
        $('.node-edit .form-actions .form-submit, .node-edit .form-actions .button-submit').show();

          $('textarea#edit-field-webinar-sh-description-und-0-value', context).after('<span class="notification"></span>');
          $('textarea#edit-field-webinar-sh-description-und-0-value', context).keyup(function() {
            if($(this).val().length > 250) {
              $(this).parent().find('span.notification').addClass('marker');
            }
            else {
              $(this).parent().find('span.notification').removeClass('marker');
            }
            $(this).parent().find('span.notification').html($(this).val().length + ' / 250');

          });
          if($('input[name="field_webinar_access[und]"]:radio:checked', context).val() == 'public') {
            $('#edit-field-participants').hide();
          }
          else {
            $('#edit-field-participants').show();
          }

          $('input[name="field_webinar_access[und]"]:radio', context).change(function(){
            if($(this).val() == 'public') {
              $('#edit-field-participants').hide();
            }
            else {
              $('#edit-field-participants').show();
            }
          });

          if($('input[name="field_webinar_price[und]"]:radio:checked', context).val() == 'paid') {
            $('#edit-field-webinar-pr-view').show();
            $('#edit-field-webinar-pr-participate').show();
            $('#edit-field-webinar-hd').show();
          }
          else {
            $('#edit-field-webinar-pr-view').hide();
            $('#edit-field-webinar-pr-participate').hide();
            $('#edit-field-webinar-hd').hide();
            $('#edit-field-has-discount').hide();
          }

          $('input[name="field_webinar_price[und]"]:radio', context).change(function(){
            if($(this).val() == 'paid') {
              $('#edit-field-webinar-pr-view').show();
              $('#edit-field-webinar-pr-participate').show();
              $('#edit-field-webinar-hd').show();
              $('#edit-field-has-discount').show();
              if($('input[name="field_has_discount[und]"]').attr('checked')) {
                $('#edit-field-webinar-prhd-view').show();
                $('#edit-field-webinar-prhd-participate').show();
              }
            }
            else {
              $('#edit-field-has-discount').hide();
              $('#edit-field-webinar-pr-view').hide();
              $('#edit-field-webinar-pr-participate').hide();
              $('#edit-field-webinar-prhd-view').hide();
              $('#edit-field-webinar-prhd-participate').hide();
              $('#edit-field-webinar-hd').hide();
            }
          });

          if($('input[name="field_has_discount[und]"]', context).attr('checked')){
            $('#edit-field-webinar-prhd-view').show();
            $('#edit-field-webinar-prhd-participate').show();
          }
          else {
            $('#edit-field-webinar-prhd-view').hide();
            $('#edit-field-webinar-prhd-participate').hide();
          }

          $('input[name="field_has_discount[und]"]', context).change(function(){
            if($(this).attr('checked')) {
              $('#edit-field-webinar-prhd-view').show();
              $('#edit-field-webinar-prhd-participate').show();
              $('#edit-field-webinar-prhd-view-und-0-value').val($('#edit-field-webinar-pr-view-und-0-value').val());
              $('#edit-field-webinar-prhd-participate-und-0-value').val($('#edit-field-webinar-pr-participate-und-0-value').val());
              $('#edit-field-webinar-pr-view-und-0-value').val('');
              $('#edit-field-webinar-pr-participate-und-0-value').val('');
            }
            else {
              $('#edit-field-webinar-pr-view-und-0-value').val($('#edit-field-webinar-prhd-view-und-0-value').val());
              $('#edit-field-webinar-pr-participate-und-0-value').val($('#edit-field-webinar-prhd-participate-und-0-value').val());
              $('#edit-field-webinar-prhd-view-und-0-value').val('');
              $('#edit-field-webinar-prhd-participate-und-0-value').val('');
              $('#edit-field-webinar-prhd-view').hide();
              $('#edit-field-webinar-prhd-participate').hide();
            }
          });

          $('#edit-field-webinar-sh-description-und-0-value').attr('maxlength','250');
          $('#edit-field-webinar-sh-description-und-0-value').keyup(func);
          $('#edit-field-webinar-sh-description-und-0-value').blur(func);
        });
    }
  };
})(jQuery);

var func = function() {
  var len = parseInt(this.getAttribute("maxlength"), 10);
  if(this.value.length > len) {
    this.value = this.value.substr(0, len);
    $(this).parent().find('span.notification').html($(this).val().length + ' / 250');
    return false;
  }
}