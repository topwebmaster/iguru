(function ($) {
  $(document).ready(function(e) {
    $('.calendar-calendar .month-view ul li div.hide-date').parent().hide();
  });

  Drupal.behaviors.daisyCabinet = {
    attach: function(context) {
      //Triggers the display of calendar weeks on 'Cabinet' page.
      $('.calendar-calendar a.nav', context).click(function(event){
        var index = $('.calendar-calendar .month-view ul:visible').index();
        var count = $('.calendar-calendar .month-view ul').length;
        if($(this).hasClass('next')) {
          if(index < (count - 1)) {
            $('ul.week-' + index).hide();
            $('ul.week-' + (index + 1)).show();
          }
        }
        else {
          if(index > 0) {
            $('ul.week-' + index).hide();
            $('ul.week-' + (index - 1)).show();
          }
        }
        event.preventDefault();
      });

      //Triggers the resizing - display of Friends + Groups preview from User/Group blocks.
      $('.friend-user-image-small, .group-user-image-small').once('DaisyFriendPhoto').click(function(){
        $('.friend-user-info').hide();
        var user_height = $(this).parent().find('.friend-user-info, .group-user-info').css('height');
        $(this).parent().find('.friend-user-info, .group-user-info').show();
          $(this).parents('.view-content').css({'height': user_height, 'overflow': 'hidden', 'width': '100%'});
      });
      $('.friend-user-image a.close, .group-user-image a.close').once('DaisyFriendPhoto').click(function(event){
        $(this).parents('.friend-user-info, .group-user-info').hide();
        $(this).parents('.view-content').css({'height': 'auto', 'overflow': 'hidden', 'width': '100%'});
        event.preventDefault();
      });

    }
  };
})(jQuery);
