(function ($) {
  /**
   * Timeout for messages.
   */
  Drupal.behaviors.MessagesTimeout = {
    attach: function(context) {
      $('.alert.alert-block', context).once('MessagesTimeout', function(e) {
        $(this).delay(80000).fadeOut(300);
      });
    }
  }

  /**
   * Popup centering on fluid layouts.
   */
  Drupal.behaviors.PopupCentering = {
    attach: function(context){
      $(document).ready(function(){
        $(window).resize(function(){
          // get the screen height and width
          var maskHeight = $(window).height();
          var maskWidth = $(window).width();

          // calculate the values for center alignment
          var dialogTop =  (maskHeight  - $('.user_relationships_ui_popup_form').height())/2;
          var dialogLeft = (maskWidth - $('.user_relationships_ui_popup_form').width())/2;

          // assign values to the overlay and dialog box
           // $('.user_relationships_ui_popup_form').css({ height:$(document).height(), width:$(document).width() }).show();
          $('.user_relationships_ui_popup_form').css({ top: dialogTop, left: dialogLeft, position:"fixed"});
        }).resize();
      });
    }
  }

  Drupal.behaviors.SidebarToggler = {
    attach: function(context) {
      jQuery(document).ready(function($) {
        //Adjust the height of the sidebars and content to the general window size.
        var max_height = Math.max($(document).height(), $(window).height());
        $('.page-container .sidebar-first').height(max_height + 'px');
        $('.page-container .sidebar-second').height(max_height + 'px');
        $('.page-container .page-content').height(max_height + 'px');

        // Generic block functionality
        $('.block-title .visibility-toggler').click(function() {
          $(this).children('i').toggleClass('icon-angle-down').toggleClass('icon-angle-up');
          $(this).parents('.block-title').siblings('.block-content-holder').slideToggle(300);
        });

        // Left sidebar calendar toggle
        $('.page-sidebar .calendar-menu a.calendar-item').click(function(e) {
          $(this).siblings('ul.sub.calendar').toggle();
          e.preventDefault();
          return false;
        })

        //First sidebar open/close action
        $('.sidebar-first .bar-toggler').click(function() {
          $(this).parents('.page-sidebar').toggleClass('minimized');
          $(this).children('i').toggleClass('icon-angle-left').toggleClass('icon-angle-right');
          $('.page-container').toggleClass('sidebar-first-closed');
        });

        //Second sidebar open/close action
        $('.sidebar-second .bar-toggler').click(function() {
          $(this).parents('.page-sidebar').toggleClass('minimized');
          $(this).children('i').toggleClass('icon-angle-right').toggleClass('icon-angle-left');
          $(this).parents('.page-container').toggleClass('sidebar-second-closed');
          if(!$('.page-container').hasClass('sidebar-second-closed')) {
            $('.sidebar-second .block > *').removeClass('sidebar-hidden-block');
          }
        });

        // Right sidebar block toggler functionality
        $('.sidebar-second .block .block-toggler').click(function() {
          $(this).toggleClass('active');

          //Cancel the display and active state of any other blocks except current
          $('.sidebar-second .block .block-toggler').not(this).removeClass('active');
          $('.sidebar-second .block > *').not($(this).siblings()).removeClass('sidebar-hidden-block');

          //Trigger the styles for displaying block contents
          $(this).parents('.block').children(':not(.contextual-links-wrapper)').toggleClass('sidebar-hidden-block');
        });
      });
    }
  }

  // Groups page functionality
  $(document).ready(function(){
    $( ".views-header-group .group-label" ).click(function() {
      $(".group-drop-down").not($( this ).parent(".views-header-group").find(".group-drop-down")).hide();
      $( this ).parent(".views-header-group").find(".group-drop-down").toggle(  );
    });
  });

  Drupal.behaviors.CommunityTabs = {
    attach: function() {
      $(document).ready(function(){
        $('.community-tabs a').hover(function(){
          $(this).siblings().toggleClass('passive');
        });
      });
    }
  }
})(jQuery);
