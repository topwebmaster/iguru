/**
 * Define actions for the frontpage elements.
 */
jQuery.noConflict();

 function resizeUI() {
  if(jQuery(window).width() < 979)
  {
     jQuery('.page-container').addClass('sidebar-closed');
     jQuery('.page-sidebar .sidebar-toggler').hide();
  }
  if(jQuery(window).width() > 980)
  {
    jQuery('.page-sidebar .sidebar-toggler').show();
    jQuery('.page-container').removeClass('sidebar-closed');
  }
 }

(function($) {
  /**
   *  Toggle Login Form on "Login" block.
   */
  Drupal.behaviors.FrontpageJS = {
    attach: function() {
      $('#dropdown-search-icon').click(function(e) {
        e.preventDefault();
        $('#daisy-search-dropdown').toggle();
         if ($(window).width() > 720)  {
            $('#daisy-search-dropdown').show();
         }
      });

      $.curCSS = $.css;
      $('.block-daisy-user').click(function(e) {
       $(".auth-form #edit-mail").attr("id","edit-mail-front");
         e.preventDefault();
         $('.ui-dialog').hide();
         $(".ui-dialog-content").dialog("close");
         $('#join-daisy-form-wrapper').dialog({
         dialogClass:'join-daisy-register-dialog',
         resizable: false,
         modal:true,
         opacity:0.6,
         draggable: false,
         closeOnEscape:true,
         position: [490,250],
         });
      });
    }
  }

  /**
   * Catalog sidebar hover effect.
   */
  Drupal.behaviors.CategoryToggleGroup = {
    attach: function() {
      $('.categories-menu li.has-sub > a').click(function(e) {
      });
    }
  }

  Drupal.behaviors.SearchInputToggle = {
    attach: function() {
      if ($(window).width() < 767) {
        $('form.search-form button[type=submit]').click(function(e){
            $(this).siblings('input[type=text]').slideToggle();
            e.preventDefault();
        });

        $('form.search-form input[type=text]').change(function(e) {
          $(this).parents('form').submit();
        });
      }
    }
  }

  jQuery(document).ready(function() {
    if(jQuery(window).width() < 980)
    {
      jQuery(".page-container").addClass('sidebar-closed');
    }
    else
    {
      jQuery(".page-container").removeClass('sidebar-closed');
    }
   jQuery(window).on('resize',function () {
      window.clearTimeout(this.id);
      this.id = window.setTimeout('resizeUI()',100);
    });
  });

  /**
   * Prev - Next Schedule button toggle.
   */
  Drupal.behaviors.ScheduleDateNavigation = {
    attach: function() {
      var prev = jQuery('.prev.chyr');
      var next = jQuery('.next.chyr');
      var yearSearch = jQuery('input[name="webinar_date[value][date]"]');

      //trigger last year search on schedule input.
      prev.click(function(){
        var year = parseInt(yearSearch.val());
        yearSearch.val(--year);
      });

      //trigger next year search on schedule input.
      next.click(function() {
        var year = parseInt(yearSearch.val());
        yearSearch.val(++year);
      });
    }
  }


  /**
   * Fade in the info-bottom block for Webinar tiles.
   */
  Drupal.behaviors.CatalogFront = {
    attach: function() {
      $(document).ready(function() {
        // Show info on click
        $('.tile .bottom-info').click(function() {
          $(this).parents('.tile-body').siblings('.info-bottom').slideToggle(300);
        });
        // Bottom info close button
        $('.tile .info-bottom .hide-info-button').click(function() {
          $(this).parents('.info-bottom').slideToggle(300);
        });
        // Social links opener
        $('a.social-toggle').click(function() {
          $(this).toggleClass('open').siblings('.social-links').stop(true, true).animate({'width': 'toggle'}, 500);
          return false;
        });
      });

      /**
       * Interview preview for the webinar video.
       */
      $('.preview-interview').live('click',function() {
        var playerID = $(this).parents('.tile').find('object').attr('id');

        if ($(this).hasClass('preview-back')) {
          var elem = $(this).parents('.tile').find('.tile-body');
          elem.parents('.tile').find('.preview-back').hide();
          elem.children('.webinar-info, .user-info').toggle();
          elem.children('.webinar_preview').toggleClass('hidden');

          //Toggle Uppod settings via JS API.
          uppodSend(playerID, 'pause');
        } else {
          var elem = $(this).parents('.tile-body');
          elem.parents('.tile').find('.preview-back').show();
          elem.children('.webinar-info, .user-info').toggle();
          elem.children('.webinar_preview').toggleClass('hidden');

          //Toggle Uppod settings via JS API.
          //uppodSend('' + playerID + '', 'play');
        }
      })
    }
  }


  Drupal.behaviors.CartTrigger = {
    attach: function(){
      /**
       * Redirects to product page on "Add to Cart" link click.
       */
      $('.add_to_cart').live('click', function(e) {
        e.preventDefault();
        window.location = Drupal.settings.basePath+'node/'+$(this).parent().attr('id')+'?add_to_cart=1';
      })

      /**
       * Redirects to Webinar page on "Participate" link click.
       */
      $('.basket .participate').live('click', function(e) {
        e.preventDefault();
        window.location = Drupal.settings.basePath+'node/'+$(this).parent().attr('id')+'?participate=1';
      })
    }
  }

  /**
   * Trigger the tooltips container on form items.
   */
  Drupal.behaviors.TooltipToggle = {
    attach: function(context) {
      $('.form-item').hover(function() {
        var item = this;
        $(this).children('.tooltips').trigger('hover');
      });
    }
  }
})(jQuery);
