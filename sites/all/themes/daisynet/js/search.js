jQuery.extend(jQuery.expr[":"], {
  //Extend the :contains selector to enable case insensitive search.
    "containsNC": function(elem, i, match, array) {
        return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
});

$ = jQuery;

var KEYCODE_ESC = 27;//The Esc key code.
  $(document).keyup(function(e) {
    if (e.keyCode === KEYCODE_ESC) {
      //Hide the search block on Esc key press.
      $('.search_block').addClass('hidden');
    }
  });

var search = function (find) {
  $(".product_search").parent().addClass('hidden')
  if (find != '') {
    find = find.toLowerCase();
    $(".product_search a").filter(":containsNC('"+find+"')").parent().parent().removeClass('hidden')
  }
}

$(function(){

  //Trigger search on keypress.
  $('input#search_input').keyup(function(){
    search($(this).val())
  })
  //Trigger search on change.
  $('input#search_input').change(function(){
    search($(this).val())
  })

  $('#block-views-catalog-block-6 h2').click(function(){
    if ($('.search_block').hasClass('hidden')) {
      $('.search_block').removeClass('hidden')
      $('#search_input').focus()
    }else{
      $('.search_block').addClass('hidden')
    }
  })

  var categories = new Array();
  i=0;
  $('.product_search').each(function(){
    categories[i] = $(this).children('.field-content.terms').children('.term').html()
    i++;
  })

});