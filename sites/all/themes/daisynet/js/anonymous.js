jQuery(document).ready(function($) {
  $(function(){
		$('#block-user-login, #block-formblock-user-register').hide()

		$('.startpage-wrapper .auth-form img.arrow').click(function() {
			turn_around( $('.startpage-wrapper .auth-form'), $('.get-started-new'), $('#block-user-login') )
		});

		$('#toreg').live('click', function() {
			turn_around( $('.startpage-wrapper .auth-form'), $('#block-user-login'), $('#block-formblock-user-register, .turn.add') )
		})

		$('#returnertoenter').live('click', function() {
			turn_around( $('.startpage-wrapper .auth-form'), $('#block-formblock-user-register, .turn.add'), $('#block-user-login') )
		})
	})

	var turn_around = function(wrapper, toHide, toShow){
		toShow.hide(0).find('form').removeClass('hidden');

	  $(wrapper).animate({
	    width: "1px",
	    opacity: 0.9,
	    marginLeft: "135px",
	    backgroundColor:"#753129",
	    color:"#d1594b"
	  }, 500);

	  $(toHide).hide();

	  $(toShow).show(1100);
	  $(wrapper).animate({
	   	width:"340px",
	   	opacity:1,
	    marginLeft: "5px",
	  	backgroundColor:"#d1594b",
	   	color:"white"
	  },500);
	}
});

