if (jQuery('body').attr('lang') == 'en') {
  reformal_wdg_w    = "713";
  reformal_wdg_h    = "460";
  reformal_wdg_domain    = "daisynet";
  reformal_wdg_mode    = 0;
  reformal_wdg_title   = "DaisyNet";
  reformal_wdg_ltitle  = "Feedback and ideas...";
  reformal_wdg_lfont   = "";
  reformal_wdg_lsize   = "";
  reformal_wdg_color   = "#FFA000";
  reformal_wdg_bcolor  = "#6568ab";
  reformal_wdg_tcolor  = "#FFFFFF";
  reformal_wdg_align   = "left";
  reformal_wdg_waction = 0;
  reformal_wdg_vcolor  = "#f2be11";
  reformal_wdg_cmline  = "#E0E0E0";
  reformal_wdg_glcolor  = "#6568ab";
  reformal_wdg_tbcolor  = "#FFFFFF";
  reformal_wdg_bimage = "29a2770fafec079073069edf2daf22af.png";
}
else {
  var reformalOptions = {
    project_id: 57796,
    project_host: "DaisyNet.reformal.ru",
    tab_orientation: "left",
    tab_indent: "50%",
    tab_bg_color: "#fafafa",
    tab_border_color: "#6569ab",
    tab_image_url: "http://tab.reformal.ru/0JLQsNGI0LggT9GC0LfRi9Cy0Ysg0Lgg0LjQtNC10LguLi4=/6569ab/bac3e0b958e545f4bc8a82335a8ef5b7/left/0/tab.png",
    tab_border_width: 0
  };

  (function() {
    var script = document.createElement('script');
    script.type = 'text/javascript'; script.async = true;
    script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'media.reformal.ru/widgets/v3/reformal.js';
    document.getElementsByTagName('head')[0].appendChild(script);
  })();

  setTimeout(function(){
    document.getElementById('reformal_tab').href='javascript:Reformal.widgetOpen();';
    document.getElementById('reformal_tab').onclick = function(){};
  },1000);
}

jQuery(document).ready(function(){
  setTimeout(function(){
    jQuery("#product-node-form > div > div:eq(13)").css({'float':'left','width':'180px'});;
    jQuery("#product-node-form > div > div:eq(15)").css({'float':'left','width':'180px'});;
  },500);

  jQuery('table.views-view-grid td').click(function(){
    jQuery('table.views-view-grid td').css('position','static');
    jQuery('table.views-view-grid td div.friend-user-wrapper').css('position','static');
    jQuery('div.friend-user-wrapper',this).css('position','relative');
    jQuery('a.close',this).css('top','-10px');
    jQuery(this).css('position','relative');
  })
});