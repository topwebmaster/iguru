<article class="clearfix <?php print $classes . ' ' . $zebra; ?>"<?php print $attributes; ?>>
<div class = "comment-fields">
 <div class="user-picture">
      <?php
        $comment_author = user_load($comment->uid);
        print daisy_user_get_picture($comment_author, 'user_image_small');
      ?>
  </div>
 <div class="commented-body">

  <?php
    print t('!username  !datetime', array('!username' => $author, '!datetime' => '<time datetime="' . $datetime . '">' . $created . '</time>'));
  ?>
  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['links']);
    print render($content);
  ?>
 </div>
</div>
</article> <!-- /.comment -->
