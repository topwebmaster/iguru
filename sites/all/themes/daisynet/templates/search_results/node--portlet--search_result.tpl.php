<?php
  $classes .= ' node-view-searche';
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="row-fluid">
    <div class="img-description span12">
      <div class="image">
        <?php print drupal_render($content['uc_product_image']); ?>
      </div>
      <div class="description">
        <h3<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print t($title); ?></a></h3>
        <?php print drupal_render($content['body']); ?>
      </div>
    </div>
  </div>
  <?php hide($content); ?>
</article>
