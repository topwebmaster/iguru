<?php
  $no_margin = ' margin5';
  $classes .= ' node-view-searche';
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="row-fluid">
    <div class="img-description span12">
      <?php if($content['uc_product_image']) : ?>
        <div class="image">
          <?php print drupal_render($content['uc_product_image']); ?>
        </div>
      <?php $no_margin = ''; ?>
      <?php endif; ?>
      <div class="description<?php print $no_margin; ?>">
        <h3<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print t($title); ?></a></h3>
        <?php
          hide($content['comments']);
          hide($content['links']);
          hide($content['field_tags']);
          print render($content);
        ?>
      </div>
    </div>
  </div>
</article>
