<?php
  $classes .= ' node-view-searche';
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="row-fluid">
    <div class="img-description span8">
      <div class="image">
        <?php print drupal_render($content['field_group_picture']); ?>
      </div>
      <div class="description">
        <h3<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print t($title); ?></a></h3>
        <?php
          hide($content['comments']);
          hide($content['links']);
          hide($content['field_tags']);
          hide($content);
          ?>
      </div>
    </div>
    <div class="public-private span2">
      <?php print drupal_render($content['group_access']); ?>
    </div>
    <div class="vbuttons span2">
      <?php print l(t('View'), 'node/'.$node->nid, array('attributes' => array('class' => array('btn bigicn-only')))); ?>
    </div>
  </div>
</article>
