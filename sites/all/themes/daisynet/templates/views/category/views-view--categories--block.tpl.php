<div class="accordion in collapse" id="categories-accordion">
  <div class="accordion-group">
    <div class="accordion-heading pull-right">
        <a class="accordion-toggle collapsed pull-right" data-toggle="collapse"
         data-parent="categories-accordion" href="#categories-list"
         id="categories-toggle-arrow"></a>
    </div>
    <div id="categories-list" class="accordion-body collapse">
      <h4><?php print t('Categories'); ?></h4>
      <?php if ($rows): ?>
        <?php print $rows; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
