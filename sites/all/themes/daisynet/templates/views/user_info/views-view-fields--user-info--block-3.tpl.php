<div class="front-user-info">
  <?php foreach ($fields as $id => $field): ?>
    <?php if (!empty($field->separator)): ?>
      <?php print $field->separator; ?>
    <?php endif; ?>

	  <?php //print $field->wrapper_prefix; ?>

	  <?php print $field->label_html; ?>
	  <?php if ($id == 'nothing') : ?>
      <div class="dropdown">
        <a href="#" class="dropdown-toggle pull-left" data-toggle="dropdown">
          <span class="daisy-user-name">
            <?php print l(daisynet_get_username($user->uid), 'user/' . $user->uid); ?>
          </span>
        </a>
        <?php print _daisynet_block_render('menu-account-menu'); ?>
      </div>
	  <?php else : ?>
	    <?php print $field->content; ?>
	  <?php endif; ?>

	  <?php //print $field->wrapper_suffix; ?>
  <?php endforeach; ?>
</div>
