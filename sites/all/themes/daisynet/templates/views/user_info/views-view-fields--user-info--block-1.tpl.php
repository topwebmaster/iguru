<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
$uid = $row->uid;
?>

<div class="friend-user-wrapper">

  <?php // The Information shown onLoad ?>
  <div class="friend-user-image-small"><?php print $fields['field_user_image']->content ?></div>

  <?php // The Information shown onClick ?>
  <div class="friend-user-info">
    <div class="friend-user-image">
      <?php print $fields['field_user_image_1']->content ?><a href="#" class="close"></a>
    </div>
    <div class="friend-user-top">
      <div class="field-content"><?php print daisynet_get_username($uid) ?></div>
      <?php print $fields['field_user_country']->content ?>
    </div>
    <div class="friend-user-links">
      <?php // Custom link because privatemsg does now allow classes + icons
        if (($recipient = user_load($fields['uid']->raw)) && ($url = privatemsg_get_link(array($recipient), NULL, ''))) :?>
      <div class="field-content">
      <?php
        $options = array(
          'html' => TRUE,
          'attributes' => array('class' => 'send-message'),
          'query' => drupal_get_destination(),
        );
        print l('<i class="icon-envelope-alt"></i>' . t('Send message'), $url, $options);
      ?>
      </div>
      <?php endif; ?>
      <?php if (count(og_get_groups_by_user()) && FALSE): ?>
      <div class="field-content">
        <a href="/groups/invite/<?php print $uid ?>" title="<?php print t('Invite to group');?>"><?php print t('Invite to group');?></a>
      </div>
      <?php endif; ?>
      <div class="field-content">
        <?php print daisy_user_relationships($uid, TRUE) ?>
      </div>
    </div>
  </div>

</div>
