<?php
$nid = $fields['nid']->raw;
$links = og_daisy_get_user_group_links($nid, array('attributes' => array('class' => array('link'))), 'group-user-links');
?>

<div class="group-user-wrapper">

  <?php // The Information shown onLoad ?>
  <div class="group-user-image-small">
    <?php print $fields['field_group_picture_1']->content; ?>
  </div>

  <?php // The Information shown onClick ?>
  <div class="group-user-info">
    <div class="group-user-image">
      <?php print $fields['field_group_picture']->content; ?><a class="close"></a>
    </div>
    <div class="group-user-top">
      <div class="group-content"><?php print $fields['title']->content; ?></div>
    </div>
    <?php print $links; ?>
  </div>

</div>
