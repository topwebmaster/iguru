<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

// Unset not needed fields
unset($fields['points']);
unset($fields['field_user_country']);
?>
<?php foreach ($fields as $id => $field): ?>
  <?php if ($id == 'nothing'): ?>
    <div class="dasy-user-wrapp">
      <div class="daisy-user-name">
        <?php print daisynet_get_username($user->uid); ?>
      </div>
      <div class="user-status">
        <?php if (in_array('teacher', $user->roles)): ?>
          <?php print t('Status: EXPERT'); ?>
        <?php else: ?>
          <?php print t('Status: Participant'); ?>
        <?php endif; ?>
      </div>
    </div>

    <div class="account-menu-settings">
      <ul class="sub" style="display: block;">
        <?php print _daisynet_block_render('menu-account-menu'); ?>
      </ul>
    </div>
  <?php else: ?>
    <?php print $field->content; ?>
  <?php endif; ?>
<?php endforeach; ?>
