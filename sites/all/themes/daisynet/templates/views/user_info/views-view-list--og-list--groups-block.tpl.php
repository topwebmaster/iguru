<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php print $list_type_prefix; ?>
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print $classes_array[$id]; ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
      <li class="join-group">
        <div class="group-user-wrapper">
          <div class="group-user-image-small">
            <img src="<?php print file_create_url(drupal_get_path('theme', 'daisynet') . '/img/empty_square.png'); ?>"/>
            <div class="cover">
              <?php print l('<i class="icon-plus"></i><div class="link-text">' .t('Join group') . '</div>', 'groups/all', array('html' => TRUE)); ?>
            </div>
          </div>
        </div>
      </li>
  <?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>
