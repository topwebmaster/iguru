<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
$link = daisy_user_relationships($row->uid, TRUE);
$user_name = daisynet_get_username($row->uid);
?>


  <div class="friend-user-info">
    <div class="friend-user-image">
      <?php print $fields['field_user_image_1']->content; ?>
      <a href="#" class="close"></a>
    </div>
    <div class="friend-user-top">
      <div class="field-content">
        <?php print $user_name; ?>
      </div>
      <?php print $fields['field_user_country']->content; ?>
    </div>
    <?php if($row->uid != $user->uid) : ?>
    <div class="friend-user-links">
      <?php if (isset($fields['privatemsg_link'])) : ?>
        <?php print $fields['privatemsg_link']->content; ?>
      <?php endif; ?>
      <span class="field-content">
        <?php print $link; ?>
      </span>
    </div>
    <?php endif; ?>
  </div>
</div>
