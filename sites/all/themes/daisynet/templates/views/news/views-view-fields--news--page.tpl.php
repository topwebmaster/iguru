<?php
/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

// Set Template Variables
$image_content = $fields['field_image']->content;
$image_class = !$image_content ? ' hidden-desktop hidden-tablet' : '';
?>

<div class="row-fluid">
  <?php if (!empty($row->field_field_bg_color[0]['rendered']['#markup'])): ?>
    <div class="news_author_block span3" style="background-color: #<?php print $row->field_field_bg_color[0]['rendered']['#markup']; ?>;">
    <?php else: ?>
    <div class="news_author_block span3">
  <?php endif; ?>
      <div class="news_post_date_day "><?php print $fields['created']->content; ?></div>
      <div class="news_post_date_month "><?php print $fields['created_1']->content; ?></div>
      <div class="news_comments_cont"><?php print $fields['comment_count']->content; ?><i class="icon-comment"></i></div>
      <div class="news_authors"><?php print $fields['name']->content; ?><i class="icon-pencil"></i></div>
    </div>
  <div class="span9 block-need">
    <div class="row-fluid">
      <div  class="news_text_block span12">
        <div class="news_image_block hidden-phone<?php print $image_class; ?>">
          <?php print $image_content; ?>
        </div>
        <div class="news-header">
          <div class="news_title"><?php print $fields['title']->content; ?></div>
          <div class="news_link"><?php print $fields['view_node']->content; ?></div>
        </div>
        <div class="news_body_all">
          <div class="news_category span7"><?php print $fields['field_webinar_category']->content; ?></div>
          <div class="news_body"><?php print $fields['body']->content; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
