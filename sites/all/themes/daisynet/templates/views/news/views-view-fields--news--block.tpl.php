<div class="news-container tile">
  <div class="news-item clearfix" id="<?=$row->nid?>">
    <div class="news-color-line"
      <?php if ($fields['field_bg_color']->content != ''):?>
        style="background: #<?php print $fields['field_bg_color']->content; ?>;"
      <?php else: ?>
        style="background: #808080;"
      <?php endif; ?>>
    </div>
    <div class="news-fields clearfix">
      <div class="news-image pull-left">
        <div class="image-thumb"><?php print $fields['field_image']->content;?></div>
      </div>
      <div class="news-info">
        <h3 class="news-title"><?php print $fields['title']->content; ?></h3>
        <div class="news-summary"><?php print $fields['body']->content; ?></div>
      </div>
      <div class="clearfix"></div>
    </div>
    <?php if (isset($fields['field_catalog_logo']->content) && trim($fields['field_catalog_logo']->content) != '') : ?>
    <div class="catalog-logo">
      <?php print $fields['field_catalog_logo']->content; ?>
    </div>
      <?php endif; ?>
      <?php print $fields['totalcount']->content; ?>
  </div>
</div>

