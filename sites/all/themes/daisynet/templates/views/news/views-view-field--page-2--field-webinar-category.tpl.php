<?php

/**
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 */

$tid = $row->taxonomy_term_data_field_data_field_webinar_category_tid;
$term = $row->field_field_webinar_category[0]['rendered']['#title'];
?>

<?php print l($term, 'news/' . $tid); ?>
