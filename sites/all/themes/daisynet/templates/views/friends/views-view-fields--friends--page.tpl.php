<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
//dpm($fields);
?>

<div class="user-image">
  <?php print $fields['field_user_image']->content; ?>
</div>
<div class="user-info-wrapper">
  <div class="daisy-user-name">
    <?php print l(daisynet_get_username($row->uid), 'user/' . $row->uid); ?>
  </div>
  <div class="user-degree">
    <?php print $fields['field_user_grade']->content; ?>
  </div>
</div>
<div class="user-links">
  <span class="mail">
    <?php print $fields['privatemsg_link']->content; ?>
  </span>
  <span class="share">
    <ul class="share-links">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle addthis-link" data-toggle="dropdown"><i class="icon-thumbs-up"></i></a>
        <ul class="dropdown-menu clearfix">
          <li><?php print $fields['field_share']->content; ?></li>
        </ul>
      </li>
    </ul>
  </span>
  <span class="delete">
    <?php print $fields['status_link']->content; ?>
  </span>
</div>
