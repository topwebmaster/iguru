<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="clearfix">
      <div class="group views-header-group group-my-contact">
        <a class="group-label dropdown-toggle" data-toggle="dropdown" href="#">
          <?php print t('My contacts'); ?>
        </a>
        <ul class="group-dropdown dropdown-menu">
          <li>
            <a href="#"><i class="icon-smile"></i><?php print t('Daisy contacts'); ?></a>
          </li>
          <li>
            <a href="#"><i class="icon-facebook"></i><?php print t('Facebook contacts'); ?></a>
          </li>
          <li>
            <a href="#"><i class="icon-google-plus"></i><?php print t('Google+ contacts'); ?></a>
          </li>
          <li>
            <a href="#"><i class="icon-vk"></i><?php print t('VK contacts'); ?></a>
          </li>
          <li>
            <a href="#"><i class=""></i><?php print t('All contacts'); ?></a>
          </li>
        </ul>
      </div>
      <div class="group views-header-group group-settings">
        <a class="group-label dropdown-toggle" data-toggle="dropdown" href="#">
          <?php print t('Settings'); ?>
        </a>
        <ul class="group-dropdown dropdown-menu">
          <li>
            <a href="#"><i class="icon-flag"></i><?php print t('Invite to a seminar'); ?></a>
          </li>
          <li>
            <a href="#"><i class="icon-envelope-alt"></i><?php print t('Write a message'); ?></a>
          </li>
          <li>
            <a href="#"><i class="icon-group"></i><?php print t('Invite into group'); ?></a>
          </li>
          <li>
            <a href="#"><i class="icon-remove"></i><?php print t('Delete the contact'); ?></a>
          </li>
        </ul>
      </div>
      <div class="view-header">
        <?php print $header; ?>
      </div>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>