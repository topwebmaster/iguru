<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
//dpm(l('node', 'node/' . $row->nid));
$webinar_is_free = $fields['field_webinar_price']->is_free;
$webinar_is_public = $fields['field_webinar_access']->is_public;
if($webinar_is_public) {
  $webinar_access_class = ' public';
}
else {
  $webinar_access_class = ' private';
}
$author_uid = $row->users_node_uid;
$author_name = daisynet_get_username($author_uid);
$row->field_field_user_image[0]['rendered']['#item']['alt'] = $author_name;
$row->field_field_user_image[0]['rendered']['#item']['title'] = $author_name;
$author_img = render($row->field_field_user_image);
?>

<div class="top-webinar-node">
  <div class="info-left">
    <?php print $fields['field_webinar_date']->content; ?>
    <div class="sb-links">
      <?php print $fields['addtoany_link']->content; ?>
      <?php print $fields['ops']->content; ?>
    </div>
  </div>

  <div class="info-content">
    <?php print l($fields['title']->content, 'node/' . $row->nid); ?>
    <?php print $fields['field_webinar_category']->content; ?>
    <div class="webinar-description">
      <?php print $fields['field_webinar_sh_description']->content; ?>
    </div>
  </div>
  <div class="info-right">
    <?php print l($author_img, 'user/' . $author_uid, array('html' => TRUE)); ?>
    <div class="webinar-price">
      <?php if ($webinar_is_free) : ?>
        <?php print theme('image', array('path' => base_path() . path_to_theme() . '/img/webinar-free.png')); ?>
      <?php else : ?>
        <?php print $fields['field_webinar_pr_participate']->content; ?>
        <div class="webinar-old-price">
          <?php print $fields['field_webinar_prhd_participate']->content; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
  <div class="info-bottom">
    <div class="duration first">
      <i class="icon-time"></i>
      <?php print $fields['field_webinar_duration']->content; ?>
    </div>
    <div class="nodelang">
      <i class="icon-globe"></i>
      <?php print $fields['field_webinar_language']->content; ?>
    </div>
    <div class="count-views">
      <i class="icon-eye-open"></i>
      <?php print $fields['totalcount']->content; ?>
    </div>
    <div class="comments">
      <i class="icon-comments-alt"></i>
      <?php print $fields['comment_count']->content; ?>
    </div>
    <div class="access<?php print $webinar_access_class; ?>">
      <?php print $fields['field_webinar_access']->content; ?>
    </div>
    <div class="date">
      <i class="icon-calendar"></i>
      <?php print $fields['field_webinar_date_1']->content; ?>
    </div>
  </div>
</div>
