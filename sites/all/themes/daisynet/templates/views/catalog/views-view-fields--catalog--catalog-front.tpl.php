<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<div class="catalog-front tile" id="wn<?php print $fields['nid']->content; ?>" style="background: #<?php print $fields['field_weinar_bg_color']->content; ?>">
  <?php /* if ($webinar_free) : ?><div class="webinar-free">&nbsp;</div><?php endif; */?>
  <?php /* print l(theme_image($variables), 'node/' . $row->nid, array('html' => TRUE)); */?>
  <div class="tile-body">
    <div class="category pull-right">
      <?php // if (isset($fields['field_bg_color'])): ?>
        <!-- <div style="background: #<?php print // $fields['field_bg_color']->content; ?>" class="bg" ></div> -->
      <?php // endif; ?>
      <?php print $fields['field_webinar_category_front']->content; ?>
    </div>
    <div class="user-info pull-left">
      <div class="user-image">
        <?php print $fields['field_user_image']->content; ?>
      </div>
      <div class="username">
        <?php print $fields['field_user_title']->content; ?>
        <?php print $fields['field_user_name']->content; ?>
        <?php print $fields['field_user_last_name']->content; ?>
      </div>
    </div>
    <div class="webinar-info pull-right">
      <?php if (isset($fields['field_abobe_connect_video'])):?>
        <div class="adobe"><?php print $fields['field_abobe_connect_video']->content?></div>
      <?php endif; ?>
      <div class="webinar-title"><?php print $fields['title']->content; ?></div>
      <div class="preview-interview" style="color: #<?php print $fields['field_bg_color']->content; ?>">
        <?php print t('watch the interview'); ?>
      </div>
    </div>
    <div class="event-info clearfix">
      <div class="webinar-date pull-right clearfix">
        <?php print $fields['field_webinar_date']->content; ?>
        <?php if (!$webinar_free && !$webinar_author): ?>
          <?php if (!empty($fields['field_tickets']->content)/* && $fields['field_tickets']->content > $tickets_bought*/): ?>
            <div class="tickets">
              <div><?php echo t('tickets bought'); ?></div>
              <?php echo $tickets_bought; ?> / <?php echo $fields['field_tickets']->content; ?>
            </span>
          <?php endif; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class="info-bottom clearfix">
    <div class="rating">
      <div class="average-rating"><?php echo $rating; ?></div>
      <?php if ($user->uid > 0): ?>
        <ul class="current-rating">
          <?php for ($i = 1; $i <= 5; $i++): ?>
            <li <?php if ($i <= $current_user_mark): ?>class="active"<?php endif; ?> >
              <input type="hidden" value="<?=$i?>" />
            </li>
          <?php endfor; ?>
        </ul>
      <?php endif; ?>
    </div>
    <div class="webinar-links pull-right">
      <div class="pull-right">
        <div class="sb-links" style="display: block;">
          <?php print $fields['field_share']->content; ?>
          <?php //print $fields['addtoany_link']->content; ?>
        </div>
        <div class="bookmark">
          <?php print $fields['ops']->content; ?>
        </div>
        <?php if(!$webinar_free && !$webinar_author): ?>
          <div class="basket <?php if($webinar_order_status == 'completed'): ?>participate<?php endif; ?>" id="<?php print $fields['nid']->content; ?>">
          <?php
            if (empty($fields['field_tickets']->content)) {
              $fields['field_tickets']->content = 0;
            }
            $all_tickets = intval(str_replace(',','',$fields['field_tickets']->content));
          ?>
          <?php if ($tickets_bought >= $all_tickets): ?>
            <div class="popup"><?php print t('All tickets bought'); ?></div>
          <?php else: ?>
            <?php if ($webinar_order_status == 'completed'): ?>
              <span class="participate"><?php print t('participate'); ?></span>
            <?php else: ?>
              <a class="add_to_cart" href="/cart/add/p<?php print $fields['nid']->content; ?>_a3"></a>
            <?php endif; ?>
          <?php endif; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>

<div class="webinar_preview hidden" style="background: #<?php print $fields['field_weinar_bg_color']->content; ?>" id="wp<?php print $fields['nid']->content; ?>">
  <?php if(isset($fields['field_webinar_preview']->content)): ?>
    <?php print $fields['field_webinar_preview']->content; ?>
  <?php endif; ?>
  <span class="preview_interview"></span>
</div>
