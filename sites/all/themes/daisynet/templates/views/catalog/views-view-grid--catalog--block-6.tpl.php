<script>

</script>

<div class="search_block hidden">

	<h3><?=t('Search')?></h3>
	<input type="text" id="search_input" />
	<div class="search_content all ">


		<ul class="<?php print $class; ?>"<?php print $attributes; ?>>

			<?php foreach ($rows as $row_number => $columns): ?>
				<?php foreach ($columns as $column_number => $item): ?>

					<li <?php if ($row_classes[$row_number]) { print 'class="' . $row_classes[$row_number] .' hidden"';  } ?>>
						<?php print $item; ?>
					</li>

				<?php endforeach; ?>
			<?php endforeach; ?>

		</ul>
	</div>

</div>