<?php
// Set Template Variables
$nid_content = $fields['nid']->content;
$tickets_content = empty($fields['field_tickets']->content) ? 0 : $fields['field_tickets']->content;
$all_tickets = intval(str_replace(',', '', $tickets_content));
?>

<div class="catalog-front tile" id="wn<?php echo $nid_content; ?>">
  <div class="tile-body clearfix">
    <div class="user-info clearfix" style="border-top:4px solid #<?php echo $fields['field_bg_color']->content; ?>">
      <div class="user-image clearfix">
        <?php print $fields['field_user_image']->content; ?>
      </div>
      <div class="user-dates">
        <div class="username">
          <?php
            $author_name =  $fields['field_user_name']->content . ' ' . $fields['field_user_last_name']->content;
            $author_name = mb_convert_case($author_name, MB_CASE_TITLE, "UTF-8");
            $author_name = $fields['field_user_title']->content . $author_name;
            $trim = array(
              'max_length' => 18,
              'ellipsis' => TRUE,
            );
            $author_name = views_trim_text($trim, $author_name);
            print $author_name
          ?>
        </div>
        <div class="country">
          <?php print $fields['field_user_country']->content; ?>
        </div>
        <div class="rating">
          <div class="average-rating"><?php echo $rating; ?></div>
            <ul class="current-rating">
              <?php for ($i = 1; $i <= 5; $i++): ?>
                <li <?php if ($i <= $current_user_mark): ?>class="active"<?php endif; ?> >
                  <input type="hidden" value="<?php print $i; ?>" />
                </li>
              <?php endfor; ?>
            </ul>
        </div>
      </div>
      <div class="preview-interview">
        <?php print $modal_preview_link; ?>
      </div>
    </div>
    <div class="webinar-info">
      <div class="webinar-name">
        <span class="webinar-title"><?php print $fields['title']->content; ?></span>
      </div>
    </div>
    <div class="bottom-info clearfix">
      <div class="show-info-button">
        <i class="icon-double-angle-up"></i>
      </div>
      <?php if ($tickets_content > 0): ?>
        <div class="tickets pull-right clearfix">
          <?php echo $tickets_label; ?><br /><span><?php echo $tickets_bought . '/' . $tickets_content; ?></span>
        </div>
      <?php endif; ?>
      <div class="webinar-date clearfix">
        <?php print $fields['field_webinar_date']->content; ?>
      </div>
    </div>
  </div>
  <div class="info-bottom clearfix">
    <div class="hide-info-button">
      <i class="icon-double-angle-down"></i>
    </div>
    <div class="webinar-links pull-right">
      <div class="pull-right">
        <div class="sb-links" title="<?php print t('Like this'); ?>">
          <a href="#" class="addthis-link social-toggle"><i class="icon-thumbs-up"></i></a>
          <div class="social-links">
            <div class="network facebook">
              <i class="icon-facebook"></i>
            </div>
            <div class="network google">
              <i class="icon-google-plus"></i>
            </div>
            <div class="network vkontakte">
              <i class="icon-vk"></i>
            </div>
          </div>
        </div>
        <div class="bookmark">
          <?php if ($user->uid && $user->uid != $fields['uid']->content): ?>
            <?php print $fields['ops']->content; ?>
          <?php elseif ($user->uid == 0): ?>
            <a class="bookmark" href="/user/register" title="<?php print t('Register to bookmark this'); ?>"><i class="icon-bookmark"></i></a>
          <?php endif; ?>
        </div>
        <div class="link-content-arrow" title="<?php print t('Go to Webinar Page'); ?>">
          <a href="<?php print url('node/' . $nid_content); ?>" class="icon-link-content">
            <i class="icon-circle-arrow-right"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
