<div class="<?php print $classes; ?>">
<?php if ($rows): ?>
<div id="intro">
  <div id="intro_wrap">
    <div id="image_intro" class="container_12">
      <div id="image_intro_preview" style="height: 320px">
        <div class="slides_container">
          <?php print $rows; ?>
        </div>
        <!--
        <span class="s_button_prev_holder">
          <a class="s_button_prev" href="javascript:;"></a>
        </span>
        <span class="s_button_next_holder">
          <a class="s_button_next" href="javascript:;"></a>
        </span>
        -->
        <div id="slidePager"></div>
      </div>
    </div>
  </div>
</div>
<?php 
  drupal_add_js(drupal_get_path('theme', 'daisy').'/js/libs/jquery.cycle.all.min.js');
  drupal_add_js(drupal_get_path('theme', 'daisy').'/js/libs/jquery.easing.1.3.min.js');
  drupal_add_js(drupal_get_path('theme', 'daisy').'/js/shoppica.images_slide.js');
  /*
	drupal_add_js('
		(function ($) { $(document).ready(function() {
			if ($(\'.slides_container\').length > 0) {
				$(\'.slides_container\').cycle({ 
					fx: \'scrollHorz\',
					speed: 800,
					timeout: 800, 
					randomizeEffects: false, 
					easing: \'easeOutCubic\',
					next:   \'.s_button_next\', 
					prev:   \'.s_button_prev\',
					pager:  \'#slidePager\',
					cleartypeNoBg: true
				});
			}
  }); })(jQuery);
	', array('type' => 'inline'));
  */
  /*
?>
<script type="text/javascript">
		(function ($) { $(document).ready(function() {
			if ($('.slides_container').length > 0) {
				$('.slides_container').cycle({ 
					fx: 'scrollHorz',
					speed: 800,
					timeout: 800, 
					randomizeEffects: false, 
					easing: 'easeOutCubic',
					next:   '.s_button_next', 
					prev:   '.s_button_prev',
					pager:  '#slidePager',
					cleartypeNoBg: true
				});
			}
  }); })(jQuery);
</script>
<?php */ endif; ?>
</div> 