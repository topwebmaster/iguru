<?php
  $class = strip_tags($fields['field_image_align']->content);
  $class = (empty($class))?('full'):$class;
?>
<?php if ($fields['field_image']->content) : ?>
  <?php $preset = ($class != 'full')?'slide_show_small':'slide_show'; ?>
  <?php $media_content = theme_image_style(array('style_name' => $preset, 'path' => $fields['uri']->content, 'width' => NULL, 'height' => NULL)); ?>
<?php else : ?>
  <?php $media_content = $fields['field_youtube']->content; ?>
<?php endif; ?>

<?php if(!$media_content && $class == 'full') $class .= '_noimage'; ?>

<div class="slide_front_<?php print $class; ?>">
  <?php if ($class == 'left'): ?>
    <?php print $media_content; ?>
  <?php endif; ?>
  <?php if ($class != 'full'): ?>
  <div class="slide_content">
    <?php if ($fields['field_url']->content) : ?>
      <?php print '<a href="'.url($fields['field_url']->content).'">'; ?>
    <?php endif; ?>

    <?php //print $fields['title']->content; ?>

    <?php if ($fields['field_url']->content) : ?>
      <?php print '</a>';?>
    <?php endif; ?>

    <?php print $fields['body']->content; ?>
  </div>
  <?php else: ?>
     <?php if ($fields['field_url']->content) : ?>
      <?php print '<a href="'.url($fields['field_url']->content).'">'; ?>
    <?php endif; ?>
    <?php print $media_content; ?>
    <?php if ($fields['field_url']->content) : ?>
        <?php print '</a>';?>
      <?php endif; ?>
    <?php endif; ?>
  <?php if ($class == 'right'): ?>
    <?php print $media_content; ?>
  <?php endif; ?>
</div>


