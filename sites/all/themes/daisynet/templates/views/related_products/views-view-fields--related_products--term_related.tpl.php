<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
$webinar_free = FALSE;
if (check_plain($fields['field_webinar_price']->content) == 'Free') {
  $webinar_free = TRUE;
}
?>

<?php
$variables['path'] = drupal_get_path('theme', 'daisynet') . '/img/video.jpg';
$variables['width'] = '178px';
$variables['height'] = '98px';
$variables['alt'] = $fields['title']->content;
$variables['title'] = $fields['title']->content;
?>

<div class="archive-video span3">
  <?php print l(theme_image($variables), 'node/' . $row->field_webinar_category_taxonomy_term_data_nid, array('html' => TRUE)); ?>
  <div class="info-bottom">
    <div class="info-wrapper">
      <div class="count-views first">
        <i class="icon-eye-open"></i>
        <?php print $fields['totalcount']->content; ?>
      </div>
      <div class="duration">
        <i class="icon-time"></i>
        <?php print $fields['field_webinar_duration']->content; ?>
      </div>
      <div class="nodelang">
        <i class="icon-globe"></i>
        <?php print $fields['field_webinar_language']->content; ?>
      </div>
    </div>
  </div>
</div>
