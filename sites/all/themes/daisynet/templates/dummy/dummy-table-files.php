<table class="placeholder user-additional">
  <thead>
  <tr>
    <th></th>
    <th class="description"><?php print t('Description'); ?></th>
    <th><?php print t('Added on'); ?></th>
    <th></th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td class="type"><i class="icon-film"></i></td>
    <td class="description"><?php print l('Lorem dolor sit amet', '<front>'); ?></td>
    <td class="date"><?php print date('d.m.Y' , time()); ?></td>
    <td class="operations">
      <div class="operations-holder">
        <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#"><i class="icon-chevron-down"></i></a>
        <ul class="dropdown-menu operations" role="menu">
          <li><a href="#" tabindex="-1" class="preview"><i class="icon-camera"></i><?php print t('Preview'); ?></a></li>
          <li><a href="#" tabindex="-1"  class="download"><i class="icon-download-alt"></i><?php print t('Download'); ?></a></li>
          <li><a href="#" tabindex="-1"  class="delete"><i class="icon-remove"></i><?php print t('Delete'); ?></a></li>
        </ul>
      </div>
    </td>
  </tr>
  <tr>
    <td class="type"><i class="icon-film"></i></td>
    <td class="description"><?php print l('Lorem dolor sit amet', '<front>'); ?></td>
    <td class="date"><?php print date('d.m.Y' , time()); ?></td>
    <td class="operations">
      <div class="operations-holder">
        <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#"><i class="icon-chevron-down"></i></a>
        <ul class="dropdown-menu operations" role="menu">
          <li><a href="#" tabindex="-1" class="preview"><i class="icon-camera"></i><?php print t('Preview'); ?></a></li>
          <li><a href="#" tabindex="-1"  class="download"><i class="icon-download-alt"></i><?php print t('Download'); ?></a></li>
          <li><a href="#" tabindex="-1"  class="delete"><i class="icon-remove"></i><?php print t('Delete'); ?></a></li>
        </ul>
      </div>
    </td>
  </tr>
  <tr>
    <td class="type"><i class="icon-film"></i></td>
    <td class="description"><?php print l('Lorem dolor sit amet', '<front>'); ?></td>
    <td class="date"><?php print date('d.m.Y' , time()); ?></td>
    <td class="operations">
      <div class="operations-holder">
        <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#"><i class="icon-chevron-down"></i></a>
        <ul class="dropdown-menu operations" role="menu">
          <li><a href="#" tabindex="-1" class="preview"><i class="icon-camera"></i><?php print t('Preview'); ?></a></li>
          <li><a href="#" tabindex="-1"  class="download"><i class="icon-download-alt"></i><?php print t('Download'); ?></a></li>
          <li><a href="#" tabindex="-1"  class="delete"><i class="icon-remove"></i><?php print t('Delete'); ?></a></li>
        </ul>
      </div>
    </td>
  </tr>
  <tr>
    <td class="type"><i class="icon-film"></i></td>
    <td class="description"><?php print l('Lorem dolor sit amet', '<front>'); ?></td>
    <td class="date"><?php print date('d.m.Y' , time()); ?></td>
    <td class="operations">
      <div class="operations-holder">
        <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#"><i class="icon-chevron-down"></i></a>
        <ul class="dropdown-menu operations" role="menu">
          <li><a href="#" tabindex="-1" class="preview"><i class="icon-camera"></i><?php print t('Preview'); ?></a></li>
          <li><a href="#" tabindex="-1"  class="download"><i class="icon-download-alt"></i><?php print t('Download'); ?></a></li>
          <li><a href="#" tabindex="-1"  class="delete"><i class="icon-remove"></i><?php print t('Delete'); ?></a></li>
        </ul>
      </div>
    </td>
  </tr>
  <tr>
    <td class="type"><i class="icon-film"></i></td>
    <td class="description"><?php print l('Lorem dolor sit amet', '<front>'); ?></td>
    <td class="date"><?php print date('d.m.Y' , time()); ?></td>
    <td class="operations">
      <div class="operations-holder">
        <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#"><i class="icon-chevron-down"></i></a>
        <ul class="dropdown-menu operations" role="menu">
          <li><a href="#" tabindex="-1" class="preview"><i class="icon-camera"></i><?php print t('Preview'); ?></a></li>
          <li><a href="#" tabindex="-1"  class="download"><i class="icon-download-alt"></i><?php print t('Download'); ?></a></li>
          <li><a href="#" tabindex="-1"  class="delete"><i class="icon-remove"></i><?php print t('Delete'); ?></a></li>
        </ul>
      </div>
    </td>
  </tr>
  </tbody>
</table>
