<?php
/**
 * @file
 * Template to display a column
 *
 * - $item: The item to render within a td element.
 */
$id = (isset($item['id'])) ? $item['id'] : '';
$date = (isset($item['date'])) ? 'date="' . $item['date'] . '" ' : '';
?>
<?php if ((strstr($id, 'box') !== FALSE) && !empty($item['entry'])) : ?>
  <li class="<?php print $item['class']; ?>">
    <?php print $item['entry']; ?>
  </li>
<?php elseif (strstr($item['entry'], 'item')): ?>
  <li id="<?php print $item['date']; ?>" class="post-list-holder">
    <?php print $item['entry']; ?>
    <?php if (arg(0) != 'catalog' && user_is_logged_in()) : ?>
      <div class="calendar-legend clearfix">
        <div class="category"><span class="btn green"></span><?php print t('Bookmark') ?></div>
        <div class="category"><span class="btn red"></span><?php print t('Author') ?></div>
      </div>
    <?php elseif (arg(0) == 'catalog'): ?>
      <div class="calendar-legend clearfix">
        <div class="green color clearfix"><?php print t('Top attended', array(), array('context' => 'webinar-legend')) ?></div>
        <div class="blue color clearfix"><?php print t('New events') ?></div>
        <div class="red color clearfix"><?php print t('Promotion\'s day') ?></div>
      </div>
    <?php else: ?>
      <div class="calendar-legend clearfix">
      </div>
    <?php endif; ?>
  </li>
<?php else: ?>
  <?php print ''; ?>

<?php endif; ?>
