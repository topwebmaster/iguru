<?php
/**
 * @file
 * Template to display the date box in a calendar.
 *
 * - $view: The view.
 * - $granularity: The type of calendar this box is in -- year, month, day, or week.
 * - $mini: Whether or not this is a mini calendar.
 * - $class: The class for this box -- mini-on, mini-off, or day.
 * - $day:  The day of the month.
 * - $date: The current date, in the form YYYY-MM-DD.
 * - $link: A formatted link to the calendar day view for this day.
 * - $url:  The url to the calendar day view for this day.
 * - $selected: Whether or not this day has any items.
 * - $items: An array of items for this day.
 */
$div1 = '';
$div2 = '';
$wd = '';
if (!$mini) {
  $wd = '<span>' . t(substr(date('D', strtotime($date)), 0, 2)) . '</span>';
}
$date_url = '#' . $date;
if (($view->current_display == 'mini_calendar' ||  $view->current_display == 'block_1') && $selected) {
  global $user;
  // Change index on view results
  $keyed_results = array();
  foreach ($view->result as $result) {
    $keyed_results[$result->date_id_field_webinar_date] = $result;
  }
  // Check for authored or bookmarked items for this day
  if (isset($items[$date])) {
    foreach ($items[$date] as $hour) {
      while (!empty($hour)) {
        $event = array_pop($hour);
        if ($keyed_results[$event->id]->node_uid == $user->uid) {
          $div1 = '<span class="author"></span>';
        }
        if ($keyed_results[$event->id]->flag_content_node_timestamp !== NULL) {
          $div2 = '<span class="bookmark"></span>';
        }
      }
    }
  }
  $class = (empty($class) ? $class1 : (empty($class1) ? $class : ($class . ' ' . $class1)));
  $date_url = url('cabinet', array('fragment' => $date));
}
?>

<?php
$arg = $view->argument['date_argument']->argument;
$current_date = date('Y-m', strtotime($date));
$wrapper_class = '';
if ($arg !== $current_date && $view->name == 'cabinet' && $view->current_display != 'page') {
  $wrapper_class = ' hide-date';
}
?>
<div class="date-wrapper<?php print $wrapper_class;?>">
  <?php if ($selected): ?>
    <a href="<?php print $date_url ?>"><?php print sprintf('%02d', $day); ?><?php print $wd; ?><?php print $div2; ?><?php print $div1; ?></a>
  <?php else: ?>
    <?php print sprintf('%02d', $day); ?><?php print $wd; ?>
  <?php endif; ?>
</div>

