<?php
/**
 * @file
 * Template to display a view as a calendar month.
 * 
 * @see template_preprocess_calendar_month.
 *
 * $day_names: An array of the day of week names for the table header.
 * $rows: An array of data for each day of the week.
 * $view: The view.
 * $calendar_links: Array of formatted links to other calendar displays - year, month, week, day.
 * $display_type: year, month, day, or week.
 * $block: Whether or not this calendar is in a block.
 * $min_date_formatted: The minimum date for this calendar in the format YYYY-MM-DD HH:MM:SS.
 * $max_date_formatted: The maximum date for this calendar in the format YYYY-MM-DD HH:MM:SS.
 * $date_id: a css id that is unique for this date, 
 *   it is in the form: calendar-nid-field_name-delta
 */

$special_displays = array('page_2');
$is_special_display = in_array($view->current_display, $special_displays);

$left = theme('image', array('path' => base_path() . $directory . '/img/calendar-big-arrow-left.png', 'alt' => t('Prev', array(), array('context' => 'date_nav'))));
$right = theme('image', array('path' => base_path() . $directory . '/img/calendar-big-arrow-right.png', 'alt' => t('Next', array(), array('context' => 'date_nav'))));
$left_link = l($left, '', array('html' => TRUE, 'attributes' => array('class' => array('nav', 'prev'))));
$right_link = l($right, '', array('html' => TRUE, 'attributes' => array('class' => array('nav', 'next'))));
if ($view->name == 'cabinet' && $is_special_display) {
  $left_link = '';
  $right_link = '';
}
?>
<div class="calendar-calendar s_tabs">
  <?php print $left_link; ?>
  <div class="month-view tabset-holder">

    <?php
    $cust_class = '';
    if ($view->name == 'cabinet' && $is_special_display) {
      print '<ul class="tabset s_tabs_nav clearfix cabinet-tabs">';
    }
    foreach ((array) $rows as $key => $row) {
      if ($view->name == 'cabinet' && (!$is_special_display)) {
        $cust_class = ' week-' . $key;
        $data = '<ul class="tabset s_tabs_nav clearfix cabinet-tabs week-container ' . $cust_class . '">';
        $data .= $row['data'];
        $data .= '</ul>';
        print $data;
      } else {
        print $row['data'];
      }
    }
    if ($view->name == 'cabinet' && $is_special_display) {
      print '</ul>';
    }
    ?>
    <div class="current-item">
      <div class="no-webinars">
        <?php print t('There are no webinars for this day.'); ?>
      </div>
    </div>
  </div>
  <?php print $right_link; ?>
</div>
