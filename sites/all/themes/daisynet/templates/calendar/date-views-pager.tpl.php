<?php
/**
 * @file
 * Template file for the example display.
 *
 * Variables available:
 * - $plugin: The pager plugin object. This contains the view as well as a lot
 *
 * $nav_title
 *   The formatted title for this view. In the case of block
 *   views, it will be a link to the full view, otherwise it will
 *   be the formatted name of the year, month, day, or week.
 *
 * $prev_url
 * $next_url
 *   Urls for the previous and next calendar pages. The links are
 *   composed in the template to make it easier to change the text,
 *   add images, etc.
 *
 * $prev_options
 * $next_options
 *   Query strings and other options for the links that need to
 *   be used in the l() function, including rel=nofollow.
 *
 * $block:
 *   Whether or not this view is in a block.
 *
 * $view
 *   The view object for this navigation.
 *
 * $pager_prefix
 *   Additional information that might be added by overriding template_process_date_views_pager().
 *
 * $extra_classes
 *   Extra classes for the wrapper, if any.
 *
 */

if ($plugin->view->name == 'cabinet' && (strstr($plugin->view->current_display, 'page') !== FALSE)) {
  $arg = $plugin->view->argument['date_argument']->argument;
  $view_arg_date = date('Y', strtotime($arg));
  $view_arg_month = date('n', strtotime($arg));
  $view_name = $plugin->view->name;
  if($plugin->view->name == 'cabinet') {
    if($plugin->view->current_display == 'page') {
      $page_path = 'cabinet';
    }
    else if($plugin->view->current_display == 'page_1'){
      $page_path = 'mywebinars';
    }
    else {
      $page_path = 'webinars';
    }
  }
  // Month links
  for ($i = 1; $i <= 12; $i++) {
    $active_class = '';
    if($i == $view_arg_month) {
      $active_class = 'active';
    }
    $date_argument = date('Y-m', mktime(0, 0, 0, $i, 1, $view_arg_date));
    $url = url($page_path . '/' . $date_argument, array('absolute' => TRUE));
    $months_arr[] = '<li class="month' . $i . '">' . l(t(date('M', mktime(0, 0, 0, $i, 1, $view_arg_date))), $url, array('attributes' => array('class' => array($active_class)))) . '</li>';
  }

  // Years links
  $url = url($page_path . '/' . date('Y-m', mktime(0, 0, 0, 1, 1, $view_arg_date - 1)), array('absolute' => TRUE));
  $years_arr[] = '<li>' . l(t(date('Y', mktime(0, 0, 0, 1, 1, $view_arg_date - 1))), $url) . '</li>';
  $url = url($page_path . '/' . date('Y-m', mktime(0, 0, 0, 1, 1, $view_arg_date)), array('absolute' => TRUE));
  $years_arr[] = '<li>' . l(t(date('Y', mktime(0, 0, 0, 1, 1, $view_arg_date))), $url, array('attributes' => array('class' => array('active')))) . '</li>';
  $url = url($page_path . '/' . date('Y-m', mktime(0, 0, 0, 1, 1, $view_arg_date + 1)), array('absolute' => TRUE));
  $years_arr[] = '<li>' . l(t(date('Y', mktime(0, 0, 0, 1, 1, $view_arg_date + 1))), $url) . '</li>';

}
?>
<?php
if(isset($mini) && $mini) {
  $left = theme('image', array('path' => base_path() . $directory . '/img/calendar-arrow-left.png', 'alt' => t('Prev', array(), array('context' => 'date_nav'))));
  $right = theme('image', array('path' => base_path() . $directory . '/img/calendar-arrow-right.png', 'alt' => t('Next', array(), array('context' => 'date_nav'))));
}
else {
  $left = '&laquo;';
  $right = '&raquo;';
}
?>
<?php if ($plugin->view->name == 'cabinet' && !(strstr($plugin->view->current_display, 'page_2') !== FALSE)): ?>
<?php if (!empty($pager_prefix)) print $pager_prefix; ?>
<div class="date-nav-wrapper clearfix<?php if (!empty($extra_classes))
  print $extra_classes; ?>">
  <div class="date-nav item-list">
    <?php if(isset($mini) && $mini): ?>
    <div class="date-heading">
      <h3><?php print $nav_title ?></h3>
    </div>
    <?php endif; ?>

    <?php if ($plugin->view->name == 'cabinet' && (strstr($plugin->view->current_display, 'page') !== FALSE)): ?>
    <ul class="pager years">
      <?php print implode('', $years_arr); ?>
    </ul>
    <?php endif; ?>

    <ul class="pager months">
      <?php if ($plugin->view->name == 'cabinet' && (strstr($plugin->view->current_display, 'page') !== FALSE)): ?>
        <?php print implode('', $months_arr); ?>
      <?php else: ?>
        <?php if (!empty($prev_url)) : ?>
          <li class="date-prev">
            <?php print l($left . ($mini ? '' : ' ' . t('Prev', array(), array('context' => 'date_nav'))), $prev_url, $prev_options); ?>
            &nbsp;</li>
        <?php endif; ?>
        <?php if (!empty($next_url)) : ?>
          <li class="date-next">&nbsp;
            <?php print l(($mini ? '' : t('Next', array(), array('context' => 'date_nav')) . ' ') . $right, $next_url, $next_options); ?>
          </li>
        <?php endif; ?>

      <?php endif; ?>
    </ul>

  </div>
</div>
<?php endif; ?>
