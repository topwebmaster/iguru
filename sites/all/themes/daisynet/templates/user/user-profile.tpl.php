<?php
/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 */

$terms = array();
$account = user_load(arg(1));
if (isset($user_profile['field_user_category']['#items'])) {
  foreach ($user_profile['field_user_category']['#items'] as $key => $item) {
    $level = count(taxonomy_get_parents_all($item['tid']));
    $terms[$level][] = i18n_taxonomy_term_name($item['taxonomy_term']);
  }
}
$user_is_online = daisynet_user_is_online(arg(1));
//$ref_statistics = daisynet_extra_get_received_points();
//$user_profile['field_user_image'][0]['#image_style'] = 'user_image_profile';
?>
<div class="profile profile-top "<?php print $attributes; ?>>
  <div class="user-profile-left">
    <?php print render($user_profile['field_user_image']); ?>
  </div>
  <div class="user-profile-right">
    <div class="user-info-top clearfix">
      <div class="daisy-user-name">
        <?php print daisynet_get_username(arg(1)); ?>
      </div>
      <div class="user-grad">
        <?php print render($user_profile['field_user_grade']); ?>
      </div>
      <div class="user-country">
        <?php print render($user_profile['field_user_country']); ?>
      </div>
      <?php if (daisy_user_is_expert($account->uid)): ?>
      <div class="user-range-wrapp">
        <span class="user-range expert"><?php print t('Expert'); ?></span>
      </div>
      <?php endif;?>
      <div class="user-online<?php print ($user_is_online ? ' online' : ' offline') ?>">
        <span><?php print ($user_is_online ? t('Online') : t('Offline')); ?></span>
      </div>
      <div class="profile-option">
        <ul class="list-daisy pull-right">
          <?php if (user_edit_access($account)) :?>
            <li>
              <?php print l(t("Edit"), 'user/' . $account->uid . '/edit', array('attributes' => array('class' => array('btn', 'btn-daisy')))); ?>
            </li>
          <?php endif; ?>
          <li>
            <a class="btn btn-daisy" href="#" onclick="window.print(); return false;">
              <?php print t('Print'); ?>
            </a>
          </li>
        </ul>
      </div>

      <div class="sb-links pull-right" title="Like this">
        <ul class="list-daisy">
          <?php if ($account->uid != $user->uid && ($user->uid > 0) && ($account->uid > 0)) :?>
            <li class="send-request">
              <?php print daisy_user_relationships($account->uid, FALSE, ' btn btn-daisy'); ?>
            </li>
          <?php endif; ?>
          <li class="dropdown">
            <div class="social-links">
              <div class="network facebook">
                <i class="icon-facebook"></i>
              </div>
              <div class="network google">
                <i class="icon-google-plus"></i>
              </div>
              <div class="network vkontakte">
                <i class="icon-vk"></i>
              </div>
            </div>
            <a href="#" class="btn btn-daisy addthis-link social-toggle"><i class="icon-thumbs-up-alt"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="profile profile-middle">
  <div class="user-info">
    <?php print render($user_profile['field_user_about']); ?>
    <?php print render($user_profile['field_user_languages']); ?>
    <?php print render($user_profile['field_user_age_category']); ?>
    <?php if ($user->uid == arg(1) || in_array('administrator', $user->roles)) : ?>
      <?php $user_profile['field_user_recommended_name']['#title'] = t('Recommended by');?>
      <?php print render($user_profile['field_user_recommended_name']); ?>
      <?php hide($user_profile['field_user_recommended_grade']); ?>
      <?php hide($user_profile['field_user_recommended_email']); ?>
      <?php hide($user_profile['field_user_recommended_country']); ?>
      <?php hide($user_profile['field_user_recommended_phone']); ?>
    <?php endif; ?>
      <?php $user_profile['field_prefered_categories']['#title'] = t('Interested in');?>
    <?php print render($user_profile['field_prefered_categories']); ?>
      <?php $user_profile['field_user_category']['#title'] = t('Expert in');?>
      <?php $user_profile['field_user_category']['#label_display'] = 'above';?>
    <?php print render($user_profile['field_user_category']); ?>
      <?php if (isset($user_profile['Referrals'])) : ?>
      <div class="field field-referral-info">
        <?php print render($user_profile['Referrals']); ?>
      </div>
      <?php endif; ?>
    <?php //print $ref_statistics; ?>
    <?php hide($user_profile); ?>
  </div>
</div>
<div class="profile profile-bottom">
  <div class="additional profile-additional">
    <ul class="nav nav-tabs">
      <li class="active">
        <a href="#tab_files" data-toggle="tab">
          <?php print t('Files'); ?>
          <i class="icon-chevron-down"></i>
        </a>
      </li><li>
        <a href="#tab_events" data-toggle="tab">
          <?php print t('Events'); ?>
          <i class="icon-chevron-down"></i>
        </a>
      </li><li>
        <a href="#tab_friends" data-toggle="tab">
          <?php print t('Friends'); ?>
          <i class="icon-chevron-down"></i>
        </a>
      </li><li>
        <a href="#tab_group" data-toggle="tab">
          <?php print t('Groups'); ?>
          <i class="icon-chevron-down"></i>
        </a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_files">
        <?php include drupal_get_path('theme', 'daisynet') . '/templates/dummy/dummy-table-files.php'; ?>
      </div>
      <div class="tab-pane" id="tab_events">
        <?php include drupal_get_path('theme', 'daisynet') . '/templates/dummy/dummy-table-files.php'; ?>
      </div>
      <div class="tab-pane" id="tab_friends">
        <?php include drupal_get_path('theme', 'daisynet') . '/templates/dummy/dummy-table-files.php'; ?>
      </div>
      <div class="tab-pane" id="tab_group">
        <?php include drupal_get_path('theme', 'daisynet') . '/templates/dummy/dummy-table-files.php'; ?>
      </div>
    </div>
  </div>
</div>
<?php
 /* if ($user->uid && $user->uid != arg(1)) : ?>
  <div class="relationships">
    <?php // print daisy_user_relationships(arg(1)); ?>
  </div>
  <?php // endif; */
?>
