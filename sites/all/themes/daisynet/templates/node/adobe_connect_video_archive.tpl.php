<?php
// $Id: block.tpl.php,v 1.3.2.1 2011/01/12 23:05:22 mauritsl Exp $

/**
 * @file
 * Advanced
 */
?>
<div class="adobe-connect-recordings-holder">
  <table class="adobe-connect-recording-table">
    <thead>
      <tr>
        <th></th>
        <th><?php print t("Webinar name"); ?></th>
        <th><?php print t("Length"); ?></th>
        <th><?php print t("Action"); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php if (empty($recordings)) : ?>
      <tr>
        <td colspan="4" class="no-results"><?php print t('No recordings are available'); ?></td>
      </tr>
      <?php else:?>
      <?php foreach ($recordings as $index => $recording ) { ?>
      <tr>
        <td class="number"><?php print $index+1; ?></td>
        <td class="webinar-name"><?php print $recording['name']; ?></td>
        <td class="duration"><?php print $recording['duration']; ?></td>
        <td class="action">
        <?php if ($user_access): ?>
          <a href="<?php print url('adobe_connect/recording' .$recording['url']); ?>" target="_blank"><?php print t('Play');?><i class="icon-play"></i></a>
        <?php else:?>
          <a href="#"><?php print t('Limited access')?></a>
        <?php endif; ?>
        </td>
      </tr>
      <?php } ?>
      <?php endif; ?>
    </tbody>
  </table>
</div>
