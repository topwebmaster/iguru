<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content-wrapp clearfix">
    <div class="phot-wrapp">
      <?php print render($content['field_group_picture']); ?>
    </div>
    <div class="group-info-wrapp">
      <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
      <div class="author-name">
        <?php print t("Created by:"); ?> <?php print $name; ?>
      </div>
      <div class="group-status">
        <?php print t("Status:"); ?> <?php print $group_status; ?>
      </div>
      <?php print render($content['body']); ?>
      <ul class="buttons-group">
        <li class="text-link">
          <?php print l($subscribe_label, 'group/node/' . $node->nid . '/subscribe'); ?>
        </li>
        <li class="text-link icons-link">
          <?php print l(t('<i class="icon-pencil"></i>'), 'node/' . $node->nid . '/edit', array('html' => TRUE)); ?>
        </li>
        <li class="icons-link">
          <?php print l(t('<i class="icon-remove"></i>'), 'node/' . $node->nid . '/delete', array('html' => TRUE)); ?>
        </li>
      </ul>
    </div>
    <?php
      // Render Group's Content
      hide($content['field_group_category']);
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_tags']);
      unset($content['links']['statistics']);
      print render($content);
      // Render Group's Members
    ?>
    <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
      <footer>
        <?php print render($content['field_tags']); ?>
        <?php print render($content['links']); ?>
      </footer>
    <?php endif; ?>
  </div>
  <div class="additional group-additional tabbable row-fluid">
    <ul class="nav nav-tabs">
      <li class="active">
        <a href="#tab_dashboard" data-toggle="tab">
          <?php print t('Dashboard'); ?>
        </a>
      </li>
      <li>
        <a href="#tab_files" data-toggle="tab">
          <?php print t('Files'); ?>
        </a>
      </li>
      <li>
        <a href="#tab_events" data-toggle="tab">
          <?php print t('Events'); ?>
        </a>
      </li>
      <li>
        <a href="#tab_members" data-toggle="tab">
          <?php print t('Members'); ?>
        </a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_dashboard">
        <?php print render($content['comments']); ?>
      </div>
      <div class="tab-pane" id="tab_files">
        <?php include drupal_get_path('theme', 'daisynet') . '/templates/dummy/dummy-table-files.php'; ?>
      </div>
      <div class="tab-pane" id="tab_events">
        <?php print views_embed_view('catalog', 'archive_top'); ?>
      </div>
      <div class="tab-pane" id="tab_members">
        <a class="group-settings" href="#"><?php t('Settings'); ?></a>
        <?php include drupal_get_path('theme', 'daisynet') . '/templates/dummy/dummy-glossary.php'; ?>
        <table class="administer-group-members">
          <thead>
            <tr>
              <th class="check-list"><input type="checkbox" class="select-all"></th>
              <th class="members-name"><?php print t('Members name'); ?></th>
              <th class="status"><?php print t('Status'); ?></th>
              <th class="operations"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="check-list"><input type="checkbox" checked="checked"></td>
              <td class="members-name">
                <img src="/sites/default/files/image015.png" class="group-member-thumb" />
                <a href="#" class="user-link">Mr.Albrecht ZIMMERMAN</a>
              </td>
              <td class="status"><?php print t('Expert'); ?></td>
              <td class="operations">
                <input type="button" class="btn btn-daisy negative" value="Block" />
              </td>
            </tr>
            <tr>
              <td class="check-list"><input type="checkbox" checked="checked"></td>
              <td class="members-name">
                <img src="/sites/default/files/image015.png" class="group-member-thumb" />
                <a href="#" class="user-link">Mr.Albrecht ZIMMERMAN</a>
              </td>
              <td class="status"><?php print t('Expert'); ?></td>
              <td class="operations">
                <input type="button" class="btn btn-daisy negative" value="Block" />
              </td>
            </tr>
            <tr>
              <td class="check-list"><input type="checkbox" checked="checked"></td>
              <td class="members-name">
                <img src="/sites/default/files/image015.png" class="group-member-thumb" />
                <a href="#" class="user-link">Mr.Albrecht ZIMMERMAN</a>
              </td>
              <td class="status"><?php print t('Expert'); ?></td>
              <td class="operations">
                <input type="button" class="btn btn-daisy negative" value="Block" />
              </td>
            </tr>
            <tr>
              <td class="check-list"><input type="checkbox" checked="checked"></td>
              <td class="members-name">
                <img src="/sites/default/files/image015.png" class="group-member-thumb" />
                <a href="#" class="user-link">Mr.Albrecht ZIMMERMAN</a>
              </td>
              <td class="status"><?php print t('Expert'); ?></td>
              <td class="operations">
                <input type="button" class="btn btn-daisy negative" value="Block" />
              </td>
            </tr>
            <tr>
              <td class="check-list"><input type="checkbox" checked="checked"></td>
              <td class="members-name">
                <img src="/sites/default/files/image015.png" class="group-member-thumb" />
                <a href="#" class="user-link">Mr.Albrecht ZIMMERMAN</a>
              </td>
              <td class="status"><?php print t('Expert'); ?></td>
              <td class="operations">
                <input type="button" class="btn btn-daisy negative" value="Unblock" />
              </td>
            </tr>
            <tr>
              <td class="check-list"><input type="checkbox" checked="checked"></td>
              <td class="members-name">
                <img src="/sites/default/files/image015.png" class="group-member-thumb" />
                <a href="#" class="user-link">Mr.Albrecht ZIMMERMAN</a>
              </td>
              <td class="status"><?php print t('Expert'); ?></td>
              <td class="operations">
                <input type="button" class="btn btn-daisy negative" value="Block" />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</article>
