<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php
    $term_id = $content['field_webinar_category_front']['#items']['0']['tid'];
    $term = taxonomy_term_load($term_id);
  ?>
  <header>


    <?php if ($page && $title): ?>
      <h2 class="webinar-title"><?php print $title; ?></h2>

      <?php print render($title_suffix); ?>
    <?php endif; ?>
  </header>
  <!--generic wrappers start-->
  <div class="webinar-info-bar clearfix nav navbar" style="border-top:5px solid <?php print $category_color; ?>">
    <ul class="webinar-info-bar-menu nav">
      <li class="first-one">
        <?php if (isset($webinar_day) && isset($webinar_month)): ?>
          <div class="date_day_month">
            <span class="day"><?php print $webinar_day;?></span>
            <span class="month"><?php print $webinar_month; ?></span>
          </div>
        <?php endif; ?>
        <?php print render($content['field_webinar_date']); ?>
      </li>
      <li class="donation-for-webinar">
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
          <input type="hidden" name="cmd" value="_s-xclick">
          <input type="hidden" name="hosted_button_id" value="5ZS6UWQ99DZJC">
          <button class="btn btn-primary" type="submit" name="submit">Пожертвовать $$</button>
          <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>
      </li>
      <?php if($content['links']['flag'] && ($user->uid != $node->uid)): ?>
        <li class="second">
          <div class="sb-links">
            <?php print render($content['links']['flag']); ?>
          </div>
        </li>
      <?php endif; ?>
      <li class="third share-links sb-links">
        <a href="#" class="btn btn-daisy addthis-link social-toggle"><i class="icon-thumbs-up-alt"></i></a>
        <div class="social-links">
          <div class="network facebook">
            <i class="icon-facebook"></i>
          </div>
          <div class="network google">
            <i class="icon-google-plus"></i>
          </div>
          <div class="network vkontakte">
            <i class="icon-vk"></i>
          </div>
        </div>
      </li>
      <li class="forth">
        <div class="webinar-price">
          <?php if (($is_author || $webinar_ordered == 'Participate') && $private_access) : ?>
            <?php print '<span class="btn default btn-free">' . render($content['field_abobe_connect_video']) . '</span>'; ?>
          <?php elseif (!$webinar_is_free): ?>
            <?php if (($webinar_ordered === FALSE || $webinar_ordered == 'View') && !$is_author  && $private_access) : ?>
              <div class="buy-webinar<?php if ($webinar_ordered == 'View') print ' buy-participate';?>">
                <?php print render($content['add_to_cart']); ?>
              </div>
            <?php endif; ?>
          <?php else: ?>
            <span class="btn default btn-free<?php if (user_is_logged_in() && $private_access && $is_participant && $webinar_is_free) print ' btn-has-link'; ?>">
              <?php if (user_is_logged_in() && $private_access) :?>
                <?php print render($content['field_abobe_connect_video']); ?>
              <?php else: ?>
                <?php print t('Limited access'); ?>
              <?php endif; ?>
            </span>
          <?php endif; ?>
        </div>
      </li>
      <li class="play-preview-wrapp">
        <div class="play-preview">
          <a href="#">
            <i class="icon-play"></i>
          </a>
        </div>
      </li>
      <?php if ($is_live) : ?>
        <li class="live">
          <?php print t('LIVE'); ?>
        </li>
      <?php endif; ?>
    </ul>
  </div>

  <div class="webinar-content clearfix">
    <?php if ($can_edit = true): ?>
      <div class="tools">
        <a href="javascript:;" class="config"></a>
        <a href="javascript:;" class="remove"></a>
      </div>
    <?php endif; ?>
    <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['rate_vote']);
    hide($content['comments']);
    hide($content['field_tags']);
    hide($content['field_webinar_date']);
    hide($content['field_webinar_duration']);
    hide($content['field_webinar_language']);
    hide($content['field_webinar_keywords']);
    hide($content['field_webinar_image']);
    hide($content['field_webinar_access']);
    hide($content['field_webinar_dfiles']);
    hide($content['field_tickets']);
    hide($content['sell_price']);
    hide($content['display_price']);
    hide($content['cost']);
    hide($content['list_price']);
    hide($content['field_webinar_price']);
    hide($content['add_to_cart']);
    hide($content['field_webinar_category']);
    hide($content['field_webinar_category_front']);
    hide($content['field_abobe_connect_video']);
    hide($content['field_abobe_connect_video_arch']);
    hide($content['links']);
    hide($content['field_webinar_prhd_participate']);
    hide($content['field_webinar_sh_description']);
    hide($content['field_webinar_prhd_view']);
    hide($content['field_webinar_pr_participate']);
    hide($content['field_webinar_pr_view']);
    hide($content['field_has_discount']);
    hide($content['field_share']);
    print render($content);
    print '<div class="webinar-keywords">' . render($content['field_webinar_keywords']) . '</div>';
    ?>
      <ul class="webinar-data inline">
        <li class="participants">
          <i class="icon-male"></i>
          <?php print $participants_count . '/' . $content['field_tickets'][0]['#markup']; ?>
        </li>
        <?php if (isset($content['field_webinar_duration'])) : ?>
          <li class="duration">
            <i class="icon-time"></i>
            <?php print $content['field_webinar_duration'][0]['#markup'] . ' ' . t('min.'); ?>
          </li>
        <?php endif; ?>
        <?php if (isset($content['field_webinar_language'])) : ?>
          <li class="nodelang">
            <i class="icon-globe"></i>
            <?php print $content['field_webinar_language'][0]['#markup']; ?>
          </li>
        <?php endif; ?>
        <li class="count-views">
          <i class="icon-eye-open"></i>
          <?php print $views_count; ?>
        </li>
        <li class="comments">
          <i class="icon-comments-alt"></i>
          <?php print $comment_count; ?>
        </li>
        <?php if (isset($content['field_webinar_access'])) : ?>
          <?php $webinar_access_class = ($content['field_webinar_access']['#items'][0]['value'] == 'public') ? 'icon-unlock' : 'icon-lock'; ?>
          <li class="access">
            <i class="<?php print $webinar_access_class; ?>"></i>
            <?php print $content['field_webinar_access'][0]['#markup']; ?>
          </li>
        <?php endif; ?>
      </ul>
  </div>
  <!--generic wrappers end-->
  <!--outer wrapper end-->
  <div class="webinar-additional tabbable row-fluid">
    <ul class="nav nav-tabs">
      <li>
        <a href="#tab_related" data-toggle="tab"><?php print t('Similar Watches'); ?></a>
      </li>
      <li class="active">
        <a href="#tab_comments" data-toggle="tab"><?php print t('Comments'); ?></a>
      </li>
    </ul>
    <div class="tab-content">
      <?php if (true): ?>
        <div class="tab-pane" id="tab_related">
          <?php print views_embed_view('related_products', 'term_related'); ?>
        </div>
      <?php endif; ?>
      <div class="tab-pane active" id="tab_comments">
        <?php print render($content['comments']); ?>
      </div>
    </div>
    <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
      <footer>
        <?php print render($content['field_tags']); ?>
      </footer>
    <?php endif; ?>
  </div>
</article> <!-- /.node -->
