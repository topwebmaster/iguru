<article  id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix past webinar-teaser-past"<?php print $attributes; ?>
  style="border-top:4px solid <?php print $category_color; ?>">
  <header>
    <div class="buttons-wrapp">
      <?php if(!$webinar_is_free): ?>
        <?php if ($webinar_ordered === FALSE && !$is_author && !$webinar_is_free): ?>
          <div class="buy-webinar">
            <?php print render($content['add_to_cart']); ?>
          </div>
        <?php endif; ?>
      <?php else: ?>
        <a class="btn default btn-play" href="<?php print url('node/' . $node->nid); ?>"><i class="icon-play"></i></a>
      <?php endif; ?>
    </div>
    <div class="author-wrapp">
      <div class="author-picture">
        <?php print $author_picture; ?>
      </div>
      <h4 class="webinar-title">
        <?php print l($trimmed_title, 'node/' . $node->nid); ?>
      </h4>
    </div>
  </header>
  <!--generic wrappers start-->
  <div class="webinar-content clearfix">
    <?php if(empty($content['field_webinar_image'])){ ?>
      <div class="webinar-preview">
      </div>
    <?php }
    else {
      print render($content['field_webinar_image']);
    } ?>
  </div>
</article> <!-- /.node -->
<?php hide($content); ?>
