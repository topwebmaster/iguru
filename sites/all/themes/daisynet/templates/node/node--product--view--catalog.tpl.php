<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> span6 clearfix"<?php print $attributes; ?>>
  <header>
    <?php if ($display_submitted): ?>
      <span class="submitted">
        <?php print $user_picture; ?>
        <?php print $submitted; ?>
      </span>
    <?php endif; ?>
  </header>
  <!--generic wrappers start-->
  <div class="webinar-content-outer-wrapper row-fluid">
    <div class="webinar-content-wrapper span12">
      <div class="webinar-date-vote span1">
        <?php if (isset($webinar_day) && isset($webinar_month)): ?>
        <div class="webinar-date">
          <span class="day"><?php print $webinar_day; ?></span>
          <span class="month"><?php print $webinar_month; ?></span>
        </div>
        <?php endif; ?>
        <div class="sb-links">
        <?php print render($content['addtoany']); ?>
        <?php if ($user->uid != $node->uid) : ?>
          <?php print render($content['links']['flag']); ?>
        <?php endif; ?>
        <?php //if ($user_hase_prod && $webinar_expired) : ?>
        <?php print render($content['rate_vote']); ?>
        <?php //endif; ?>
        </div>
      </div>
      <div class="webinar-content span9">
        <?php if ($can_edit = true): ?>
        <div class="tools">
          <a href="javascript:;" class="config"></a>
          <a href="javascript:;" class="remove"></a>
        </div>
        <?php endif; ?>

        <div class="webinar-categories row-fluid">
         <?php print render($content['field_webinar_category']); ?>
        </div>
        <?php
          // Hide comments, tags, and links now so that we can render them later.
          hide($content['comments']);
          hide($content['links']);
          hide($content['field_tags']);
          hide($content['field_webinar_date']);
          hide($content['field_webinar_duration']);
          hide($content['field_webinar_language']);
          hide($content['field_webinar_keywords']);
          hide($content['field_webinar_image']);
          hide($content['field_webinar_access']);
          hide($content['field_webinar_dfiles']);
          hide($content['field_tickets']);
          hide($content['sell_price']);
          hide($content['display_price']);
          hide($content['cost']);
          hide($content['list_price']);
          hide($content['field_webinar_price']);
          hide($content['add_to_cart']);
          hide($content['field_webinar_category_front']);
          if (!$page && $teaser) {
            print render($content);
            print '<div class="webinar-keywords">' . render($content['field_webinar_keywords']) . '</div>';
          }
          else {
            hide($content['field_webinar_sh_description']);
            print render($content);
            print '<div class="webinar-keywords">' . render($content['field_webinar_keywords']) . '</div>';
          }
        ?>
      </div>
      <div class="webinar-author-info span2">
        <?php print $author_picture; ?>
        <div class="author-name"><?php print $author_name; ?></div>
        <?php if($author_country || false): ?>
          <div class="author-country"><i class="icon-map-marker"></i><?php print $author_country; ?></div>
        <?php endif; ?>
        <?php if (!$webinar_expired && !$webinar_ordered && !$is_author && !$webinar_is_free): ?>
          <div class="webinar-price">
            <div class="buy-webinar"><?php print render($content['add_to_cart']); ?></div>
          </div>
        <?php endif; ?>
        <?php if (isset($content['field_webinar_date']) && !$teaser) : ?>
          <div class="date">
            <span><i class="icon-calendar"></i><?php print t('Starts on'); ?></span>
            <?php print render($content['field_webinar_date']); ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<!--generic wrappers end-->
  <div class="webinar-data-wrapper row-fluid">
    <ul class="webinar-data inline">
      <?php if (isset($content['field_webinar_duration'])) : ?>
        <li class="duration">
          <i class="icon-time"></i>
          <?php print $content['field_webinar_duration'][0]['#markup'] . ' ' . t('min.'); ?>
        </li>
      <?php endif; ?>
      <?php if (isset($content['field_webinar_language'])) : ?>
        <li class="nodelang">
          <i class="icon-globe"></i>
          <?php print $content['field_webinar_language'][0]['#markup']; ?>
        </li>
      <? endif; ?>
      <li class="count-views">
        <i class="icon-eye-open"></i>
        <?php print $views_count; ?>
      </li>
      <li class="comments">
        <i class="icon-comments-alt"></i>
        <?php print $comment_count; ?>
      </li>
      <?php if (isset($content['field_webinar_access'])) : ?>
        <?php $webinar_access_class = ($content['field_webinar_access']['#items'][0]['value'] == 'public') ? 'icon-unlock' : 'icon-lock'; ?>
        <li class="access">
          <i class="<?php print $webinar_access_class; ?>"></i>
          <?php print $content['field_webinar_access'][0]['#markup']; ?>
        </li>
      <?php endif; ?>
      <?php if (isset($content['field_webinar_date'])) : ?>
        <li class="date">
          <?php print render($content['field_webinar_date']); ?>
        </li>
      <?php endif; ?>
    </ul>
  </div>
  <!--outer wrapper end-->
  <?php if ($page && !$teaser): ?>
    <div class="webinar-additional tabbable row-fluid">
      <ul class="nav nav-tabs">
        <li class="active">
          <a href="#tab_1_1" data-toggle="tab"><?php print t('Comments'); ?></a>
        </li>
        <li>
          <a href="#tab_1_2" data-toggle="tab"><?php print t('Files'); ?></a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1_1">
          <?php print render($content['comments']); ?>
        </div>
        <div class="tab-pane" id="tab_1_2">
          <?php print render($content['field_webinar_dfiles']);?>
        </div>
      </div>
    </div>
    <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
      <footer>
        <?php print render($content['field_tags']); ?>
        <?php hide($content['links']); ?>
      </footer>
    <?php endif; ?>
  <?php endif;?>
</article> <!-- /.node -->
