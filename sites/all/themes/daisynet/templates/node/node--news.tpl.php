<?php if ($user_can_edit): ?>
  <div class="node-portlet portlet box blue">
    <div class="portlet-title">
      <h4><?php print $title; ?></h4>
      <div class="tools"><?php print render($title_suffix); ?></div>
    </div>
    <div class="portlet-body">
<?php endif; ?>
      <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> news clearfix"<?php print $attributes; ?>>
        <header>
          <?php if (!$page && $title): ?>
            <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
          <?php endif; ?>
          <?php if ($display_submitted): ?>
            <span class="submitted">
              <?php //print $user_picture; print $submitted; ?>
            </span>
          <?php endif; ?>
        </header>

        <?php
          //Hide comments, tags, and links now so that we can render them later.
          hide($content['comments']);
          hide($content['links']);
          hide($content['field_tags']);
          hide($content['language']);
          // print render($content);
        ?>
        <div class="image-category-comments-date">
          <div class="news-title"><p><?php print render($title); ?></p></div>
          <div class="news-image-field"><?php print render($content['field_image']);?></div>
          <div class="news-under-image-tags">
            <div class="news-category-field">
              <i class="icon-tags"><?php print render($content['field_webinar_category']); ?></i>
            </div>
            <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
              <div class="news-comment-field">
                <?php print render($content['field_tags']); ?>
                <?php print render($content['links']); ?>
              </div>
            <?php endif; ?>
            <?php print render($content['comments']); ?>
            <div class="news-created-date">
              <p><i class="icon-calendar"></i><?php print render($date);?></p>
            </div>
          </div>
        </div>
       <div class="news-field-body">
       <?php print render($content['body']);?>
       </div>
      </article><!-- /.node -->
<?php if ($user_can_edit): ?>
    </div>
  </div>
<?php endif; ?>
