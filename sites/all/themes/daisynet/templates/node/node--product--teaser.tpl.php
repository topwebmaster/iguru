<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>
  style="border-top: 4px solid <?php print $category_color; ?>"
>
  <header>
    <div class="date-wrapp">
      <?php if (isset($webinar_day) && isset($webinar_month) && isset($webinar_year)): ?>

        <div class="date_day_month">
          <span class="day"><?php print $webinar_day;?></span>
          <span class="month"><?php print $webinar_month; ?></span>
          <span class="year"><?php print $webinar_year; ?></span>
        </div>
        <div class="date_hour_timezone">
          <span class="hour"><?php print $webinar_hour; ?></span>
        </div>
      <?php endif; ?>
    </div>
    <?php if ($is_live) : ?>
      <div class="live-wrapp">
        <span class="live"><?php print t('LIVE');?></span>
      </div>
    <?php endif; ?>
    <div class="author-wrapp">
      <div class="author-picture">
        <?php print $author_picture; ?>
      </div>
      <div class="author-name">
        <?php print $author_name; ?>
      </div>
      <?php if ($author_country): ?>
        <div class="author-country">
          <?php print $author_country; ?>
        </div>
      <?php endif; ?>
      <li class="donation-for-webinar">
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
          <input type="hidden" name="cmd" value="_s-xclick">
          <input type="hidden" name="hosted_button_id" value="5ZS6UWQ99DZJC">
          <button class="btn btn-primary" type="submit" name="submit">Пожертвовать $$</button>
          <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>
      </li>
    </div>
  </header>
  <!--generic wrappers start-->
  <div class="webinar-content clearfix">
    <h3 class="webinar-title">
      <?php print l($trimmed_title, 'node/'. $node->nid); ?>
    </h3>
    <?php print render($content['field_webinar_sh_description']); ?>
  </div>
  <div class="webinar-price">
    <?php if (($is_author || $webinar_ordered == 'Participate') && $private_access) : ?>
      <?php print '<span class="btn default btn-free">' . render($content['field_abobe_connect_video']) . '</span>'; ?>
    <?php elseif (!$webinar_is_free): ?>
      <?php if (($webinar_ordered === FALSE || $webinar_ordered == 'View') && !$is_author  && $private_access) : ?>
        <div class="buy-webinar<?php if ($webinar_ordered == 'View') print ' buy-participate';?>">
          <?php print render($content['add_to_cart']); ?>
        </div>
      <?php endif; ?>
    <?php else: ?>
      <span class="btn default btn-free<?php if (user_is_logged_in() && $private_access && $is_participant && $webinar_is_free) print ' btn-has-link'; ?>">
        <?php if (user_is_logged_in() && $private_access) :?>
          <?php print render($content['field_abobe_connect_video']); ?>
        <?php else: ?>
          <?php print t('Limited access'); ?>
        <?php endif; ?>
      </span>
    <?php endif; ?>
  </div>
</article> <!-- /.node -->
<?php hide($content); ?>
