<?php if ($user_can_edit): ?>
<div class="node-portlet portlet box blue">
  <div class="portlet-title">
    <h4><?php print $title; ?></h4>
    <div class="tools">
      <?php print render($title_suffix); ?>
    </div>
  </div>
  <div class="portlet-body">
<?php endif; ?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <header>
    <div class="category-border-color" id="wn<?php echo $content; ?>" style="background-color: <?php echo $category_color; ?>">
    </div>
    <?php if ($display_submitted): ?>
      <span class="submitted">
        <?php print $user_picture; ?>
        <?php print $submitted; ?>
      </span>
    <?php endif; ?>
  </header>
  <!--generic wrappers start-->
  <?php if ($teaser || !$page): ?>
    <h3 class="webinar-title">
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h3>
  <?php endif; ?>
  <?php if ($page && $title): ?>
    <h2 class="webinar-title"><?php print $title; ?></h2>
  <?php endif; ?>

  <div class="webinar-content-outer-wrapper row-fluid">
    <div class="webinar-content-wrapper span12">
      <div class="webinar-info-bar clearfix nav navbar">
        <ul class="webinar-info-bar-menu nav">
          <li class="first-one">
            <?php if (isset($webinar_day) && isset($webinar_month)): ?>
              <div class="date_day_month">
                <span class="day"><?php print $webinar_day;?></span>
                <span class="month"><?php print $webinar_month; ?></span>
              </div>
            <?php endif; ?>
            <?php print render($content['field_webinar_date']); ?>
          </li>
          <li class="second">
            <div class="sb-links">
              <?php if ($user->uid != $node->uid) : ?>
                <?php print render($content['links']['flag']); ?>
              <?php endif; ?>
            </div>
          </li>
          <li class="third">
            <ul class="share-links">
              <li class="dropdown user">
                <a href="#" class="dropdown-toggle addthis-link" data-toggle="dropdown"><i class="icon-thumbs-up"></i></a>
                <ul class="dropdown-menu menu clearfix">
                  <li><?php print render($content['field_share']); ?></li>
                </ul>
              </li>
            </ul>
            <?php hide($content['rate_vote']); ?>
            <?php if ($user_has_product && $webinar_expired) : ?>
            <?php print render($content['rate_vote']); ?>
            <?php endif; ?>
          </li>
          <li class="forth">
            <div class="webinar-price">
              <?php if(!$webinar_is_free): ?>
                <ul class="prices nav">
                  <li class="participate-fee">
                    <?php if (!empty($content['field_webinar_pr_participate'])): ?>
                      <ul class="nav participate-prices-fee">
                        <li class="price price-participate">
                          <?php print t('Participate') . ':' ;?>
                        </li>
                        <li class="price-participate">
                          <?php print render($content['field_webinar_pr_participate']); ?>
                        </li>
                        <li class="discount-value price-participate">
                          <?php print render($content['field_webinar_prhd_participate']); ?>
                        </li>
                      </ul>
                    <?php endif; ?>
                  </li>
                  <li class="watch-fee">
                    <?php if (!empty($content['field_webinar_pr_view'])): ?>
                      <ul class="nav watching-fee-prices">
                        <li class="price price-view">
                          <?php print t('Watch') . ':'; ?>
                        </li>
                        <li class="price-watch">
                          <?php print render($content['field_webinar_pr_view']); ?>
                        </li>
                        <li class="discount-value price-view">
                          <?php print render($content['field_webinar_prhd_view']); ?>
                        </li>
                      </ul>
                    <?php endif; ?>
                  </li>
                </ul>
                <?php if (!$webinar_expired && $webinar_ordered === FALSE && !$is_author && !$webinar_is_free): ?>
                  <div class="buy-webinar">
                    <?php print render($content['add_to_cart']); ?>
                  </div>
                <?php endif; ?>
              <?php else: ?>
                <span class="btn default btn-free"><?php print t('Free'); ?></span>
              <?php endif; ?>

              <?php if (!$webinar_expired && ($webinar_ordered == 'Participate' || $is_author || $webinar_is_free)): ?>
                <div class="participate">
                  <?php print render($content['field_abobe_connect_video']); ?>
                </div>
              <?php endif; ?>
              <?php if ($webinar_ordered == 'View' || $webinar_ordered == 'Participate' || $is_author || $webinar_is_free): ?>
                <div class="view-recordings">
                  <?php print render($content['field_abobe_connect_video_arch']); ?>
                </div>
              <?php endif; ?>
              <?php if (isset($content['field_webinar_date'])) : ?>
              <?php endif; ?>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="webinar-content clearfix">
    <?php if ($can_edit = true): ?>
      <div class="tools">
        <a href="javascript:;" class="config"></a>
        <a href="javascript:;" class="remove"></a>
      </div>
    <?php endif; ?>
    <div class="webinar-categories row-fluid">
      <?php if($page): ?>
        <?php print render($content['field_webinar_category']);?>
      <?php else: ?>
        <?php print render($content['field_webinar_category_front']); ?>
      <?php endif; ?>
    </div>
    <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
    hide($content['field_webinar_date']);
    hide($content['field_webinar_duration']);
    hide($content['field_webinar_language']);
    hide($content['field_webinar_keywords']);
    hide($content['field_webinar_image']);
    hide($content['field_webinar_access']);
    hide($content['field_webinar_dfiles']);
    hide($content['field_tickets']);
    hide($content['sell_price']);
    hide($content['display_price']);
    hide($content['cost']);
    hide($content['list_price']);
    hide($content['field_webinar_price']);
    hide($content['add_to_cart']);
    hide($content['field_webinar_category']);
    hide($content['field_webinar_category_front']);
    hide($content['field_abobe_connect_video']);
    hide($content['field_abobe_connect_video_arch']);
    hide($content['links']);
    hide($content['field_webinar_prhd_participate']);
    hide($content['field_webinar_prhd_view']);
    hide($content['field_webinar_pr_participate']);
    hide($content['field_webinar_pr_view']);
    hide($content['field_has_discount']);
    if (!$page && $teaser) {
      print render($content);
      print '<div class="webinar-keywords">' . render($content['field_webinar_keywords']) . '</div>';
    } else {
      hide($content['field_webinar_sh_description']);
      print render($content);
      print '<div class="webinar-keywords">' . render($content['field_webinar_keywords']) . '</div>';
    ?>
      <ul class="webinar-data inline">
        <?php if (isset($content['field_webinar_duration'])) : ?>
          <li class="duration">
            <i class="icon-time"></i>
            <?php print $content['field_webinar_duration'][0]['#markup'] . ' ' . t('min.'); ?>
          </li>
        <?php endif; ?>
        <?php if (isset($content['field_webinar_language'])) : ?>
          <li class="nodelang">
            <i class="icon-globe"></i>
            <?php print $content['field_webinar_language'][0]['#markup']; ?>
          </li>
        <?php endif; ?>
        <li class="count-views">
          <i class="icon-eye-open"></i>
          <?php print $views_count; ?>
        </li>
        <li class="comments">
          <i class="icon-comments-alt"></i>
          <?php print $comment_count; ?>
        </li>
        <?php if (isset($content['field_webinar_access'])) : ?>
          <?php $webinar_access_class = ($content['field_webinar_access']['#items'][0]['value'] == 'public') ? 'icon-unlock' : 'icon-lock'; ?>
          <li class="access">
            <i class="<?php print $webinar_access_class; ?>"></i>
            <?php print $content['field_webinar_access'][0]['#markup']; ?>
          </li>
        <?php endif; ?>
      </ul>
    <?php } ?>
  </div>
  <!--generic wrappers end-->
  <!--outer wrapper end-->
  <?php if ($page && !$teaser): ?>
    <div class="additional webinar-additional tabbable row-fluid">
      <ul class="nav nav-tabs">
        <li>
          <a href="#tab_related" data-toggle="tab"><?php print t('Similar Watches'); ?></a>
        </li>
        <li class="active">
          <a href="#tab_comments" data-toggle="tab"><?php print t('Comments'); ?></a>
        </li>
      </ul>
      <div class="tab-content">
        <?php if (true): ?>
          <div class="tab-pane" id="tab_related">
            <?php print views_embed_view('related_products', 'term_related'); ?>
          </div>
        <?php endif; ?>
        <div class="tab-pane active" id="tab_comments">
          <?php print render($content['comments']); ?>
        </div>
      </div>
      <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
        <footer>
          <?php print render($content['field_tags']); ?>
          <?php hide($content['links']); ?>
        </footer>
      <?php endif; ?>
    </div>
  <?php endif;?>
</article> <!-- /.node -->
<?php if ($user_can_edit): ?>
  </div>
</div>
<?php endif; ?>
