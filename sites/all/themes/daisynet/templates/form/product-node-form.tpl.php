<div class="portlet box default product-node-form">
  <div class="portlet-title">
    <h4>
      <i class="icon-reorder"></i>
      <?php
        $title = isset($form['#node']->nid)
                 ? t('Edit Webinar @title', array('@title' => $form['#node']->title))
                 : t('Create Webinar');
        print $title;
      ?>
    </h4>
  </div>

  <div class="portlet-body">
    <div class="form-wizard">
      <!-- navbar steps start -->
      <div class="navbar steps">
        <div class="navbar-inner">
          <ul class="row-fluid nav nav-pills">
             <li class="span4 active">
                <a href="#name-and-duration" data-toggle="tab" class="step active">
                <span class="number">1</span>
                <span class="desc"><i class="icon-ok"></i><?php print t('General Info'); ?></span>
                </a>
             </li>
             <li class="span4">
                <a href="#access-and-price" data-toggle="tab" class="step">
                <span class="number">2</span>
                <span class="desc"><i class="icon-ok"></i><?php print t('Price and Tickets'); ?></span>
                </a>
             </li>
             <li class="span4">
                <a href="#description-and-category" data-toggle="tab" class="step">
                <span class="number">3</span>
                <span class="desc"><i class="icon-ok"></i><?php print t('Description and Files'); ?></span>
                </a>
             </li>
          </ul>
        </div>
      </div>
      <!-- navbar steps end -->
      <div id="bar" class="progress progress-success progress-striped active">
        <div class="bar" style="width: 33%"></div>
      </div>
      <div class="tab-content">
        <div class="tab-pane active" id="name-and-duration">
          <!-- Fields: Name and Duration -->
          <div class="row-fluid">
            <div class="span6">
              <?php print drupal_render($form['title']); ?>
              <?php print drupal_render($form['field_webinar_sh_description']); ?>
              <?php print drupal_render($form['body']); ?>
            </div>
            <div class="span6">
              <?php print drupal_render($form['uc_product_image']); ?>
              <?php print drupal_render($form['field_webinar_duration']); ?>
              <?php print drupal_render($form['field_webinar_language']); ?>
              <?php print drupal_render($form['field_webinar_category_front']); ?>
              <?php print drupal_render($form['field_webinar_category']); ?>
            </div>
          </div>
          <div class="row-fluid">
            <div class="span6"></div>
            <div class="span6"></div>
          </div>
        </div>
        <!-- Fields: Access and Price -->
        <div class="tab-pane" id="access-and-price">
          <div class="row-fluid">
            <div class="span6">
              <?php print drupal_render($form['field_webinar_price']); ?>
              <?php print drupal_render($form['field_has_discount']); ?>
              <?php print drupal_render($form['field_webinar_prhd_view']); ?>
              <?php print drupal_render($form['field_webinar_prhd_participate']); ?>
              <?php print drupal_render($form['field_webinar_pr_view']); ?>
              <?php print drupal_render($form['field_webinar_pr_participate']); ?>
              <?php print drupal_render($form['field_tickets']); ?>
            </div>
            <div class="span6">
              <?php print drupal_render($form['field_webinar_access']); ?>
              <?php print drupal_render($form['field_participants']); ?>
              <?php print drupal_render($form['field_webinar_date']); ?>
            </div>
          </div>
          <div class="row-fluid">
            <div class="span4">
              <?php print drupal_render($form['field_webinar_top']); ?>
            </div>
            <div class="span4">
              <?php print drupal_render($form['field_webinar_master']); ?>
            </div>
            <div class="span4">
              <?php print drupal_render($form['field_webinar_hd']); ?>
            </div>
          </div>
        </div>
        <!-- Fields: Informational : Description, Language, Category, and KeysWords-->
        <div class="tab-pane" id="description-and-category">
          <div class="row-fluid">
            <div class="span6">
              <?php print drupal_render($form['field_adobe_connect_video']); ?>
              <?php print drupal_render($form['field_webinar_keywords']); ?>
            </div>
            <div class="span6">
              <?php print drupal_render($form['field_adobe_connect_video_arch']); ?>
              <?php print drupal_render($form['field_webinar_preview']); ?>
            </div>
          </div>
          <div class="row-fluid">
            <div class="span12">
              <div class="span6"><?php print drupal_render($form['field_webinar_dfiles']); ?></div>
              <div class="span6"><?php /*print drupal_render($form['field_webinar_files']);*/ print drupal_render($form['field_webinar_image']);?></div>
            </div>
          </div>
        </div>
      </div>
      <div class="form-actions clearfix">
        <a href="javascript:;" class="btn button-previous" style="display: none;">
          <i class="m-icon-swapleft"></i> <?php print t('Back'); ?>
        </a>
        <a href="javascript:;" class="btn blue button-next"><?php print t('Next'); ?> <i class="m-icon-swapright m-icon-white"></i></a>
        <a href="javascript:;" class="btn green button-submit" style="display: none;">
          <?php $end_form_label = isset($form['#node']->nid) ? t('Save Webinar') : t('Create Webinar'); ?>
          <?php print $end_form_label; ?> <i class="m-icon-swapright m-icon-white"></i>
        </a>
        <?php print drupal_render($form['actions']); ?>
        <div class="hidden-fields">
          <?php print drupal_render_children($form); ?>
        </div>
      </div>
    </div>
  </div>

</div>
