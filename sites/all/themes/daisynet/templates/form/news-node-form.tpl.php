<div class="portlet box default news-node-form">
  <div class="portlet-title">
    <h4><i class="icon-reorder"></i>
      <?php
        $title = isset($form['#node']->nid)
                 ? t('Edit News @title', array('@title' => $form['#node']->title))
                 : t('Create News');
        print $title;
      ?>
    </h4>
  </div>
  <div class="portlet-body">
    <?php hide($form['additional_settings']); ?>
    <div class="row-fluid">
      <div class="span6"><?php print drupal_render($form['title']); ?></div>
      <div class="span6"><?php print drupal_render($form['language']); ?></div>
    </div>
    <div class="row-fluid">
      <div class="span12"><?php print drupal_render($form['body']); ?></div>
    </div>
    <div class="row-fluid">
      <div class="span6"><?php print drupal_render($form['field_image']); ?></div>
      <div class="span6"><?php print drupal_render($form['field_webinar_category']); ?></div>
    </div>
    <?php print drupal_render_children($form); ?>
    <div class="hidden-fields">
      <?php print drupal_render($form['additional_settings']); ?>
    </div>
  </div>
</div>