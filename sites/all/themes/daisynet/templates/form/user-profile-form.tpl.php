<div class="portlet box grey">
  <div class="portlet-title">
    <h4><i class="icon-reorder"></i><?php print t("@user's Profile", array('@user' => $user->name)); ?></h4>
    <?php if ($experts_manager && arg(2) == 'become_expert'):?>
    <div class="tools">
    <?php
      $query = array(
        'destination' => 'manage_experts',
      );
      print l('', 'user/' . $user->uid . '/become_expert/accept', array('query' => $query, 'attributes' => array('class' => 'icon-ok-circle', 'title' => t('Accept'))));
      print l('', 'user/' . $user->uid . '/become_expert/decline/nojs', array('query' => $query, 'attributes' => array('class' => 'icon-ban-circle ctools-use-modal', 'title' => t('Decline'))));
    ?>
    </div>
    <?php endif;?>
  </div>
  <div class="portlet-body">
    <div class="form">
      <div class="tabbable tabble-user-form form-horizontal">
        <div id="daisy-user-form" class="tab-content">
          <?php if($show_general_form || $show_password_form): ?>
            <div class="tab-pane active" id="user-general-fields">
              <h3 class="form-section">
                <?php if($show_general_form): ?><?php print t('User Info'); ?><?php endif; ?>
                <?php if($show_password_form): ?><?php print t('Password Info'); ?><?php endif; ?>
              </h3>
              <div class="row-fluid">
                <div class="span6">
                  <?php if($show_general_form): print render($form['account']['name']); endif; ?>
                  <?php if($show_password_form): print render($form['account']['current_pass_required_values']); endif; ?>
                  <?php if($show_password_form): print render($form['account']['pass']); endif; ?>
                  <?php if($show_password_form): print render($form['account']['current_pass']); endif; ?>
                </div>
                <?php if($show_general_form): ?>
                  <div class="span6">
                    <?php print render($form['account']['mail']);?>
                    <?php print render($form['account']['current_pass']); ?>
                  </div>
                <?php endif; ?>
              </div>
              <?php if ($show_general_form): ?>
              <div class="row-fluid">
                <div class="span6">
                  <?php print render($form['field_user_name']); ?>
                </div>
                <div class="span6">
                  <?php print render($form['field_user_last_name']); ?>
                  <?php print render($form['field_user_father_name']); ?>
                </div>
              </div>
              <div class="row-fluid">
                <div class="span6">
                  <?php if($show_general_form): print render($form['account']['status']); endif; ?>
                </div>
                <div class="span6">
                  <?php if($show_general_form): print render($form['account']['roles']); endif; ?>
                </div>
              </div>
              <div class="span12">
                <?php render($form['account']['notify']);?>
              </div>
              <div class="row-fluid">
                <div class="span12 user-title">
                  <?php print render($form['field_user_title']); ?>
                </div>
              </div>
              <div class="row-fluid">
                <div class="span12 user-image">
                  <?php print render($form['field_user_image']); ?>
                </div>
              </div>
              <div class="row-fluid">
                <div class="span6 preffered-categories">
                  <?php print render($form['field_prefered_categories']); ?>
                </div>
                <div class="span6">
                  <?php print render($form['group_audience']); ?>
                </div>
              </div>
              <div class="row-fluid">
                <div class="span6">
                  <?php print render($form['timezone']); ?>
                </div>
                <div class="span6">
                  <?php print render($form['privatemsg']); ?>
                </div>
              </div>
              <?php endif; ?>
            </div>
          <?php endif; ?>
          <?php if ($show_expert_form): ?>
            <div class="tab-pane active" id="user-expert-fields">
              <h3 class="form-section"><?php print t('Expert Info'); ?></h3>
              <div class="row-fluid">
                <div class="span6">
                  <?php print render($form['field_user_about']); ?>
                  <?php //print render($form['field_user_phone']); ?>
                  <?php print render($form['field_user_country']); ?>
                  <?php print render($form['field_user_languages']); ?>
                </div>
                <div class="span6">
                  <?php print render($form['field_user_grade']); ?>
                </div>
              </div>
              <h3 class="form-section"><?php print t('Recommendations'); ?></h3>
              <div class="row-fluid">
                <div class="span6">
                  <?php //print render($form['field_user_recommended']); ?>
                  <?php print render($form['field_user_recommended_name']); ?>
                  <?php print render($form['field_user_recommended_grade']); ?>
                </div>
                <div class="span6">
                  <?php print render($form['field_user_recommended_country']); ?>
                  <?php print render($form['field_user_recommended_email']); ?>
                  <?php print render($form['field_user_recommended_phone']); ?>
                </div>
              </div>
              <h3 class="form-section"><?php print t('Webinar Settings'); ?></h3>
              <div class="row-fluid">
                <div class="span6">
                  <?php print render($form['field_user_category']); ?>
                </div>
                <div class="span6">
                  <?php print render($form['field_user_age_category']); ?>
                </div>
              </div>
              <?php if(FALSE): ?>
                <h3 class="form-section"><?php print t('Social Links'); ?></h3>
                <div class="row-fluid">
                  <div class="span6">
                    <?php print render($form['field_user_facebook']); ?>
                    <?php print render($form['field_user_twitter']); ?>
                  </div>
                  <div class="span6">
                    <?php print render($form['field_user_vkontakte']); ?>
                    <?php print render($form['field_user_']); ?>
                  </div>
                </div>
              <?php endif; ?>
                <h3 class="form-section"><?php print t('DaisyNet Agreement Policy'); ?></h3>
                <div class="row-fluid">
                  <div class="span12">
                    <?php print render($form['field_user_agreement']); ?>
                  </div>
                </div>
            </div>
          <?php endif; ?>
          <input type="hidden" name="form_id" value="<?php print $form['#form_id']; ?>" />
          <input type="hidden" name="form_build_id" value="<?php print $form['#build_id']; ?>" />
          <input type="hidden" name="form_token" value="<?php print $form['form_token']['#default_value']; ?>" />
          <?php print render($form['actions']); ?>
        </div>
      </div>
    </div>
  </div>
</div>
