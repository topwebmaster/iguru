<div class="portlet box blue">
  <div class="portlet-title">
    <h4><i class="icon-reorder"></i>
      <?php
        $title = isset($form['#node']->nid) ? t('Edit Webinar @title', array('@title' => $form['#node']->title)) : t('Create Webinar');
        print $title;
      ?>
    </h4>
  </div>

  <div class="portlet-body">
    <div class="node-edit">
      <!-- navbar steps start -->
      <div class="tab-content node-edit">
        <div class="tab-edit active" id="name-and-duration">
          <h3 class="desc"><?php print t('General Info'); ?></h3>
          <!-- Fields: Name and Duration -->
          <div class="row-fluid">
            <div class="span6">
              <?php print drupal_render($form['title']); ?>
              <?php print drupal_render($form['field_webinar_sh_description']); ?>
              <?php print drupal_render($form['body']); ?>
            </div>
            <div class="span6">
              <?php print drupal_render($form['uc_product_image']); ?>
              <?php print drupal_render($form['field_webinar_duration']); ?>
              <?php print drupal_render($form['field_webinar_language']); ?>
              <?php print drupal_render($form['field_webinar_category_front']); ?>
              <?php print drupal_render($form['field_webinar_category']); ?>
            </div>
          </div>
          <div class="row-fluid">
            <div class="span6"></div>
            <div class="span6"></div>
          </div>
        </div>
        <!-- Fields: Access and Price -->
        <div class="tab-edit" id="access-and-price">
          <h3 class="desc"><?php print t('Price and Tickets'); ?></h3>
          <div class="row-fluid">
            <div class="span6">
              <?php print drupal_render($form['field_webinar_price']); ?>
              <?php print drupal_render($form['field_has_discount']); ?>
              <?php print drupal_render($form['field_webinar_prhd_view']); ?>
              <?php print drupal_render($form['field_webinar_prhd_participate']); ?>
              <?php print drupal_render($form['field_webinar_pr_view']); ?>
              <?php print drupal_render($form['field_webinar_pr_participate']); ?>
              <?php print drupal_render($form['field_tickets']); ?>
            </div>
            <div class="span6">
              <?php print drupal_render($form['field_webinar_access']); ?>
              <?php print drupal_render($form['field_participants']); ?>
              <?php print drupal_render($form['field_webinar_date']); ?>
            </div>
          </div>
          <div class="row-fluid">
            <h3 class="desc"><?php print t('Display settings'); ?></h3>
            <div class="span4">
              <?php print drupal_render($form['field_webinar_top']); ?>
            </div>
            <div class="span4">
              <?php print drupal_render($form['field_webinar_master']); ?>
            </div>
            <div class="span4">
              <?php print drupal_render($form['field_webinar_hd']); ?>
            </div>
          </div>
        </div>
        <!-- Fields: Informational : Description, Language, Category, and KeysWords-->
        <div class="tab-edit" id="description-and-category">
          <h3 class="desc"><?php print t('Description and Files'); ?></h3>
          <div class="row-fluid">
            <div class="span4">
              <?php print drupal_render($form['field_adobe_connect_video']); ?>
              <?php print drupal_render($form['field_webinar_keywords']); ?>
            </div>
            <div class="span4">
              <?php print drupal_render($form['field_adobe_connect_video_arch']); ?>
              <?php print drupal_render($form['field_webinar_preview']); ?>
            </div>
            <div class="span4">
              <?php print drupal_render($form['field_webinar_image']);?>
            </div>
          </div>
          <?php if(FALSE): ?>
          <div class="row-fluid">
            <div class="span6"><?php print drupal_render($form['field_webinar_dfiles']); ?></div>
            <div class="span6"><?php print drupal_render($form['field_webinar_files']); ?></div>
          </div>
          <?php endif; ?>
        </div>
      </div>
      <?php print drupal_render($form['actions']); ?>
      <div class="hidden-fields" style="visibility:hidden; position: absolute;">
        <?php print drupal_render_children($form); ?>
      </div>
    </div>
  </div>

</div>
