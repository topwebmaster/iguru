<div class="portlet box default group-node-form">
  <div class="portlet-title">
    <h4><i class="icon-reorder"></i>
      <?php
        $title = isset($form['#node']->nid)
                 ? t('Edit Group @title', array('@title' => $form['#node']->title))
                 : t('Create Group');
        print $title;
      ?>
    </h4>
  </div>
  <div class="portlet-body">
    <div class="row-fluid">
      <div class="span6">
        <?php print drupal_render($form['title']); ?>
        <?php print drupal_render($form['field_group_category']); ?>
        <?php print drupal_render($form['field_group_picture']); ?>
      </div>
      <div class="span6">
        <?php print drupal_render($form['field_group_type']); ?>
        <?php print drupal_render($form['body']); ?>
        <?php print drupal_render($form['actions']); ?>
      </div>
    </div>
    <div class="hidden-fields">
      <?php print drupal_render_children($form); ?>
    </div>
  </div>
</div>
