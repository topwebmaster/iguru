<div id="edit-categories" class="form-checkboxes">
  <?php foreach($form['categories']['#options'] as $key => $option): ?>
    <?php $is_checked = (in_array($key, $form['categories']['#default_value'])) ? 'checked="checked"' : ''; ?>
    <div class="form-item form-type-checkbox form-item-categories-<?php print $key; ?>">
      <label class="option" for="edit-categories-<?php print $key; ?>"><?php print t($option); ?></label>
      <input type="checkbox" id="edit-categories-<?php print $key; ?>" name="categories[<?php print $key; ?>]" class= "form-checkbox"
        <?php print $is_checked; ?> 
        value="<?php print $key; ?>"/>
    </div>
  <?php endforeach; ?>
</div>
<?php unset($form['categories']); ?>
<div class="form-action form-wrapper" id="edit-actions">
  <?php print drupal_render($form['actions']); ?>
</div>
<div class="hidden-fields">
  <?php print drupal_render_children($form); ?>
</div>
