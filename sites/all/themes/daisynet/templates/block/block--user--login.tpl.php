<section id="block-user-multi-login" class="<?php print $classes; ?>">
  <?php if ($title): ?>
    <?php print render($title_prefix); ?>
      <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
    <?php print render($title_suffix); ?>
  <?php endif;?>

  <div class="auth-form tile red">
    <ul class="nav nav-tabs" id="myTab">
      <li class="active"><a href="#login-form" data-toggle="tab"><?php print t('Log In'); ?></a></li>
      <li><a href="#register-form" data-toggle="tab"><?php print t('Sign Up'); ?></a></li>
    </ul>
    <div class="tab-content">
      <div id="login-form" class="auth-form-wrapper tab-pane active clearfix multi-pane">
        <h2><?php print t('Welcome!'); ?></h2>
        <?php print $content; ?>
      </div>
      <div id="register-form" class="auth-form-wrapper tab-pane clearfix multi-pane">
        <h2><?php print t('Register Now'); ?></h2>
        <?php print drupal_render($user_register); ?>
        <?php
        /*
        <ul>
          <li>
            <?php print l(t('Request new password'), 'user/password', array('attributes' => array('class' => 'user-password'))); ?>
          </li>
        </ul>
         */
        ?>
      </div>
      <?php
      /*
      <div id="recover-password" class="auth-form-wrapper clearfix multi-pane">
        <h2><?php print t('Forgot your password?'); ?></h2>
        <?php print drupal_render($user_pass); ?>
        <ul>
          <li>
            <?php print l(t('Already a member?'), 'user/login', array('attributes' => array('class' => 'user-login'))); ?>
          </li>
          <li>
            <?php print l(t('Create a new account'), 'user/register', array('attributes' => array('class' => 'user-register'))); ?>
          </li>
        </ul>
      </div>
       */ ?>
    </div>
  </div>
</section>
