<section id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?> >
  <div class="block-toggler">
    <?php if (isset($title_icon)): ?>
      <?php print $title_icon; ?>
    <?php endif;?>
  </div>
  <?php if ($title): ?>
    <?php print render($title_prefix); ?>
      <h2<?php print $title_attributes; ?>>
        <?php
          if (isset($title_icon)) {
            print $title_icon;
            print '<span>' . $title . '</span>';
          } else {
            print $title;
          }
        ?>
        <span class="visibility-toggler">
          <?php print $close_icon; ?>
        </span>
      </h2>
    <?php print render($title_suffix); ?>
  <?php endif;?>
  <div class="block-content-holder">
    <?php print $content ?>
  </div>
</section> <!-- /.block -->
