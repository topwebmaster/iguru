<?php
/**
 * Front page messages block.
 * Uses the internal messaging system to display user's messages.
 *
 * Template variables:
 * $block_not_empty - determines if there are notifications and messages to display.
 * $notifications - determines the notifications list for display.
 *    Structure:
 *      - title - the link to the title of the notification.
 *      - summary - the summary text of the notification.
 *      - type - the type of the message. Set to 'notification'.
 * $messages - determines the messages list for display.
 *    Structure:
 *      - title - the link to the title of the messages.
 *      - summary - the summary text of the message.
 *      - author_link - the link to the author of the message.
 *      - type - the type of the message. Set to 'message'.
 *
 * @see template.php
 */
?>
<?php if ($block_not_empty): ?>
<section id="<?php print $block_html_id; ?>" class=" <?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if ($title): ?>
    <?php print render($title_prefix); ?>
      <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
    <?php print render($title_suffix); ?>
  <?php endif;?>
	<div class="content front-messages tile double-down">
        <?php if (isset($notifications) && count($notifications)>0): ?>
          <ul class="msg visible clearfix">
            <?php foreach ($notifications as $notification):	?>
              <li class="<?php print $notification['type']; ?> row-fluid clearfix">
                <img class="author" src="<?=file_create_url('/sites/all/themes/daisy/img/logo_old.png')?>">
                <div class="message-description">
                  <?php print $notification['title']; ?>
                  <span class="summary"><?php print $notification['summary']; ?></span>
                </div>
              </li>
            <?php endforeach; ?>
          </ul>
        <? endif; ?>

        <?php if (isset($messages) && count($messages)>0): ?>
          <ul class="msg visible clearfix">
            <?php foreach ($messages as $message): ?>
              <li class="<?php print $message['type']; ?> row-fluid clearfix">
                <div class="author-link"><?php print $message['author_link']; ?></div>
                <div class="message-description">
                  <?php print $message['title']; ?>
                  <span class="summary"><?php print $message['summary']; ?></span>
                </div>
              </li>
            <?php endforeach; ?>
          </ul>
        <?php endif; ?>

        <?php if ( (count($notifications) + count($messages)) > 4 ): ?>
          <div class="item-list">
            <ul class="pager">
              <li class="pager-next">
                <span> </span>
              </li>
            </ul>
          </div>
        <?endif;?>
	</div>
</section>
<?php endif; ?>
