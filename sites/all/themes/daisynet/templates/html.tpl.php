<!DOCTYPE html>
<html lang="<?php print $language->language; ?>">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name='yandex-verification' content='6e0a9f6356a06627' />
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <!-- ported from Daisy Theme -->
    <!--[if lt IE 8]>
    <link type="text/css" rel="stylesheet" href="<?php print $theme_path; ?>/css/style-ie7.css" media="all" />
    <![endif]-->
    <!--[if IE 8]>
    <link type="text/css" rel="stylesheet" href="<?php print $theme_path; ?>/css/style-ie8.css" media="all" />
    <![endif]-->
    <!--[if IE 9]>
    <link type="text/css" rel="stylesheet" href="<?php print $theme_path; ?>/css/style-ie9.css" media="all" />
    <![endif]-->
  <!-- end of Daisy Theme -->
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>




</body>
</html>
