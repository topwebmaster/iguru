<?php

// Set Template Variables
$arg = arg(0);

?>
<div id="page-header" class="row-fluid header">
  <?php if (user_is_logged_in()): ?>
    <div class="top-bar nav navbar clearfix">
      <div class="pull-left">
        <ul class="cabinet-menu nav">
          <li class="messages dropdown">
            <a href="#" class="dropdown-toggle<?php if ($arg == 'messages') print ' active'; ?>" data-toggle="dropdown" title="<?php print t('Messages'); ?>">
              <i class="icon-comment"></i>
              <?php $count = $unread_messages['count'];
                if ($count > 0): ?>
                  <span class="badge"><?php print $count ?></span>
              <?php endif; ?>
            </a>
            <ul class="dropdown-menu extended inbox">
              <li><p>
                <?php print t('You have @number unread ' . format_plural($count, 'message', 'messages'), array('@number' => $count)); ?>
              </p></li>
              <?php foreach ($unread_messages['messages'] as $message): ?>
              <li>
                <a href="<?php print url('messages/view/'. $message['thread_id']) ?>">
                  <span class="photo"><?php print $message['author_picture'] ?></span>
                  <span class="subject">
                    <span class="time"><?php print $message['message_time'] ?></span>
                    <span class="from"><?php print $message['author_name'] ?></span>
                  </span>
                  <span class="message"><?php print $message['body'] ?></span>
                </a>
              </li>
              <?php endforeach; ?>
              <li><a href="<?php print url('messages') ?>"><?php print t('See all messages'); ?><i class="m-icon-swapright"></i></a></li>
            </ul>
          </li>
          <li class="notifications dropdown">
            <a href="#" class="dropdown-toggle<?php if ($arg == 'notifications') print ' active'; ?>" data-toggle="dropdown" title="<?php print t('Notifications'); ?>">
              <i class="icon-bell"></i>
              <?php $count = $unread_notifications['count'];
                if ($count > 0): ?>
                  <span class="badge"><?php print $count ?></span>
              <?php endif; ?>
            </a>
            <ul class="dropdown-menu extended inbox">
              <li><p><?php
                print t('You have @number unread ' . format_plural($count, 'notification', 'notifications'), array('@number' => $count));
              ?></p></li>
              <?php foreach ($unread_notifications['notifications'] as $notification): ?>
                <li><a href="<?php print url('notifications/view/'. $notification['thread_id']) ?>">
                  <span class="subject"><span class="time"><?php print $notification['time'] ?></span></span>
                  <div class="message"><?php print $notification['summary'] ?></div>
                </a></li>
              <?php endforeach; ?>
              <li>
                <a href="<?php print url('notifications') ?>"><?php print t('See all notifications') ?><i class="m-icon-swapright"></i></a>
              </li>
            </ul>
          </li>
          <li class="friends dropdown"><?php
          // Count Groups' Notifications
          $count = $unread_community['count'];
          // Create Button as URL
          $dropdown_classes = array('dropdown-toggle');
          if ($arg == 'community') {
            $dropdown_classes[] = 'active';
          }
          if ($count < 1) {
            print l(
              '<i class="icon-group"></i>',
              'community',
              array('html' => TRUE, 'attributes' => array('class' => $dropdown_classes, 'title' => t('Community'))));
          }
          // Create Button as Drop-Down Menu
          else { ?>
            <a href="#" class="<?php print implode(' ', $dropdown_classes) ?>" data-toggle="dropdown" title="<?php print t('Community') ?>">
              <i class="icon-group"></i>
              <?php if ($count > 0): ?>
                <span class="badge"><?php print $count ?></span>
              <?php endif; ?>
            </a>
            <ul class="dropdown-menu extended inbox">
              <li><p><?php
                print t('You have @number unread ' . format_plural($count, 'notification', 'notifications'), array('@number' => $count));
              ?></p></li>
              <?php foreach ($unread_community['notifications'] as $notification): ?>
                <li><a href="<?php print url('notifications/view/'. $notification['thread_id']) ?>">
                  <span class="subject"><span class="time"><?php print $notification['time'] ?></span></span>
                  <div class="message"><?php print $notification['summary'] ?></div>
                </a></li>
              <?php endforeach; ?>
              <li>
                <a href="<?php print url('community') ?>"><?php print t('See your community') ?><i class="m-icon-swapright"></i></a>
              </li>
            </ul>
          <?php } ?>
          </li>
          <li class="dropdown">
            <?php
              print l('<i class="icon-book"></i>', 'cabinet',
                array(
                   'html' => TRUE,
                   'attributes' => array(
                    'class' => array('dropdown-toggle'),
                    'title' => t('Cabinet'),
                    'data-toggle' => 'dropdown'
                  )
                )
              );
            ?>
            <ul class="dropdown-menu">
              <li><?php print l(t('Cabinet'), 'cabinet'); ?></li>
              <li><?php print l(t('Orders'), 'my_orders'); ?></li>
              <li><?php print l(t('Referrals'), 'user/'. $user->uid . '/referrals'); ?></li>
              <li><?php print l(t('Payments'), 'user/' . $user->uid . '/payments'); ?></li>
            </ul>
          </li>
          <li class="balance dropdown">
          <a href="#" class="balance dropdown-toggle" data-toggle="dropdown">
            <i class="icon-shopping-cart"></i>
            <?php if ($cart_summary['items'] >0): ?>
              <span class="badge"><?php print $cart_summary['items']; ?></span>
            <?php endif; ?>
          </a>
          <ul class="dropdown-menu extended balance-menu">
          <li><?php
              $block = block_load('uc_cart', 'cart');
              $renderable_array = _block_get_renderable_array(_block_render_blocks(array($block)));
              print render($renderable_array);
            ?>
            </li>
          </ul>

          </li>
        </ul>
      </div>
    </div>
  <?php else: ?>
    <div class="top-bar nav navbar clearfix">
      <div class="pull-left">
        <ul class="cabinet-menu nav">
          <li class="balance dropdown">
          <a href="#" class="balance dropdown-toggle" data-toggle="dropdown">
            <i class="icon-shopping-cart"></i>
            <?php if ($cart_summary['items'] >0): ?>
              <span class="badge"><?php print ' ('. $cart_summary['items'] . ') '; ?></span>
            <?php endif; ?>
          </a>
          <ul class="dropdown-menu extended balance-menu">
          <li><?php
              $block = block_load('uc_cart', 'cart');
              $renderable_array = _block_get_renderable_array(_block_render_blocks(array($block)));
              print render($renderable_array);
            ?>
            </li>
          </ul>

          </li>
        </ul>
      </div>
    </div>
  <?php endif; ?>
  <?php print render($page['header']); ?>
</div>
