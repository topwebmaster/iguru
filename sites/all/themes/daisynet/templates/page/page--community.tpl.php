<header id="navbar" role="banner" class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid">
      <?php if (!empty($logo)): ?>
        <a class="logo pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <?php if (!empty($site_name)): ?>
        <h1 id="site-name hidden-phone hidden-tablet hidden-desktop">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="brand"><?php print $site_name; ?></a>
        </h1>
      <?php endif; ?>

      <?php if ((!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation']))): ?>
        <div class="nav-collapse collapse clearfix pull-left">
          <nav role="navigation">
            <?php if(isset($page['navigation']['menu_menu-top-menu'])): ?>
              <div class="categories-menu">
                <?php print render($page['navigation']['menu_menu-top-menu']); ?>
              </div>
            <?php endif; ?>
          </nav>
        </div>
      <?php endif; ?>
      <ul class="nav pull-right daisy-nav-links">
        <?php if (!user_is_logged_in()): ?>
        <li class="dropdown login-smile">
          <?php print $modal_register_link; ?>
        </li>
        <?php endif; ?>
        <li id="search-site" class="search dropdown">
          <a href="search" class="search-link dropdown-toggle" data-toggle="dropdown"><i class="icon-search"></i></a>
          <ul class="dropdown-menu">
            <li class="search-form">
            <?php
              $search_form = drupal_get_form('daisynet_ajax_search_block_form');
              print render($search_form);
            ?>
            </li>
          </ul>
        </li>
        <li id="language-switcher" class="dropdown lang">
          <a href="#" class="lang-selected dropdown-toggle" data-toggle="dropdown"><i class="icon-globe"></i></a>
          <?php print $lang_switcher; ?>
        </li>

        <li id="general-help" class="help dropdown">
          <a href="#" class="help-link dropdown-toggle" data-toggle="dropdown"><i class="icon-info"></i></a>
          <ul class="dropdown-menu">
            <li><?php print l(t('Help'), 'help'); ?></li>
          </ul>
        </li>
      </ul>
      <?php if (!empty($page['navigation'])): ?>
        <?php print render($page['navigation']); ?>
      <?php endif; ?>
    </div>
  </div>
</header>

<div class="page-container row-fluid">
  <div class="top-cabinet bar">
    <?php require_once('includes/top-bar-region.php'); ?>
   </div>
  <?php if (!empty($page['sidebar_first']) && $user->uid): ?>
    <div class="page-sidebar sidebar-first">
      <!-- Sidebar Toggler -->
      <ul>
        <li><div class="sidebar-toggler hidden-phone"></div></li>
      </ul>
      <!-- End of Sidebar Toggler -->
      <div class="bar-toggler">
        <i class="icon-angle-left"></i>
      </div>

      <?php print render($page['sidebar_first']); ?>
    </div>
  <?php endif; ?>

  <div class="page-content clearfix tiles">
    <div class="table">
      <div class="table-row">
        <div class="table-cell main-content">
          <?php print $breadcrumb; ?>
          <?php print $messages; ?>
          <?php if (!empty($tabs)): ?>
          <?php //print render($tabs); ?>
          <?php endif; ?>
          <?php print render($title_prefix); ?>
          <?php if (!empty($title)): ?>
            <h1 class="page-header"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php print render($page['content']); ?>
        </div>
      </div>
    </div>
    <footer class="footer">
      <div class="span top-scroller pull-right">
         <span class="go-top"><i class="icon-angle-up"></i></span>
      </div>
      <?php print render($page['footer']); ?>
    </footer>
  </div>

  <?php if (!empty($page['sidebar_second'])): ?>
    <div class="page-sidebar sidebar-second">
      <div class="bar-toggler">
        <i class="icon-angle-right"></i>
      </div>
      <?php print render($page['sidebar_second']); ?>
    </div>
  <?php endif; ?>
</div>

