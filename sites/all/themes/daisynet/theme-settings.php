<?php
/**
 * @file
 * Theme setting callbacks for the DaisyNet theme.
 */


/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 *
 * @see alpha/theme-settings.php
 */
function daisynet_form_system_theme_settings_alter(&$form, &$form_state) {
  /**
   * Defines the Vertical Tabs components for the Metronic Assets to be available inside the DaisyNet theme.
   */
  $form['daisynet_settings'] =array(
    '#type' => 'vertical_tabs',
    '#weight' => -50,
  );

  /**
   * General theme settings.
   */
  $form['bootstrap_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bootstrap Config'),
    '#description' => '<h5>' . t('Bootstrap Framework advanced configuration') . '</h5>',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'daisynet_settings',
    '#weight' => 0,
  );

  $form['bootstrap_config']['sidebars_content_width'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configure the content and sidebar regions sizes'),
    '#description' => t('Defines the size of the span used to wrap this region. See Bootstrap Framework for details.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 1,
  );

  $form['bootstrap_config']['sidebars_content_width']['sidebar_first_width'] = array(
    '#type' => 'select',
    '#title' => t('Sidebar First'),
    '#options' => range(0,8),
    '#default_value' => theme_get_setting('sidebar_first_width'),
    '#weight' => 1,
  );

  $form['bootstrap_config']['sidebars_content_width']['content_width'] = array(
    '#type' => 'select',
    '#title' => t('Content'),
    '#options' => range(0,12),
    '#default_value' => theme_get_setting('content_width'),
    '#weight' => 1,
  );

  $form['bootstrap_config']['sidebars_content_width']['sidebar_second_width'] = array(
    '#type' => 'select',
    '#title' => t('Sidebar Second'),
    '#options' => range(0, 8),
    '#default_value' => theme_get_setting('sidebar_second_width'),
    '#weight' => 3,
  );

  /**
   * Metronic UI assets.
   */
  $form['metronic_ui'] = array(
    '#type' => 'fieldset',
    '#title' => t('Metronic UI'),
    '#description' => '<h5>' . t('Metronic UI components for Bootstrap Framework') . '</h5>',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'daisynet_settings',
    '#weight' => 1,
  );

  $form['metronic_ui']['metro'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Metro styles for Bootstrap.'),
    '#default_value' => theme_get_setting('metro'),
    '#description' => t('Use the Metronic extension styles for the Bootstrap Framework.'),
    '#weight' => 1,
  );

  $form['metronic_ui']['glyphicons_pro'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Glyphicons Pro'),
    '#default_value' => theme_get_setting('glyphicons_pro'),
    '#description' => t('Use the <a href="#">Glyphicons</a> icons inside the Metronic theme.'),
    '#weight' => 2,
  );

  $form['metronic_ui']['font_awesome'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Font Awesome'),
    '#default_value' => theme_get_setting('font_awesome'),
    '#description' => t('Use the <a href="#">Iconic Font</a> designed for Bootstrap.'),
    '#weight' => 3,
  );

  $form['metronic_ui']['fonts'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Fonts Asset'),
    '#default_value' => theme_get_setting('fonts'),
    '#description' => t('Use the web fonts provided by Metronic.'),
    '#weight' => 4,
  );

  $form['metronic_ui']['gritter'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Gritter'),
    '#default_value' => theme_get_setting('gritter'),
    '#description' => t('Use the <a href="#">Gritter</a> dynamic notification messages.'),
    '#weight' => 5,
  );

  $form['metronic_ui']['jquery_slimscroll'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Slim Scroll'),
    '#default_value' => theme_get_setting('jquery_slimscroll'),
    '#description' => t('Use the <a href="#">Slim Scroll</a> jQuery extension for scrollable containers and content.'),
    '#weight' => 6,
  );


  /**
   * Metronic advanced Bootstrap form assets.
   */
  $form['bootstrap_forms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bootstrap Forms'),
    '#description' => '<h5>' . t('Responsive and friendly UI components for Boostrap forms') . '</h5>',
    '#collapsible' => TRUE,
    '#group' => 'daisynet_settings',
    '#weight' => 2,
  );

  $form['bootstrap_forms']['bootstrap_toggle_buttons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Bootstrap Toggle Buttons'),
    '#default_value' => theme_get_setting('bootstrap_toggle_buttons'),
    '#description' => t('Use fancy toggle visual components to replace the original checkboxes in Bootstrap forms.'),
    '#weight' => 1,
  );

  $form['bootstrap_forms']['bootstrap_colorpicker'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Bootstrap Colorpicker'),
    '#default_value' => theme_get_setting('bootstrap_colorpicker'),
    '#description' => t('Use advanced color selection using a Colorpicker for Bootstrap.'),
    '#weight' => 2,
  );

  $form['bootstrap_forms']['bootstrap_datepicker'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Bootstrap Datepicker'),
    '#default_value' => theme_get_setting('bootstrap_datepicker'),
    '#description' => t('Use the advanced Datepicker tool for date fields inside Bootstrap forms.'),
    '#weight' => 3,
  );

  $form['bootstrap_forms']['bootstrap_timepicker'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Bootstrap Timepicker'),
    '#default_value' => theme_get_setting('bootstrap_timepicker'),
    '#description' => t('Use the advanced UI Timepicker tool for time fields inside Bootstrap forms.'),
    '#weight' => 4,
  );

  $form['bootstrap_forms']['bootstrap_wizard'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Bootstrap Wizard'),
    '#default_value' => theme_get_setting('bootstrap_wizard'),
    '#description' => t('Use the Advanced Wizard for multistep processing of Bootstrap forms.'),
    '#weight' => 5,
  );
}
