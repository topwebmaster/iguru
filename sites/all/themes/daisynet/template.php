<?php
/**
 * Implements theme hooks and configures the themeable output.
 */


/**
 * Implements hook_theme().
 */
function daisynet_theme($existing, $type, $theme, $path) {
  $path = drupal_get_path('theme', 'daisynet'). '/templates/form';

  return array(
    'product_node_form' => array(
      'render element' => 'form',
      'template' => 'product-node-form',
      'path' => $path,
    ),

    'product_node_form_edit' => array(
      'render element' => 'form',
      'template' => 'product-node-form-edit',
      'path' => $path,
    ),

    'user_profile_form' => array(
      'render element' => 'form',
      'template' => 'user-profile-form',
      'path' => $path,
    ),

    'news_node_form' => array(
      'render element' => 'form',
      'template' => 'news-node-form',
      'path' => $path,
    ),

    'group_node_form' => array(
      'render element' => 'form',
      'template' => 'group-node-form',
      'path' => $path,
    ),

    /*TODO: Add custom template for advanced theming when necessary.*/
    'daisy_user_first_time_login_form' => array(
      'render element' => 'form',
      'template' => 'daisy-first-time-login-popup-form',
      'path' => $path,
    ),

    'node_portlet_tools' => array(
      'variables' => array(
        'primary' => array(),
        'secondary' => array(),
      ),
    ),

    'node_portlet_tools_link' => array(
      'render element' => 'element',
    ),
  );
}


/**
 * Preprocess user profile form().
 *
 * @see daisynet_theme().
 */
function daisynet_preprocess_user_profile_form(&$vars){
  global $user;

  $form = $vars['form'];
  $vars['user'] = user_load(arg(1));

  //Divide the form logic into the 3 sections - General, Password, Expert.
  $vars['experts_manager']     = $user->uid != $vars['user']->uid;
  $vars['show_general_form']  = FALSE;
  $vars['show_password_form'] = FALSE;
  $vars['show_expert_form']   = FALSE;
  if(!arg(3) && arg(2) == 'edit') {
    $vars['show_general_form'] = TRUE;
    if (user_access('edit daisy expert profile')) {
      $vars['show_expert_form'] = TRUE;
    }
  }
  else {
    //Check if user can delete account and hide cancel button on form other than the general edit form.
    if(!user_access('cancel account', $user)) {
      unset($vars['form']['actions']['cancel']);
    }
    //Various expert modes.
    switch(arg(3)) {
      case 'password':
        $vars['show_password_form'] = TRUE;
        break;
    }
  }

  //Become expert various modes.
  if(arg(2) == 'become_expert') {
    $vars['show_expert_form'] = TRUE;
  }
}

/**
 * Theming function for the product node form.
 * @param $variables
 * @return string
 *
 * @see daisynet_theme().
 */
function daisynet_preprocess_product_node_form($variables) {
  drupal_add_js(path_to_theme(). '/js/validate.js');
}


/**
 * Implements hook_preprocess_html().
 */
function daisynet_preprocess_html(&$vars) {
  global $language, $base_path, $directory;

  // Add conditional stylesheets for IE.
  drupal_add_css(
    drupal_get_path('theme', 'daisynet') . '/css/iestyle.css',
    array(
      'group' => CSS_THEME,
      'browsers' => array(
        'IE' => 'lte IE 7',
        'IE' => 'lte IE 8',
        'IE' => 'lte IE 9',
        '!IE' => FALSE,
      ),
      'weight' => 999,
      'every_page' => TRUE,
    )
  );

  if(drupal_is_front_page() || TRUE) {
    drupal_add_js(path_to_theme(). '/js/front.js');
    drupal_add_js(path_to_theme(). '/js/cabinet.js');
    drupal_add_js(path_to_theme(). '/js/tabs.js');
    drupal_add_js(path_to_theme(). '/js/daisynet.js');
  }

  //Activate various components depending on theme settings.
  if(theme_get_setting('metro')) {
    drupal_add_css(path_to_theme().'/assets/metro/css/metro.css', array('group' => CSS_THEME));
    drupal_add_css(path_to_theme().'/assets/metro/css/styles.css', array('group' => CSS_THEME));
    drupal_add_css(path_to_theme().'/assets/metro/css/style_responsive.css', array('group' => CSS_THEME));
    drupal_add_css(path_to_theme().'/assets/metro/css/style_light.css', array('group' => CSS_THEME));
    drupal_add_css(path_to_theme().'/assets/font-awesome/css/font-awesome.css', array('group' => CSS_THEME));
    drupal_add_css(path_to_theme().'/css/style.css', array('group' => CSS_THEME));
    drupal_add_js(path_to_theme(). '/assets/metro/js/breakpoints.js');
    drupal_add_js(path_to_theme(). '/assets/metro/js/jquery.cookie.js');
    drupal_add_js(path_to_theme(). '/assets/metro/js/jquery.blockui.js');
    drupal_add_js(path_to_theme(). '/assets/metro/js/jquery.pulsate.min.js');
    drupal_add_js(path_to_theme(). '/assets/metro/js/respond.js');
    drupal_add_js(path_to_theme(). '/assets/metro/js/app.js');
  }

  //Activate the Glyphicons Pro icons.
  if(theme_get_setting('glyphicons_pro')) {
    drupal_add_css(path_to_theme().'/assets/glyphicons_pro/glyphicons/css/glyphicons.css', array('group' => CSS_THEME));
  }

  //Activate the Bootstrap Datepicker asset.
  if(theme_get_setting('bootstrap_datepicker')) {
    drupal_add_css(path_to_theme().'/assets/bootstrap-datepicker/css/datepicker.css', array('group' => CSS_THEME));
    drupal_add_js(path_to_theme(). '/assets/bootstrap-datepicker/js/bootstrap-datepicker.js');
  }

  //Activate the Bootstrap Timepicker asset.
  if(theme_get_setting('bootstrap_timepicker')) {
    drupal_add_css(path_to_theme().'/assets/bootstrap-timepicker/css/timepicker.css', array('group' => CSS_THEME));
    drupal_add_js(path_to_theme(). '/assets/bootstrap-timepicker/js/bootstrap-timepicker.js');
  }

  //Adds rdf doctype support.
  $vars['rdf'] = new stdClass;
  if (module_exists('rdf')) {
    $vars['doctype'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">' . "\n";
    $vars['rdf']->version = ' version="HTML+RDFa 1.1"';
    $vars['rdf']->namespaces = $vars['rdf_namespaces'];
    $vars['rdf']->profile = ' profile="' . $vars['grddl_profile'] . '"';
  } else {
    $vars['doctype'] = '<!DOCTYPE html>' . "\n";
    $vars['rdf']->version = '';
    $vars['rdf']->namespaces = '';
    $vars['rdf']->profile = '';
  }

  $vars['theme_path'] = $base_path . $directory;

  if ($language->language == 'en'){
    drupal_add_js('
      reformal_wdg_w    = "713";
      reformal_wdg_h    = "460";
      reformal_wdg_domain    = "daisynet";
      reformal_wdg_mode    = 0;
      reformal_wdg_title   = "DaisyNet";
      reformal_wdg_ltitle  = "Feedback and ideas...";
      reformal_wdg_lfont   = "";
      reformal_wdg_lsize   = "";
      reformal_wdg_color   = "#FFA000";
      reformal_wdg_bcolor  = "#6568ab";
      reformal_wdg_tcolor  = "#FFFFFF";
      reformal_wdg_align   = "left";
      reformal_wdg_waction = 0;
      reformal_wdg_vcolor  = "#f2be11";
      reformal_wdg_cmline  = "#E0E0E0";
      reformal_wdg_glcolor  = "#6568ab";
      reformal_wdg_tbcolor  = "#FFFFFF";
      reformal_wdg_bimage = "29a2770fafec079073069edf2daf22af.png";',
      'inline');

    //drupal_add_js("http://idea.informer.com/tabn2v4.js?domain=daisynet", 'external');
  } else {
    drupal_add_js("
      var reformalOptions = {
        project_id: 57796,
        project_host: 'DaisyNet.reformal.ru',
        tab_orientation: 'left',
        tab_indent: '50%',
        tab_bg_color: '#fafafa',
        tab_border_color: '#6569ab',
        tab_image_url: 'http://tab.reformal.ru/0JLQsNGI0LggT9GC0LfRi9Cy0Ysg0Lgg0LjQtNC10LguLi4=/6569ab/bac3e0b958e545f4bc8a82335a8ef5b7/left/0/tab.png',
        tab_border_width: 0
      };

      (function() {
        var script = document.createElement('script');
        script.type = 'text/javascript'; script.async = true;
        script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'media.reformal.ru/widgets/v3/reformal.js';
        document.getElementsByTagName('head')[0].appendChild(script);
      })();
      setTimeout(function(){
        //document.getElementById('reformal_tab').href='javascript:Reformal.widgetOpen();';
        //document.getElementById('reformal_tab').onclick = function(){};
      },1000);",
      'inline');
  }
}


/**
 * Implements hook_process_block().
 * @return void
 */
function daisynet_process_block(&$vars){
  //Display all blocks as tiles on front page.
  if(drupal_is_front_page()) {
    //Add tile classes to all content blocks.
    if($vars['block']->region != 'footer') {
      $vars['classes'] .= ' tiles-block pull-left clearfix ';
    }
  }
}


/**
 * Implements hook_process_region().
 * @return void
 */
function daisynet_process_region(&$vars){
  //Display all blocks as tiles on front page.
  if($vars['region'] == 'sidebar_first') {
    $vars['theme_hook_suggestions'] = array('region__no_wrapper');
  }

  if($vars['region'] == 'sidebar_second') {
    $vars['theme_hook_suggestions'] = array('region__no_wrapper');
  }

  if($vars['region'] == 'sidebar_first') {
    //Remove unnecessary classes added by bootstrap theme.
    $vars['classes'] = str_replace('well', '', $vars['classes']);
  }
}


/**
 * Implements hook_preprocess_block().
 * TODO: Implement custom preprocess into separate module for notification and messages.
 */
function daisynet_preprocess_block(&$variables) {
  $block = $variables['block'];

  //Processing of block title icon element.

  switch ($block->delta) {
    case 'og_list-groups_block':
      $icon = 'icon-group';
      break;
    case 'user_info-block_1':
      $icon = 'icon-unlink';
      break;
    case '-exp-cabinet-page_2':
    case '-exp-og_list-groups_page':
      $icon = 'icon-sort-by-attributes-alt';
      break;
  }
  if(isset($icon)) {
    $variables['title_icon'] = '<i class="' . $icon . '"></i>';
  }
  $variables['close_icon'] = '<i class="icon-angle-up"></i>';

  //General processing by module.
  switch($block->module) {
    //Preprocess system blocks.
    case 'block':
      switch($block->delta) {
        //The user block.
        case 13:
          global $user;
          $current_user = user_load($user->uid);
          if (isset($current_user)) {
            $user_image_fid = $current_user->field_user_image['und'][0]['fid'];
            $user_image_uri = file_load($user_image_fid)->uri;
          }
          if ($user_image_uri) {
            $thumb_url = image_style_url('uc_thumbnail', $user_image_uri);
            $user_image = '<img class="user" src="'. $thumb_url .'" />';
            $variables['user_image'] = $user_image;
          }
          break;
        case 19:
          $variables['title_icon'] = '<i class="icon-info-sign"></i>';
          break;
        default:
      }
      break;
    //Preprocess menu blocks.
    case 'menu':
      //Display all menu blocks without a wrapper element - Metronic integration.
      switch($block->delta) {
        case 'menu-account-menu':
          break;
        case 'menu-cabinet-links':
        case 'menu-edit-account':
          $variables['theme_hook_suggestions'][] = 'block__no_wrapper';
          break;
      }
      break;

    //Preprocess views blocks.
    //TODO: ADD more views based template variations.
    case 'views':
      if (drupal_is_front_page() && (strpos('catalog-block', $block->delta) === 0)) {
      }
      break;
  }
}


/**
 * Implements hook_preprocess_page().
 * @param $variables
 * @return void
 */
function daisynet_preprocess_page(&$variables) {
  global $language, $user;
  drupal_add_js(array('arg' => arg()),'setting');
  ctools_include('modal');
  ctools_modal_add_js();

  //adding ui,dialog function
  drupal_add_library('system', 'ui');
  drupal_add_library('system', 'ui.dialog');

  //Building the modal register link
  $variables['modal_register_link'] = ctools_modal_text_button(
    t(''),
    'daisy-modals-register-callback/nojs',
    t('Click Here!'),
    'ctools-modal-daisy-register-style daisy-link icon-smile'
  );

  $variables['lang_switcher'] = daisynet_languages();
  $variables['user_name'] = daisy_user_get_username($variables['user']);
  $variables['balance'] = '$' . number_format(userpoints_get_current_points());

  $variables['user_image_small'] = daisy_user_get_picture($variables['user']);
  $variables['node_hide_sidebars'] = FALSE;
  if (module_exists('daisy_msg')) {
    $variables['unread_messages'] = daisy_msg_get_unread_messages($variables['user']);
    $variables['unread_notifications'] = daisy_msg_get_unread_notifications($variables['user']);
    $variables['unread_community'] = daisy_msg_get_unread_community($variables['user']);
  }
  $variables['cart_summary'] = daisy_ubercart_cart_summary_assoc();

  // Add specific page templates for node types
  $variables['user_can_edit'] = FALSE;
  if (isset($variables['node']) && !empty($variables['node'])) {
    $node = $variables['node'];
    $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
    // We need to hide the default title for users that can edit this content,
    // because we're using a portlet in the node page
    $variables['user_can_edit'] = user_access('edit own ' . $node->type . ' content', $user) || user_access('administer nodes', $user);
  }

  //hide sidebars for node view.
  if (arg(0) == 'node' && is_numeric(arg(1)) && !arg(2)) {
    $variables['node_hide_sidebars'] = TRUE;
  }

  //Anonymous user conditional processing.
  if (user_is_anonymous()) {
    if ($variables['is_front']) {
      drupal_add_js(drupal_get_path('theme', 'daisynet') . '/js/anonymous.js', 'file');
    }
  } else {
    if (!$variables['is_front'] && arg(0) != 'webinars') {
      drupal_add_js(drupal_get_path('theme', 'daisynet') . '/js/cabinet.js', 'file');
    }
    if ((arg(0) == 'node' && arg(1) == 'add') || (arg(0) == 'node' && arg(2) == 'edit') || (arg(0) == 'node' && arg(2) == 'clone')) {
      drupal_add_js(drupal_get_path('theme', 'daisynet') . '/js/editWebinar.js', 'file');
      drupal_add_js(drupal_get_path('theme', 'daisynet') . '/js/terms.js', 'file');
    }
    if ((arg(0) == 'user' && arg(2) == 'edit')) {
      drupal_add_js(drupal_get_path('theme', 'daisynet') . '/js/terms.js', 'file');
    }
  }

  // Webinars catalog pages
  if (arg(0) == 'webinars') {
    $page_title = t('Webinars');
    $variables['theme_hook_suggestions'][] = 'page__catalog';
    $tid = (int)arg(1) ? (int)arg(1) : 'all';
    if ($tid != 'all') {
      $term = taxonomy_term_load($tid);
      $page_title = t($term->name);
    }
    $time_argument = arg(2);
    $generic_arguments = array('past', 'live', 'future');
    if (isset($time_argument)) {
      if (in_array($time_argument, $generic_arguments)) {
        $page_title .= ': ' . t($time_argument);
      }
      else {
        $date = date_create_from_format('Y-m', $time_argument);
        $date = format_date(date_timestamp_get($date), 'custom', 'F Y');
        $page_title .= ': ' . $date;
      }
    }
    if (!isset($time_argument)) {
      $time_argument = date('Y-m');
    }
    $variables['calendar_links'] = views_embed_view('cabinet', 'page_2', $tid, $time_argument);

    // Use the pre_content region for this page with the content on this page
    $variables['page']['pre_content']['system_main'] = $variables['page']['content']['system_main'];
    unset($variables['page']['content']['system_main']);
    drupal_set_title($page_title);
  }

  //Taxonomy term based page template suggestions.
  if(arg(0) == 'taxonomy' && arg(1) == 'term') {
    switch (arg(2)) {
      case 132:
        $variables['theme_hook_suggestions'][] = 'page__events__participated';
        break;
    }
  }

  if(arg(0) == 'events') {
    if (arg(1) == 'bookmarks') {
      $variables['theme_hook_suggestions'][] = 'page__bookmarks';
      drupal_add_js(drupal_get_path('theme', 'daisynet') . '/js/bookmarks.js', 'file');
      if($language->language == 'en') {
        //drupal_add_js('http://idea.informer.com/tabn2v4.js?domain=daisynet', 'external');
      }
    }
    if (arg(1) == 'created--me' ) {
      $variables['theme_hook_suggestions'][] = 'page__created';
    }
  }

  if((arg(0) == 'groups') || (arg(0) == 'community')) {
    $variables['title_prefix'] = array('#type' => 'markup', '#markup' => '<div class="community-tabs">');
    $variables['title_suffix'] = array('#type' => 'markup', '#markup' => '</div>');
    $group_options = $friend_options = array();
    if (arg(0) == 'groups') {
      $group_options = array('attributes' => array('class' => array('active')));
    }
    if (arg(0) == 'community') {
      $friend_options = array('attributes' => array('class' => array('active')));
    }
    $variables['title'] = l(t('Groups'), 'groups/all', $group_options)  . l(t('Friends'), 'community', $friend_options);
  }
}

/**
 * Implements hook_process_page().
 */
function daisynet_process_page(&$vars) {
  $vars['breadcrumb'] = FALSE;
  if (!empty($vars['node']) && arg(0) == 'node' && ctype_digit(arg(1))) {
    drupal_add_css(path_to_theme().'/css/product.css', array('group' => CSS_THEME));
  }

  //Process page title display settings.
  _daisynet_title_toggle_settings($vars);
}


/**
 * Implements hook_preprocess_node().
 */
function daisynet_preprocess_node(&$vars) {
  global $user;
  $node = $vars['node'];

  $vars['user_can_edit'] = user_access('edit own ' . $node->type . ' content', $user) || user_access('administer nodes', $user);
  $vars['theme_hook_suggestions'][] = 'node__' . $vars['type'] . '__' . $vars['view_mode'];

  if ($node->type == 'product') {
    //Adds the product styles by default.
    drupal_add_css(path_to_theme().'/css/product.css', array('group' => CSS_THEME));

    //Retrieve the views count.
    $statistics = statistics_get($node->nid);
    $node_wrapper = entity_metadata_wrapper('node', $node);
    $vars['views_count'] = $statistics['totalcount'];

    //Extract the category color for this product, if available.
    $vars['category_color'] = '#bfbfbf';
    $webinar_category = field_get_items('node', $node, 'field_webinar_category_front');
    $term = taxonomy_term_load($webinar_category[0]['tid']);
    if ($term && isset($webinar_category[0]['tid'])) {
      $category_color = field_get_items('taxonomy_term', $term, 'field_bg_color');
      if ($category_color && (isset($category_color[0]['jquery_colorpicker']))) {
        $vars['category_color'] = '#' . $category_color[0]['jquery_colorpicker'];
      }
    }

    // Select different layout for past webinars
    if (isset($node->field_webinar_date[LANGUAGE_NONE][0]['value'])) {
      $duration = $node_wrapper->field_webinar_duration->value();
      $duration = empty($duration) ? 0 : $duration * 60;
      $date_raw = $node->field_webinar_date[LANGUAGE_NONE][0];
      $date = new DateObject($date_raw['value'], $date_raw['timezone_db'], 'Y-m-d H:i:s');
      // Show the date in user's timezone (if logged in) or site's timezone

      $timezone = $date_raw['timezone'];
      if ($user->uid != 0) {
        $timezone = $user->timezone;
      }
      date_timezone_set($date, timezone_open($timezone));
      $now = new DateObject();
      $diff = $now->difference($date, 'seconds', FALSE);
      $vars['webinar_day'] = date_format_date($date, 'custom', 'd');
      $vars['webinar_month'] = date_format_date($date, 'custom', 'M');
      $vars['webinar_year'] = date_format_date($date, 'custom', 'Y');
      // Custom timezone format
      $timezone_diff = (int) date_format_date($date, 'custom', 'Z');
      $timezone_diff = $timezone_diff / (60*60);
      $timezone_sign = ($timezone_diff > 0) ? '+' : '-';
      $timezone_diff = $timezone_sign . $timezone_diff;
      $vars['webinar_hour'] = date_format_date($date, 'custom', "H:i ($timezone_diff \G\M\T)");
      // Set live vebinars
      $vars['is_live'] = ($diff <= 0) && (($diff + $duration) > 0);
      if (($diff + $duration) <= 0) {
        $vars['theme_hook_suggestions'][] = 'node__' . $vars['type'] . '__' . $vars['view_mode'] . '__past';
      }
    }

    $account = user_load($node->uid);
    $account_view = user_view($account);
    $vars['is_author'] = ($account->uid == $user->uid);
    $vars['author_picture'] = daisynet_user_image_small($account, 'user_image');
    $author_name = daisynet_get_username($account->uid);
    // Trim the name if we're presenting a teaser
    if ($vars['teaser']) {
      $trim = array(
        'max_length' => 20,
        'ellipsis' => TRUE,
      );
      $author_name = views_trim_text($trim, $author_name);
    }
    $vars['author_name'] = l($author_name, 'user/' . $account->uid);
    $vars['author_country'] = drupal_render($account_view['field_user_country']);
    $vars['is_online'] = daisynet_user_is_online($node->uid);
    $vars['webinar_is_free'] = ($node_wrapper->field_webinar_price->value() == 'free');
    $invited_users = $node_wrapper->field_participants->value();
    $vars['participants_count'] = count($invited_users);
    $is_invited = FALSE;
    foreach ($invited_users as $invited_user) {
      if ($invited_user->uid == $user->uid) {
        $is_invited = TRUE;
        break;
      }
    }
    $vars['private_access'] = ($node->uid == $user->uid) || ($node_wrapper->field_webinar_access->value() == 'public') ||
      ($node_wrapper->field_webinar_access->value() == 'private' && $is_invited);
    $vars['is_participant'] = $is_invited;
    $vars['webinar_ordered'] = daisy_ubercart_node_order_status($node->nid, $user->uid);
  }

  //Add a trimmed title value if available.
  $vars['trimmed_title'] = views_trim_text(
    array('max_length' => 40, 'word_boundary' => TRUE, 'ellipsis' => TRUE, 'html' => FALSE),
    $node->title
  );

  //Add additional templates to the node, if rendered inside a view.
  if (property_exists($node,'view')) {
    $view = $node->view;
    $vars['theme_hook_suggestions'][] = 'node__view__'. $view->name;
    $vars['theme_hook_suggestions'][] = 'node__view__' . $view->name . '__' . $view->current_display;
    $vars['theme_hook_suggestions'][] = 'node__' . $node->type . ' __view__' . $view->name;
    $vars['theme_hook_suggestions'][] = 'node__' . $node->type . '__view__' . $view->name . '__' . $view->current_display;
  }

  if ($node->type == 'group') {
    $node_wrapper = entity_metadata_wrapper('node', $node);
    $vars['group_status'] = ($node_wrapper->field_group_type->value()) ? t('Closed') : t('Open');
    $vars['subscribe_label'] = ($node_wrapper->field_group_type->value()) ? t('Send request') : t('Join');
    $vars['group_is_open'] = ! ($node_wrapper->field_group_type->value());
  }
}

/**
 * Overrides theme_links__contextual().
 */
function daisynet_links__contextual($vars){
  $links = $vars['links'];
  //If not node contextual, return the default.
  if(!in_array('node-edit', array_keys($links))) {
    return theme_links($vars);
  }
  else {
    //Process the standard contextual links with additional classes.
    foreach($links as $key => $link) {
      switch($key) {
        case 'node-view':
          $vars['links'][$key]['attributes']['class'] = array('icon-eye-open');
          $vars['links'][$key]['attributes']['title'] = t('View');
          break;
        case 'node-edit':
          $vars['links'][$key]['attributes']['class'] = array('icon-wrench');
          $vars['links'][$key]['attributes']['title'] = t('Edit');
          break;
        case 'node-group':
          $vars['links'][$key]['attributes']['class'] = array('icon-group');
          $vars['links'][$key]['attributes']['title'] = t('Group');
          break;
        case 'node-delete':
          $vars['links'][$key]['attributes']['class'] = array('icon-trash');
          $vars['links'][$key]['attributes']['title'] = t('Delete');
          break;
        case 'node-clone':
          $vars['links'][$key]['attributes']['class'] = array('icon-copy');
          $vars['links'][$key]['attributes']['title'] = t('Clone');
          break;
        case 'ccl-global-node-0':
          $vars['links'][$key]['attributes']['title'] = t('Track');
          break;
        case 'ccl-global-node-1':
          $vars['links'][$key]['attributes']['title'] = t('Rating');
          break;
      }
    }
    $vars['attributes']['class'] = 'metronic-contextual-links clearfix';
    return theme_links($vars);
  }
}


/**
 * Overrides the theme_links().
 */
function daisynet_links($vars){
  return theme_links($vars);
}


/**
 * Overrides the theme_menu_tree().
 */
function daisynet_menu_tree__menu_account_menu($vars) {
  return $vars['tree'];
}


/**
 * Overrides the theme_menu_tree().
 */
function daisynet_menu_tree__menu_user_menu($vars) {
  return $vars['tree'];
}


/**
 * Overrides the theme_menu_tree().
 */
function daisynet_menu_tree__menu_top_menu($vars) {
  return $vars['tree'];
}


/**
 * Implements theme_menu_tree_MENU_ID().
 */
function daisynet_menu_tree__menu_edit_account($vars) {
  return '<ul class="menu-edit-account margin-bottom-10">' . $vars['tree'] . '</ul>';
}

/**
 * Overrides the theme_link().
 * @param $variables
 * @return string
 *
 * @see theme_link().
 */
function daisynet_link($variables) {
  global $user;
  $uid = $user->uid;
  if ($variables['path'] == 'user' && isset($variables['options']['attributes']['class']) && in_array('account', $variables['options']['attributes']['class'])) {
    $variables['path'] = 'user/' . $uid . '/edit';
  }
  $count = 0;
  $class = '';
  if (isset($variables['options']['attributes']['class']) && is_array($variables['options']['attributes']['class'])) {
    if (in_array('notifications', $variables['options']['attributes']['class'])) {
      $count = daisy_count_unread_notifications($uid);
      if ($count > 99) {
        $class = 'long numeric';
      }
    }

    if (in_array('messages', $variables['options']['attributes']['class'])) {
      $count = daisy_count_unread_messages($uid);
      if ($count > 99) {
        $class = 'long numeric';
      }
    }

    if (in_array('friends', $variables['options']['attributes']['class'])) {
      $count = user_relationships_load(array('requestee_id' => $user->uid, 'approved' => FALSE), array('count' => TRUE));
      if ($count) {
        $class = 'long';
        $count = 'new';
      }
    }

    if ($count) {
      $span = '<span class="' . $class . '">' . $count . '</span>';
      $variables['text'] .= $span;
      $variables['options']['html'] = TRUE;
    }
  }

  return theme_link($variables);
}


/**
 * Helper function to retrive a username based on user id.
 * @param $uid
 * @return string
 */
function daisynet_get_username($uid) {
  global $language;
  $user = user_load($uid);

  return daisy_user_get_username($user);
}


/**
 * Overrides theme_views_mini_pager().
 * @param $vars
 * @return string
 *
 * @see theme_views_mini_pager().
 *
function daisynet_views_mini_pager($vars) {
  global $pager_page_array, $pager_total;

  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];
  $quantity = $vars['quantity'];

  //Calculate various markers within this pager piece:
  //Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);

  //Current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;

  //Set variable for the maximum page number
  $pager_max = $pager_total[$element];


  //Show normal pager if "Pager ID" in settings is set to 100
  $normal_pager = ($vars['element'] == 100) ? TRUE : FALSE;
  $next_pager = ($normal_pager) ? t('››') : t('');

  if ($normal_pager) {
    $li_previous = theme('pager_previous',
      array(
        'text' => (isset($tags[1]) ? $tags[1] : t('‹‹')),
        'element' => $element,
        'interval' => 1,
        'parameters' => $parameters,
      )
    );
    if (empty($li_previous)) {
      $li_previous = "&nbsp;";
    }
  }
  $li_next = theme('pager_next',
    array(
      'text' => (isset($tags[3]) ? $tags[3] : $next_pager),
      'element' => $element,
      'interval' => 1,
      'parameters' => $parameters,
    )
  );
  if (empty($li_next)) {
    $li_next = "&nbsp;";
  }

  if ($pager_total[$element] > 1) {
    if ($normal_pager) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );

    return theme('item_list',
      array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => array('pager')),
      )
    );
  }
}*/


/**
 * Implements hook_menu_link__MENU_ID().
 */
function daisynet_menu_link__menu_edit_account(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  // Add missing active-trail
  if (isset($element['#href']) && ($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#origianl_link']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  $element['#localized_options']['html'] = TRUE;
  $icon_class = "";

  // Search localized_options for icon class
  if (isset($element['#localized_options']['attributes']['class']) && !empty($element['#localized_options']['attributes']['class'])) {
    foreach ($element['#localized_options']['attributes']['class'] as $key => $class) {
      if (strpos($class, 'icon-') === 0) {
        $icon_class = $class;
        unset($element['#localized_options']['attributes']['class'][$key]);
        break;
      }
    }
  }
  // Search original link for icon class
  if ($icon_class == "") {
    if (isset($element['#original_link']['options']['attributes']['class']) && !empty($element['#original_link']['options']['attributes']['class'])) {
      foreach ($element['#original_link']['options']['attributes']['class'] as $key => $class) {
        if (strpos($class, 'icon-') === 0) {
          $icon_class = $class;
          break;
        }
      }
    }
  }

  $output = l("<i class=\"$icon_class\"></i>" . '<span class="title">' . $element['#title'] . '</span>', $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}


/**
 * Implements hook_menu_link__MENU_ID().
 */
function daisynet_menu_link__menu_user_menu(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  $element['#localized_options']['attributes']['class'][] = 'user-menu-item';

  // Add missing active-trail
  if (isset($element['#href']) && ($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page()) || strpos($_GET['q'], $element['#href']) === 0) && (empty($element['#origianl_link']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  $element['#localized_options']['html'] = TRUE;
  $icon_class = "";

  // Search localized_options for icon class
  if (isset($element['#localized_options']['attributes']['class']) && !empty($element['#localized_options']['attributes']['class'])) {
    foreach ($element['#localized_options']['attributes']['class'] as $key => $class) {
      if (strpos($class, 'icon-') === 0) {
        $icon_class = $class;
        unset($element['#localized_options']['attributes']['class'][$key]);
        break;
      }
    }
  }
  // Search original link for icon class
  if ($icon_class == "") {
    if (isset($element['#original_link']['options']['attributes']['class']) && !empty($element['#original_link']['options']['attributes']['class'])) {
      foreach ($element['#original_link']['options']['attributes']['class'] as $key => $class) {
        if (strpos($class, 'icon-') === 0) {
          $icon_class = $class;
          break;
        }
      }
    }
  }

  // Dirty title fix for files default tab item created via Views
  // Read more here: http://drupal.org/node/1914996
  if ($element['#href'] == 'user-files') {
    $element['#title'] = t($element['#title']);
  }

  $output = l("<i class=\"$icon_class\"></i>" . '<span class="title">' . $element['#title'] . '</span>', $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}


/**
 * Implements hook_menu_link_MENU_ID().
 */
function daisynet_menu_link__menu_top_menu(array $variables){
  $element = $variables['element'];
  $sub_menu = '';
  $element['#localized_options']['attributes']['class'][] = 'menu-item';

  if ($element['#below']) {
    // Ad our own wrapper
    unset($element['#below']['#theme_wrappers']);
    $sub_menu = '<ul class="dropdown-menu sub">' . drupal_render($element['#below']) . '</ul>';
    $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
    $element['#localized_options']['attributes']['class'][] = 'has-sub';
    //$element['#localized_options']['attributes']['data-toggle'] = 'dropdown';

    // Check if this element is nested within another
    if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {
      // Generate as dropdown submenu
      $element['#attributes']['class'][] = 'dropdown-submenu';
      $element['#attributes']['class'][] = 'sub';
    }
    else {
      // Generate as standard dropdown
      //$element['#attributes']['class'][] = 'dropdown';
      //$element['#attributes']['class'][] = 'has-sub';
      $element['#localized_options']['html'] = TRUE;
      $element['#title'] .= '<span class="caret"></span>';
    }

    // Set dropdown trigger element to # to prevent inadvertant page loading with submenu click
    $element['#localized_options']['attributes']['data-target'] = '#';
  }

  // Add missing active-trail
  if (isset($element['#href']) && ($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#origianl_link']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }

  //Add active trail for parent category links based on the webinar page being viewed.
  $arg0 = arg(0);
  $arg1 = arg(1);
  $arg2 = arg(2);
  // Set the element to be active for all 'webinars/<tid/*' pages
  if (($arg0 == 'webinars' || $arg0 == 'news') && !empty($arg1) && $element['#href'] == 'webinars/' . $arg1) {
    $element['#attributes']['class'][] = 'active';
  }
  // Set parent element to be active if it's a subcategory
  if (($arg0 == 'webinars' || $arg0 == 'news') && !empty($arg1) && !in_array($element['#attributes']['class'], array('active'))) {
    if (ctype_digit($arg1)) {
      $parents = taxonomy_get_parents((int)arg(1));
      foreach ($parents as $pid => $parent) {
        if ($element['#href'] == 'webinars/'. $pid) {
          $element['#attributes']['class'][] = 'active';
          break;
        }
      }
    }
  }
  $element['#localized_options']['html'] = TRUE;
  $icon_class = '';

  // Search localized_options for icon class
  if (isset($element['#localized_options']['attributes']['class']) && !empty($element['#localized_options']['attributes']['class'])) {
    foreach ($element['#localized_options']['attributes']['class'] as $key => $class) {
      if (strpos($class, 'icon-') === 0) {
        $icon_class = $class;
        unset($element['#localized_options']['attributes']['class'][$key]);
        break;
      }
    }
  }

  // Search original link for icon class
  if ($icon_class == '') {
    if (isset($element['#original_link']['options']['attributes']['class']) && !empty($element['#original_link']['options']['attributes']['class'])) {
      foreach ($element['#original_link']['options']['attributes']['class'] as $key => $class) {
        if (strpos($class, 'icon-') === 0) {
          $icon_class = $class;
          break;
        }
      }
    }
  }

  //Add 'title' class for Metronic styling.
  $icon = '<i class="' . $icon_class . '"></i>';

  $output = l($icon .'<span class="title">' . $element['#title'] . '</span>', $element['#href'], $element['#localized_options']);

  /*
  $output .= theme('item_list',
    array(
      'items' => array(
        l(t('Live'), $element['#href'] . '/live'),
        l(t('Past'), $element['#href'] . '/past'),
        l(t('Future'), $element['#href'] . '/live'),
      ),
      'attributes' => array('class' => array('secondary-webinar-links')),
    )
  );
  */

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}


/**
 * Implements hook_menu_link().
 */
function daisynet_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  $element['#localized_options']['attributes']['class'][] = 'menu-item';

  if ($element['#below']) {
    // Ad our own wrapper
    unset($element['#below']['#theme_wrappers']);
    $sub_menu = '<ul class="dropdown-menu sub">' . drupal_render($element['#below']) . '</ul>';
    $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
    $element['#localized_options']['attributes']['class'][] = 'has-sub';
    //$element['#localized_options']['attributes']['data-toggle'] = 'dropdown';

    // Check if this element is nested within another
    if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {
      // Generate as dropdown submenu
      $element['#attributes']['class'][] = 'dropdown-submenu';
      $element['#attributes']['class'][] = 'sub';
    }
    else {
      // Generate as standard dropdown
      $element['#attributes']['class'][] = 'dropdown';
      $element['#attributes']['class'][] = 'has-sub';
      $element['#localized_options']['html'] = TRUE;
      $element['#title'] .= '<span class="caret"></span>';
    }

    // Set dropdown trigger element to # to prevent inadvertant page loading with submenu click
    $element['#localized_options']['attributes']['data-target'] = '#';
  }

  // Add missing active-trail
  if (isset($element['#href']) && ($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#origianl_link']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  $element['#localized_options']['html'] = TRUE;
  $icon_class = '';

  // Search localized_options for icon class
  if (isset($element['#localized_options']['attributes']['class']) && !empty($element['#localized_options']['attributes']['class'])) {
    foreach ($element['#localized_options']['attributes']['class'] as $key => $class) {
      if (strpos($class, 'icon-') === 0) {
        $icon_class = $class;
        unset($element['#localized_options']['attributes']['class'][$key]);
        break;
      }
    }
  }
  // Search original link for icon class
  if ($icon_class == '') {
    if (isset($element['#original_link']['options']['attributes']['class']) && !empty($element['#original_link']['options']['attributes']['class'])) {
      foreach ($element['#original_link']['options']['attributes']['class'] as $key => $class) {
        if (strpos($class, 'icon-') === 0) {
          $icon_class = $class;
          break;
        }
      }
    }
  }

  //Add 'title' class for Metronic styling.
  $icon = '<i class="' . $icon_class . '"></i>';

  $output = l($icon .'<span class="title">' . $element['#title'] . '</span>', $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements hook_preprocess_views_view().
 */
function daisynet_preprocess_views_view(&$vars) {
  // Add Unique Classes to Past/Live/Future Webinars Calendar
  $view = $vars['view'];
  if (($view->current_display == 'page_2' ^ $view->current_display == 'content_block') && $view->name == 'cabinet') {
    $period = isset($view->args[1]) ? $view->args[1] : '';
    if ($period == 'past' ^ $period == 'live' ^ $period == 'future') {
      $vars['classes_array'][] = $period;
      // Hide Webinars Calendar where not needed
      $vars['classes_array'][] = 'period';
    }
  }

}


/**
 * Implements hook_preprocess_views_view_fields().
 * @param $vars
 * @return void
 *
 * @see hook_preprocess_views_view_fields().
 */
function daisynet_preprocess_views_view_fields(&$vars) {
  $view = $vars['view'];
  $fields = $vars['fields'];

  switch($view->name) {
    case 'calendar':
      break;
    case 'user_info':
      if ($view->current_display == 'block_1' || $view->current_display == 'block_2') {
        if (daisynet_user_is_online($vars['row']->uid)) {
          $vars['fields']['field_user_image']->content .= '<span class="user-is-online">&nbsp;</span>';
          if (isset($vars['fields']['field_user_image_1'])) {
            $vars['fields']['field_user_image_1']->content .= '<span class="user-is-online">&nbsp;</span>';
          }
        }
      }
      break;

    case 'friends':
      if ($view->current_display == 'page') {
        if (daisynet_user_is_online($vars['row']->uid)) {
          $vars['fields']['field_user_image']->content .= '<span class="user-is-online">&nbsp;</span>';
        }
      }
      break;

    case 'webinars':
    case 'catalog':
      global $language;
      global $user;

      if(isset($fields['uid'])) {
        $vars['webinar_author'] = ($user->uid == $fields['uid']->content) ? TRUE : FALSE;
      }
      else {
        $vars['webinar_author'] = FALSE;
      }

      // Number of participants
      $vars['tickets_bought'] = 0;
      if (isset($vars['row']->field_field_participants)) {
        $vars['tickets_bought'] = count($vars['row']->field_field_participants);
      }

      // Paid or free webinar
      $vars['tickets_label'] = t('Tickets bought');
      if (isset($vars['row']->field_field_webinar_price)) {
        $vars['is_free'] = $fields['field_webinar_price']->content === 'free';
        if ($vars['is_free']) {
          $vars['tickets_label'] = t('Registered');
        }
      }

      //Get current rating.
      $query = db_select('rating', 'r');
      $query->fields('r', array('rating_mark', 'uid'));
      if(isset($fields['nid'])) {
        $query->condition('r.nid', $fields['nid']->content);
      }
      $result_rating = $query->execute();

      $sum = 0;
      $itemNumber = 0;
      $vars['current_user_mark'] = 0;
      foreach ($result_rating as $key => $value) {
        $itemNumber++;
        $sum += $value->rating_mark;

        //If current user has voted, retrieve his vote.
        $vars['current_user_mark'] = ($user->uid == $value->uid) ? $value->rating_mark : '';
      }
      $rating = 0;
      if ($itemNumber != 0) {
        $rating = round($sum/$itemNumber, 2);
      }
      $vars['rating'] = $rating;

      //Builing modal preview top webinars link
      $vars['modal_preview_link'] = ctools_modal_text_button(
        t(''),
        'daisy-modals-preview-callaback/nojs/' . $fields['nid']->content,
        t('Watch preview'),
        'ctools-modal-daisy-preview-style preview-interview'
        );

     /**
       * Preprocess all views except the enumerated below.
       */
      if(!in_array($view->current_display, array('archive_top', 'archive_anons', 'archive_hot'))) {
        $row = $vars['row'];
        // Set up the date with the correct timezone
        $node = node_load($vars['fields']['nid']->content);
        $date_raw = field_get_items('node', $node, 'field_webinar_date');
        if (isset($date_raw[0])) {
          $date_raw = $date_raw[0];
          $date = new DateObject($date_raw['value'], $date_raw['timezone_db'], 'Y-m-d H:i:s');
          $timezone = $date_raw['timezone'];
          if ($user->uid != 0) {
            $timezone = $user->timezone;
          }
          date_timezone_set($date, timezone_open($timezone));
          $date = $date->getTimestamp();

          $date_field = '<div class="field-content"><span class="day">' . format_date($date, 'custom', 'd') . '</span>';
          $date_field .= '<span class="month">' . t(format_date($date, 'custom', 'F')) . '</span>';
          $date_field .= '<span class="time">' . format_date($date, 'custom', 'H:i') . '</span></div>';
          $vars['fields']['field_webinar_date']->content = $date_field;
        }

      }
      break;
  }
}


/**
 * Helper function for rendering a block.
 * @param $delta
 * @param string $type
 * @return bool|string
 */
function _daisynet_block_render($delta, $type = 'menu') {
  global $theme_key;

  $themes = list_themes();
  $theme_object = $themes[$theme_key];

  $block = block_load($type, $delta);
  $block->theme = $theme_object->name;
  $block_content = _block_render_blocks(array($block));
  $build = _block_get_renderable_array($block_content);
  $block_rendered = drupal_render($build);

  return $block_rendered;
}


/**
 * Overrides theme_form_element().
 * @param $variables
 * @return string
 * TODO: Add more theme settings for tooltips - element types to display on, placement of tooltip box.
 * @see theme_form_element().
 */
function daisynet_form_element($variables) {
   $element = &$variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  if(isset($element['#field_prefix'])) {
    $attributes['class'][] = 'input-prepend';
  }

  if(isset($element['#field_suffix'])) {
    $attributes['class'][] = 'input-append';
  }

  if($element['#type'] == 'date_popup') {
    $attributes['class'][] = 'bootstrap-timepicker-component';
  }

  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix add-on">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix add-on">' . $element['#field_suffix'] . '</span>' : '';

  $description = '';
  if (!empty($element['#description'])) {
    //Move description as tooltip if necessary, otherwise - as normal description.
    if (theme_get_setting('metronic_tooltips') || true) {
      if (isset($element['#field_name']) && ($element['#field_name'] == 'field_user_agreement')) {
        $description = '<div class="description">' . $t($element['#description']) . "</div>\n";
      }
      elseif($element['#type'] != 'date_popup') {
        $element['#children'] = '<div class="tooltips" data-placement="bottom" data-original-title="'
                            . t(strip_tags($element['#description'])) . '">' . $element['#children'] . '</div>';
      }
    }
    else {
      $description = '<div class="description">' . $t($element['#description']) . "</div>\n";
    }
  }

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  //Add description to element if using default layout.
  $output .= $description;

  $output .= "</div>\n";

  return $output;

}


/**
 * Override radio element with Metronic component.
 * @param $variables
 * @return string
 */
function daisynet_radio($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'radio';
  element_set_attributes($element, array('id', 'name','#return_value' => 'value'));

  if (isset($element['#return_value']) && $element['#value'] !== FALSE && $element['#value'] == $element['#return_value']) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-radio'));

  return '<label class="radio"><div class="radio"><span>'.
         '<input' . drupal_attributes($element['#attributes']) . ' /></span></div>'. t($element['#title']) . '</label>';
}


/**
 * Overrides radios element with Metronic components.
 * @param $variables
 * @return string
 */
function daisynet_radios($variables) {
  $element = $variables['element'];
  $attributes = array();
  if (isset($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  $attributes['class'] = 'form-radios';
  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] .= ' ' . implode(' ', $element['#attributes']['class']);
  }
  if (isset($element['#attributes']['title'])) {
    $attributes['title'] = $element['#attributes']['title'];
  }

  $output = (!empty($element['#children'])) ? strip_tags($element['#children'], '<input><span><img><label>') : '';
  return $output;
}


/**
 * Override radio element with Metronic component.
 * @param $variables
 * @return string
 */
function daisynet_checkbox($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'checkbox';
  element_set_attributes($element, array('id', 'name','#return_value' => 'value'));

  if (isset($element['#return_value']) && $element['#value'] !== FALSE && $element['#value'] == $element['#return_value']) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-radio'));
  $title = !is_null($element['#title']) ? t($element['#title']) : '';
  return '<label class="checkbox"><div class="checker"><span>'.
         '<input' . drupal_attributes($element['#attributes']) . ' /></span></div>'. $title . '</label>';
}


/**
 * Override radio element with Metronic component.
 * @param $variables
 * @return string
 */
function daisynet_checkboxes($variables) {
  $element = $variables['element'];
  $attributes = array();
  if (isset($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  $attributes['class'][] = 'form-checkboxes';
  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = array_merge($attributes['class'], $element['#attributes']['class']);
  }
  if (isset($element['#attributes']['title'])) {
    $attributes['title'] = t($element['#attributes']['title']);
  }

  $output = (!empty($element['#children'])) ? strip_tags($element['#children'], '<input><span><label>') : '';
  return $output;
}


/**
 * Overrides textfield element with Metronic attributes structure.
 */
function daisynet_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array('id', 'name', 'value', 'size', 'maxlength'));
  _form_set_class($element, array('form-text'));

  //Adds Metronic attributes to timepicker element.
  if(in_array('field_webinar_date', $element['#parents'], TRUE) && in_array('time', $element['#parents'], TRUE)) {
    $element['#attributes']['class'][] = 'timepicker-24';
    $element['#attributes']['value'] = $element['#default_value'];
    $suffix = '<span class="add-on"><i class="icon-time"></i></span>';
    $output =   '<div class="input-append bootstrap-timepicker-component">'
              . '<input' . drupal_attributes($element['#attributes']) . ' />'
              . $suffix . '</div>';
    return $output;
  }

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}


/**
 * Override theme_form_element_label().
 *
 * Implements Metronic styles.
 * @param $variables
 * @return string
 */
function daisynet_form_element_label($variables) {
  $element = $variables['element'];
  //This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  //If processing a radio element, cancel title as well.
  if($element['#type'] == 'radio' || $element['#type'] == 'checkbox')
    return '';

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  //Make labels translatable.
  $title = t(filter_xss_admin($element['#title']));

  //Adds the Metronic form label class.
  $attributes = array('class' => 'control-label');

  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => t($title), '!required' => $required)) . "</label>\n";
}


/**
 * Overrides the theme_field().
 * Processes special fields with additional functionality.
 * @param $variables
 * @return string
 *
 * @see theme_field().
 */
function daisynet_field($vars) {
  $output = '';
  $element = $vars['element'];
  $items = $vars['items'];

  //Render the label, if it's not hidden.
  if (!$vars['label_hidden']) {
    $output .= '<div class="field-label"' . $vars['title_attributes'] . '>';
    if ($vars['element']['#field_type'] == 'taxonomy_term_reference' && $vars['element']['#entity_type'] != 'user') {
      $output .= '<i class="icon-tags"></i>';
    }
    $output .= $vars['label'] . '</div>';
  }

  $comma_sep_fields = array(
    'field_user_grade',
    'field_user_recommended',
    'field_user_age_category',
    'field_user_languages'
  );
  $comma_separated = in_array($vars['element']['#field_name'], $comma_sep_fields);

  //Translate term names in fields.
  if ($vars['element']['#field_type'] == 'taxonomy_term_reference') {
    foreach ($vars['element']['#items'] as $item_key => $item_term) {
      $vars['items'][$item_key]['#markup'] = i18n_taxonomy_term_name($item_term['taxonomy_term']);
    }
  }

  /**
   * Adds specific color for webinar category links + overrides the path with system view based paths - webinars.
   * @see views
   */
  if($element['#field_name'] == 'field_webinar_category') {
    foreach($items as $index => $item) {
      $term = (array) $item['#options']['entity'];
      $parents = taxonomy_get_parents($term['tid']);
      //If not from general category, remove from display the subcategory link.
      if(count($parents) <> 0) {
        unset($vars['items'][$index]);
      }
      else {
        if (isset($term['field_bg_color'][LANGUAGE_NONE][0])) {
          $term_color = '#' . $term['field_bg_color'][LANGUAGE_NONE][0]['jquery_colorpicker'];
          $background_color = '#' . $term['field_weinar_bg_color'][LANGUAGE_NONE][0]['jquery_colorpicker'];
          $vars['items'][$index]['#attributes']['style'] = 'color: '. $term_color;
        }
        $vars['items'][$index]['#href'] = 'webinars/' . $term['tid'];
      }
    }
  }

  //Render the items.
  $output .= '<div class="field-items"' . $vars['content_attributes'] . '>';
  if ($comma_separated) {
    foreach ($vars['items'] as $delta => $item) {
      $output_arr[] = drupal_render($item);
    }
    $output .= implode(', ', $output_arr);
  } else {
    foreach ($vars['items'] as $delta => $item) {
      $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
      $output .= '<div class="' . $classes . '"' . $vars['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
    }
  }
  $output .= '</div>';

  //Render the wrapper div.
  $output = '<div class="' . $vars['classes'] . '"' . $vars['attributes'] . '>' . $output . '</div>';

  return $output;
}


/**
 * Overrides theme_fieldset().
 * Returns HTML for a fieldset form element and its children with Metronic + Bootstrap modifications.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #attributes, #children, #collapsed, #collapsible,
 *     #description, #id, #title, #value.
 *
 * @ingroup themeable
 */
function daisynet_fieldset($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper'));

  $output = '<fieldset' . drupal_attributes($element['#attributes']) . '>';
  if (!empty($element['#title'])) {
    // Always wrap fieldset legends in a SPAN for CSS positioning.
    $output .= '<legend><span class="fieldset-legend">' . t($element['#title']) . '</span></legend>';
  }
  $output .= '<div class="fieldset-wrapper">';
  if (!empty($element['#description'])) {
    $output .= '<div class="fieldset-description">' . t($element['#description']) . '</div>';
  }
  $output .= $element['#children'];
  if (isset($element['#value'])) {
    $output .= $element['#value'];
  }
  $output .= '</div>';
  $output .= "</fieldset>\n";
  return $output;
}


/**
 * Overrides theme_user_relationshops_request_relationship_link().
 * Builds a direct relationship request link.
 *
 * @param $variables
 *
 * @see theme_user_relationshops_request_relationship_link().
 */
function daisynet_user_relationships_request_relationship_direct_link($variables) {
  $relate_to = $variables['relate_to'];
  $relationship_type = $variables['relationship_type'];
  $text = !empty($variables['text']) ? $variables['text'] : 'Friends';
  if (!isset($variables['class'])) {
    $variables['class'] = '';
  }

  //Fallback check - revert to  a generic link if relationship type is missing.
  if (!isset($relationship_type)) {
    return theme('user_relationships_request_relationship_link', array('relate_to' => $relate_to));
  }

  //Build the link.
  return l(
    t($text, array(), array('context' => 'relationship')),
    "relationship/{$relate_to->uid}/request/{$relationship_type->rtid}",
    array(
      'query' => drupal_get_destination(),
      'html' => TRUE,
      'attributes' => array('class' => array('user_relationships_popup_link add-friend' . $variables['class'])),
    )
  );
}


/**
 * Overrides theme_user_relationships_remove_link().
 * Builds the remove relationship link.
 *
 * @param $variables;
 *
 * @see theme_user_relationships_remove_link().
 */
function daisynet_user_relationships_remove_link($variables) {
  $uid = $variables['uid'];
  $rid = $variables['rid'];
  $text = !empty($variables['text']) ? $variables['text'] : t('Unfriend');
  if (!isset($variables['class'])) {
    $variables['class'] = '';
  }

  return l(
    t($text, array(), array('context' => 'relationship')),
    "user/{$uid}/relationships/{$rid}/remove",
    array(
      'query' => drupal_get_destination(),
      'html' => TRUE,
      'attributes' => array('class' => array('user_relationships_popup_link remove' . $variables['class']), 'title' => t('Remove')),
    )
  );
}


/**
 * Override the theme_user_relationships_pending_request_approve_link().
 * Builds the Approve to pending relationship link.
 *
 * @param $variables.
 *
 * @see theme_user_relationships_pending_request_approve_link().
 */
function daisynet_user_relationships_pending_request_approve_link($variables) {
  $uid = $variables['uid'];
  $rid = $variables['rid'];
  if (!isset($variables['class'])) {
    $variables['class'] = '';
  }

  return l(
    t('Approve'),
    "user/{$uid}/relationships/requested/{$rid}/approve",
    array(
      'title' => t('Friend'),
      'query' => drupal_get_destination(),
      'attributes' => array('class' => array('user_relationships_popup_link approve' . $variables['class'])),
    )
  );
}


/**
 * Override the theme_user_relationships_pending_request_disapprove_link().
 * Builds the decline pending relationship link.
 *
 * @param $variables
 *
 * @see theme_user_relationships_pending_request_disapprove_link().
 */
function daisynet_user_relationships_pending_request_disapprove_link($variables) {
  $uid = $variables['uid'];
  $rid = $variables['rid'];
  if (!isset($variables['class'])) {
    $variables['class'] = '';
  }

  return l(
    t('Decline'),
    "user/{$uid}/relationships/requested/{$rid}/disapprove",
    array(
      'title' => t('Decline'),
      'query' => drupal_get_destination(),
      'attributes' => array('class' => array('user_relationships_popup_link decline' . $variables['class'])),
    )
  );
}

/**
 * Override the theme_user_relationships_pending_request_cancel_link().
 * Builds the Cancel relationship request link.
 *
 * @params $variables
 *
 * @see theme_user_relationships_pending_request_cancel_link().
 */
function daisynet_user_relationships_pending_request_cancel_link($variables) {
  $uid = $variables['uid'];
  $rid = $variables['rid'];
  if (!isset($variables['class'])) {
    $variables['class'] = '';
  }

  return l(
    t('Cancel', array(), array('context' => 'relationship')),
    "user/{$uid}/relationships/requested/{$rid}/cancel",
    array(
      'query' => drupal_get_destination(),
      'attributes' => array('class' => array('user_relationships_popup_link cancel' . $variables['class']), 'title' => t('Cancel'),),
    )
  );
}


/**
 * Wrapper function to be used in theming
 *
 * @return bool|string
 */
function daisynet_user_image_small($account = NULL, $image_style = FALSE) {
  if($account === NULL || !isset($account->uid)) {
    global $user;
  }
  else {
    $user = $account;
  }

  return daisy_user_get_picture($user, $image_style);
}

/**
 * Helper function to check user online status.
 * @param $uid
 * @return bool
 */
function daisynet_user_is_online($uid) {
  if (!$uid) {
    return FALSE;
  }

  $sql = 'SELECT COUNT(uid) as count FROM {sessions}
          WHERE uid = :uid AND FROM_UNIXTIME(timestamp) > DATE_SUB(NOW(), INTERVAL 15 MINUTE)';
  $rez = db_query($sql, array(':uid' => $uid))->fetchObject();
  if ($rez->count) {
    return TRUE;
  }

  return FALSE;
}


/**
 * Overrides theme_field_multiple_value_form().
 *
 * @param $variables
 *
 * @return string
 *
 * @see theme_field_multiple_value_form()
 */
function daisynet_field_multiple_value_form($variables) {
  if (arg(0) == 'user' && arg(3) == 'teacher') {
    $variables['element']['#required'] = TRUE;
  }

  return theme_field_multiple_value_form($variables);
}


/**
 * Implements hook_preprocess_views_view_grid().
 *
 * @param $vars
 *
 * @return void
 *
 * @see hook_preprocess_views_view_grid().
 */
function daisynet_preprocess_views_view_grid(&$vars) {
  if ($vars['view']->name == 'catalog' && strpos($vars['view']->current_display, 'attachment') !== FALSE) {
    $vars['title'] = $vars['view']->display[$vars['view']->current_display]->display_options['title'];
  }
}


/**
 * Overrides the theme_uc_cart_complete_sale().
 *
 * @param $variables
 *
 * @return void
 *
 * @see theme_uc_cart_complete_sale().
 */
function daisynet_uc_cart_complete_sale($variables) {
  global $language;

  $mess = t('Your order is complete! Your order number is @order_id. Thank you for shopping at Daisynet. While logged in, you may continue shopping or view your current <a href="@url_link">order status</a> and order history. <a href="/'.$language->language.'/webinars">Return to catalog</a>', array('@order_id' => $variables['order']->order_id, '@url_link' => '/'.$language->language.'/user/'. $variables['order']->uid.'/orders/'.$variables['order']->order_id));
  daisynet_set_cart_message($mess);

  if($variables['order']->products[0]->model == 'add-funds-ru' || $variables['order']->products[0]->model == 'add-funds') {
    drupal_goto('catalog');
  }
  else {
    drupal_goto('node/'.$variables['order']->products[0]->nid);
  }
}


/**
 * Overrides theme_set_cart_message().
 *
 * @param null $message
 * @param string $type
 * @param bool $repeat
 *
 * @return null
 *
 * @see theme_set_cart_message().
 */
function daisynet_set_cart_message($message = NULL, $type = 'status', $repeat = TRUE) {
  if ($message) {
    if (!isset($_SESSION['cart_message'][$type])) {
      $_SESSION['cart_message'][$type] = array();
    }
    if ($repeat || !in_array($message, $_SESSION['cart_message'][$type])) {
      $_SESSION['cart_message'][$type][] = $message;
    }
    //Mark this page as being uncacheable.
    drupal_page_is_cacheable(FALSE);
  }
  // Messages not set when DB connection fails.
  return isset($_SESSION['cart_message']) ? $_SESSION['cart_message'] : NULL;
}


/**
 * Overrides theme_get_cart_message().
 *
 * @return null
 *
 * @see daisynet_get_cart_message().
 */
function daisynet_get_cart_message() {
  if ($messages = daisynet_set_cart_message()) {
    unset($_SESSION['cart_message']);
    return $messages;
  }
}


/**
 * Helper function for settings the content span width.
 */
function _daisynet_content_span($columns){
  switch($columns) {
    case 1:
      $class = 'span12';
      break;
    case 2:
      $class = 'span9';
      break;
    case 3:
      $class = 'span6';
      break;
  }

  return $class;
}


/**
 * Helper function - generates the languages list block.
 *
 * @return string
 */
function daisynet_languages() {
  $path = drupal_is_front_page() ? '<front>' : $_GET['q'];
  $links = language_negotiation_get_switch_links('language', $path);

  if (isset($links->links)) {
    $class = "lang-switch dropdown-menu";
    $menu_links = $links->links;
    // Translate the 'All languages' item
    if (isset($menu_links['all'])) {
      $menu_links['all']['title'] = t($menu_links['all']['title']);
    }
    $variables = array('links' => $menu_links, 'attributes' => array('class' => array($class)));
    $ul = theme('links__locale_block', $variables);
    return $ul;
  }
  return '';
}

/**
 * Implements hook_form_alter().
 * @param $form
 * @param $form_state
 * @param $form_id
 * @return void
 */
function daisynet_form_alter(&$form, &$form_state, $form_id) {

  if (strpos($form_id, 'comment_node_') !== FALSE) {
    $form['actions']['button']['#prefix'] = '<button type="submit" class="btn icn-only">';
    $form['actions']['button']['#suffix'] = '</button>';
    $form['actions']['button']['#markup'] = '<i class="icon-ok icon-white"></i>';
  }

  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
    $form['search_block_form']['#size'] = 20;  // define size of the textfield
    $form['search_block_form']['#default_value'] = t('Search'); // Set a default value for the textfield
    $form['actions']['submit']['#value'] = t('Search'); // Change the text on the submit button
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = '" . t('Search') . "';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == '" . t('Search') . "') {this.value = '';}";
  }

  if($form_id == 'product_node_form') {
    $form['#attached']['js'][] = drupal_get_path('theme', 'daisynet'). '/js/editWebinar.js';
    $form['#attributes'] = array('name' => 'product-webinar');
    if(theme_get_setting('bootstrap_wizard')) {
      $form['#attached']['css'][] = drupal_get_path('theme', 'daisynet'). '/assets/bootstrap-wizard/prettify.css';
      $form['#attached']['js'][] = drupal_get_path('theme', 'daisynet'). '/assets/bootstrap-wizard/prettify.js';
      $form['#attached']['js'][] = drupal_get_path('theme', 'daisynet'). '/assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js';
    }
    if(theme_get_setting('bootstrap_toggle_buttons')) {
      $form['#attached']['css'][] = drupal_get_path('theme', 'daisynet'). '/assets/bootstrap-toggle-buttons/bootstrap-toggle-buttons.css';
      $form['#attached']['js'][] = drupal_get_path('theme', 'daisynet'). '/assets/bootstrap-toggle-buttons/jquery.toggle.buttons.js';
    }

    $vocab = taxonomy_vocabulary_machine_name_load('category');
    $main_categories = taxonomy_get_tree($vocab->vid, 0, 1, FALSE);
    $main_options = array();
    foreach($main_categories as $term) {
      $main_options[$term->tid] = i18n_taxonomy_term_name($term);
    }
    $form['field_webinar_category_front'][LANGUAGE_NONE]['#options'] = $main_options;

    //If editing an existing product node, use a different template.
    if(!is_null($form['#node']->nid)) {
      $form['#theme'] = 'product_node_form_edit';
    }
  }

  //Define default color styles for the form actions elements.
  if(isset($form['actions'])) {
    if(isset($form['actions']['submit']) && strpos($form_id, 'user_') !== 0) {
      $form['actions']['submit']['#attributes']['class'][] = 'daisy-active';
    }

    if(isset($form['actions']['preview'])) {
      $form['actions']['preview']['#attributes']['class'][] = 'daisy-default';
    }

    if(isset($form['actions']['save_continue'])) {
      $form['actions']['save_continue']['#attributes']['class'][] = 'daisy-default';
    }

    if(isset($form['actions']['delete'])) {
      $form['actions']['delete']['#attributes']['class'][] = 'daisy-alert';
    }
  }

  //Add the clearfix class to all forms.
  $form['#attributes']['class'][] = 'clearfix';
}


/**
 * Overrides the theme_date_nav_title().
 * @param $params
 * @return array|null|string
 *
 * @see theme_date_nav_title().
 */
function daisynet_date_nav_title($params) {
  $granularity = $params['granularity'];
  $view = $params['view'];
  $date_info = $view->date_info;
  $link = !empty($params['link']) ? $params['link'] : FALSE;
  $format = !empty($params['format']) ? $params['format'] : NULL;

  if ($date_info->mini && $view->name == 'cabinet') {
    return date_format_date($date_info->min_date, 'custom', 'F Y');
  } else {
    return theme_date_nav_title($params);
  }
}

/**
 * New function to render tabs as metronic action icons
 */
function daisynet_node_portlet_tools($variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    foreach ($variables['primary'] as $element) {
      $element['#theme'] = 'node_portlet_tools_link';
      $output .= render($element);
    }
  }

  return $output;
}

/**
 * Rendering a single portlet action link
 */
function daisynet_node_portlet_tools_link($variables) {
  $link = $variables['element']['#link'];
  $link_text = $link['title'];

  if (!empty($variables['element']['#active'])) {
    // Add text to indicate active tab for non-visual users.
    $active = '<span class="element-invisible">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array('!local-task-title' => $link['title'], '!active' => $active));
  }

  // Output as custom icons:
  switch ($link['page_callback']) {
    case 'node_page_view':
      $link_text = '';
      $link['localized_options']['attributes']['class'] = array('icon-eye-open');
      $link['localized_options']['attributes']['title'] = t('View');
      break;
    case 'node_page_edit':
      $link_text = '';
      $link['localized_options']['attributes']['class'] = array('icon-wrench');
      $link['localized_options']['attributes']['title'] = t('Edit');
      break;
    case 'statistics_node_tracker':
      $link_text = '';
      $link['localized_options']['attributes']['class'] = array('icon-bar-chart');
      $link['localized_options']['attributes']['title'] = t('Analytics');
      break;
    case 'rate_results_page':
      $link_text = '';
      $link['localized_options']['attributes']['class'] = array('icon-check');
      $link['localized_options']['attributes']['title'] = t('Votes rating');
      break;
    case 'devel_load_object':
      $link_text = '';
      $link['localized_options']['attributes']['class'] = array('icon-cog');
      $link['localized_options']['attributes']['title'] = t('Devel');
      break;
    case 'clone_node_check':
      $link_text = '';
      $link['localized_options']['attributes']['class'] = array('icon-copy');
      $link['localized_options']['attributes']['title'] = t('Clone');
      break;
  }
  $formatted_link =  l($link_text, $link['href'], $link['localized_options']);

  return $formatted_link;
}


/**
 * Helper function for advanced title display settings check.
 * @see daisynet_preprocess_page().
 */
function _daisynet_title_toggle_settings(&$vars) {
  $conditions = 0;

  //Hide title for front page.
  $conditions += (int)drupal_is_front_page();

  //Hide title for node type displays pages.
  $conditions += (int)(isset($vars['node']) && in_array($vars['node']->type, array('product', 'news', 'group')));

  //Hide title for user profile pages.
  $conditions += (int)(arg(0) == 'user' && is_numeric(arg(1)) && arg(2));

  //Hide title for webinar catalog, schedule,  pages.
  //$conditions += (int)(in_array(arg(0), array('webinars', 'schedule', 'community')));

  //Hide title for Webinar / Products creation pages.
  $conditions += (int)(arg(0) == 'node' && arg(1) == 'add'  && in_array(arg(2), array('product', 'news', 'group')));

  //If any of the conditions is met, hide the title.
  if($conditions > 0) {
    unset($vars['title']);
  }
}
