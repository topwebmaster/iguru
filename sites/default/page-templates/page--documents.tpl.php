<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
$theme_path = $base_path . $directory;

if ($language->language == 'en') :
    ?>
    <script type="text/javascript">
        reformal_wdg_w    = "713";
        reformal_wdg_h    = "460";
        reformal_wdg_domain    = "daisynet";
        reformal_wdg_mode    = 0;
        reformal_wdg_title   = "DaisyNet";
        reformal_wdg_ltitle  = "Feedback and ideas...";
        reformal_wdg_lfont   = "";
        reformal_wdg_lsize   = "";
        reformal_wdg_color   = "#FFA000";
        reformal_wdg_bcolor  = "#6568ab";
        reformal_wdg_tcolor  = "#FFFFFF";
        reformal_wdg_align   = "left";
        reformal_wdg_waction = 0;
        reformal_wdg_vcolor  = "#f2be11";
        reformal_wdg_cmline  = "#E0E0E0";
        reformal_wdg_glcolor  = "#6568ab";
        reformal_wdg_tbcolor  = "#FFFFFF";
        reformal_wdg_bimage = "29a2770fafec079073069edf2daf22af.png";
    </script>

    <script type="text/javascript" language="JavaScript" src="http://idea.informer.com/tabn2v4.js?domain=daisynet"></script><noscript><a href="http://daisynet.idea.informer.com">DaisyNet feedback</a> <a href="http://idea.informer.com"> Powered by <img src="http://widget.idea.informer.com/tmpl/images/widget_logo.jpg" /></a></noscript>

<?php else : ?>
    <script type="text/javascript">
        var reformalOptions = {
            project_id: 57796,
            project_host: "DaisyNet.reformal.ru",
            tab_orientation: "left",
            tab_indent: "50%",
            tab_bg_color: "#fafafa",
            tab_border_color: "#6569ab",
            tab_image_url: "http://tab.reformal.ru/0JLQsNGI0LggT9GC0LfRi9Cy0Ysg0Lgg0LjQtNC10LguLi4=/6569ab/bac3e0b958e545f4bc8a82335a8ef5b7/left/0/tab.png",
            tab_border_width: 0
        };
                                  
        (function() {
            var script = document.createElement('script');
            script.type = 'text/javascript'; script.async = true;
            script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'media.reformal.ru/widgets/v3/reformal.js';
            document.getElementsByTagName('head')[0].appendChild(script);
        })();
        setTimeout(function(){
            document.getElementById('reformal_tab').href='javascript:Reformal.widgetOpen();';
            document.getElementById('reformal_tab').onclick = function(){};
        },1000);
    </script>
<?php endif; ?>

<!--[if lt IE 8]>
<link type="text/css" rel="stylesheet" href="<?php print $theme_path; ?>/css/style-ie7.css" media="all" />
<script>
jQuery(document).ready(function(){
        setTimeout(function(){
                jQuery("#product-node-form > div > div:eq(13)").css({'float':'left','width':'180px'});;
                jQuery("#product-node-form > div > div:eq(15)").css({'float':'left','width':'180px'});;
        },500);
        
        jQuery('table.views-view-grid td').click(function(){
                jQuery('table.views-view-grid td').css('position','static');
                jQuery('table.views-view-grid td div.friend-user-wrapper').css('position','static');
                jQuery('div.friend-user-wrapper',this).css('position','relative');
                jQuery('a.close',this).css('top','-10px');
                jQuery(this).css('position','relative');
        })
});
</script>
<![endif]-->
<!--[if IE 8]>
<link type="text/css" rel="stylesheet" href="<?php print $theme_path; ?>/css/style-ie8.css" media="all" />
<![endif]-->
<!--[if IE 9]>
<link type="text/css" rel="stylesheet" href="<?php print $theme_path; ?>/css/style-ie9.css" media="all" />
<![endif]-->



<header>
    <a id="main-content"></a>
    <div class="header-top container">
        <a href="<?php print $base_path; ?>" class="logo"><img src="<?php print $theme_path; ?>/img/logo.png" /></a>
        <nav>
            <?php print render($page['top_menu']); ?>
        </nav>
        <div class="slm">
        	<div id="shopping_ico">
	            <a href="<?php print $base_path.$language->language ?>/cart" title="<?php print t('Shopping cart') ?>"><img border="0" alt="<?php print t('Shopping cart') ?>" src="<?php print $theme_path; ?>/img/shop.png" /></a>
	        </div>
            <div id="top_logout">
                <?php if ($user->uid > 0) : ?>
                    <a href="<?php print $base_path ?>user/logout" title="<?php print t('logout') ?>"><img border="0" alt="<?php print t('logout') ?>" src="<?php print $theme_path; ?>/img/logout.png" /></a>
                <?php else: ?>
                    <?/*
                    <a href="<?php print $base_path ?>user" title="<?php print t('login') ?>"><img border="0" alt="<?php print t('login') ?>" src="<?php print $theme_path; ?>/img/logout.png" /></a>
                    */?>
                    <span title="<?php print t('Choose Daisy') ?>" class="choose_daisy" ><img border="0" alt="<?php print t('Choose Daisy') ?>" src="<?php print $theme_path; ?>/img/logout.png" /></span>
                    
                    <div class="register_block hidden">
                    	<?php print render($page['auth_form']); ?>
                    </div>
                    
                <?php endif; ?>
            </div>
            <div class="lang">      
                <a href="#" class="lang-selected"><span><?php print $language->language; ?></span><span class="lang-arrow">&nbsp;</span></a>
                <?php print $lang_switcher; ?>
            </div>
        </div>
    </div>
    <?php if (user_is_logged_in()): ?>
        <div class="header-bottom container">
            <a href="<?php print $front_page; ?>" class="home"><img src="<?php print $theme_path; ?>/img/home.png" /></a>
            <?php print _daisy_block_render('menu-cabinet-links'); ?>
            <ul class="user-menu">
                <li><?php print l(t('Office'), 'cabinet'); ?></li>
                <li class="padding-left-5">
                    <?php //print l(t('Add funds'), $language->language == 'ru' ? 'node/232' : 'node/176'); ?>
                    <?php
                    if ($language->language == 'ru') {
                        print render(drupal_get_form('uc_product_add_to_cart_form_' . 176, node_load(176)));
                    } else {
                        print render(drupal_get_form('uc_product_add_to_cart_form_' . 232, node_load(232)));
                    }
                    ?>
                </li>
                <li><span class="balance"><?php print $balance; ?></span></li>        
                <li>
                    <span class="user-icon"><?php print $user_image_small; ?></span><span class="user-name"><?php print $user_name; ?></span>
                    <?php print _daisy_block_render('menu-account-menu'); ?>
                </li>
            </ul>
        </div>
    <?php endif; ?>
</header>
<?php
if (!$page['sidebar_first'] && !$page['sidebar_second']) {
    $node_hide_sidebars = TRUE;
}
?>
<div role="main" class="container<?php print ($node_hide_sidebars ? ' node-view' : ''); ?>" id="page">
    <?php if ($page['sidebar_first'] && !$node_hide_sidebars): ?>
        <div id="sidebar-first">
            <?php print render($page['sidebar_first']); ?>
        </div>
    <?php endif; ?>
    <div id="content">
        <?php print $messages; ?>
        <?php if ($cart_messages = daisy_get_cart_message()) { ?>
            <link type="text/css" rel="stylesheet" href="<?php print $theme_path; ?>/css/basic.css" media="all" />
            <script type="text/javascript" src="<?php print $theme_path; ?>/js/libs/jquery.simplemodal.js"></script>
            <script type="text/javascript">
                jQuery(function ($) {
             
                        $.modal('<?php print ($cart_messages['status'][0]) ?>', {
                            containerCss:{
                                backgroundColor:"#fff",
                                padding:0,
                                height:"200px",
                                width:"200px",
                                marginTop:"91px"
                            },
                            overlayClose:true
                        });
                    });
        
            </script>
        <?php } ?>
        <?php print render($title_prefix); ?>
        <?php if ($title && !$webinar_hide_title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if ($tabs && user_access('view tabs')): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
    	<div class="empty content_tabs">
        	<?php print render($page['content']); ?>
        </div>	
        <?
        
        $query = db_select('node', 'n');
		$query->innerJoin('field_data_field_webinar_files', 'f', 'n.nid = f.entity_id');
		$query->innerJoin('field_data_field_webinar_dfiles', 'd', 'n.nid = d.entity_id');
		$query->fields('f', array('field_webinar_files_fid'));
		$query->fields('d', array('field_webinar_dfiles_fid'));
		$query->condition('n.uid',$user->uid);
		$resultFiles = $query->execute();
        
        
        foreach ($resultFiles as $key => $value) {
            if ( isset($value->field_webinar_files_fid) ) {
            	echo '<br><a href="'.file_create_url(file_load($value->field_webinar_files_fid)->uri).'" >'.file_load($value->field_webinar_files_fid)->filename.'</a><hr />';
            }
			if ( isset($value->field_webinar_dfiles_fid) ) {
				echo '<br><a href="'.file_create_url(file_load($value->field_webinar_dfiles_fid)->uri).'" >'.file_load($value->field_webinar_dfiles_fid)->filename.'</a><hr />';
			}
        }
        ?>	
    </div>
    <?php if ($page['sidebar_second'] && !$node_hide_sidebars): ?>
        <div id="sidebar-second">
            <?php print render($page['sidebar_second']); ?>
        </div>
    <?php endif; ?>

</div>
<div class="back-to-top clearfix">
    <a href="#main-content"><?php print t('Back to top'); ?></a>
</div>
<footer>
    <div class="footer-wrapper">
        <div id="footer">
            <div id="payments"><img src="<?php print $theme_path; ?>/img/footer-av.png" /></div>
            <div class="copyright"><span><?php print t('Copyrights @year Daisy. All right reserved.', array('@year' => date('Y'))); ?></span></div>
            <?php //print theme('links', array('links' => menu_navigation_links('menu-footer-menu'), 'attributes' => array('class' => array('menu'), 'id' => 'footer-menu')));   ?>     
            <?php print render($page['footer_menu']); ?>
            <div id="search_region_block">
                <?php print render($page['search_block']); ?>
            </div>
            <ul class="social">
                <li class="twitter"><a href="https://twitter.com/daisynetinfo" target="_blank"></a></li>
                <li class="facebook"><a href="http://www.facebook.com/pages/daisynetinfo/195304700481634" target="_blank"></a></li>
                <li class="youtube"><a href="http://www.youtube.com/daisynetinfo" target="_blank"></a></li>
            </ul>
        </div>
    </div>
</footer>