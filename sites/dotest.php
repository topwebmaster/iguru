<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>DO test</title>
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body>

<div id="PGWformPlace">
    <form action="https://paymentgateway.ru/pgw/" id="pgw_payments" method="get">
        <input id="project" name="project" type="hidden" value="294" />
        <!-- Идентификатор проекта, выдается при интеграции -->
        Аккаунт:
        <input id="nickname" name="nickname" type="text" value="test_nickname" />
        Сумма:
        <input id="amount" name="amount" type="text" value="100" />
        <input id="pgw_payment_submit" name="submit" type="submit" value="Оплатить" />
    </form>
</div>
<script src="https://paymentgateway.ru/pgw/js/build/ext.min.js" id="PGWform"
        data-pgw_place=".PGWform"
        data-pgw_host="https://paymentgateway.ru/pgw/"
        data-pgw_type="modal"
        data-pgw_button="true"
        data-pgw_button_text="Оплатить заказ"
        data-pgw_button_style="//yandex.st/bootstrap/3.0.3/css/bootstrap.min.css"
        data-pgw_button_class="btn btn-primary"
        data-pgw_zindex="99999"
        data-pgw_nickname="test_nickname"
        data-pgw_orderid="test_order_123456"
        data-pgw_amount="100"
        data-pgw_project="294"
        data-pgw_comment="Комментарий к прохождению платежа">
</script>

<div style="margin: 100px; ">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="5ZS6UWQ99DZJC">
        <button type="submit" name="submit">Сделать пожертвование</button>
        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
    </form>

</div>

</body>
</html>