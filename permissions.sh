echo "Начинаю изменение прав..."

echo "Устанавливаю владельца www-data для всех папок и файлов"

chown -R www-data:www-data './'

echo "Выставляю права 755 для всех папок"

find './' -type d -exec chmod 755 {} \;

echo "Выставляю права 644 для всех файлов"

find './' -type f -exec chmod 644 {} \;

echo "Выставляю права 440 для .htaccess"

chmod 440 './.htaccess'

echo "Выставляю права 775 для tmp"

chmod 775 './tmp'

echo "Выставляю права 440 для tmp/.htaccess"

chmod 440 './tmp/.htaccess'

echo "Выставляю права 775 для sites"

chmod 775 './sites'

echo "Выставляю права 775 для sites/default"

chmod 755 './sites/default'

echo "Выставляю права 775 для sites/default/files"

chmod 775 './sites/default/files'

echo "Корректирую права g+w для поддеррикторий sites/default/files"

chmod g+w -R './sites/default/files'

echo "Выставляю права 440 для sites/default/files/.htaccess"

chmod 440 './sites/default/files/.htaccess'

echo "Выставляю права 440 для sites/default/settings.php"

chmod 440 './sites/default/settings.php'

echo "Выставляю права 440 для sites/default/default.settings.php"

chmod 440 './sites/default/default.settings.php'

echo "Выставляю права 775 для sites/all/themes"

chmod 755 -R './sites/all/themes'

echo "Выставляю права 775 для sites/all/modules"

chmod 755 -R './sites/all/modules'

echo "Выставляю права 775 для sites/all/libraries"

chmod 755 -R './sites/all/libraries'

echo "Изменение прав закончил! Убедись, что всё верно..."